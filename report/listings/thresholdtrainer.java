public double train(){
  //Find initial error
  double falseNeg = 0;
  double falsePos = 0;
  for(PredictionInfo info : infos) {
    falseNeg += info.predicate ? info.d : 0;
  }
  for(PredictionInfo info : infos) {
    falsePos += info.predicate ? 0 : info.d;
  }
  //Initial error for Less-than
  double errorLess = falseNeg;
  double thresholdLess = Double.NEGATIVE_INFINITY;
  
  //Find the smallest error for Less-than
  for(int i=0; i<infos.length; i++){
    if(infos[i].predicate)
      falseNeg -= infos[i].d;
    else
      falseNeg += infos[i].d;
      
    if(errorLess > Math.abs(falseNeg)){
      errorLess = Math.abs(falseNeg);
      thresholdLess = i+1<infos.length ? infos[i+1].value : Double.NEGATIVE_INFINITY;
    }
  }
    
  //Initial error for Greater-than
  double errorGreater = falsePos;
  double thresholdGreater = Double.NEGATIVE_INFINITY;
    
  //Find the smallest error for Greater-than
  for(int i=0; i<infos.length; i++) {
    if(infos[i].predicate) {
      falsePos += infos[i].d;
	}
    else {
      falsePos -= infos[i].d;
	} 
    if(errorGreater > Math.abs(falsePos)) {
      errorGreater = Math.abs(falsePos);
      thresholdGreater = infos[i].value;
    }
  }
}