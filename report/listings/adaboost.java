public ThresholdedClassifier<T> boost(T[] trainingSamples, boolean[] predicates, WeakLearner<T> weakLearner) {
  StrongClassifier<T> result = new StrongClassifier<T>();
  int numTrainingSamples = trainingSamples.length;
  double[] d = new double[numTrainingSamples];
  ThresholdedClassifier<T> thresholded = null;
    
  //Initialize
  System.out.println("Initial d: " + (1.0 / numTrainingSamples));
  for (int i = 0; i < numTrainingSamples; i++){
    d[i] = 1.0 / numTrainingSamples;
  }
  weakLearner.init(trainingSamples, predicates);
    
  //Iterate
  for (int t = 0; t < iterations; t++) {
    System.out.println("Boosting... " + (t+1) + " of " + iterations);
	System.out.println("Calculating epsilon...");
    BClassifier<T> weakClassifier = weakLearner.learn(d);
    double error = weakLearner.getError();
    double alpha = 0.5 * Math.log((1 - error) / error);
    System.out.println("Looping samples...");
    for (int i = 0; i < numTrainingSamples; i++) {
      //indicator is 1 or -1
      d[i] = (d[i] * Math.exp(alpha * weakLearner.getIndicator(i)));
    }
    normalize(d);
    result.add(weakClassifier, alpha);
      
    //Find optimal threshold for StrongClassifier
    double[] finalD = new double[d.length];
    for (int i=0; i<finalD.length; i++)
      finalD[i] = 1.0 / finalD.length;
    ThresholdTrainer<T> trainer = new ThresholdTrainer<>(result, trainingSamples, predicates, finalD);
    trainer.train();
    thresholded = trainer.getThresholded();
      
    if (evaluator != null) {
      if (evaluator.evaluate(thresholded, trainingSamples, predicates))
        break;
    }
      
    if (quickSaver != null)
      quickSaver.saveClassifier(thresholded);
  }
  return thresholded;
}  