private void createIntegralImage() {
  if (root != null) {
    if (root.integralImage == null) {
      root.createIntegralImage();
    }
    integralImage = root.integralImage;
    return;
  }
  int width = region.width;
  int height = region.height;
  if (grayscale == null) {
    createGrayscale();
  }
  Raster r = grayscaleRaster;
  integralImage = new long[width][height];
  for (int x = 0; x < width; x++) {
    for (int y = 0; y < height; y++) {
      int value = r.getSample(region.x + x, region.y + y, 0);
      if (x == 0 && y == 0) {
        integralImage[0][0] = value;
      }
      else if (x == 0) {
        integralImage[0][y] = value + integralImage[0][y - 1];
      }
      else if (y == 0) {
        integralImage[x][0] = value + integralImage[x - 1][0];
      }
      else {
        integralImage[x][y] = value + integralImage[x - 1][y] 
		+ integralImage[x][y - 1] - integralImage[x - 1][y - 1];
      }
    }
  }
}