\subsection{Thresholded Learner}
\label{impl-thresholdedlearner}
In \secref{findingoptimalthreshold}, we described how the optimal threshold and polarity
could be found for a binary classifier made from a classifier which output was a real value.

Given a set of training samples the threshold and polarity is chosen so the number of correctly predicted
training samples is maximised. In \tableref{determiningthresholdunsorted} we show as an example, the value outputted by
a single classifier given different training samples.

\tab{determiningthresholdunsorted}{1}{Training samples and the value outputted by a classifier.}
                    {A classifier's output given a training set}
             {Value}{Predicted classification}{   
    \tabrow{$5138$ }{true}
    \tabrow{$-142$ }{false}
    \tabrow{$45$   }{false}
    \tabrow{$-4$   }{true} 
    \tabrow{$243$  }{true}
    \tabrow{$-2534$}{false}       
    \tabrow{$2553$ }{true}
}

For seven training samples, we are only interested in eight particular
thresholds for the binary classifier.  Any other threshold will give the same
result as one of these eight thresholds, which can be seen in
\tableref{differentbinaryclassifiers}.  

\tab{differentbinaryclassifiers}{3}{The 16 binary classifiers that can be made
from seven training samples.}
         {Binary classifiers made from a single classifier}
  {Correct predictions}{Binary classifier & Threshold & Polarity}{
    \tabrow{$4$}{$b_1$ & $< -2534$ & Positive}
    \tabrow{$5$}{$b_2$ & $-2534 < x < -142$ & Positive}
    \tabrow{$6$}{$b_3$ & $-142 < x < -4$ & Positive}
    \tabrow{$5$}{$b_4$ & $-4 < x < 45$ & Positive} 
    \tabrow{$6$}{$b_5$ & $45 < x < 243$ & Positive}
    \tabrow{$5$}{$b_6$ & $243 < x < 2553$ & Positive}    
    \tabrow{$4$}{$b_7$ & $2553 < x < 5138$ & Positive}
    \tabrow{$3$}{$b_8$ & $> 5138$ & Positive}
    \tabrow{$3$}{$b_9$ & $< -2534$ & Negative}
    \tabrow{$2$}{$b_10$ & $-2534 < x < -142$ & Negative}
    \tabrow{$1$}{$b_11$ & $-142 < x < -4$ & Negative}
    \tabrow{$2$}{$b_12$ & $-4 < x < 45$ & Negative} 
    \tabrow{$1$}{$b_13$ & $45 < x < 243$ & Negative}
    \tabrow{$2$}{$b_14$ & $243 < x < 2553$ & Negative}
    \tabrow{$3$}{$b_15$ & $2553 < x < 5138$ & Negative}
    \tabrow{$4$}{$b_16$ & $> 5138$ & Negative}
}

For each threshold we can also choose between a positive or a negative polarity
for making the binary classifier. The 16 possible binary classifiers are all
listed in \tableref{differentbinaryclassifiers}. The best binary classifiers
are $b_3$ and $b_5$ as they both predict six out of seven training samples
correctly. 

Calculating the optimal threshold and polarity for a single classifier can be
done in $\mathcal{O}(n^2)$ time, where $n$ is the number of training samples.
This complexity is achieved by trying every different threshold, and for each
threshold determine the number of training samples predicted correct.

\tab{determiningthresholdsorted}{2}{Training samples sorted by the value outputted by a classifier.}
                    {A classifiers output given a training set.}
             {Value}{Training sample & Predicted classification}{
    \tabrow{$-2534$}{$t_1$           & false}
    \tabrow{$-142$ }{$t_2$           & false}
    \tabrow{$-4$   }{$t_3$           & true} 
    \tabrow{$45$   }{$t_4$           & false}
    \tabrow{$243$  }{$t_5$           & true}
    \tabrow{$2553$ }{$t_6$           & true}
    \tabrow{$5138$ }{$t_7$           & true}
}

As a matter of fact, we can make a more efficient algorithm. We can start by sorting the training
samples according to the value outputted by the classifier in descending order, as seen in
\tableref{determiningthresholdsorted}. This can be done in $\mathcal{O}(n \cdot
log(n))$ time. 

Now, consider only the binary classifiers with a positive
polarity. For the first binary classifier, $b_1$, we have to go through all of
the training samples to check how many it predicts correctly. This takes $O(n)$
steps, and we find that four training samples are predicted correctly. For the
next binary classifier, $b_2$, the only prediction that changes is the
prediction of the training sample $t_1$, which value lies between the threshold
of $b_1$ and $b_2$. $b_2$ will predict $t_1$ correctly, which means that $b_1$
did not. From this, we have that $b_2$ predicts one more training sample
correct than $b_1$ does, which gives us the number of correct predictions to five. 

When we know the
number of training samples predicted correctly by the binary classifiers with a
positive polarity, inverting the polarity of one of them will cause $n - x$
training samples to be predicted correctly, where $n$ is the number of training
samples and $x$ is the number of training samples it predicts correctly. This is
because every training sample that were previously predicted correct will now be
wrong, and vice versa. 

For each binary classifier, except the first, we can
hereby calculate the number of correct predictions in $\mathcal{O}(1)$ time.
The total complexity of this algorithm is thus $\mathcal{O}(n \cdot
log(n)+n+(n-1)) = \mathcal{O}(n \cdot log(n))$.
