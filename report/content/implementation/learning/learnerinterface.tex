\subsection{Generic interfaces for learning}
\label{sec:learnerinterface}

We want our implementation to be very generic and reflect that many boosting
algorithms as well as weak learning methods exist. If we some day want to use
another algorithm for either boosting or weak learning, or maybe replace the use
of \hf{}s with another type of feature, we want an interface that allows us to
easily do so.

\subsubsection{Classifier}
\interfaceref{Classifier} is an interface that requires a method for classifying
objects of the type \emph{T}, where \emph{T} is a generic type parameter. The
result of the classification must be a value.

\subsubsection{BClassifier}
\interfaceref{BClassifier} is an interface used for binary classification of
objects of the type \emph{T}, where \emph{T} is a generic type parameter.  When
implementing this interface, a \methodref{predict()} method must be provided
that classifies an object of type \emph{T} as either \emph{true} or
\emph{false}. 

\subsubsection{HaarFeature}
The \classref{HaarFeature} class implements \interfaceref{Classifier}.  A
\classref{HaarFeature} contains a set of positive rectangles and a set of negative
rectangles.  It will output the value $S_{pos} - S_{neg}$,
where $S_{pos}$ is the sum of pixel intensities below the set of positive
rectangles and $S_{neg}$ is the sum of pixel intensities below the set of
negative rectangles.

An object of the class \classref{HaarFeature} will never be expected by any
other class or interface. Rather, a class implementing the
\interfaceref{BClassifier} interface will be expected, thus we can easily
interchange between different types of features used.

\subsubsection{ThresholdedClassifier}
The \classref{ThresholdedClassifier} class implements \interfaceref{BClassifier}
and provides a generic type parameter \emph{T}.  When instantiated, it takes a
\interfaceref{Classifier} object having the same generic type parameter
\emph{T}, a threshold and a predicate.  It implements the
\interfaceref{BClassifer}.\methodref{predict()} method, such that it returns the
value of its predicate whenever the \interfaceref{Classifier} object returns a
value greater than its threshold when predicting an object of type \emph{T}.

To better understand how this class works, imagine how it for this project just
contains a \classref{HaarFeature} and a threshold.  When the
\classref{HaarFeature} returns a value given a picture, this value is compared
to the threshold.  Based on whether the value is above or below the threshold,
the \methodref{predict()} method returns true or false, indicating whether the
picture contains a number plate or not.

\subsubsection{WeakLearner}
The \interfaceref{WeakLearner} interface has a generic type parameter \emph{T}.
The interface requires any implementer to provide a \methodref{learn()} method.
\methodref{learn()} takes a set of training samples of type \emph{T}.  For each
training sample, a predicate and a weight is associated with it.  The method
must output an object that implements the \interfaceref{BClassifier} interface,
with the same generic type parameter \emph{T}, such that it can classify objects
of the type \emph{T}.  The classifier should take the weight of each training
sample into consideration and try to maximise the sum of weights of training
samples it classifies correct.

\subsubsection{Booster}
\interfaceref{Booster} is an interface that just contains a
\methodref{boost()} method.  The interface has the generice type parameter
\emph{T}.  The \methodref{boost()} methods takes a set of training samples of
type \emph{T}, where the predicate of each training sample is known.  It returns
an object of the class \classref{ThresholdedClassifier} with the generic type
parameter \emph{T}.

We have implemented an \classref{AdaBoost} class which basically just implements the
\interfaceref{Boosting} interface using the AdaBoost algorithm.  The
\classref{ThresholdedClassifier} outputted by \classref{AdaBoost} will contain a
\interfaceref{Classifier} object that is actually made up of many different
\classref{BClassifier} objects each having a weight associated, indicating the
reliability of each \classref{BClassifier}.
