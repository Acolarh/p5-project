\section{Image abstraction}
\label{sec:impl-image}

During all parts of the implementation we need to perform different operations on images,
such as cropping, edge detection, binarisation, normalisation, resizing etc.
It is therefore ideal to create a single class that implements and simplifies all of these
operations.

An \classref{Image} object can be constructed in several ways, depending on the situation.
One way to construct an \classref{Image} object is from a file in the file system. Java's
built-in \classref{ImageIO} class is used to read the image from a file. It supports
reading and writing JPEG, PNG, BMP, WBMP, and GIF formats \cite{java-imageio}. The
\classref{Image} class also reads the EXIF metadata of JPEG images in order to rotate
them according to the EXIF orientation tag (this is important for some of the training 
samples we have collected). \classref{Image} objects can also be saved to files using the
\methodref{save()} method.

The second way to construct an \classref{Image} object, is to give the constructor a
\classref{BufferedImage} object. \classref{BufferedImage} is another built-in Java-class,
which is typically used to handle images in different Java-libraries, it is also the
underlying data structure used by the \classref{Image} class. Being able to convert
\classref{Image} objects to and from the \classref{BufferedImage} objects allows us to
easily interface with other Java libraries.

The third way to construct an \classref{Image} object, is from a width and a height
parameter which creates an empty canvas of that size.

Some of the basic functionalities of the \classref{Image} class, is to read individual
pixel values using the methods \methodref{getRed()}, \methodref{getGreen()}, \methodref{getBlue()}, and
\methodref{getGray()}, setting individual values using \methodref{setRed()} etc., converting
an image to greyscale using \methodref{getGrayscale()}, and 
resizing an image with \methodref{getResized()}.

\subsection{Cropping}

A feature that is required in many different places is cropping an image, e.g.\ extracting
just the number plate from a picture of a car. The \classref{Image} class has a method
\methodref{getSubimage()} to do exactly that. Based on four parameters (x, y, width, and height), it creates a new image from that region, as illustrated on \figref{subimage}.

\fig[width=0.5\textwidth]{subimage}{Extracting a region of interest from an image.}

As this operation has to be done many times in some parts of \productname, it has to be
fast. In order to do it efficiently, the \classref{Image} object created by
\methodref{getSubimage()} keeps a reference to the original image data of the parent image
along with a \emph{region of interest} attribute, instead of creating new image data. This
way subimages can quickly be created and disposed.

\subsection{Edge detection}

Edge detection is a way of identifying edges in an image. An edge detector basically
creates a new image with the edges of an image highlighted. There are a number of
different edge detection algorithms, and since selecting the correct algorithm depends
on the task at hand, the \classref{Image} class does not implement a single edge detection
algorithm. Instead the \methodref{getEdges()} method relies on an object that implements
the \interfaceref{EdgeDetector} interface. Three different edge detection algorithms
have been implemented: the Sobel operator, the Prewitt operator, and the Canny edge detector.

\Figref{edgedet} shows the results from different edge detection algorithms. The result
of the Canny edge detector is very good, but the algorithm is much slower than
the other two, so for many use cases, the Canny edge detector is overkill.

\fig[width=\textwidth]{edgedet}{The different edge detection algorithms implemented.}

\subsection{Integral images}

As explained in \secref{haarfeatures} we need a quick way to calculate the sum of a region in
an image.
Looping through each pixel of the region would be a slow process, so we'll be using a
data structure known as an \emph{integral image}.

An \emph{integral image} (also known as a summed area table) is a two-dimensional array where
the value at $(x, y)$ is the sum of all pixels above, to the left of, and including $(x, y)$:
\[
  I(x, y) = \sum_{x'=0}^x \sum_{y'=0}^y i(x', y')
\]
where $I$ is the integral image and $i$ is the original image. The integral image can
be created by looping through every pixel of the image once (from top left to bottom
right) so that:
\begin{equation}
  \label{eqn:create-integral}
  I(x, y) = i(x, y) + I(x, y - 1) + I(x - 1, y) + I(x - 1, y - 1)
\end{equation}
In the \classref{Image} class implementation the \methodref{getSum(x, y, width, height)} method is
available for calculating the sum of a region (given by the top left corner $(x, y)$ and
a $width$ and a $height$).
When \methodref{getSum()} is called, and an integral image has not yet been
created, it is generated using \eqnref{create-integral}.
In order to calculate the sum of the region, we will need to retrieve the integral
image value at four different points. For this we calculate the following:
\begin{eqnarray*}
  x_0 &=& x - 1\\
  y_0 &=& y - 1\\
  x_1 &=& x_0 + width\\
  y_1 &=& y_0 + height
\end{eqnarray*}
We then have four points, as illustrated on \figref{integral2}, which we use to retrieve the
sum of the regions $A$, $B$, $C$, and $D$ from the integral image. By subtracting $B$ and
$C$ from $D$ and adding $A$ (since both $B$ and $C$ cover the top left area the area has
been subtracted twice) we get the sum of the intended region:
\begin{eqnarray*}
  sum &=& D - B - C + A\\
   &=& I(x_1, y_1) - I(x_1, y_0) - I(x_0, y_1) + I(x_0, y_0)
\end{eqnarray*}

\fig[width=0.3\textwidth]{integral2}{Calculating the sum of a region (the dark grey
square) in an image.}

Another use of the integral image is to calculate the histograms used for segmentation
as explained in \secref{segmentation}.

\subsection{Binarisation}

Another operation that is useful for segmentation (\secref{segmentation}) is binarisation.
The result of binarisation is a black-and-white image (or \emph{binary image}). 

The binarisation algorithm (used by calling \methodref{getBinarized()} on an
\classref{Image} object) first finds the average value of pixels in the image using the
integral image:
\[
  i_{avg} = \frac{I(width - 1, height - 1)}{width \cdot height}
\]
Then it calculates the binarised value $b(x, y)$ for each pixel value $i(x, y)$ (assuming 
pixel values between $0$ and $1$):
\[
  b(x, y) = \begin{dcases*}
    1 & if $i(x, y) > i_{avg}$\\
    0 & if $i(x, y) \le i_{avg}$
  \end{dcases*}
\]
\Figref{binarisation} shows the results of a binarising the image of a car and the image of
a number plate.

\fig[width=0.5\textwidth]{binarisation}{Binarisation of a car (top) and a number plate
(bottom).}

\subsection{Normalisation}

A process somewhat similar to binarisation that is useful for OCR (\secref{ocr}) is
normalisation.
Normalisation changes the pixel values of the image to automatically fix contrast and 
lightness issues, such as in a very dark number plate.
The reason this is useful in OCR, is because normalised characters are more similar
in appearance (black character on white background rather than grey character on grey
background).

The normalisation algorithm (\methodref{getNormalized()}) first calculates the minimum
and maximum value for every channel. For a greyscale image there's only one channel, so
we get the values $i_{min}$ and $i_{max}$.
For each pixel we then calculate the normalised value $n(x, y)$ for each pixel value
$i(x, y)$ (assuming pixel values between $0$ and $1$):
\[
  n(x, y) = \frac{i(x, y) - i_{min}}{i_{max} - i_{min}}
\]
The process can be generalised to colour images as well, just by doing the same thing
for every channel of the image. \Figref{normalisation} shows the normalisation result
for a colour image and a greyscale image.


\fig[width=0.5\textwidth]{normalisation}{Normalisation of a dark number plate in colour (top) and greyscale (bottom).}

