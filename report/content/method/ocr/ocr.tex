
\subsection{The neuron}
\label{sec:theneuron}

In the central nervous system a neuron is a cell that transmits information via
synapses (connections between neurons). 

An artificial neuron is a mathematical abstraction of a biological neuron, and the basic
unit of a neural network. It receives one or more inputs and produces a single output.
The output is usually the weighted sum of inputs passed through a transfer function
(also known as the activation function). The output of the $i$th neuron, given
$m$ inputs, can be expressed as:
\begin{equation}
  \label{eqn:neuronoutput}
  y_i = \varphi\left( \sum_{j=1}^m w_{i j} x_j \right)
\end{equation}
where $\varphi$ is the transfer function, $x_j$ is the value of input $j$, and
$w_{i j}$ is the weight of that input.

The transfer function is chosen, so that it either enhances or simplifies the network. A
typical transfer function is the \emph{sigmoid} function:
\begin{equation}
  \label{eqn:sigmoid}
S(t) = \frac{1}{1+e^{-t}}
\end{equation}
It returns a value between $0$ and $1$, which is ideal for approximating a boolean
output (i.e.\ being close to $1$ means true, while being close to $0$ means false).
It also has an easily calculated derivative:
\begin{equation}
  \label{eqn:sigmoidderivative}
  S'(t) = S(t)(1 - S(t))
\end{equation}
Which is important when calculating the weight updates in the network.

Other transfer functions exist, such as the signum function:
\[ \mathrm{sgn}(x) =
  \begin{dcases*}
    -1 & if $x < 0$\\
    0  & if $x = 0$\\
    1  & if $x > 0$
  \end{dcases*}
\]

\subsubsection{Example: Boolean operators}

Consider a neuron that is supposed to emulate an OR gate ($A \vee B$ is true if either
$A$ or $B$ is true, false otherwise). The neuron will have two inputs, $x_1$ and $x_2$, and two weights, $w_1$ and $w_2$. The output of the neuron (using a linear transfer function
$\varphi(x) = x$) is:
\[ y=w_1 x_1 + w_2 x_2 \]
Since this will return a real value, we need a function that converts the
neuron output to a boolean value:
\[ f(y) = 
  \begin{dcases*}
    \mathrm{true} & if $y \ge 0.5$\\
    \mathrm{false} & if $y < 0.5$
  \end{dcases*}
\]

If we set $w_1 = 0.5$ and $w_2 = 0.5$ we see that the neuron (illustrated in
\figref{neuron}) successfully emulates an OR gate as seen on \tableref{orGateNeuron}.

\tikzfig{neuron}{A single neuron with two inputs.}

\tab[\textwidth]{orGateNeuron}{4}{The outputs of the OR gate neuron.}
         {Neuron output}
  {Case}{$x_1$ & $x_2$ & $y$ & $f(y)$}{
    \tabrow{1}{$0$ & $0$ & $0$ & false }
    \tabrow{2}{$0$ & $1$ & $0.5$ & true }
    \tabrow{3}{$1$ & $0$ & $0.5$ & true }
    \tabrow{4}{$1$ & $1$ & $1$ & true }
}

Similarly, we can emulate an AND gate ($A \wedge B$ is true only if both $A$ and $B$ are true) with $w_1 = 0.4$ and $w_2 = 0.4$ as seen on
\tableref{andGateNeuron}.

\tab[\textwidth]{andGateNeuron}{4}{The outputs of the AND gate neuron.}
         {Neuron output}
  {Case}{$x_1$ & $x_2$ & $y$ & $f(y)$}{
    \tabrow{1}{$0$ & $0$ & $0$ & false }
    \tabrow{2}{$0$ & $1$ & $0.4$ & false }
    \tabrow{3}{$1$ & $0$ & $0.4$ & false }
    \tabrow{4}{$1$ & $1$ & $0.8$ & true }
}

It is, however, not possible to emulate an XOR gate (exclusive or, $A \oplus B$ is true only if $A \ne B$)
using this simple network model. This is because XOR is not a linearly separable problem.
Two sets of points are linearly separable if they can be separated by a single line. For
instance both the OR and AND gate problems are linearly separable as shown on
\figref{linsep_or} and \figref{linsep_and}, respectively. The figures illustrate the first
input on the x-axis and the second input on the y-axis. A blue circle represents a boolean
value of true, while a red circle represents a boolean value of false.


\doublefig[scale=1.2]
{linsep_or}{Linear separability of OR.}
{linsep_and}{Linear separability of AND.}

For the XOR problem, two lines
are necessary in order to separate the two sets, as seen on
\figref{linsep_xor}. Therefore, the XOR problem is not linearly separable.
This means that we cannot represent an XOR gate with just a single neuron. In the
next section we will take a look at combining multiple neurons to solve more
complicated problems.

\fig[scale=1.2]{linsep_xor}{Linear separability of XOR.}

\subsection{The network}

While a single neuron might be able to solve some simple problems, we need multiple
neurons for more complex models. Our previous model of a single neuron with multiple 
inputs and a single output, is easily broadened to a full network.

A \emph{neural network} consists of a number of input neurons, hidden neurons, and output
neurons.

The input neurons are tasked with feeding data into the network from the outside,
e.g.\ observations of features, such as the intensity of a pixel in an image. The
input neuron merely passes along its value without doing any calculations.

The output neurons form the combined result of the network, e.g.\ a set of boolean
output features that classify the input. Each output neuron calculates its output
using the previously given formula \eqnref{neuronoutput}.

The hidden neurons form the backbone of the computational power of the network.
They are located between the input neurons and the output neurons, and will each
calculate an output using the same formula\eqnref{neuronoutput} as the output neurons.

There are many different types of neural network models. The model we will be
looking at is the \emph{multilayer perceptron}, which is a feed-forward neural
network. A feed-forward network is a network in which the connections between
neurons do not form cycles, i.e.\ the information moves in only one diretion
through the network. The multilayer perceptron is a directed graph of multiple
layers; one input layer, one output layer, and one or more hidden layers. Each
layer is fully connected to the next one, which means that in each layer, every
neuron is connected to all the neurons of the next layer. A network with five
neurons in the input layer, three in the output layer, and a single hidden layer
with four neurons, would therefore have $5 \cdot 4 + 4 \cdot 3 = 32$ weighted
connections.

\subsubsection{Example: Exclusive OR}

In \secref{theneuron} we explored the possibility of making single neurons emulate
OR and AND gates, we also saw how the same thing wouldn't be possible with XOR. In
order to emulate the XOR operator (and other problems that are not linearly separable)
we need more neurons, specifically hidden neurons. 

Our XOR network will contain two input neuron, two hidden neurons, and an output neuron.
Every input neuron is connected to every hidden neuron, and every hidden neuron is connected
to the output neuron, as shown in \figref{neuralnetwork}.

\tikzfig{neuralnetwork}{The XOR gate network.}

The input neurons just pass along values without changing them, while the hidden neurons
and output neuron values are calculated using:
\begin{eqnarray*}
  h_1 &=& S(w_1 x_1 + w_2 x_2) \\
  h_2 &=& S(w_3 x_1 + w_4 x_2) \\
  y &=& S(w_5 h_1 + w_6 h_2)
\end{eqnarray*}
This time using the Sigmoid function (see \eqnref{sigmoid}) as the transfer function.

If we set the weights as seen on \figref{neuralnetwork}, we get a network that successfully
emulates an XOR operator as seen on \tableref{xorGateNeuron}.

\tab[\textwidth]{xorGateNeuron}{6}{The outputs of the XOR gate network.}
         {Network output}
  {Case}{$x_1$ & $x_2$ & $h_1$ & $h_2$ & $y$ & $f(y)$}{
    \tabrow{1}{$0$ & $0$ & $0.50$ & $0.50$ & $0.27$ & false }
    \tabrow{2}{$0$ & $1$ & $0.73$ & $0.99$ & $0.52$ & true }
    \tabrow{3}{$1$ & $0$ & $0.73$ & $0.99$ & $0.52$ & true }
    \tabrow{4}{$1$ & $1$ & $0.88$ & $1.00$ & $0.26$ & false }
}

Selecting weights is not a trivial task, and in the next section we will take a look
at how we can train the weights of our network using machine learning.

\subsection{Learning}
\label{sec:nnlearning}

For OCR we wish to teach a neural network to recognise different characters
based on data we have collected. This task is known as \emph{supervised
learning}.

Like with number plate detection, as explained earlier in this chapter, we wish
to create a prediction rule based on a set of labelled training samples. For
a neural network this means finding weights that minimise the combined error of
the network.

We will be using the method of \emph{backpropagation} (backward propagation
of errors), which is a common method for supervised learning of feed-forward neural
networks.

For each iteration (or epoch) of backpropagation, we loop through all training
samples and calculate the weight updates.

\begin{algorithm}
  \caption{A backpropagation algorithm.}
  \label{alg:backprop}
  \KwData{
    \;Training samples $(x_1,y_1),\cdots,(x_n,y_n)$ where $x_i$ is an input vector with expected output vector $y_i$.\;
    Neural network with $m$ neurons.\;
    A learning rate $\eta$.\;
  }
  \Begin{
    \For{$(x_i, y_i) = (x_1, y_1)$ \KwTo $(x_n, y_n)$}{
      Set input vector $x_i$.\; 
      Calculate output vector of network.\;
      \For{$j\gets1$ \KwTo $m$}{
        Calculate error term:
        \[\delta_j = 
          \begin{dcases*}
            \varphi'(o_j) (y_{i j} - o_j) & for output neurons \\
            \varphi'(o_j) \sum_{k = 1}^m w_{j k} \delta_k & for hidden neurons
          \end{dcases*}
        \]\;
      }
      \For{$j\gets1$ \KwTo $m$}{
        \For{$k\gets1$ \KwTo $m$}{
          Calculate weight change:
          \[\Delta w_{j k} = \eta \delta_k o_j\]
          \;
          Update weight $w_{j k}$.\;
        }
      }
    }
  }
\end{algorithm}

Algorithm \ref{alg:backprop} shows a backpropagation algorithm. The first step is
to calculate the error term for each neuron (excluding input neurons). For output neurons
we use the expected output of that neuron, $y_{i j}$, and the actual output of that
neuron, $o_j$, to calculate the error. We multiply this with the derivative of the
transfer function $\varphi'$. For the Sigmoid function we use the derivative as given by
\eqnref{sigmoidderivative}, and get the following error term for output neurons:
\[ \delta_j = o_j ( 1 - o_j ) (y_{i j} - o_j) \]
For hidden neurons we obviously don't have an expected output at hand, so we
propagate the error terms of the neurons in the lower levels upwards and multiply
them with the weights of those connections. $w_{j k}$ is read as ``the weight
of the connection between the $j$th and the $k$th neuron''. If there is no connection
between neuron $j$ and neuron $k$ then $w_{j k} = 0$, so only valid forward feeding
connections matter in the calculation of the sum:
\[ \delta_j = o_j ( 1 - o_j ) \sum_{k = 1}^m w_{j k} \delta_k \]

The next step of the algorithm is to calculate the weight updates using the previously
calculated error terms:
\[ \Delta w_{j k} = \eta \delta_k o_j \]
$\eta$ is the learning rate and determines how fast weights are updated. A high learning
rate makes learning faster while a low rate makes it more accurate. 

It is possible to speed up learning using momentum, in which case the error change is
calculated as a function of time \cite{istook2001}:
\[\Delta w_{j k} \left( t \right) = \eta \delta_k o_j + \alpha \Delta w_{j k} ( t - 1 )\]
$\alpha$ is the momentum and $\Delta w_{i j} ( t - 1 )$ is the previous weight update.
Using this method, the weight change will speed up when consecutive weight updates
have the same sign.

Running the algorithm once is not enough to train the network so we need to know when
to stop training. This can be done by calculating the error of the network after each
iteration. The error can be calculated as the \emph{mean squared error} (MSE) using
%iteration. The error can be calculated as a sum of squared errors of prediction (SSE).
%For each training sample with expected output vector $y$ the output vector $o$ of the network is
%calculated. The SSE for that training sample is then calculated as:
%\[SSE = \sum_{j=1}^m (o_j - y_{j})^2\]
%Calculating the SSE for every training sample gives a combined error of the network. We
%can then stop training if the error gets below a predetermined maximum error.
\[\mathsf{MSE} = \frac{1}{n} \sum_{i=1}^n \sum_{j=1}^{m} \left(o_j (x_i) - y_{i j}\right)^2\]
where $n$ is the number of training samples, $m$ is the number of output neurons,
$x_i$ is the $i$th input vector, $y_i$ is the $i$th expected output vector, and 
$o_j(x_i)$ is the value of the $j$th output neuron with the network input vector
set to $x_i$.

\subsection{Recognising characters}
\label{sec:recognisingcharacters}
Based on what we now know about neural networks, it should be possible to construct
and train a network to recognise individual characters.

The output layer should consist of a neuron for each character we wish to recognise. If
a character has been perfectly recognised by the network, the output neuron for that
character should have a value of $1$ while every other output neuron has a value of
$0$.

The input layer should consist of features that we can observe in the input image. The
easy approach for greyscale images is to map an input neuron to the colour value of
each pixel in the input image, as shown on \figref{imagetoneuron}, i.e.\ each input
neuron gets a value between $0$ (black)
and $1$ (white). For colour images we would need multiple neurons per pixel (one for
each colour channel), but this shouldn't be necessary for character recognition.

\tikzfig{imagetoneuron}{Mapping pixels of a $3 \times 3$ image to $9$ input neurons.}

The number of hidden neurons and layers needed is not as easy to determine as it depends
on the difficulty of the problem. A greater amount of hidden neurons will make it easier
to correctly recognise a pattern in a number of samples, but it will also make the network
slower and increase the risk of overfitting the model to the training samples. For a single
hidden layer a rule of thumb is to use
\[ \frac{m_{input} + m_{output}}{2} \]
hidden neurons, where $m_{input}$ is the number of input neurons and $m_{output}$ is the
number of output neurons. 

The initial weights of the connections in the network should be selected randomly. Choosing
random numbers between $-0.5$ and $0.5$ for each connection seems to be a popular
choice, although more advanced algorithms exist for choosing proper weights.

\fig{overfitting}{An exaggerated example of overfitting when learning.}

In order to train the network we need a set of pre-classified images of characters from number
plates. Ideally we should have both a training set and a smaller validation set. The
training set will then be used in the neural network learning algorithm, while the
validation set will be used to test the neural networks ability to generalise. Testing
the validation set will also reveal if the network has been overfitted to the training
samples. Overfitting in neural network learning can be illustrated by plotting the MSE
 of both the training samples and the validation samples as seen
on \figref{overfitting}. On the figure overfitting starts to happen when the validation
error (the MSE) gets bigger while the training error gets smaller. To get the most general network
it would therefore be ideal to stop learning when the validation error has reached its
global minimum.

So, what happens when overfitting occurs is, that the neural network has been
trained to recognise some noise which is only contained in the training samples.
This means that we will get increasingly incorrect results by testing on the
validation samples. The reason for the increasing inaccurate results is that
the network is gradually getting just a bit better at recognising the training
samples, but worse at recognising the validation samples, because it is
being trained to find the noise contained in the training samples as well. This
is undesired behaviour and we wish to avoid overfitting.


%\subsubsection{Example: $2 \times 2$ patterns}
%
%As an example, we will create a neural network that recognises two different $2 \times 2$
%pixel patterns (horizontal lines and vertical lines). The input is a greyscale image of 
%width $2$ and height $2$, so we need four input neurons. And since we wish to distinguish 
%between two different patterns, we need two output neurons in the output layer. 
%The number of hidden neurons and layers needed is not as easy to determine as it depends
%on the difficulty of the problem. A greater amount of hidden neurons will make it easier
%to correctly recognise a pattern in a number of samples, but it will also make the network
%slower and increase the risk of overfitting the model to the training samples. For a single
%hidden layer a rule of thumb is to use
%\[ \frac{m_{input} + m_{output}}{2} \]
%hidden neurons, where $m_{input}$ is the number of input neurons and $m_{output}$ is the
%number of output neurons. Using that rule we need $\frac{1}{2}\left( 4 + 2 \right)=3$ hidden
%neurons for our simple network.
%
%The network model used can be seen in \figref{patternexample}. The network comprises
%a total of $18$ weighted connections. There is no way to guesstimate these weights, so
%we need to use the previously explained learning algorithm to find a set of weights that
%work. As for the initial values of these weights, we will use a random value between
%$-0.5$ and $0.5$ for each weight.
%
%\tikzfig{patternexample}{A network recognising two different patterns in a grid of four
%pixels.}


