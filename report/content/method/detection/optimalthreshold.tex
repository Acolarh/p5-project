\subsubsection{Finding optimal thresholds}
\label{sec:findingoptimalthreshold}

A \hf{} can be used to make a \emph{binary classifier}, which classifies whether an image is a number plate
or not. One way of transforming a real value produced by a \hf{} on a picture
into a binary classification is by selecting a threshold to compare it against, using either greater than or less than.
The best threshold maximises the number of correct classifications. This is done
by comparing different thresholds. The expected classification is known before
hand and the thresholds are compared with this classification. 
The best threshold thus uses logical comparison ($<$ and $>$) to maximise the number of
correct predictions on a training set.

\paragraph{Algorithm}

Our classifier is in the following form\cite{rapidobjectdetection}:

\[
  H(x) =
  \begin{dcases*}
    1 & if $pC(x) < p\Theta$\\
    0 & otherwise
  \end{dcases*}
\]
  
where $p$ is the polarity, which is positive for less than and negative for greater than.
$C(x)$ is the original classifier and $\Theta$ is the threshold.

The polarity is very useful because of the ability to reverse the polarity. If
we reverse the polarity from negative ($-$) to positive ($+$) it would be equal
to changing between less than ($<$) and greater than ($>$). An example will
illustrate this:

\begin{align*}
  \text{ positive polarity } & \rightarrow +2.0 < +40.0 \\
  \text{ negative polarity } & \rightarrow -2.0 > -40.0
\end{align*}

By reversing the polarity we also reversed the boolean value, which was no
longer true. %This means that by having a negative polarity it would be the same
%as using the greater than. $H(x)$ gives us the possibility to work with both
%greater than and less than in one equation by including the polarity.

We evaluate the binary classifier based on a set of samples $s$ and the wanted classifications $t$.
The error for sample $x$ is then:

\[
  E_x =
  \begin{dcases*}
    0 & if $H(s_x) = t_x$\\
    1 & otherwise
  \end{dcases*}
\]

The total error is then the sum of the weights $d$ of the classified samples.
We want to find the $p$ and $\Theta$ which minimises the total error:

\[
\argmin_{p,\Theta}\sum\limits_{i=1}^sE_id_i
\]

% When working with learning it is often needed to have a threshold of when to
% stop a learning process. This threshold can vary according to what type of
% learning process is being done. A threshold could for instance be how many
% iterations are needed before the given processed is optimal. 
% 
% This process of finding the threshold is often based on trial and error and on
% adjusting the threshold based on data obtained after trying a high threshold. I
% will give two examples to make this as clear as possible. The first example will
% be about finding a good threshold for \hf{}s, and the second will be of finding
% a good threshold for the error size of a neural network.
% 
% \subsubsection{Examples}
% 
% \todo{is the following correct?}
% 
% Say we need to find some suiting \hf{}s that fit an image optimally. We have a
% lot of these \hf{}s but we only need the best of them. The idea is to remove the
% ones that falsely predict some given object on an image, and we can do this
% because we know what each image represents. So for every test run of a \hf{} on
% an image we know the expected output.
% 
% At every test run each \hf{} gets a rating on how well it has predicted the
% object. It is this rating we need to find a threshold for. We can then sort the
% \hf{}s in ascending order, and by moving down from the top (the lowest rating at
% the top) we can obtain a better probability of the entire set of rated \hf{}s.
% This does not mean that we will get a 100 \% correct prediction from the \hf{}s
% because some of the better rated \hf{}s can be mispredicted. We must have a
% threshold and by finding a threshold we can eliminate many worthless \hf{}s.
% %Consider \figref{findoptimalthreshold}.
% 
% %\todo{insert figure below illustrating this}
% %\fig{findoptimalthreshold}{An illustration of how \hf{}s are selected.}
% 
% When the best probability has been found the given rating of the new top of the
% list (after the previous top has been removed) would be the threshold for which
% \hf{}s can be used to predict the given object on an image.
% 
% This next example is about teaching a neural network to recognise something
% arbitrary from some set of training data. Overfitting is a real risk when
% working with learning. To avoid this we split our training data in two sets -
% one for the learning and one for validation. This is explained in
% \secref{impl-ocr-learning}. An example of overfitting is given in
% \figref{overfitting} in \secref{recognisingcharacters}. The optimal size of
% error would be when the test run on the validation set no longer improved. When
% the test run on the validation set worsens then we have overfitting.

