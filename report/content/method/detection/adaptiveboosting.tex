\subsection{Boosting}
\label{sec:boosting}

Boosting is a method in machine learning used to produce accurate prediction rules
from many inaccurate rules\cite{schapire2003}. In general terms, the \emph{booster}
is provided with a set of training samples, where a training sample is an \emph{instance}
and an associated label. An instance is some observed data, e.g.\ a picture, while the label is
some knowledge about that data, e.g.\ whether or not that picture contains a number plate.
The booster is also provided with an algorithm for finding rough/inaccurate prediction rules
(a \emph{weak} or \emph{base learning algorithm}). This algorithm is then called repeatedly
by the booster on different subsets of the training sample, and at each iteration,
a new weak prediction rule is generated. After many iterations, the booster finally combines
the weak prediction rule into a single strong prediction rule.

Problems such as choosing each distribution over the training samples, and combining weak rules into
a single strong rule, are solved by different boosting algorithms. One such algorithm is
\emph{AdaBoost}.

\subsubsection{AdaBoost}
\label{sec:adaboost}

The AdaBoost algorithm (adaptive boosting) is a boosting algorithm introduced by Freund and Schapire\cite{freund1997}. AdaBoost runs for a number of iterations, and for each iteration calls the weak learning algorithm on the training set using a certain distribution. The distribution is a set of
weights over the training set, maintained by AdaBoost. Initially all weights are equal, but after
each iteration, the distribution is updated, so that the weights of incorrectly classified training
samples are increased. This forces the learning algorithm to put more focus on hard training samples. 

\begin{algorithm}
  \caption{The AdaBoost boosting algorithm in generalised form \cite{schapire2003}.}
  \label{alg:adaboost}
  \KwIn{
    Training samples $(x_1,y_1),\cdots,(x_m,y_m)$ where $x_i \in X$ and $y_i\in Y=\left\{ -1,+1 \right\}.$\;
    Number of iterations $T$.\;
  }
  \KwOut{A strong classifier $H : X \rightarrow \mathbb{R}$}
  \Begin{
    Initialise $D_1(i)=1/m$.\nllabel{alg:adaboost-initialise}\;
    \For{$t\gets1$ \KwTo $T$}{
      Train weak learner using distribution $D_t$.\;
      Get weak classifier $h_t : X \rightarrow \mathbb{R}$.\;
      Choose $\alpha_t \in \mathbb{R}$.\nllabel{alg:adaboost-alpha}\;
      Update distribution:
      \[D_{t+1}(i) = \frac{D_t(i)\exp(-\alpha_t y_i h_t(x_i))}{Z_t}\]
      where $Z_t$ is a normalisation factor (chosen so that the sum of $D_{t+1}$ is $1$).\;
    }
    \Return final strong classifier:
    \[H(x)=\mathrm{sign}\left( \sum_{t=1}^T \alpha_t h_t(x) \right)\]
  }
\end{algorithm}

In \algref{adaboost} pseudocode for the AdaBoost algorithm is given. $D_t$ is the list of
weights over the training set for iteration $t$, i.e.\ $D_t(i)$ is the weight (between $0$
and $1$) for training sample $i$. For the first iteration ($t=1$, see \alglref{adaboost-initialise}) all weights are set to
$1/m$, meaning that all weights are equal and they all sum up to one.

For each iteration the weak learner is run on the training samples using the distribution
for that iteration. The weak learner should return a weak classifier $h_t$ based on the
distribution $D_t$. It should choose this weak classifier in order to minimise the
error:
\[\epsilon_t=P_{i \sim D_t} \left[ h_t(x_i) \ne y_i \right]\]
which can be read as the probability of the weak classifier $h_t$ being wrong for the 
training sample $(x_i, y_i)$, where $i$ is distributed as $D_t$. How this error is calculated
depends on the weak learning algorithm used.

After running the weak learner, AdaBoost chooses a value $\alpha_t \in \mathbb{R}$, that 
measures the importance of the chosen weak classifier $h_t$. Choosing $\alpha_t$ is also
up to the implementation, but is typically set to:
\[\alpha_t=\frac{1}{2} \ln \left( \frac{1-\epsilon_t}{\epsilon_t} \right)\]
as described by Schapire\cite{schapire2003}.

The last part of the loop body updates the distribution for the next iteration ($D_{t+1}$).

The final result of the algorithm is a strong classifier, that uses the sum of all
selected weak classifiers and their weights to compute an answer.

