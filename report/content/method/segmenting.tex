\section{Segmentation of number plates}
\label{sec:segmentation}

After the previous stages are finished, we end up with a number plate containing
characters, which we want to recognise. We call the combination of these characters the
ID of the number plate. Before we can recognise the ID, we need to segment the
characters, so we have an image of each of the characters.

We know that each image of a number plate has been deskewed. We can now use the fact 
that there is a lot of white space between each character on the number plate. 
We use a \emph{histogram} to find the white space between the characters.

\subsection{Separating the symbols}

A histogram is a graphical representation of the distribution of a set of data\cite{wiki-histogram}. 
In our use of a histogram, we must map the pixel intensity of each
vertical region to the curve. The more white the region contains the bigger
the peak on the histogram. Consider \figref{histogramexample} where an example
of a histogram is given. The curve on the graph symbolises the pixel intensity.
The width of the graph symbolises the width of the image as each vertical
region is examined and mapped to the histogram.

\fig[width=0.5\textwidth]{histogramexample}{An example of a histogram. Peaks
represent the white space between symbols.}

The y-axis of the histogram in \figref{histogramexample} represents the sum of
the pixel intensities for a specific x-coordinate. The x-axis represents
the width of the original image.

When the histogram has been constructed from the image, it is possible to
determine where the characters are separated from each other. In \algref{histogram}
we give the pseudocode for the steps needed to determine how to segment each character
based on the peaks of the histogram. 

\begin{algorithm}
  \caption{Algorithm showing necessary steps to segment image based on
	  histogram.}
  \label{alg:histogram}
  \KwIn{
    Image of number plate.\;
    Threshold constant $\alpha$.\;
  }
  \KwOut{List of segmented characters, $L$.}
  \Begin{
    Construct vertical histogram, $H = h_0, h_1 ... h_{w-1}$ based on image having width $w$.\;
    $\mathit{sum} \gets \sum_{i=0}^{w-1} h_i$\;
    $\mathit{avg} \gets \frac{sum}{w}$\;
    $\mathit{max} \gets \max H$\;
    $T \gets \mathit{avg} + \alpha(\mathit{max} - \mathit{avg})$ \;
    %$\mathit{sum_{right}} \gets \mathit{sum}$ \;
    %$w_\mathit{right} \gets w$ \;
    $\mathit{start} \gets 0$ \;

    \For{$i \gets 0$ \KwTo $w-1$}{
      %$\mathit{sum_{right}} \gets \mathit{sum_{right}} - \mathit{h_i}$\;
      %$w_\mathit{right} \gets w_\mathit{right} - 1$ \;
      \uIf{$\mathit{h_i} > T$}{
      \If{$\mathit{start} \geq 0$}{
      Add sub-image between $[\mathit{start}, \mathit{i}]$ to $L$.\;
	$\mathit{start} \gets -1$\;
	%\If{$w_\mathit{right} > 0$}{
	  %$\mathit{avg} \gets \frac{\mathit{sum_{right}}}{w_\mathit{right}}$\;
	  %$T \gets \mathit{avg} + \alpha(\mathit{max} - \mathit{avg})$\;
      %  }
        }
      }
      \ElseIf{$\mathit{start} < 0$}{
	      $\mathit{start} \gets i$\;
      }
    }
    \Return list of segmented symbols, $L$. \;
  }
\end{algorithm}

The algorithm takes as input an image of a number plate and a constant $\alpha$ which must be in the range
$[0, 1]$. Basically, an $\alpha$-value too close to $0$, will cause too many image segments
to be found, while too few segments will be found as the value approaches $1$.

First, a histogram $H$ is constructed, such that $h_i \in H$ is the sum of pixels in the
$i$th column of the image. 
$\mathit{sum}$ is set to the sum of all pixels intensity in the entire image,
$\mathit{avg}$ is set to the average pixel intensity in the entire image, and
$\mathit{max}$ is set to the value of the most intensive column.

The threshold $T$ is set to a value between $\mathit{avg}$ and $\mathit{max}$ based on the
$\alpha$-value.

Now, we start to scan the image from left to right.
For any column $i$ in the picture, where $h_i > T$
we will make a cut. Every time we cut, we will wait until we reach a column $i$ for which $h_i < T$,
before we again will consider to make a cut. This is controlled by setting the variable 
$\mathit{start} = -1$ each time we cut. When the value of the current column $i$ drops below the
threshold, we set $\mathit{start} = i$, which is a positive value, and thus allows us to cut again by 
entering the if statement in line 10.

Finally, we have a list of segmented characters $\mathit{L}$ which we will return.

\subsection{Extracting each symbol}

When we have segmented each character, we want to remove unnecessary noise contained
within the images of the segmented characters. We also want to scale and minimise
unnecessary white space around the character. To do this it is possible to
calculate a horizontal histogram of each segmented image from $\mathit{L}$.

%\begin{algorithm}
%  \caption{Symbol extraction algorithm showing the necessary steps. Modified
%	  from \cite[pg. 23]{javaanpr}.}
%  \label{alg:extraction}
%  \KwIn{
%	  List of segmented symbols, $L_{segm}$.\;
%  }
%  \KwOut{List of scaled images with less noise, $L_{scale}$.}
%  \Begin{
%    \ForEach{$l_{segm}\; \in\; L_{segm}$}{
%	    Check if symbol is continuous. \;
%	    Remove small pieces of blobs (noise). \;
%	    Remove unnecessary white space. \;
%	    Scale image to desired size.\;
%	    Save new image $l_{scale}$ to list of scaled images $L_{scale}$.\;
%    }
%    \Return list of scaled images with less noise, $L_{scale}$. \;
%  }
%\end{algorithm}

By finding the horizontal histogram, we can cut of the top and the bottom of the
sub-image, which we do not need. This minimises eventual white space and noise.
We know that each character is either a number or a capital letter, which means
that the characters are continuous. By detecting and keeping the largest continuous
characters and removing everything else, we will have the character without extra
white areas.

When we have removed unnecessary white space, we can scale the image to a
desired size. We now have a list of scaled images containing only characters that
must be identified.

After these steps have been completed it is possible to identify each character
contained in the number plate. This means that we now can recognise the
characters we have segmented from the number plate.

