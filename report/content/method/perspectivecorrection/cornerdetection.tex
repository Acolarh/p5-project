\subsection{Corner detection}
\label{sec:methodcornerdetection}

Hough transform is a technique used in image analysis, for identifying lines in
a picture. Hough transform has been used for a variety of problems, e.g.\ number
plate skew correction\cite{numberplatehougharticle}, horizontally aligning text
in a document\cite{skewtextarticle}, and locating the crop rows of a
field\cite{croprowsarticle}. Most of the articles about number plate skew
correcting, which we have come across, only use Hough transform to find the slope
of the line, which is supposed to be horizontal. We propose a method for finding all four
edges of a number plate. The corners are then found by calculating the
intersection of the lines, which are found by help of the edges.

Hough space is a two-dimensional plane with the axes $(\theta, r)$. Every line through a picture in the $(x, y)$-plane 
is mapped to a single point in Hough space\cite{houghtransform}. 
The value of a specific point in Hough space represents the strength of the corresponding line in the original picture. 
Figure \ref{fig:xytohoughspace} shows how a red line in a picture can be mapped to a red point in Hough space.

\fig[width=.8\textwidth]{xytohoughspace}{The red line in the original picture is mapped to the single red point in Hough space.} 

By finding the Hough space of an image containing a number plate, we can find the four most intense points 
in Hough space and map these back to four lines in the original picture. Ideally, we want these points to be the four edges of the number plate.

\subsubsection{Defining the strength of a line}
One important thing to consider is how to define the strength of a line through a picture. Summing all the pixel 
intensities under the line in the original picture does not work very well, as this favours any line through a 
bright area, which could be above the number plate or diagonally through the number plate. 

We found that
a better approach is to use a binary edge detection on the original picture and then define the strength
of a line as the sum of positive pixels beneath it. But yet again, a diagonal line through the number plate
will be considered strong, as it hits many symbols on the number plate which have many edges. 
We overcame this problem by defining the strength of a line as the length of the longest
unbroken line segment in that line. However, this requires a binary image where edges of the number plate appear as
unbroken lines. 

We manually examined the results of five different techniques applied to 20
pictures of number plates, to see which technique would produce the best binary
image.  The techniques we tried were simple thresholding, Otsu binarisation,
Prewitt edge detection, Sobel edge detection, and Canny edge detection. For
each technique, we experimented with many different settings, e.g.\ adjusting
thresholds. The best results were achieved using Canny edge
detection\cite{cannyedgedetection}. Canny edge detection was the only technique
that for all 20 test pictures, produced a binary image where all four edges of
the number plate appeared as both thin and unbroken lines.

\Figref{usefuledgedetection} shows a binary image, made by applying Canny edge
detection to a picture of a number plate using the settings we found working
best. Notice how the four edges of the number plate are all unbroken lines.
\Figref{uselessedgedetection} is a binary image made from the same picture using
Canny edge detection with another setting. Notice how this binary image has less
noise which is good, but the image becomes useless because three of the number
plate edges appear as broken lines.  In fact, a line through the symbol \num{7}
will now be considered stronger than the right most edge of the number plate,
because the line through \num{7} is a longer unbroken line.

\doublefig[width=0.5\textwidth]
{usefuledgedetection}{A good binary image of a number plate for which the edges of the number plate are unbroken lines.}
{uselessedgedetection}{A bad binary image of a number plate where three edges of the number plate are broken lines.}

Using Canny edge detection to obtain a binary image of a number plate and then calculating Hough space
from the binary image, we discovered that the strongest point in Hough space almost always corresponds to a line 
through one of the two horizontal edges of a number plate, which is good. We now need to find the three other edges
of the number plate. But now, the second, third, and fourth strongest points in Hough space will usually correspond 
to lines very similar to the first edge found.
This problem can be seen in \figref{4strongestlinesproblem}, which visualises the four lines corresponding to the four strongest points found in the Hough space of the picture.

\fig[width=.5\textwidth]{4strongestlinesproblem}{The four strongest points in Hough space corresponds to very similar lines.} 

To overcome this problem, we require that any of the four strongest lines selected
must have a $\theta$ value of at least some degrees difference from the $\theta$ value of any previously selected line.
The optimal minimum angle difference would first seem to be 90 degrees, but this only holds for a perfect aligned number plate. 
If a picture is taken from an oblique angle, some of the number plate corners can have an angle narrower than 90 degrees.
It is clear that the minimum angle difference between the four strongest lines selected cannot be above 90 degrees, 
since four times such a value will be more than 360 degrees, thus at most three lines can be found.
We found that $\theta = 50^\circ$ works very well for detecting all four edges correct.
This value was found by maximising the results of different $\theta$ values on a test set of 20 number plate images. 

\subsubsection{Finding the corners}
With the four edges of the number plate, we can find the intersection of any two
of these edges, which will be a corner of the number plate. This method has some
weaknesses. First, an intersection can be outside the given picture, as seen in
\figref{platecorneroutsideframe}. This is however something which must be
handled by the deskewing technique described in \secref{deskewing}.

\fig[width=.5\textwidth]{platecorneroutsideframe}{A number plate corner can be located outside the frame of the given picture.} 

It is not realistic to assume that we always find the four correct edges.
Something can go wrong and we might end up with four parallel lines that have no
intersections. If less than four intersections exists, we will just return the
four corners of the original image frame, hoping that our OCR component still
recognises the symbols. This is the same as just skipping the perspective correction phase.

As seen in \figref{finding4bestcorners}, there can be more than four
intersections among the number plate edges when the image is skewed. This will
actually happen in most pictures because the two horizontal and two vertical
edges are not completely parallel. The intersections that we are not interested
in will however lie far out of the image. To discard the intersections between
almost parallel lines we choose the following strategy for selecting the four
correct intersections: For each of the four corners in the picture frame (the
whole picture containing the number plate), we select the intersection that are
closest to that corner and has not previously been selected. Considering the
intersections in \figref{finding4bestcorners}, the top-most intersection will
correctly be omitted, because it is so far away from the top-left corner of the
picture frame, which is the closest corner of the frame.

\fig[width=.3\textwidth]{finding4bestcorners}{If the four edges are not pairwise parallel, there will be more than four intersections among the edges.} 

Hough space is represented by a matrix with dimensions $a \times b$. Each element $(i, p)$ in
the matrix represents a line in the picture from which Hough space is
constructed. Recall from \figref{xytohoughspace}, that any line going through a
picture is represented with the parameters $(r, \theta)$ in Hough space.  The
value $m$ in \figref{xytohoughspace} is the maximum $r$ value for which a line
still goes through the picture, thus we define the $r$ value of any element $(i,
p)$ to be $i\frac{m}{a}$. The maximum $\theta$ value is $2\pi$, thus we define
the $\theta$ value of any element $(i, p)$ to be $p\frac{2\pi}{b}$.

\subsubsection{Concerns about line intensities}
For each point $(i, p)$ in Hough-space, we calculate the parameters $(r, \theta)$ of the corresponding line in the $(x,y)$-plane
using geometry. Using the edge detected image, we determine the strength of the line.
The strength of a line is defined as the length of the longest unbroken segment of that line. The line becomes broken whenever the binary image is negative for a point on the line. 
Consider the example in \figref{lineonbinarisedplate}, which illustrates a line through a binary image of a number plate.
The strength of the red line is 5 because the longest unbroken segment of that line is 5 pixels long. However, in this particular case, 
we would like the strength of the red line to equal the entire length of the top line of the number plate. 
This is desired because the red line almost perfectly fits the top line of the number plate, even though two pixels are misplaced. 
Many factors can cause a few pixels in the binary image to deviate from the line in Hough space. This could be due to:
\begin{dlist}
\item The technique used to find the pixels below a line.
\item Edge detection technique does not always produce straight lines.
\item The line represented in Hough space is slightly different from the line in the 
binarised picture because we only calculate $a \times b$ elements in the infinite Hough-space.
The closest $r$ and $\theta$ value in our $a \times b$ representation of Hough space may be slightly off.
\item The number plate is bent a little.
\end{dlist}

\fig[width=0.5\textwidth]{lineonbinarisedplate}{A binarised image determining the strength of a line.}

To correct these issues, we will not just decide that a line is broken if a single pixel below the line is not present in the binarised image.
Rather, we will search for a pixel in a $3 \times 3$ square centred at the pixel in question. If any pixel is found in this $3 \times 3$ neighbourhood,
we will not consider the line as being broken. This technique is illustrated for a single pixel in \figref{nearestneighbourpixel}. 
Previously, the pixel in \figref{nearestneighbourpixel} would have caused the red line to be considered broken.

\fig[width=0.5\textwidth]{nearestneighbourpixel}{Looking in a $3 \times 3$ neighbourhood for pixels.}
