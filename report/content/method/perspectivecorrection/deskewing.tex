\subsection{Deskewing}
\label{sec:deskewing}

When the four corners of the number plates are successfully detected, it is possible to deskew 
the image. Initially, we create a new, empty image. For every pixel in this
empty image we want to find the best matching pixel in the skewed image. The
size of this new image is set so its height, $h$, is equal to:

\[ h = \frac{h_{left} + h_{right}}{2} \]

where $h_{left}$ is the height of the left side of the region containing the
number plate, and $h_{right}$ is the height of the right side of the region
containing the number plate. The width, $w$, of the new image is equal to:

\[ w = \frac{w_{top} + w_{bottom}}{2} \]

where $w_{top}$ is the width of the top of the region containing the
number plate, and $w_{bottom}$ is the width of the bottom of the region
containing the number plate. We illustrate this in \figref{deskewedimagesize}.

\fig[width=0.6\textwidth]{deskewedimagesize}{Creating a deskewed image from a skewed image.}

The technique we use for mapping a pixel between a skewed and a deskewed image
is very simple. As we thought about how deskewing could be done, we came up with
a very simple idea that turned out to give a satisfying result. Someone else may
have discovered the same technique before us, but we have not been able to
confirm this. 

For every pixel in the deskewed image ($\mathit{img_{deskewed}}$), the mapping technique
calculates which pixel to grab from the skewed image ($\mathit{img_{skewed}}$). 
The mapping technique is depicted in \figref{mappingfromdeskewedtoskewed}.

\fig[width=0.6\textwidth]{mappingfromdeskewedtoskewed}{Mapping pixels to their
corresponding positions from the skewed image to the deskewed image.}

For each pixel $(x, y) \in \mathit{img_{deskewed}}$, we map the $x$-coordinate
to two points: one point on the bottom- and one point on the top line of the skewed image.
This can be seen in \figref{mappingfromdeskewedtoskewed}, where the white point in the deskewed image
is mapped to the two black points in the skewed image. 
The mapped points maintain the same ratio between the two line segments they create, such that:

\[ \frac{a}{a'} = \frac{b}{b'} = \frac{x}{x'} \]  

%For each pixel in the deskewed image, its x-coordinate in the deskewed image is
%mapped to a position on the bottom line ($a+a'$) of the skewed image and a
%position on the top line ($b+b'$) of the skewed image. The y-coordinate in the
%deskewed image is now used to determine the position on the line ($c+c'$)
%between the two points just calculated.

By defining a line($c + c'$ in \figref{mappingfromdeskewedtoskewed}) 
between the two points we have just found in the skewed image, we can map the
$y$-coordinate of the pixel in the deskewed image to a point on this line.
Again, we map such that the ratios are maintained:

\[ \frac{c}{c'} = \frac{y}{y'} \]  

The point we have just found is the final result of mapping a pixel's position in a 
deskewed image to a position in a skewed image.

The algorithm we use for deskewing can be seen in \algref{deskew}.

\begin{algorithm}
  \caption{The deskew algorithm.}
  \label{alg:deskew}
  \KwIn{
    A skewed image: $\mathit{img_{skewed}}$\;
    The four corners of the skewed image as vectors: $\mathit{TL}$, $\mathit{TR}$\; 
    $\mathit{BL}$, $\mathit{BR}$\;
  }
  \KwOut{
    A deskewed image: $\mathit{img_{deskewed}}$\;
  }
  \Begin{
	  $w \gets \frac{(\mathit{TR_x} - \mathit{TL_x}) + (\mathit{BR_x} -
	  \mathit{BL_x})}{2}$ \;
	  $h \gets \frac{(\mathit{BR_y} - \mathit{TR_y}) + (\mathit{BL_y} -
	  \mathit{TL_y})}{2}$ \;
	  $\mathit{FB} \gets \frac{\mathit{BR} - \mathit{BL}}{w-1}$ \;
	  $\mathit{FT} \gets \frac{\mathit{TR} - \mathit{TL}}{w-1}$ \;
	  Initialise new empty image: $\mathit{img_{deskewed}}(w, h)$\;
    \For{$x \gets 0$ \KwTo $w$}{
	    $\mathit{PB} \gets \mathit{BL} + x \cdot \mathit{FB}$ \;
	    $\mathit{PT} \gets \mathit{TL} + x \cdot \mathit{FT}$ \;
	    $\mathit{FC} \gets \frac{\mathit{PB} - \mathit{PT}}{h}$ \;
      \For{$y \gets 0$ \KwTo $h$}{
	      $\mathit{mappedPixel} \gets \mathit{PT} + y \cdot \mathit{FC}$ \;
	      $\mathit{img_{deskewed}}(x, y) \gets
	      \mathit{img_{skewed}}(\mathit{mappedPixel_x},
	      \mathit{mappedPixel_y})$\;
      }
    }
    \Return $\mathit{img_{deskewed}}$
  }
\end{algorithm} 

The size of the deskewed image (width and height) is calculated and stored by
$w$ and $h$. The vector $\mathit{FB}$ maps a position along the $x$-axis of the
deskewed image ($\mathit{img_{deskewed}}$) to a corresponding position on the
vector representing the bottom line of the skewed image
($\mathit{img_{deskewed}}$), which is the line $a+a'$ in
\figref{mappingfromdeskewedtoskewed}. $\mathit{FT}$ does the same, but on the
vector representing the top line of $\mathit{img_{skewed}}$, which is the line
$b+b'$ in \figref{mappingfromdeskewedtoskewed}. 

For every $x$-coordinate in $\mathit{img_{deskewed}}$, a position on the bottom
and top line of the skewed number plate is found ($\mathit{PB}$ and
$\mathit{PT}$). The vector $\mathit{FC}$ maps a $y$-coordinate from
$\mathit{img_{deskewed}}$ in the range $[0, h]$ to a position on the line
between the two points $\mathit{PT}$ and $\mathit{PB}$, which is the line $c+c'$
in \figref{mappingfromdeskewedtoskewed}.

