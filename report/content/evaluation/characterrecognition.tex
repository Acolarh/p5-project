\section{Optical character recognition}
\label{sec:evaluationofcharacterrecognition}

The neural network used for OCR is trained
using a training set and validated using a validation set. Calculating the MSE for 
the two sets gives an estimate of how well the network performs. Other measures include
how fast the learning algorithm runs, how many iterations are required to reach a minimum,
and how long the running time of the network is.

%To teach our neural network we used supervised learning. This means that the
%network receives an input and the expected result which it compares its actual
%result with. So the network uses the input and makes the calculations and outputs 
%a probability for each output neuron which in fact represents a symbol. We then
%use the mean of square errors (MSE) to adjust the distribution of weights. Updating
%weight distributions is explained in \secref{nnlearning}.

\subsection{Training and validation samples}
%
%The network performs these calculations with the weights that each connection
%between the neurons has. This weight distribution is initially set randomly to a
%value from $0.0$ to $1.0$. So each of the $\num{9000}$ connections has a weight. It
%does not make any sense to begin and figure out which weights to give each
%connection because these are adjusted through the learning process. 
%
%It was in fact not a good idea to have weights from zero to one. We tested the
%learning process and found ourselves waiting an eternity for the learning to
%finish. After doing some research and trying different solutions, we figured out
%that we needed negative weights as well. So we changed the weight distribution
%to be from $-0.5$ to $0.5$ instead. With the old distribution the learning would
%continue for several $\num{100000}$ iterations and it would most likely never finish
%because the MSE would change very little. This was with only about $\num{300}$
%training samples. When adjusting the distribution it would take less then
%$\num{600}$ iterations with $\num{1920}$ training samples.

%\subsubsection{Training samples}

We have extracted characters from images we have taken of different number plates using
segmentation.
%These characters were saved as individual images with the size of $10 \times 12$ pixels.
A subset of the samples is illustrated in
\figref{ocr_samples}. 
We have collected a total of \num{2880} character images that can be used to train
the neural network.
Each character has been classified using the user interface explained
in \secref{impl-classification-characters}.
Using a $\frac{2}{3}$ split, we end up with \num{1920} training
samples and \num{960} validation samples.


\fig[width=\textwidth]{ocr_samples}{A tiny subset of the training samples used.}

It is important to note that because there are more numbers than letters on the majority
of our number plates, we also have an excess of numbers in our OCR training samples. This
is illustrated in \figref{chardistribution}. Some letters, such as ``O'', ``K'' and ``L'', are not well
represented in the training samples (we only have two samples of the letter ``O''). This could
negatively influence our ability to generalise the network for those characters.
Another observation to be made is that we don't have a single ``I''.
This is primarily because the character is completely absent from 
regular Danish number plates. They can however appear on custom plates.

\fig[width=0.75\textwidth]{chardistribution}{Distribution of the \num{2880} character samples.}

%When the entire set of training samples has been used to calculate the error of
%the network's performance and update the weights accordingly, one iterations has 
%finished. 

%\subsubsection{Validation}

%We have manually classified $\num{2800}$ samples to be used as training and
%validation samples using the interface explained in
%\secref{impl-classification-characters}. $\frac{2}{3}$ of the samples are used for learning
%($\num{1920}$ training samples) and $\frac{1}{3}$ of the samples are used for
%validating the result of the learning process ($\num{960}$ validating samples). 

The validation set is automatically used to validate the network after a
learning process has finished to give us an estimate of how well the network
performs. It also lets us know that the network has not been overfitted 
to only work on the training data.

When the learning is finished then the network can be used to recognise symbols.
Based on the calculations the network performs on the input, the output is an
approximation of what the network believes it is reading.

\subsection{Training speed}

Training performance of the neural network is a combination of the speed of each iteration
and the number of iterations needed to reach a minimum error. Obviously, having to update
many connections each iteration, means that training becomes slower. A low learning
rate also means that it takes more iterations to reach an acceptable error, and depending
on the configuration of the network it might not even reach an acceptable error.

\subsubsection{Learning rate}

The learning rate of the learning algorithm can be increased to increase the speed of 
training. Increasing the learning rate too much can make learning unstable and prevent
the network from reaching its minimum. Figure \ref{fig:ocr-eval-lr} shows the results of
testing different learning rates on a network of $60$ hidden neurons. An increase from
$0.1$ to $0.2$ shows an improvement in speed, while $0.3$ and $0.5$ doesn't improve much
over $0.2$. A learning rate of $1.0$ makes the error unstable, but it still decreases over
time. A learning rate of $1.5$ however makes learning very unstable, and the MSE does not
decrease much over the course of $100$ iterations.

\fig[width=0.75\textwidth]{ocr-eval-lr}{Impact of learning rate on 100 iterations of neural network training (using a logarithmic scale for the error-axis).}

\subsubsection{Momentum}

Momentum is a method for speeding up learning. Figure \ref{fig:ocr-eval-mo} shows the results
from testing three different momentums when training for 100 iterations. A momentum of 
$0.8$ clearly shows an improvement over a momentum of $0$ (no momentum), however a momentum
of $0.9$ makes learning unstable. So as with learning rate, it is important to find a balance
between speed and stability.

\fig[width=0.75\textwidth]{ocr-eval-mo}{Impact of momentum on 100 iterations of neural network training with
a learning rate of $0.1$ (using a logarithmic scale for the error-axis).}

\subsection{Network performance}

The performance of a neural network is mostly a measure of how well the network
classifies inputs. During and after training, calculating the MSE of the validation
samples can be used to measure the performance of the network.

Based on our validation test we have achieved a recognition rate of $95.83 \%$. This number
is based on running the network on all validation samples. Out of the \num{960} validation
samples only \num{40} where misclassified which gives an error of $\frac{40}{960} = 4.17\%$.

Comparing different network configurations is not an easy task. Comparing the minimum
MSE of the validation set for each network would be ideal, but it is not easy to tell
when such a minimum has been reached. Comparing different networks after for example
$500$ would therefore not work.

During training of the network there were no signs of overfitting, as seen on 
\figref{realmse}, even after \num{2000} iterations. The final MSE was $0.00215$ for the
training samples and $0.05899$ for the validation samples.

\fig[width=0.75\textwidth]{realmse}{\num{2000} iterations of network training showing no signs of overfitting (using a logarithmic scale for the error-axis).}

\subsubsection{Running time}

Time can also be taken into account, i.e.\ how much time the network uses to compute an answer. The speed
mostly depends on the number of connections in the network. In \tableref{networkPerformance}
the computation times for networks of different sizes are presented in the rightmost column.
Even with an unnecessarily large network consisting of $4$ hidden layers of $80$ neurons
each, it only takes \SI{360}{\micro\second} to calculate the output vector. Creating
the input vector may also be taken into account since it usually includes resizing the
input image to a certain size. For all speed tests in the table an input size of
$10 \times 12$ pixels was used, and for each test it took on average of \SI{50}{\micro\second}
to resize the input image and create the input vector and an average of \SI{10}{\micro\second}
to convert the output vector to a list of weighted characters.

\tab[\textwidth]{networkPerformance}{4}{Speed of OCR for different network sizes.}
         {Network timings}
  {Case}{Hidden layers & Hidden neurons per layer & Number of connections & Calculating output vector }{
    \tabrow{1}{$1$ & $60$ & $\num{9360}$ & $\SI{56}{\micro\second}$}
    \tabrow{2}{$1$ & $200$ & $\num{31200}$ & $\SI{254}{\micro\second}$}
    \tabrow{3}{$4$ & $80$ & $\num{31680}$ & $\SI{360}{\micro\second}$}
}
