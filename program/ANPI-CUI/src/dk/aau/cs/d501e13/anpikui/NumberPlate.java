package dk.aau.cs.d501e13.anpikui;

import dk.aau.cs.d501e13.anpi.data.models.NumberPlateFlags;

public class NumberPlate {
	public int x;
	public int y;
	public int width;
	public int height;
	public NumberPlateFlags flags = new NumberPlateFlags();
	public String value;
}
