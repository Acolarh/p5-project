package dk.aau.cs.d501e13.anpikui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.data.models.NumberPlateFlags;

public class NumberPlateMarker extends JPanel implements MouseInputListener {
	private static final long serialVersionUID = -2147247855451235028L;
	
	protected File file;
	protected BufferedImage img;
	protected int offsetX;
	protected int offsetY;
	protected List<NumberPlate> numberPlates = new ArrayList<NumberPlate>();
	protected NumberPlate current;
	protected Point mouse;
	
	public NumberPlateMarker() {
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public NumberPlateFlags currentFlags(){
		if( current != null ){
			return current.flags;
		}
		else{
			return new NumberPlateFlags();
		}
	}
	
	protected int scale(int x, int full, int actual) {
		double percentage = (double)x / full;
		return (int)(percentage * actual + 0.5);
	}
	
	protected int scaleX( int x ){
		return offsetX + scale(x, img.getWidth(), getWidth() - 2*offsetX);
	}
	
	protected int scaleY (int y) {
		return offsetY + scale(y, img.getHeight(), getHeight() - 2*offsetY);
	}
	
	protected int scaleWidth (int width) {
		return scale(width, img.getWidth(), getWidth() - 2*offsetX);
	}
	
	protected int scaleHeight (int height) {
		return scale(height, img.getHeight(), getHeight() - 2*offsetY);
	}
	
	protected int revertScale(int y, int full, int actual) {
		return y * full / actual;
	}
	
	protected int revertScaleX(int x) {
		return revertScale(x - offsetX, img.getWidth(), getWidth() - 2*offsetX);
	}
	
	protected int revertScaleY(int y) {
		return revertScale(y - offsetY, img.getHeight(), getHeight() - 2*offsetY);
	}
	
	@Override
	public Dimension getPreferredSize(){
		if( img != null ){
			return new Dimension(img.getWidth(), img.getHeight() - 2*offsetY);
		}
		else
			return new Dimension( 0, 0 );
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if( img != null ){
			//Keep aspect
			double aspectOrg = (double)img.getWidth() / img.getHeight();
			double aspectWindow = (double)getWidth() / getHeight();
			int width = getWidth();
			int height = getHeight();
			if( aspectOrg > aspectWindow ){
				height = (int)(width / aspectOrg + 0.5);
			}
			else{
				width = (int)(height * aspectOrg + 0.5);
			}
			
			//Find offsets
			offsetX = (getWidth() - width) / 2;
			offsetY = (getHeight() - height) / 2;
			g.drawImage(img, offsetX,offsetY, width,height, this);
	
			g.setColor(new Color(1.0f, 0.0f, 0.0f));
			for( NumberPlate p : numberPlates ){
				g.drawRect( scaleX(p.x), scaleY(p.y), scaleWidth(p.width), scaleHeight(p.height));
			}
			
			if (current != null) {
				g.setColor(new Color(0.0f, 1.0f, 0.0f));
				g.drawRect(scaleX(current.x), scaleY(current.y), scaleWidth(current.width), scaleHeight(current.height));
			}
		}
	}
	
	
	void setImage( File f ){
		file = f;
		numberPlates.clear();
		current = null;
		try {
			img = (new Image( file )).getBufferedImage();
		} catch (ImageReadException e) {
			img = null;
		}
		repaint();
	}
	
	public void acceptRect() {
		if (current != null) {
			System.out.println("test" + currentFlags().getFlags());
			if(!currentFlags().isEmpty()){
				numberPlates.add(current);
				current = null;
				repaint();
			}
		}
	}
	
	public void undo() {
		if (current != null) {
			current = null;
		}
		else if (numberPlates.size() > 0) {
			numberPlates.remove(numberPlates.size() - 1);
		}
		repaint();
		
	}
	
	public List<NumberPlate> getNumberPlates() {
		return numberPlates;
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (img != null) {
			current = new NumberPlate();
			current.x = revertScaleX(arg0.getX());
			current.y = revertScaleY(arg0.getY());
			current.width = 0;
			current.height = 0;
			mouse = new Point(current.x, current.y);
			repaint();
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (current != null && mouse != null) {
			current.x = Math.min(mouse.x, revertScaleX(arg0.getX()));
			current.y = Math.min(mouse.y, revertScaleY(arg0.getY()));
			current.width = Math.abs(revertScaleX(arg0.getX()) - mouse.x);
			current.height = Math.abs(revertScaleY(arg0.getY()) - mouse.y);
			repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		mouse = null;
	}
	
	@Override
	public void mouseMoved(MouseEvent arg0) {}
	@Override
	public void mouseExited(MouseEvent arg0) {}
	@Override
	public void mouseClicked(MouseEvent arg0) {}
	@Override
	public void mouseEntered(MouseEvent arg0) {}
	
}
