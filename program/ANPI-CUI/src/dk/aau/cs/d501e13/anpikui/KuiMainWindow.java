package dk.aau.cs.d501e13.anpikui;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class KuiMainWindow implements ActionListener {
	protected JFrame frame;
	protected JMenuItem chooseDir;
	protected JButton doneButton;
	protected JButton undoButton;
	protected JButton nextButton;
	protected JCheckBox whiteNumberPlateCB;
	protected JCheckBox yellowNumberPlateCB;
	protected JCheckBox squareNumberPlateCB;
	protected JCheckBox countryMarkNumberPlateCB;
	protected JCheckBox partialNumberPlateCB;
	protected JCheckBox skewedNumberPlateCB;
	protected JCheckBox blurredNumberPlateCB;
	
	protected Database db;
	protected ArrayList<File> files = new ArrayList<File>();
	protected File currentFile;
	protected ImageModel imgModel;
	protected NumberplateModel plateModel;
	protected NumberPlateMarker numberPlateMarker;

	protected int height = 300;
	protected int width = 400;
	protected int xCoord = 100;
	protected int yCoord = 100;
	
	public static void main(String[] args) {
	    try {
	      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (ClassNotFoundException | InstantiationException
	        | IllegalAccessException | UnsupportedLookAndFeelException e) {
	      System.out.println("Could not use native look and feel!");
	    }
		EventQueue.invokeLater(new Runnable() {
		      public void run() {
		        try {
		          new KuiMainWindow();
		        }
		        catch (Exception e) {
		          e.printStackTrace();
		        }
		      }
		    });
	}
	
	public KuiMainWindow() {
		initialize();
	}
	
	public void initialize() {
		frame = new JFrame("KUI window");
		frame.setBounds(xCoord, yCoord, width, height);  
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// File menu
		JMenuBar menu = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		menu.add(fileMenu);
		frame.add(menu);
		frame.getContentPane().add(menu, BorderLayout.NORTH);
		chooseDir = new JMenuItem( "Choose directory" );
		chooseDir.addActionListener( this );
		fileMenu.add( chooseDir );
		
		// Main panel
		JPanel mainPanel = new JPanel();
		frame.add(mainPanel);
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		
		// Image panel
		numberPlateMarker = new NumberPlateMarker();
		mainPanel.add(numberPlateMarker);
		
		// Button and Check box panel
		JPanel buttonAndCheckBoxPanel = new JPanel();
		buttonAndCheckBoxPanel.setLayout(new BoxLayout(buttonAndCheckBoxPanel, BoxLayout.Y_AXIS));
		mainPanel.add(buttonAndCheckBoxPanel);
		
		// Button panel
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		doneButton = addButton("Done", buttonPanel, true);
		undoButton = addButton("Undo", buttonPanel, true);
		nextButton = addButton("Next", buttonPanel, true);
		buttonAndCheckBoxPanel.add(buttonPanel);
		
	    // Check Box panel
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.Y_AXIS));
		checkBoxPanel.setBounds(200, 200, width, height);
		whiteNumberPlateCB = addCheckBox("White", checkBoxPanel, true, "1");
		yellowNumberPlateCB = addCheckBox("Yellow", checkBoxPanel, true, "2");
		squareNumberPlateCB = addCheckBox("Square", checkBoxPanel, true, "3");
		countryMarkNumberPlateCB = addCheckBox("Country Mark", checkBoxPanel, true, "4");
		partialNumberPlateCB = addCheckBox("Partial", checkBoxPanel, true, "5");
		skewedNumberPlateCB = addCheckBox("Skewed", checkBoxPanel, true, "6");
		blurredNumberPlateCB = addCheckBox("Blurry", checkBoxPanel, true, "7");
		buttonAndCheckBoxPanel.add(checkBoxPanel);
		
		try {
			db = new Database("db.properties");
			imgModel = new ImageModel(db);
			plateModel = new NumberplateModel(db);
		} catch (ClassNotFoundException | IOException | SQLException e) {
			JOptionPane.showMessageDialog(frame, "Couldn't open database", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	protected JCheckBox addCheckBox( String text, Container container, boolean listen, String hotkey ){
		final JCheckBox checkbox = new JCheckBox(text);
		addAbstractButton(checkbox, container, listen);
		checkbox.getInputMap(javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(hotkey), "hotkey");
		AbstractAction aa = new AbstractAction(){
			private static final long serialVersionUID = -3050346984016475125L;

			public void actionPerformed(ActionEvent e) {
				checkbox.setSelected(!checkbox.isSelected());
				checkboxesChanged();
			}
		};
		
		checkbox.getActionMap().put("hotkey", aa);
		
		
		return checkbox;
	}
	
	protected JButton addButton(String text, Container container, boolean listen) {
		JButton button = new JButton(text);
		addAbstractButton(button, container, listen);
		return button;
	}
	
	protected void addAbstractButton( AbstractButton abstractButton, Container container, boolean listen ){
		container.add(abstractButton);
		if(listen){
			abstractButton.addActionListener(this);
		}
	}
	
	protected void clearCheckBoxes() {
		whiteNumberPlateCB.setSelected(false);
		yellowNumberPlateCB.setSelected(false);
		squareNumberPlateCB.setSelected(false);
		countryMarkNumberPlateCB.setSelected(false);
		blurredNumberPlateCB.setSelected(false);
		partialNumberPlateCB.setSelected(false);
		skewedNumberPlateCB.setSelected(false);
	}
	
	public void checkboxesChanged(){
		numberPlateMarker.currentFlags().setWhite(whiteNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setYellow(yellowNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setSquare(squareNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setCountry(countryMarkNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setPartial(partialNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setBlurry(blurredNumberPlateCB.isSelected());
		numberPlateMarker.currentFlags().setSkewed(skewedNumberPlateCB.isSelected());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getSource() == chooseDir ){
			File[] fileArray = getFilesFromDir();
			files.clear();
			for( File f : fileArray ){
				files.add( f );
			}
			getNextImage();
		}
		if (e.getSource() == nextButton){
			if (numberPlateMarker.current == null) {
				try {
					saveNumberPlateList(numberPlateMarker.getNumberPlates());
				} catch (ImageReadException | ImageCloneException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				getNextImage();
			}
		}
		if (e.getSource() == undoButton){
			numberPlateMarker.undo();
			clearCheckBoxes();
		}
		if (e.getSource() == doneButton){
			numberPlateMarker.acceptRect();
			clearCheckBoxes();
		}
		if (e.getSource() == whiteNumberPlateCB
			|| e.getSource() == yellowNumberPlateCB
			|| e.getSource() == squareNumberPlateCB
			|| e.getSource() == countryMarkNumberPlateCB
			|| e.getSource() == partialNumberPlateCB
			|| e.getSource() == blurredNumberPlateCB
			|| e.getSource() == skewedNumberPlateCB
			) {
			checkboxesChanged();
		}	
	}

	public void saveNumberPlateList(List<NumberPlate> numberPlateList) throws ImageReadException, ImageCloneException, IOException {
		System.out.print("Saved");
		
		NumberPlate NumberPlateRegion = numberPlateList.get(0);
		Rectangle rectangle = new Rectangle(NumberPlateRegion.x, NumberPlateRegion.y, NumberPlateRegion.width, NumberPlateRegion.height);
		
		Image currentImage = new Image(currentFile);
		currentImage.getSubimage(rectangle).save("C:\\Users\\SimonBuus\\Documents\\hjørnedetektionsresultater\\" + currentFile.getName(), "png");;
		files.remove(currentFile);
		
		/*
		ImageRecord record;
		try {
			record = imgModel.create(currentFile.getName(), ImageRecord.SET_UNSET);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(frame, "Couldn't add image to database", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		try {
			for( NumberPlate p : numberPlateList ){
				plateModel.create(record, Math.max(p.x,0), Math.max(p.y,0), p.width, p.height, p.flags.getFlags(), "");
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(frame, "Couldn't save all number plates to " + record.id, "Error", JOptionPane.ERROR_MESSAGE);
		}
		*/
	}
	
	public File[] getFilesFromDir(){
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int returnVal = fc.showOpenDialog( frame );
		if( returnVal == JFileChooser.APPROVE_OPTION ){
			return fc.getSelectedFile().listFiles();
		}
		else{
			return new File[0];
		}
	}
	
	public void getNextImage(){
	
		if (files.size() != 0) {
			currentFile = files.get(0);
			numberPlateMarker.setImage( currentFile );
		}
		else if ( files.size() == 0 ) {
			JOptionPane.showMessageDialog(frame, "No more images in directory", "No images to klaskify", JOptionPane.ERROR_MESSAGE);
		}
				
		/*
		while( files.size() > 0 ){
			
			Random rand = new Random();
			int newIndex = rand.nextInt( files.size() );
			
			ImageRecord record;
			try{
				record = imgModel.findByName(files.get(newIndex).getName());
			}
			catch( SQLException e ){
				files.clear();
				break;
			}
			
			if( record == null ){
				currentFile = files.get(newIndex);
				numberPlateMarker.setImage( currentFile );
				break;
			}
			else{
				files.remove(newIndex);
			}
		}
		
		if( files.size() == 0 ) {
			JOptionPane.showMessageDialog(frame, "No more images in directory", "No images to klaskify", JOptionPane.ERROR_MESSAGE);
		}
		*/
	}
	
}
