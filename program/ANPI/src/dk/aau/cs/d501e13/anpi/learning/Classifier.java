package dk.aau.cs.d501e13.anpi.learning;

/**
 * A value based classifier
 * @param <T> The type of object to classify
 */
public interface Classifier<T> extends java.io.Serializable {
  
  /**
   * Classifiers obj as a value. Value can be arbitrary,
   * but must be consistent and should have a strong connection
   * to one or more properties of the object type.
   * @param obj The object to classify
   * @return The classification
   */
  public double predictValue(T obj);
}
