package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;

/**
 * Predicts the gradient density of a block of pixels.
 */
public class GradientDensityFeature implements BClassifier<Image> {
  
  private static final long serialVersionUID = 649870213331428833L;
  /**
   * The threshold.
   */
  private double threshold = 0.5;

  /**
   * Constructor for class.
   */
  public GradientDensityFeature(){ }

  /**
   * @return The threshold.
   */
  public double getThreshold() {
    return threshold;
  }

  /**
   * Set the threshold.
   * @param t The threshold.
   */
  public void setThreshold(double t) {
    this.threshold = t;
  }

  /**
   * @param img The given block to be checked.
   * @return The gradient density of the block.
   */
  public double getGradientDensity(Image img) {
    double pixelsInBlock = img.getWidth() * img.getHeight();
    double gradientMagnitude = img.getSum(0, 0, img.getWidth(), img.getHeight());
      
    return gradientMagnitude / pixelsInBlock;

  }

  @Override
  public boolean predict(Image img) {
    double gradientDensity = getGradientDensity(img);
    
    // How do we optimize the threshold?
    if(gradientDensity <= threshold){
      return true;
    }
    else{
      return false;
    }
  }

}
