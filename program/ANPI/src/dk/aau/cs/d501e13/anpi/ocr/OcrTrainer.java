package dk.aau.cs.d501e13.anpi.ocr;

import java.awt.Graphics2D;
import java.awt.image.WritableRaster;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.CharacterModel;
import dk.aau.cs.d501e13.anpi.data.models.CharacterRecord;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.DotConstructor;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkTrainingSample;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 * OCR Trainer.
 */
public class OcrTrainer {
  
  public final static int NUM = 1;
  public final static int DEN = 3;
  
  public final static int MAX_SAMPLES_PER_CHAR = 10;
  
  /**
   * Validates based on the validation set.
   * @param ocr The ocr object.
   * @param nn The neural network object.
   * @param samples The training samples.
   * @throws CorruptedInputException
   */
  public static void validate(OCR ocr, NeuralNetwork nn, List<OcrTrainingSample> samples) throws CorruptedInputException {
    int error = 0;
    for (OcrTrainingSample sample : samples) {
      nn.setInputVector(sample.getInputs());
      double[] output = nn.calculate();
      char o = ocr.getCharacter(output);
      char e = ocr.getCharacter(sample.getExpectedOutputs());
      if (e != o) {
        error++;
        System.out.println(o + " != " + e);
      }
    }
    System.out.println("Error: " + error + " / " + samples.size());
  }

  public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException, CorruptedInputException {
    if (args.length < 1) {
      System.out.println("usage: trainter CONFIG_FILE");
      System.exit(1);
    }
    Config conf = new Config(args[0]);
    OCR ocr = new OCR(conf);
    
    CharacterRecord[] records;
    
    try {
      FileInputStream fis = new FileInputStream("ocr-trainer-samples");
      ObjectInputStream ois = new ObjectInputStream(fis);
      System.out.println("Getting samples from file...");
      records = (CharacterRecord[])ois.readObject();
      ois.close();
      fis.close();
    }
    catch (IOException e) {
      Database db = new Database(conf.getConfig("database"));
      CharacterModel characters = new CharacterModel(db);
      System.out.println("Getting samples from db...");
      records = characters.all();
      FileOutputStream fos = new FileOutputStream("ocr-trainer-samples");
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      System.out.println("Writing samples to file...");
      oos.writeObject(records);
      oos.close();
      fos.close();
    }
    
    NeuralNetwork nn = ocr.getNeuralNetwork();
    
    TransferFunction transferFunction = ocr.getTransferFunction();
//    
//    // Create custom network with one hidden layer
//    nn = new NeuralNetwork();
//    int numInputs = ocr.getWidth() * ocr.getHeight();
//    int numOutputs = ocr.getNumOutputs();
//    for(int i = 0; i < numInputs; i++){
//      nn.addInput(new InputNeuron(transferFunction));
//    }
//
//    for(int i = 0; i < numOutputs; i++){
//      nn.addOutput(new Neuron(transferFunction));
//    }

    
    DotConstructor dotter = new DotConstructor(nn);
    FileOutputStream fos = new FileOutputStream("nn1.dot");
    dotter.construct(fos);
    fos.close();
    
    NeuralNetworkLearner learner = new NeuralNetworkLearner(nn);

    
    
    
    List<OcrTrainingSample> trainingSamples = new ArrayList<OcrTrainingSample>();
    List<OcrTrainingSample> validationSamples = new ArrayList<OcrTrainingSample>();
    

    int[] num = new int[ocr.getNumOutputs()];
    Arrays.fill(num, 0);
    
    System.out.println("Adding samples...");
    for (CharacterRecord record : records) {
      int offset = ocr.charToOffset((char)record.value);
      if (offset >= ocr.getNumOutputs())
        continue;
      if (record.id % DEN < NUM) {
        validationSamples.add(new OcrTrainingSample(ocr, record.getImage(), (char)record.value));
      }
      else {
//        if (num[offset] >= MAX_SAMPLES_PER_CHAR) {
//          continue;
//        }
        Image image = record.getImage();
        trainingSamples.add(new OcrTrainingSample(ocr, image, (char)record.value));
        num[offset]++;
      }
    }
    
    System.out.println("Creating image...");
    Image cunt = new Image(trainingSamples.size() * ocr.getWidth(), ocr.getHeight());
    Graphics2D g = cunt.createGraphics();
    int xOffset = 0;
    for (OcrTrainingSample t : trainingSamples) {
      Image c = new Image(ocr.getWidth(), ocr.getHeight()).getGrayscale();
      WritableRaster r = c.getRaster();
      double[] iv = t.getInputs();
      int j = 0;
      for (int x = 0; x < ocr.getWidth(); x++) {
        for (int y = 0; y < ocr.getHeight(); y++) {
          r.setSample(x, y, 0, (int)(iv[j++] * 255));
        }
      }
      g.drawImage(c.getBufferedImage(), xOffset, 0, ocr.getWidth(), ocr.getHeight(), null);
      xOffset += ocr.getWidth();
    }
    g.dispose();
    cunt.save(Utilities.ocrFileName(conf) + ".png", "png");
    
    OcrTrainingSample[] samplesArray = trainingSamples.toArray(new OcrTrainingSample[0]);

    
    System.out.println("Validating..");
    validate(ocr, nn, validationSamples);
    
    OcrTrainingSample[] validationSamplesArray = validationSamples.toArray(new OcrTrainingSample[0]);
    
    System.out.println("SSE: " + MSE(nn, validationSamplesArray));

    System.out.println("Training (" + samplesArray.length + " samples)...");
    int i = 0;
    double trainingError = 1.0;
    double validationError = 1.0;
    double maxError = conf.getDouble("ocrMaxError", 0.01);
    PrintStream ps = new PrintStream("mse.csv");
    do {
      learner.learn(samplesArray);
      trainingError = MSE(nn, samplesArray);
      validationError = MSE(nn, validationSamplesArray);
//      if (i % 10 == 0)
        System.out.println(i + "," + trainingError + "," + validationError);
      ps.println(i + "," + trainingError + "," + validationError);
      ocr.save(Utilities.ocrFileName(conf));
      i++;
    } while (trainingError > maxError);
    ps.close();
    System.out.println("iteration " + i + ": " + trainingError);
    
    System.out.print("Saving...");
    if (ocr.save(Utilities.ocrFileName(conf)))
      System.out.println("OK");
    else
      System.out.println("FAIL");
    
    System.out.println("Validating..");
    validate(ocr, nn, validationSamples);
    System.out.println("MSE: " + MSE(nn, validationSamplesArray));
  }

  /**
   * Calculates the sum of squared errors.
   * @param nn The neural network object.
   * @param samples The training samples.
   * @return The sum of sqaured errors.
   * @throws CorruptedInputException
   */
  public static double MSE(NeuralNetwork nn, NeuralNetworkTrainingSample[] samples) throws CorruptedInputException {
    double errorSum = 0.0;
    for (NeuralNetworkTrainingSample ts : samples) {
      nn.setInputVector(ts.getInputs());
      double[] outputs = nn.calculate();
      double[] expected = ts.getExpectedOutputs();
      for (int i = 0; i < outputs.length; i++) {
        double error = outputs[i] - expected[i];
        errorSum += error * error;
      }
    }
    return errorSum / samples.length;
  }
}
