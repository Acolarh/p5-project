package dk.aau.cs.d501e13.anpi.testing;

import java.awt.Window.Type;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.CharacterModel;
import dk.aau.cs.d501e13.anpi.data.models.CharacterRecord;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.DotConstructor;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;
import dk.aau.cs.d501e13.anpi.ocr.OCR;
import dk.aau.cs.d501e13.anpi.ocr.OcrTrainingSample;
import dk.aau.cs.d501e13.anpi.testing.OcrTest.VideoPanel;

public class ValidateOCRTS {

  public final static int NUM = 1;
  public final static int DEN = 3;
  public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException, CorruptedInputException {
    if (args.length < 1) {
      System.out.println("usage: trainter CONFIG_FILE");
      System.exit(1);
    }
    Config conf = new Config(args[0]);
    OCR ocr = new OCR(conf);
    
    CharacterRecord[] records;
    
    try {
      FileInputStream fis = new FileInputStream("ocr-trainer-samples");
      ObjectInputStream ois = new ObjectInputStream(fis);
      System.out.println("Getting samples from file...");
      records = (CharacterRecord[])ois.readObject();
      ois.close();
      fis.close();
    }
    catch (IOException e) {
      Database db = new Database(conf.getConfig("database"));
      CharacterModel characters = new CharacterModel(db);
      System.out.println("Getting samples from db...");
      records = characters.all();
      FileOutputStream fos = new FileOutputStream("ocr-trainer-samples");
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      System.out.println("Writing samples to file...");
      oos.writeObject(records);
      oos.close();
      fos.close();
    }
    
    NeuralNetwork nn = ocr.getNeuralNetwork();
    
    TransferFunction transferFunction = ocr.getTransferFunction();
    


    JDialog f = new JDialog();
    
    VideoPanel p = new VideoPanel();
    f.add(p);
    f.setSize(300, 360);
    f.setVisible(true);

    int[] num = new int[ocr.getNumOutputs()];
    Arrays.fill(num, 0);
    
    System.out.println("Adding samples...");
    for (CharacterRecord record : records) {
      int offset = ocr.charToOffset((char)record.value);
      if (offset >= ocr.getNumOutputs())
        continue;
      if (record.id % DEN < NUM) {
      }
      else {
        
      }
      Image image = record.getImage();
      p.setImage(image.getBufferedImage());
      OcrTrainingSample sample = new OcrTrainingSample(ocr, image, (char)record.value);

      nn.setInputVector(sample.getInputs());
      double[] output = nn.calculate();
      char o = ocr.getCharacter(output);
      char e = ocr.getCharacter(sample.getExpectedOutputs());
      if (e != o) {
        System.out.println("Error: (id = " + record.id + ") " + e + " classified as " + o);
        System.in.read();
      }
      num[offset]++;
    }
    System.exit(0);
  }

}
