package dk.aau.cs.d501e13.anpi.learning;

public class ThresholdedClassifier<T> implements BClassifier<T> {
  private static final long serialVersionUID = -7600358089121621575L;
  
  //TODO: Elias, javadoc this!
  private double threshold;
  private boolean greater;
  private Classifier<T> classifier;
  
  public ThresholdedClassifier(Classifier<T> classifier, double threshold, boolean greater) {
    this.classifier = classifier;
    this.threshold = threshold;
    this.greater = greater;
  }
  
  @Override
  public boolean predict(T t) {
    double value = classifier.predictValue(t);
    return greater ? value > threshold : value < threshold;
  }

  public Classifier<T> getClassifier(){
    return classifier;
  }
}
