package dk.aau.cs.d501e13.anpi.neuralnetwork.transfer;

import java.io.Serializable;

/**
 * A transfer function. Also called an activation function.
 */
public interface TransferFunction extends Serializable {
  /**
   * A specific activation function.
   * @param inputSum The result based on the calculation of the value of a neuron and its weight.
   * @return the activated output.
   */
  public double activate(double inputSum);
  /**
   * Returns the derivative of the transfer function.
   * @param output The actual output.
   * @return The derivative of the transfer function.
   */
  public double getDerivative(double output);
}
