package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.util.HashSet;
import java.util.List;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 * Algorithm for supervised learning of neural networks
 */
public class NeuralNetworkLearner {
  /**
   * A neural network.
   */
  private NeuralNetwork neuralNetwork;
  
  /**
   * The learning rate. Default is 1/4.
   */
  private double learningRate = 0.25;
  
  /**
   * Momentum to speed up the learning.
   */
  private double momentum = 0.0;

  /**
   * @return The learning rate.
   */
  public double getLearningRate() {
    return learningRate;
  }

  /**
   * @return The momentum.
   */
  public double getMomentum() {
    return momentum;
  }

  /**
   * Set the value of the learning rate.
   * @param learningRate The new value of the learning rate.
   */
  public void setLearningRate(double learningRate) {
    this.learningRate = learningRate;
    for (Neuron n : neuralNetwork.getHiddenNeurons()) {
      for (Connection c : n.getInputs()) {
        c.learningRate = learningRate;
      }
    }
    for (Neuron n : neuralNetwork.getOutputNeurons()) {
      for (Connection c : n.getInputs()) {
        c.learningRate = learningRate;
      }
    }
  }

  /**
   * Set the value of the learning rate.
   * @param learningRate The new value of the learning rate.
   */
  public void setMomentum(double momentum) {
    this.momentum = momentum;
    for (Neuron n : neuralNetwork.getHiddenNeurons()) {
      for (Connection c : n.getInputs()) {
        c.momentum = momentum;
      }
    }
    for (Neuron n : neuralNetwork.getOutputNeurons()) {
      for (Connection c : n.getInputs()) {
        c.momentum = momentum;
      }
    }
  }

  /**
   * Add a complete neural network.
   * @param neuralNetwork The complete neural network.
   */
  public NeuralNetworkLearner(NeuralNetwork neuralNetwork) {
    this.neuralNetwork = neuralNetwork;
  }

  /**
   * The learn method updates the weight of the different connections.
   * @param trainingSamples  An array of the training samples for the neural network.
   * @throws CorruptedInputException
   */
  public void learn(NeuralNetworkTrainingSample[] trainingSamples)
    throws CorruptedInputException {
    for (NeuralNetworkTrainingSample ts : trainingSamples) {
      neuralNetwork.flush();
      double[] inputs = ts.getInputs();
      List<InputNeuron> inputNeurons = neuralNetwork.getInputNeurons();

      if (inputs.length != inputNeurons.size())
        throw new CorruptedInputException("Length of inputs "
          + "is not equal to number of input neurons!");

      for (int i = 0; i < inputs.length; i++)
        inputNeurons.get(i).setValue(inputs[i]);

      double[] expectedOutputs = ts.getExpectedOutputs();
      List<Neuron> outputNeurons = neuralNetwork.getOutputNeurons();

      if (outputNeurons.size() != expectedOutputs.length)
        throw new CorruptedInputException("Length of expected outputs "
          + "is not equal to the number of output neurons!");

      double[] actualOutputs = new double[expectedOutputs.length];

      HashSet<Neuron> hiddenNeurons = new HashSet<Neuron>();

      for (int i = 0; i < actualOutputs.length; i++) {
        Neuron outputNeuron = outputNeurons.get(i);
        TransferFunction transferFunction = outputNeuron.getTransferFunction();
        actualOutputs[i] = outputNeuron.calculate();

        double o = actualOutputs[i];

        outputNeuron.setError(transferFunction.getDerivative(o)
          * (expectedOutputs[i] - o));
        List<Connection> connections = outputNeuron.getInputs();

        for (Connection c : connections) {
          c.updateWeight(outputNeuron.getError());
//          c.weight = c.weight + learningRate * outputNeuron.getError()
//            * c.from.calculate();

          if (!(c.from instanceof InputNeuron))
            hiddenNeurons.add(c.from);
        }
      }

      while (!hiddenNeurons.isEmpty()) {
        HashSet<Neuron> tempSet = new HashSet<Neuron>();
        for (Neuron n : hiddenNeurons) {
          double errorSum = 0.0;
          List<Connection> outputConnections = n.getOutputs();

          for (Connection c : outputConnections)
            errorSum += c.weight * c.to.getError();

          double o = n.calculate();

          TransferFunction transferFunction = n.getTransferFunction();
          n.setError(transferFunction.getDerivative(o) * errorSum);

          List<Connection> inputConnections = n.getInputs();

          for (Connection c : inputConnections) {
            c.updateWeight(n.getError());
//            c.weight = c.weight + learningRate * n.getError()
//              * c.from.calculate();

            if (!(c.from instanceof InputNeuron))
              tempSet.add(c.from);
          }
        }
        hiddenNeurons.clear();
        hiddenNeurons.addAll(tempSet);
      }
      for (Neuron n : neuralNetwork.getHiddenNeurons()) {
        for (Connection c : n.getInputs()) {
          c.weight += c.weightChange;
          c.weightChange = 0.0;
        }
      }
      for (Neuron n : neuralNetwork.getOutputNeurons()) {
        for (Connection c : n.getInputs()) {
          c.weight += c.weightChange;
          c.weightChange = 0.0;
        }
      }
    }
  }
}
