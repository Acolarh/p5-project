package dk.aau.cs.d501e13.anpi.learning;

import java.util.ArrayList;
import java.util.List;

public class CascadeClassifier<T1> implements BClassifier<T1> {
  private static final long serialVersionUID = -5351783186903071873L;

  /**
   * List of classifiers
   */
  private List<BClassifier<T1>> classifiers;
  
  /**
   * Construct with an empty list of classifiers
   */
  public CascadeClassifier(){
    classifiers = new ArrayList<>();
  }
  
  /**
   * Constructor.
   * @param classifiers List of classifiers
   */
  public CascadeClassifier(List<BClassifier<T1>> classifiers) {
    this.classifiers = classifiers;
  }
  
  /*
   * Gets the classifier contained at a given index
   */
  public BClassifier<T1> get(int index){
    return classifiers.get(index);
  }
  
  /*
   * Get the size of the cascade
   */
  public int size(){
    return classifiers.size();
  }
  
  public void add(BClassifier<T1> classifier){
    classifiers.add(classifier);
  }

  @Override
  public boolean predict(T1 t) {
    for(BClassifier<T1> classifier : classifiers)
      if(!classifier.predict(t))
        return false;
    return true;
  }

}
