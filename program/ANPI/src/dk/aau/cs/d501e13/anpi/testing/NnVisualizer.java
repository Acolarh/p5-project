package dk.aau.cs.d501e13.anpi.testing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import dk.aau.cs.d501e13.anpi.neuralnetwork.Connection;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;

public class NnVisualizer extends JPanel {

  private NeuralNetwork nn;
  
  private boolean weights;
  
  public NnVisualizer(NeuralNetwork nn, boolean weights) {
    this.nn = nn;
    this.weights = weights;
  }
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.setColor(Color.black);
    g.fillRect(0, 0, getWidth(), getHeight());
    
    int y = 0;
    int x = 0;
    
    if (weights) {
      List<Connection> connections = new ArrayList<Connection>();
      double max = 0.0;
      double min = 0.0;
      for (Neuron n : nn.getHiddenNeurons()) {
        for (Connection c : n.getInputs()) {
          double val = c.weight * c.from.calculate();
          if (val < min)
            min = val;
          if (val > max)
            max = val;
          connections.add(c);
        }
      }
      for (Neuron n : nn.getOutputNeurons()) {
        for (Connection c : n.getInputs()) {
          double val = c.weight * c.from.calculate();
          if (val < min)
            min = val;
          if (val > max)
            max = val;
          connections.add(c);
        }
      }
      int numConnections = connections.size();
      int side = (int)Math.ceil(Math.sqrt(numConnections));
      int width = getWidth() / side;
      int height = getHeight() / side;
      int i = 0;
      for (Connection c : connections) {
        double val = c.from.calculate() * c.weight;
        Color color;
        if (val < 0) {
          int intensity = (int)(val / min * 255);
          color = new Color(intensity, 0, 0);
        }
        else {
          int intensity = (int)(val / max * 255);
          color = new Color(0, intensity, 0);
        }
        g.setColor(color);
        g.fillRect(x, y, width, height);
        i++;
        if (i >= side) {
          i = 0;
          x = 0;
          y += height;
        }
        else {
          x += width;
        }
      }
    }
    else {
      int numInputs = nn.getInputNeurons().size();
      
      int width = getWidth() / numInputs;
      int height = getHeight() / 3;
      for (Neuron n : nn.getInputNeurons()) {
        double val = n.calculate();
        int c = (int)(val * 255);
        Color color;
        if (c < 0) {
          color = new Color(c, 0, 0);
        }
        else {
          color = new Color(0, c, 0);
        }
        g.setColor(color);
        g.fillRect(x, y, width, height);
        x += width;
      }
      y += height;
      x = 0;
      
      int numHidden = nn.getHiddenNeurons().size();
      width = getWidth() / numHidden;
      for (Neuron n : nn.getHiddenNeurons()) {
        double val = n.calculate();
        int c = (int)(val * 255);
        Color color;
        if (c < 0) {
          color = new Color(-c, 0, 0);
        }
        else {
          color = new Color(0, c, 0);
        }
        g.setColor(color);
        g.fillRect(x, y, width, height);
        x += width;
      }
      y += height;
      x = 0;
      
      int numOutputs = nn.getOutputNeurons().size();
      width = getWidth() / numOutputs;
      for (Neuron n : nn.getOutputNeurons()) {
        double val = n.calculate();
        int c = (int)(val * 255);
        Color color;
        if (c < 0) {
          color = new Color(c, 0, 0);
        }
        else {
          color = new Color(0, c, 0);
        }
        g.setColor(color);
        g.fillRect(x, y, width, height);
        x += width;
      }
      y += height;
      x = 0;
    }
  }
  
  public static NnVisualizer open(NeuralNetwork nn, boolean weights) {
    NnVisualizer nv = new NnVisualizer(nn, weights);
    JFrame f = new JFrame("Neural Network");
    f.add(nv);
    f.setSize(800, 600);
    f.setVisible(true);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    return nv;
  }
}
