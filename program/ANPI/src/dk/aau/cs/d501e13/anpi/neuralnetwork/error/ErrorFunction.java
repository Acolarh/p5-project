package dk.aau.cs.d501e13.anpi.neuralnetwork.error;

/**
 * Error function.
 *
 */
public interface ErrorFunction {
  /**
   * Reset the error.
   */
  public void reset();
  
  /**
   * Add an error.
   * @param error The error.
   * @return The error.
   */
  public double addError(double error);
  
  /**
   * @return The error.
   */
  public double getError();
}
