package dk.aau.cs.d501e13.anpi.geometry;

import java.io.Serializable;

/**
 * A point in two-dimensional Euclidean space
 */
public class Point implements Serializable {
  private static final long serialVersionUID = 7564759105131307959L;

  /**
   * X coordinate
   */
  public int x;
  
  /**
   * Y coordinate
   */
  public int y;
  
  /**
   * Constructor
   * @param x X coordinate
   * @param y Y coordinate
   */
  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  /**
   * Cloning constructor
   * @param p Other point
   */
  public Point(Point p) {
    this.x = p.x;
    this.y = p.y;
  }
}
