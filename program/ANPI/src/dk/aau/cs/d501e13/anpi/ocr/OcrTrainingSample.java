package dk.aau.cs.d501e13.anpi.ocr;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkTrainingSample;

/**
 *  The training sample.
 */
public class OcrTrainingSample implements NeuralNetworkTrainingSample {
  
  /**
   * The inputs.
   */
  private double[] inputs;
  
  /**
   * The expected outputs.
   */
  private double[] expectedOutputs;
  
  /**
   * Initializes the inputs and expected outputs.
   * @param image The image with a character to be used.
   * @param character The actual character that is expected.
   */
  public OcrTrainingSample(OCR ocr, Image image, char character) {
    inputs = ocr.getInputVector(image);
    expectedOutputs = ocr.getExpectedOutputs(character);
  }
  
  @Override
  public double[] getInputs() {
    return inputs;
  }

  @Override
  public double[] getExpectedOutputs() {
    return expectedOutputs;
  }

}
