package dk.aau.cs.d501e13.anpi.data.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.Model;

/**
 * Model for table images
 */
public class ImageModel extends Model {
  /**
   * Constructor
   * @param db Database object
   * @throws SQLException on SQL error
   */
  public ImageModel(Database db) throws SQLException {
    super(db, "images", new String[]{"name", "dataset"});
    prepare("find", "SELECT * FROM " + name() + " WHERE id = ?");
    prepare("findByName", "SELECT * FROM " + name() + " WHERE name = ?");
    prepare("findByDataset", "SELECT * FROM " + name() + " WHERE dataset = ?");
    prepare("save", "UPDATE " + name() + " SET name = ?, dataset = ? WHERE id = ?");
    prepare("delete", "DELETE FROM " + name() + " WHERE id = ?");
  }
  
  /**
   * Create a record
   * @param name Image name
   * @param set Data set
   * @return New record
   * @throws SQLException on SQL error
   */
  public ImageRecord create(String name, int set) throws SQLException {
    int id = executeCreate(name, set);
    if (id > 0) {
      return new ImageRecord(this, id, name, set);
    }
    return null;
  }
  
  /**
   * Find record by id
   * @param id Id
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public ImageRecord find(int id) throws SQLException {
    return (ImageRecord)querySingle("find", id);
  }

  /**
   * Find record by name
   * @param name Image name
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public ImageRecord findByName(String name) throws SQLException {
    return (ImageRecord)querySingle("findByName", name);
  }
  
  /**
   * Find records by data set
   * @param dataset Data set
   * @return Records
   * @throws SQLException on SQL error
   */
  public ImageRecord[] findByDataset(int dataset) throws SQLException {
    return (ImageRecord[])queryMultiple("findByDataset", dataset);
  }

  @Override
  public ImageRecord makeRecord(ResultSet r) throws SQLException {
    ImageRecord record = null;
    if (r.next()) {
      record = new ImageRecord(this, r);
    }
    r.close();
    return record;
  }

  @Override
  public ImageRecord[] makeRecordList(ResultSet r) throws SQLException {
    List<ImageRecord> records = new ArrayList<ImageRecord>();
    while (r.next()) {
      records.add(new ImageRecord(this, r));
    }
    r.close();
    return records.toArray(new ImageRecord[0]);
  }
  
  @Override
  public ImageRecord[] all() throws SQLException {
    return (ImageRecord[])super.all();
  }

}
