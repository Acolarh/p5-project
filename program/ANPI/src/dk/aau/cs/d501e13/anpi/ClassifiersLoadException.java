package dk.aau.cs.d501e13.anpi;

public class ClassifiersLoadException extends Exception {

  public ClassifiersLoadException() {
    // TODO Auto-generated constructor stub
  }

  public ClassifiersLoadException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public ClassifiersLoadException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public ClassifiersLoadException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public ClassifiersLoadException(String message, Throwable cause,
    boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

}
