package dk.aau.cs.d501e13.anpi.detector;

import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

/**
 * Weighted rectangle.
 */
public class WeightedRectangle extends Rectangle {
  private static final long serialVersionUID = 3745184855371897914L;
  
  /**
   * Weight.
   */
  public double weight;
  
  /**
   * Constructor.
   * @param x X coordinate
   * @param y Y coordinate
   * @param width Width
   * @param height Height
   * @param weight Weight
   */
  public WeightedRectangle(int x, int y, int width, int height, double weight) {
    super(x, y, width, height);
    this.weight = weight;
  }

  /**
   * Constructor.
   * @param p Point
   * @param width Width
   * @param height Height
   * @param weight Weight
   */
  public WeightedRectangle(Point p, int width, int height, double weight) {
    super(p, width, height);
    this.weight = weight;
  }

  /**
   * Constructor.
   * @param r Rectangle
   * @param weight Weight
   */
  public WeightedRectangle(Rectangle r, double weight) {
    super(r);
    this.weight = weight;
  }
  
  /**
   * Constructor.
   * @param r Weighted rectangle
   */
  public WeightedRectangle(WeightedRectangle r) {
    super(r);
    this.weight = r.weight;
  }

}
