package dk.aau.cs.d501e13.anpi.data.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import dk.aau.cs.d501e13.anpi.data.Record;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

/**
 * Record for table numberplates
 */
public class NumberplateRecord extends Record {
  private static final long serialVersionUID = -1820717806557456810L;

  /**
   * Id
   */
  public int id;
  
  /**
   * Image id
   */
  public int imageId;
  
  /**
   * X coordinate
   */
  public int x;
  
  /**
   * Y coordinate
   */
  public int y;
  
  /**
   * Width in pixels
   */
  public int width;
  
  /**
   * Height in pixels
   */
  public int height;
  
  /**
   * Flags (see {@link NumberplateRecord})
   */
  public NumberPlateFlags flags;
  
  /**
   * Value of number plate
   */
  public String value;
  
  /**
   * Image model
   */
  private transient ImageModel imageModel;
  
  /**
   * Construct from data
   * @param model Model
   * @param id Id
   * @param imageId Image id
   * @param x X coordinate
   * @param y Y coordinate
   * @param width Width
   * @param height Height 
   * @param flags Flags
   * @param value Value
   */
  public NumberplateRecord(NumberplateModel model, int id, int imageId, int x,
      int y, int width, int height, NumberPlateFlags flags, String value) {
    super(model);
    this.id = id;
    this.imageId = imageId;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.flags = flags;
    this.value = value;
    // Dependencies
    imageModel = (ImageModel)model.getDb().getModel("ImageModel");
  }

  /**
   * Construct from data
   * @param model Model
   * @param id Id
   * @param image Image
   * @param x X coordinate
   * @param y Y coordinate
   * @param width Width
   * @param height Height 
   * @param flags Flags
   * @param value Value
   */
  public NumberplateRecord(NumberplateModel model, int id, ImageRecord image, int x,
      int y, int width, int height, NumberPlateFlags flags, String value) {
    this(model, id, image.id, x, y, width, height, flags, value);
  }
  
  /**
   * Construct from result set
   * @param model Model
   * @param result Result set
   * @throws SQLException on SQL error
   */
  public NumberplateRecord(NumberplateModel model, ResultSet result) throws SQLException {
    this(model,
        result.getInt("id"),
        result.getInt("image_id"),
        result.getInt("x"),
        result.getInt("y"),
        result.getInt("width"),
        result.getInt("height"),
        new NumberPlateFlags(result.getInt("flags")),
        result.getString("value")
    );
  }

  /**
   * Get associated image
   * @return ImageRecord
   * @throws SQLException on SQL error
   */
  public ImageRecord getImage() throws SQLException {
    return imageModel.find(imageId);
  }
  
  /**
   * @return Rectangle
   */
  public Rectangle getRectangle() {
    return new Rectangle(x, y, width, height);
  }
  
  @Override
  public void save() throws SQLException {
    model.execute("save", imageId, x, y, width, height, flags.getFlags(), value, id);
  }
  
  @Override
  public void delete() throws SQLException {
    model.execute("delete", id);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof NumberplateRecord)) {
      return false;
    }
    NumberplateRecord r = (NumberplateRecord)obj;
    return r.id == id;
  }
  
}
