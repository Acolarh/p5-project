package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.Learner;

/**
 * Calculates the threshold for the global feature Gradient Density.
 */
public class GradientDensityLearner implements Learner<Image> {
  /**
   * The highest mean value.
   */
  private double bestMean = 0.0;
  
  @Override
  public void configure(Config conf) {  }

  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates)
      throws InvalidTrainingSamplesException {
    GradientDensityFeature feature = new GradientDensityFeature();
    
    if(trainingSamples.length != predicates.length)
      throw new InvalidTrainingSamplesException("Length of arrays are unequal.");
    
    for(int i = 0; i < trainingSamples.length; i++){
      if(predicates[i] != true){
        System.out.println("Negative: " + feature.getGradientDensity(trainingSamples[i]));
        continue;
      }

      System.out.println("Positive: " + feature.getGradientDensity(trainingSamples[i]));
      double imgPixelMean = feature.getGradientDensity(trainingSamples[i]);
      
      if(bestMean < imgPixelMean)
        bestMean = imgPixelMean;
      
    }
    System.out.println("Selected: " + bestMean);
    feature.setThreshold(bestMean);
    
    return feature;
  }

}
