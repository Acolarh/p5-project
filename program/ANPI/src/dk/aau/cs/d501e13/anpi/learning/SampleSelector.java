package dk.aau.cs.d501e13.anpi.learning;

import java.io.IOException;
import java.sql.SQLException;

import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;

/**
 * The sample selector.
 */
public class SampleSelector {
  
  /**
   * Clears all sets.
   * @param model Image model.
   * @throws SQLException
   */
  static public void clearAllSets(ImageModel model) throws SQLException{
    ImageRecord[] all = model.all();
    for(ImageRecord image : all){
      image.set = ImageRecord.SET_UNSET;
      try {
        image.save();
      }
      catch (SQLException e) {
        e.printStackTrace();
        System.out.println("Warning, could not save NumberplateRecord!");
        continue;
      }
    }
  }
  
  /**
   * Adds images which do not belong to a set, to a set.
   * @param model
   * @param trainingSize Value from 0.0 to 1.0 which describes the probability of a image being added to the training set
   * @throws SQLException 
   */
  static public void initalizeUnset(ImageModel model, double trainingSize) throws SQLException{
    ImageRecord[] all = model.all();
    for(ImageRecord image : all){
      if(image.set == ImageRecord.SET_UNSET){
        if(Math.random() < trainingSize){
          image.set = ImageRecord.SET_TRAINING;
        }
        else{
          image.set = ImageRecord.SET_TEST;
        }
        
        try {
          image.save();
        }
        catch (SQLException e) {
          e.printStackTrace();
          System.out.println("Warning, could not save NumberplateRecord!");
          continue;
        }
      }
    }
  }
  
  public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException{
    Database db = new Database("db.properties");
    ImageModel imgModel = new ImageModel(db);
    initalizeUnset(imgModel, 2.0/3.0);
  }
}
