package dk.aau.cs.d501e13.anpi;

public class ImageCloneException extends Exception {
  private static final long serialVersionUID = -71936027191345377L;

  public ImageCloneException() {
    // TODO Auto-generated constructor stub
  }

  public ImageCloneException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public ImageCloneException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public ImageCloneException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public ImageCloneException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

}
