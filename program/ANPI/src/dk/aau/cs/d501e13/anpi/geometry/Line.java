package dk.aau.cs.d501e13.anpi.geometry;

import java.util.ArrayList;

/**
 * Line of a polygon
 */
public class Line{
  /**
   * Coordinates
   */
  public int x; 
  public int y;
  public int x2;
  public int y2; 
  /**
   * Constructor.
   * @param x1
   * @param y1
   * @param x2
   * @param y2
   */
  public Line(int x1, int y1, int x2, int y2) {
      this.x = x1;
      this.y = y1;
      this.x2 = x2;
      this.y2 = y2;
  }
  
  /*
   *
   */
  /**
   * Extends the length of the line with a factor while keeping its middle in the same place.
   * Examples:
   * (1, 1, 2, 2) * 2 = (0.5, 0.5, 3, 3)
   * (-1, -1, 2, 2) * 2 = (-2, -2, 4, 4)
   * @param factor The factor
   */
  public void extend(double factor){
    double difX = x2 - x;
    double difY = y2 - y;
    x -= difX * 0.5 * factor;
    x2 += difX * 0.5 * factor;    
    y -= difY * 0.5 * factor;
    y2 += difY * 0.5 * factor;
  }
  
  /**
   * Returns a list of all the points where any line from lines intersects
   * @param lines List of lines.
   * @return A list of intersections as points.
   */
  public static ArrayList<Point> getIntersections(ArrayList<Line> lines){
    ArrayList<Point> intersections = new ArrayList<Point>();
    for (int i = 0; i < lines.size(); i++){
      for (int p = i+1; p < lines.size(); p++){
        Point intersection = getIntersection(lines.get(i), lines.get(p));
        if (intersection != null){
          intersections.add(intersection);
        }
      }
    }
    return intersections;
  }
  
  /**
   * Finds the point where two lines intersect
   * @author IMalc {@link http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect}
   * @param a Line A
   * @param b Line B
   * @return The point where they intersect, otherwise null.
   */
  public static Point getIntersection(Line a, Line b) {
    int p0x = a.x;
    int p0y = a.y;
    int p1x = a.x2;
    int p1y = a.y2;
    int p2x = b.x;
    int p2y = b.y;
    int p3x = b.x2;
    int p3y = b.y2;

    int s10_x = p1x - p0x;
    int s10_y = p1y - p0y;
    int s32_x = p3x - p2x;
    int s32_y = p3y - p2y;

    int denom = s10_x * s32_y - s32_x * s10_y;

    if (denom == 0)
      return null;

    boolean denom_is_positive = denom > 0;

    int s02_x = p0x - p2x;
    int s02_y = p0y - p2y;

    int s_numer = s10_x * s02_y - s10_y * s02_x;

    if (s_numer < 0 == denom_is_positive)
      return null;

    int t_numer = s32_x * s02_y - s32_y * s02_x;

    if (t_numer < 0 == denom_is_positive)
      return null;

    if ((s_numer > denom == denom_is_positive)
      || (t_numer > denom == denom_is_positive))
      return null;

    // collision detected

    double t = t_numer / (double) denom;

    return new Point((int) (p0x + t * s10_x), (int) (p0y + t * s10_y));
  }
}