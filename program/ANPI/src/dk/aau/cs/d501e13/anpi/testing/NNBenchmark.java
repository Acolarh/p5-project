package dk.aau.cs.d501e13.anpi.testing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.DotConstructor;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkTrainingSample;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class NNBenchmark {
  
  public static class Column {
    public enum Type {
      DOUBLE,
      INTEGER,
      ENUM
    };
    
    public Type type = Type.INTEGER;
    
    public String[] values;
    
    private Column() {
    }
    
    public static Column analyze(List<String[]> data, int column) {
      Column c = new Column();
      Set<String> values = new HashSet<String>();
      for (String[] row : data) {
        if (row.length <= column) {
          return null;
        }
        String value = row[column];
        values.add(value);
        if (c.type == Type.INTEGER) {
          try {
            int i = Integer.parseInt(value);
          }
          catch (NumberFormatException e) {
            c.type = Type.DOUBLE;
          }
        }
        if (c.type == Type.DOUBLE) {
          try {
            double d = Double.parseDouble(value);
          }
          catch (NumberFormatException e) {
            c.type = Type.ENUM;
          }
        }
      }
      if (c.type == Type.ENUM) {
        int i = 0;
        c.values = new String[values.size()];
        for (String value : values) {
          c.values[i++] = value;
        }
      }
      return c;
    }
  }
  
  public static class BenchmarkTrainingSample implements NeuralNetworkTrainingSample {

    private double[] inputs;
    private double[] outputs;
    
    public BenchmarkTrainingSample(Column[] columns, String[] row, int targetColumn) {
      inputs = new double[columns.length - 1];
      Arrays.fill(inputs, 0.0);
      outputs = new double[columns[targetColumn].values.length];
      Arrays.fill(outputs, 0.0);
      int inputI = 0;
      for (int i = 0; i < row.length; i++) {
        if (i == targetColumn) {
          for (int j = 0; j < columns[i].values.length; j++) {
            if (columns[i].values[j].equals(row[i])) {
              outputs[j] = 1.0;
              break;
            }
          }
          continue;
        }
        switch (columns[i].type) {
          case DOUBLE:
            inputs[inputI++] = Double.parseDouble(row[i]);
            break;
          case ENUM:
            break;
          case INTEGER:
            inputs[inputI++] = (double)Integer.parseInt(row[i]);
            break;
        }
      }
    }
    
    @Override
    public double[] getInputs() {
      return inputs;
    }

    @Override
    public double[] getExpectedOutputs() {
      return outputs;
    }
    
  }

  public static void main(String[] args) throws IOException, CorruptedInputException {
    if (args.length < 1) {
      System.out.println("usage: nnbenchmark DATA_FILE");
      System.exit(1);
    }
    
    List<String[]> rows = new ArrayList<String[]>();
    FileReader fr = new FileReader(args[0]);
    BufferedReader br = new BufferedReader(fr);
    String line;
    int numColumns = -1;
    while ((line = br.readLine()) != null) {
      String[] row = line.split(",");
      if (numColumns < 0) {
        numColumns = row.length;
      }
      rows.add(row);
    }
    br.close();
    fr.close();
    Collections.shuffle(rows);
    
    System.out.println(rows.size() + " rows loaded");
    
    int targetColumn = 4;
    double trainingPercentage = 0.66;
    
    Column[] columns = new Column[numColumns];
    for (int i = 0; i < numColumns; i++) {
      columns[i] = Column.analyze(rows, i);
    }
    
    int numInputs = numColumns - 1;
    
    int numTraining = (int)(rows.size() * trainingPercentage);
    int numValidation = rows.size() - numTraining;
    
    int numOutputs = columns[targetColumn].values.length;
    
    BenchmarkTrainingSample[] samples = new BenchmarkTrainingSample[numTraining];
    BenchmarkTrainingSample[] validation = new BenchmarkTrainingSample[numValidation];
    
    for (int i = 0; i < numTraining; i++) {
      samples[i] = new BenchmarkTrainingSample(columns, rows.get(i), targetColumn);
    }
    
    for (int i = 0; i < numValidation; i++) {
      validation[i] = new BenchmarkTrainingSample(columns, rows.get(numTraining + i), targetColumn);
    }
    
    NeuralNetwork nn = new NeuralNetwork();
    
    TransferFunction transferFunction = new SigmoidFunction();
    
    int numHidden = (numInputs + numOutputs) / 2;
    
    for (int i = 0; i < numInputs; i++) {
      nn.addInput(new InputNeuron());
    }
    
    for (int i = 0; i < numOutputs; i++) {
      nn.addOutput(new Neuron(transferFunction));
    }
    
    for (int i = 0; i < numHidden; i++) {
      Neuron n = new Neuron(transferFunction);
      nn.addHidden(n);
      for (Neuron in : nn.getInputNeurons()) {
        n.addInput(in);
      }
      for (Neuron out : nn.getOutputNeurons()) {
        out.addInput(n);
      }
    }
    
    double error = SSE(nn, samples);
    NeuralNetworkLearner learner = new NeuralNetworkLearner(nn);
    
//    learner.setLearningRate(0.25);
//    learner.setMomentum(0.2);
    
    int i = 0;
    int maxEpochs = 5000;
    
    while (error > 0.1) {
      if (maxEpochs > 0 && i > maxEpochs) {
        break;
      }
      System.out.println("iteration " + (i++) + " e: " + error);
      learner.learn(samples);
      error = SSE(nn, samples);
    }
    System.out.println("iteration " + (i++) + " e: " + error);
    
    System.out.println("Validating...");

    int errors = 0;
    for (BenchmarkTrainingSample sample : validation) {
      nn.setInputVector(sample.getInputs());
      double[] expected = sample.getExpectedOutputs();
      int expectedMax = 0;
      for (i = 0; i < expected.length; i++) {
        if (expected[i] > 0.5) {
          expectedMax = i;
          break;
        }
      }
      double[] actual = nn.calculate();
      double max = 0.0;
      int actualMax = 0;
      for (i = 0; i < actual.length; i++) {
        if (actual[i] > max) {
          actualMax = i;
        }
      }
      if (expectedMax != actualMax) {
        errors++;
      }
      else {
        
      }
    }
    System.out.println("Errors: " + errors + " / " + validation.length);
    
    System.out.println("Exporting network...");
    DotConstructor dotter = new DotConstructor(nn);
    dotter.setShowWeights(true);
    FileOutputStream fos = new FileOutputStream(args[0] + ".dot");
    dotter.construct(fos);
    fos.close();
    
    System.exit(0);
  }
  

  public static double SSE(NeuralNetwork nn, NeuralNetworkTrainingSample[] samples) throws CorruptedInputException {
    double errorSum = 0.0;
    for (NeuralNetworkTrainingSample ts : samples) {
      nn.setInputVector(ts.getInputs());
      double[] outputs = nn.calculate();
      double[] expected = ts.getExpectedOutputs();
      for (int i = 0; i < outputs.length; i++) {
        double error = outputs[i] - expected[i];
        errorSum += error * error;
      }
    }
    return errorSum;
  }

}
