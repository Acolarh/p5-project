package dk.aau.cs.d501e13.anpi.deskewing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.geometry.PointD;

/**
 * An image deskewer
 */
public class Deskew {
  
  /**
   * The Points returned from the corner detector
   */
  public Point cornerTopLeft;
  public Point cornerBottomLeft;
  public Point cornerTopRight;
  public Point cornerBottomRight;
  
  /**
   * The input image which should be deskewed
   */
  private Image image;
  
  /**
   * A comparator for sorting the list of corners, in order
   * to access which corner is the top left corner (the one with 
   * smallest x and y values), the bottom left
   * corner, the top right corner and the bottom right corner
   */
  Comparator<Point> xComparer = new Comparator<Point>(){
    @Override
    public int compare(Point p1, Point p2) {
      return p1.x - p2.x; 
    }
  };
  
  /**
   * Constructor. 
   * @param image The image.
   * @param corners List of points.
   * @throws InvalidParameterException
   */
  public Deskew(Image image, List<Point> corners) throws InvalidParameterException {
    this.image = image;
    determineCorners(corners);
  }
  
  /**
   * Determines the four corners: cornerTopLeft, cornerTopRight, cornerBottomLeft and cornerBottomRight
   * Example: The cornerTopLeft and cornerBottomLeft are the two corners with lowest x value, and the cornerTopLeft
   * is the corner with lowest y value of the two.
   * @param corners List of points.
   * @throws InvalidParameterException
   */
  public void determineCorners(List<Point> corners) throws InvalidParameterException {
    if (corners.size() != 4) {
      throw new InvalidParameterException("invalid number of corners");
    }
    Collections.sort(corners, xComparer);
    if (corners.get(0).y == corners.get(1).y) {
      throw new InvalidParameterException("The two corners in left side of the number plate have same y coordinate");
    }
    else if (corners.get(0).y <= corners.get(1).y) {
      cornerTopLeft = corners.get(0);
      cornerBottomLeft = corners.get(1);
    }
    else {
      cornerTopLeft = corners.get(1);
      cornerBottomLeft = corners.get(0);
    }
    
    if (corners.get(2).y == corners.get(3).y) {
      throw new InvalidParameterException("The two corners in right side of the number plate have same y coordinate");
    }
    else if (corners.get(2).y <= corners.get(3).y) {
      cornerTopRight = corners.get(2);
      cornerBottomRight = corners.get(3);
    }
    else {
      cornerTopRight = corners.get(3);
      cornerBottomRight = corners.get(2);
    }
  }
  
  /**
   * Deskews the image
   * @return A deskewed image
   */
  public Image deskew() {
    double h = (Math.abs(cornerTopLeft.y - cornerBottomLeft.y) 
      + Math.abs(cornerTopRight.y - cornerBottomRight.y)) / 2;
    double w = (Math.abs(cornerTopLeft.x - cornerTopRight.x) 
      + Math.abs(cornerBottomLeft.x - cornerBottomRight.x)) / 2;
    Image newImage = new Image((int)w, (int)h);
      
    PointD FT = new PointD((cornerTopRight.x - cornerTopLeft.x)/(w-1), (cornerTopRight.y - cornerTopLeft.y)/(w-1));
    PointD FB = new PointD((cornerBottomRight.x - cornerBottomLeft.x)/(w-1), (cornerBottomRight.y - cornerBottomLeft.y)/(w-1));
    for(int x = 0; x < w; x++) {
      PointD P1 = new PointD(FT.x * x + cornerTopLeft.x, FT.y * x + cornerTopLeft.y);
      PointD P2 = new PointD(FB.x * x + cornerBottomLeft.x, FB.y * x + cornerBottomLeft.y);
      PointD FC = new PointD((P2.x - P1.x) / h, (P2.y - P1.y) / h);
      for(int y = 0; y < h; y++) {
        Point pixel = new PointD(FC.x * y + P1.x, FC.y * y + P1.y).convertToPoint();
        if(pixel.x < image.getWidth() && pixel.y < image.getHeight() && pixel.x >= 0 && pixel.y >= 0){
          newImage.setBlue(x, y, image.getBlue(pixel.x, pixel.y));
          newImage.setRed(x, y, image.getRed(pixel.x, pixel.y));
          newImage.setGreen(x, y, image.getGreen(pixel.x, pixel.y));
        }
        else{
          newImage.setBlue(x, y, 255);
          newImage.setGreen(x, y, 255);
          newImage.setRed(x, y, 255);
        }
      }
    }
    return newImage;
  }
  
  public static void main(String[] args) throws ImageReadException, IOException, InvalidParameterException {
    Image img = new Image("C:\\Users\\Kent\\Desktop\\skewedplate1.jpg");
    List<Point> corners = new ArrayList<Point>();
    corners.add(new Point(42,26));
    corners.add(new Point(42,128));
    corners.add(new Point(452,145));
    corners.add(new Point(452,263));
    Deskew deskew = new Deskew(img, corners);
    deskew.deskew().save("C:\\Users\\Kent\\Desktop\\numberplate_deskewtest.png", "png");
  } 
  
}
