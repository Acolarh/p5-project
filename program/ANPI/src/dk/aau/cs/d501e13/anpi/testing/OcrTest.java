package dk.aau.cs.d501e13.anpi.testing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.ailal.Ailal;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberPlateFlags;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.edgedetectors.PrewittEdgeDetector;
import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.ocr.OCR;
import dk.aau.cs.d501e13.anpi.ocr.WeightedChar;
import dk.aau.cs.d501e13.anpi.ocr.WeightedString;

public class OcrTest {
  
  private static long start;
  
  public static class VideoPanel extends JPanel {

    private BufferedImage image = null;


    public VideoPanel() {
    }


    public VideoPanel(BufferedImage image) {
      this.image = image;
    }

    public void setImage(BufferedImage image) {
      this.image = image;
      repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }
  }

  public static void reset(String action) {
    System.out.print(action + "...");
    reset();
  }

  public static void reset() {
    start = System.currentTimeMillis();
  }
  
  public static long read() {
    return System.currentTimeMillis() - start;
  }
  
  public static void pread() {
    System.out.println(read() + "ms");
  }

  static int imageId = 0;
  static boolean up = true;
  
  public static void main(String[] args) throws IOException, ImageReadException, ClassNotFoundException, SQLException, ImageCloneException, InterruptedException, ClassifiersLoadException, CorruptedInputException {

    if (args.length < 1) {
      System.out.println("usage: tester CONFIG_FILE");
      System.exit(1);
    }
    JFrame f = new JFrame("Image test");
    
    Config conf = new Config(args[0]);
    
//    OCR ocr = new OCR(conf);
//    System.out.println("Loaded... Press enter to save!");
//    System.in.read();
//    System.out.println("Saving..." + ocr.save(Utilities.ocrFileName(conf)));
//    System.exit(1);
    
    Anpi anpi = new Anpi(conf);
    
    Ailal<Image> ailal = new Ailal<Image>(conf);
    
    BClassifier<Image> classifier = ailal.getClassifier();
    
    System.out.print("Connection to database...");
    Database db = new Database(conf.getConfig("database"));
    System.out.println("OK");
    
    ImageModel images = new ImageModel(db);
    NumberplateModel numberplates = new NumberplateModel(db);
    
    boolean isWhitelist = false;
    int numberplateFlags = NumberPlateFlags.BLURRY //| NumberPlateFlags.SKEWED
      | NumberPlateFlags.PARTIAL;
//    int numberplateFlags = 0;
    
    if (isWhitelist) {
      numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
        + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = " + numberplateFlags);
    }
    else {
      numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
        + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = 0");
    }
    

    VideoPanel p = new VideoPanel();
    f.add(p);
    f.setSize(800, 600);
    f.setVisible(true);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    imageId = 0;
    
    f.addKeyListener(new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
      }
      
      @Override
      public void keyReleased(KeyEvent e) {
      }
      
      @Override
      public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == 34) {
          up = true;
          imageId++;
        }
        else if (key == 33) {
          up = false;
          imageId--;
        }
      }
    });
    
    while (true) {
    
      ImageRecord record = images.find(imageId);
      if (record == null) {
        if (up)
          imageId++;
        else
          imageId--;
        continue;
      }
      NumberplateRecord[] plates = (NumberplateRecord[])numberplates.queryMultiple("select-flags", record.id);
      String path = conf.getString("imagesDir") + "/" + record.name;
      
      if (plates.length < 1) {
        System.out.println("no plates");
        if (up)
          imageId++;
        else
          imageId--;
        continue;
      }
      
      NumberplateRecord plate = plates[0];
      
      Image full = new Image(path);
      
      Image numberplate = full.getSubimage(new Rectangle(
        plate.x,
        plate.y,
        plate.width,
        plate.height
      ));
      
      Image original = numberplate.getResized(240, 50, false);
      numberplate = original;
      
      reset("Classifying");
      boolean classification = false; //classifier.predict(
      //  original.getEdges(new PrewittEdgeDetector())
//          .getResized(42, 11, false)
     // );
      pread();
      
      reset("Identifying");
      String value = "";
      try {
        value = anpi.identifyCharacters(numberplate);
      }
      catch (InvalidParameterException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
        if (up)
          imageId++;
        else
          imageId--;
        continue;
      }
      pread();
      
      reset("Detecting corners");
      List<Point> corners = anpi.detectCorners(numberplate);
      pread();

      reset("Deskewing");
      Image skewed = null;
      try {
        skewed = anpi.skewPlate(numberplate, corners);
      }
      catch (InvalidParameterException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
        if (up)
          imageId++;
        else
          imageId--;
        continue;
      }
      pread();
//      skewed = numberplate;
      
      reset("Segmenting");
      List<Image> characters = anpi.segmentPlate(skewed);
      pread();
      
      reset("Resizing");
      List<Image> resized = anpi.resizeCharacters(characters, 20, 24);
      pread();
      
      reset("Reading");
      value = "";
      for (Image c : resized) {
        value += anpi.identifyCharacter(c);
      }
      pread();
      
      long[] histogram = numberplate.getVHistogram();
      long max = 0;
      for (long l : histogram) {
        if (l > max) {
          max = l;
        }
      }
      
      PrintStream w = new PrintStream("histogram.csv");
      
      for (int i = 0; i < histogram.length; i++) {
        w.println(i + "," + histogram[i]);
      }
      w.close();
      
      Image report = new Image(800, 600);
      
      Graphics2D g = report.createGraphics();
      
      g.setColor(new Color(30, 30, 60));
      g.fillRect(0, 0, 800, 600);
      g.setColor(Color.white);
      
      g.drawString("Input: (id: " + imageId + " classification: " + classification + " name: " + record.name + ") [" + original.getWidth() + "x" + original.getHeight() + "]", 20, 20);
      
      g.drawImage(original.getBufferedImage(), 20, 25, null);
      g.drawImage(numberplate.getBufferedImage(), 20, 75, null);
      
      for (Point corner : corners) {
        g.setColor(Color.yellow);
        g.fillRect(20 + corner.x - 3, 75 + corner.y - 3, 6, 6);
        g.setColor(Color.black);
        g.drawRect(20 + corner.x - 3, 75 + corner.y - 3, 6, 6);
      }
      g.setColor(Color.white);
  
      g.drawString("Histogram:", 20, 150);
      
      for (int i = 0; i < histogram.length; i++) {
        g.drawLine(20 + i, 235, 20 + i, 235 - (int)(((double)histogram[i] / max) * 80));
      }
      
      g.drawString("Skewed:", 20, 255);
      
      g.drawImage(skewed.getBufferedImage(), 20, 275, null);
      
      g.drawString("Segmentation: [" + characters.size() + "]", 20, 400);
      
      int x = 20;
      for (Image character : characters) {
        g.drawImage(character.getBufferedImage(), x, 405, null);
        x += character.getWidth() + 20;
      }

      g.drawString("Resized: ", 20, 500);
      
      x = 20;
      for (Image character : resized) {
        g.drawImage(character.getBufferedImage(), x, 505, null);
        x += character.getWidth() + 20;
      }
  
      reset("Creating strings");
      g.drawString("Values: ", 380, 300);
      List<WeightedString> values = anpi.identifyCharacters(resized);
      Collections.sort(values);
      int y = 320;
      for (WeightedString ws : values) {
        g.drawString(ws.s + " : " + String.format("%.2f", ws.weight), 380, y);
        y += 20;
      }
      pread();
      
      g.drawString("OCR: ", 380, 100);
      
      OCR ocr = anpi.getOcr();
      NeuralNetwork nn = ocr.getNeuralNetwork();
      
      double threshold = ocr.getThreshold();
      
      g.setFont(g.getFont().deriveFont(10f));
      x = 380;
      for (Image character : resized) {
        g.drawImage(character.getBufferedImage(), x, 105, null);
        nn.setInputVector(ocr.getInputVector(character));
        double[] outputs = nn.calculate();
        ArrayList<WeightedChar> charWeights = new ArrayList<WeightedChar>();
        for (int i = 0; i < outputs.length; i++) {
          charWeights.add(new WeightedChar(ocr.offsetToChar(i), outputs[i]));
        }
        Collections.sort(charWeights);
        double previous = -1.0;
        for (int i = 0; i < 5; i++) {
          WeightedChar cw = charWeights.get(i);
          if (cw.weight <= threshold) {
            g.setColor(Color.red);
          }
          else if (previous < 0.0) {
            previous = cw.weight;
          }
          else {
            double diff = previous - cw.weight;
            if (diff < threshold) {
              g.setColor(Color.white);
              previous = cw.weight;
            }
            else {
              g.setColor(Color.red);
            }
          }
          g.drawString(cw.c + ": " + String.format("%.2f", cw.weight), x, 155 + i * 20);
        }
        g.setColor(Color.white);
        x += character.getWidth() + 30;
      }
      
      g.dispose();
      p.setImage(report.getBufferedImage());

      
      Thread.sleep(100);
    }
    
  }
  
  private static class CharWeight implements Comparable<CharWeight> {
    public char c;
    public double weight;
    public CharWeight(char c, double weight) {
      this.c = c;
      this.weight = weight;
    }
    @Override
    public int compareTo(CharWeight o) {
      if (o.weight < weight)
        return -1;
      else if (o.weight > weight)
        return 1;
      else
        return 0;
    }
  }
}
