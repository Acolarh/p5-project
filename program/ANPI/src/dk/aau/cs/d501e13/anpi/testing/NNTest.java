package dk.aau.cs.d501e13.anpi.testing;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import dk.aau.cs.d501e13.anpi.neuralnetwork.Connection;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.DotConstructor;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkTrainingSample;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class NNTest {
  
  static final double TRUE = 1.0;
  static final double FALSE = 0.0;
  static final double THRESHOLD = (TRUE - FALSE) / 2;
  
  public static double toDouble(boolean b) {
    return b ? TRUE : FALSE;
  }
  
  public static boolean toBoolean(double d) {
    return d > THRESHOLD;
  }

  static class BinarySample implements NeuralNetworkTrainingSample {

    double[] inputs = new double[2];
    double[] outputs = new double[1];
    
    public BinarySample(boolean inputA, boolean inputB, boolean output) {
      inputs[0] = toDouble(inputA);
      inputs[1] = toDouble(inputB);
      outputs[0] = toDouble(output);
    }
    
    @Override
    public double[] getInputs() {
      return inputs;
    }

    @Override
    public double[] getExpectedOutputs() {
      return outputs;
    }
  }
  
  public static void compute(NeuralNetwork nn, String operator, boolean inputA, boolean inputB) throws CorruptedInputException {
    nn.setInputVector(inputA ? TRUE : FALSE, inputB ? TRUE : FALSE);
    double dResult = nn.calculate()[0];
    boolean bResult = toBoolean(dResult);
    System.out.println(inputA + " " + operator + " " + inputB + " = " + dResult + " ~ " + bResult);
  }
  
  static Random r = new Random();
  
  public static double weight() {
    double min = 0.1;
    double max = 0.9;
//    return min + Math.random() * (max - min);
    return r.nextDouble();
//    return 0.5;
  }
  
  /*
   * Function to approximate:
   */
  public static boolean f(boolean a, boolean b) {
    return a ^ b;
  }
  
  public static String fSym = "XOR";
  
  /* NEURAL NETWORK USED:
   * 
   * inputs:  inputA     inputB
   *            |  \_____/  |         1.0
   *            |  /     \  |          |
   * hidden:  hiddenA    hiddenB    hiddenC
   *                \_____ | _______/
   *                      \|/
   * output:             output
   */
  
  public static void main(String[] args) throws CorruptedInputException, IOException {
    TransferFunction transferFunction = new SigmoidFunction();
    NeuralNetwork bitGate = new NeuralNetwork();
    InputNeuron inputA = new InputNeuron();
    InputNeuron inputB = new InputNeuron();
    bitGate.addInput(inputA);
    bitGate.addInput(inputB);
    Neuron hiddenA = new Neuron(transferFunction);
    hiddenA.addInput(inputA);
    hiddenA.addInput(inputB);
    bitGate.addHidden(hiddenA);
    Neuron hiddenB = new Neuron(transferFunction);
    hiddenB.addInput(inputA);
    hiddenB.addInput(inputB);
    bitGate.addHidden(hiddenB);
//    InputNeuron hiddenC = new InputNeuron();
//    hiddenC.setValue(1.0);
//    bitGate.addHidden(hiddenC);
    Neuron output = new Neuron(transferFunction);
    output.addInput(hiddenA);
    output.addInput(hiddenB);
//    hiddenA.setThreshold(Math.random() - 0.5);
//    hiddenB.setThreshold(Math.random() - 0.5);
//    output.setThreshold(Math.random() - 0.5);
//    output.addInput(hiddenC);
    bitGate.addOutput(output);
    BinarySample[] samples = new BinarySample[]{
      new BinarySample(true, true, f(true, true)),
      new BinarySample(true, false, f(true, false)),
      new BinarySample(false, true, f(false, true)),
      new BinarySample(false, false, f(false, false)),
    };
    DotConstructor dotter = new DotConstructor(bitGate);
    dotter.setShowWeights(true);
    FileOutputStream fos = new FileOutputStream("nntest_init.dot");
    dotter.construct(fos);
    fos.close();
    NeuralNetworkLearner learner = new NeuralNetworkLearner(bitGate);
    learner.setLearningRate(0.01);
    learner.setMomentum(0.99);
    int i = 0;
    double error = 0.0;
    double maxError = 0.5;
    do {
      System.out.println("Iteration " + ++i + ":");
      learner.learn(samples);
      error = SSE(bitGate, samples);
      System.out.println("error: " + error);
      System.out.print("weights H:");
      for (Neuron n : bitGate.getHiddenNeurons()) {
        for (Connection c : n.getInputs()) {
          System.out.print(" " + c.weight);
        }
      }
      System.out.println();
      System.out.print("weights O:");
      for (Neuron n : bitGate.getOutputNeurons()) {
        for (Connection c : n.getInputs()) {
          System.out.print(" " + c.weight);
        }
      }
      System.out.println();
    } while (error > maxError);

    fos = new FileOutputStream("nntest_done.dot");
    dotter.construct(fos);
    fos.close();

    compute(bitGate, fSym, true, true);
    compute(bitGate, fSym, true, false);
    compute(bitGate, fSym, false, true);
    compute(bitGate, fSym, false, false);
  }
  
  public static double SSE(NeuralNetwork nn, NeuralNetworkTrainingSample[] samples) throws CorruptedInputException {
    double errorSum = 0.0;
    for (NeuralNetworkTrainingSample ts : samples) {
      nn.setInputVector(ts.getInputs());
      double[] outputs = nn.calculate();
      double[] expected = ts.getExpectedOutputs();
      for (int i = 0; i < outputs.length; i++) {
        double error = outputs[i] - expected[i];
        errorSum += error * error;
      }
    }
    return errorSum;
  }
}
