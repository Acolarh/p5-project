package dk.aau.cs.d501e13.anpi.learning.weak;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeature;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeatureGenerator;

/**
 * Thresholded Haar feature learner.
 */
public class ThresholdedHaarFeatureLearner implements WeakLearner<Image> {
  /**
   * List of features
   */
  private HaarFeature[] features;

  /*
   * Array of training samples
   */
  Image[] trainingSamples;
  
  /*
   * Array of predicates for training samples
   */
  boolean[] predicates;
  
  double ratio;
  
  /**
   * Id of selected feature
   */
  private int selected = -1;
  private double threshold = 0.0;
  private boolean greater = true;
  private ThresholdedClassifier<Image> classifier = null;
  
  /**
   * Error of selected feature
   */
  private double error = 0.0;
  
  /**
   * Constructor
   */
  public ThresholdedHaarFeatureLearner() {
  }

  /**
   * Contains a value and the error of the value.
   * @param <Type> A value type.
   * @param <ErrorType> A error type.
   */
  class ArgMin<Type,ErrorType>{
    public Type val;
    public ErrorType error;
  };
  
  /**
   * Train part of samples.
   * @param start Starting point.
   * @param end Finishing point.
   * @param distribution The weights from AdaBoost.
   * @return Callable object.
   */
  private Callable<ArgMin<ThresholdedClassifier<Image>,Double>> trainPart(final int start, final int end, final double[] distribution){
    return new Callable<ArgMin<ThresholdedClassifier<Image>,Double>>(){
      @Override
      public ArgMin<ThresholdedClassifier<Image>,Double> call() throws Exception {
        double error = Double.MAX_VALUE;
        int selected = -1;
        double threshold = 0;
        boolean sign = true;
        for(int i=start; i<end; i++){
          ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(features[i], trainingSamples, predicates, distribution);
          //double currentError = trainer.train(1.0 * ratio);
          double currentError = trainer.train();
          if(currentError < error){
            selected = i;
            error = currentError;
            threshold = trainer.getThreshold();
            sign = trainer.getSign();
          }
        }
        ArgMin<ThresholdedClassifier<Image>,Double> result = new ArgMin<>();
        result.error = error;
        result.val = new ThresholdedClassifier<Image>(features[selected], threshold, sign);
        return result;
      }
    };
  }

  @Override
  public ThresholdedClassifier<Image> learn(double[] distribution) {
    selected = -1;
    error = Double.MAX_VALUE;
    long time = System.currentTimeMillis();
    
    //Train HaarFeatures
    int cores = Runtime.getRuntime().availableProcessors();
    final ExecutorService executor = Executors.newFixedThreadPool(cores);
    List<Callable<ArgMin<ThresholdedClassifier<Image>,Double>>> tasks = new ArrayList<>();
    int split = features.length / cores;
    for(int i=0; i<cores; i++){
      int end = i!=cores-1 ? (i+1)*split : features.length;
      tasks.add(trainPart(i*split,end, distribution));
    }
    ArgMin<ThresholdedClassifier<Image>,Double> best = new ArgMin<>();
    best.error = Double.MAX_VALUE;
    best.val = null;
    try {
      List<Future<ArgMin<ThresholdedClassifier<Image>,Double>>> results = executor.invokeAll(tasks);
      for(Future<ArgMin<ThresholdedClassifier<Image>,Double>> result : results){
        ArgMin<ThresholdedClassifier<Image>,Double> current = result.get();
        if(current.error < best.error){
          best = current;
        }
      }
    }
    catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    catch (ExecutionException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    classifier = best.val;
    error = best.error;
    
    /*/
    for(int i=0; i<features.length; i++){
      if(i%10000 == 0){
        System.out.println("[" + i + "/" + features.length + "] HaarFeatureTrainer");
      }
      FastHaarFeatureTrainer trainer = new FastHaarFeatureTrainer(features[i], trainingSamples, predicates, distribution);
      double currentError = trainer.train();
      if(currentError <= error){
        selected = i;
        error = currentError;
      }
    }
    */
    
    System.out.println("Training time: " +  (System.currentTimeMillis() - time));
    return classifier;
  }

  @Override
  public double getError() {
    return error;
  }

  @Override
  public int getIndicator(int trainingSample) {
    return classifier.predict(trainingSamples[trainingSample]) == predicates[trainingSample] ? -1 : 1;
  }

  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates) {
    double[] distribution = new double[trainingSamples.length];
    for(int i=0; i<distribution.length; i++){
      distribution[i] = 1.0;
    }
    return learn(distribution);
  }
  
  @Override
  public void configure(Config conf) {
    System.out.println("Generating haar features...");
    features = HaarFeatureGenerator.generateHaarFeatures(
      conf.getByte("width"),
      conf.getByte("height"),
      conf.getInt("types")
    ).toArray(new HaarFeature[0]);
    System.out.println(features.length + " features generated.");
  }

  @Override
  public void init(Image[] trainingSamples, boolean[] predicates) {
      this.trainingSamples = trainingSamples;
      this.predicates = predicates;
      
      int positives = 0;
      for(int i=0; i<predicates.length; i++)
        if(predicates[i])
          positives++;
      
      ratio = (double)(predicates.length-positives) / positives;
      
      //Make sure integral images have been made
      for(Image i : this.trainingSamples)
        i.getSum(0, 0);
  }
  
}
