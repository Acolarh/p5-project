package dk.aau.cs.d501e13.anpi.learning.boosting;

import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.Booster;
import dk.aau.cs.d501e13.anpi.learning.Evaluate;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.QuickSave;
import dk.aau.cs.d501e13.anpi.learning.StrongClassifier;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.weak.ThresholdTrainer;


/**
 *A Booster using the adaboost algorithm to boost a set of binary classifiers on a set of training samples
 * @param <T> The type of training samples the booster operates on
 */
public class AdaBooster<T> implements Booster<T>, QuickSave<T>, Evaluate<T> {
  
  /**
   * Number of iterations
   */
  private int iterations;
  
  /**
   * Quick saver
   */
  private QuickSaver<T> quickSaver = null;
  
  private Evaluator<T> evaluator = null;
  
  /**
   * Constructor
   * @param iterations Number of iterations
   */
  public AdaBooster(int iterations) {
    this.iterations = iterations;
  }
  
  @Override
  public void setQuickSaver(QuickSaver<T> quickSaver) {
    this.quickSaver = quickSaver;
  }

  @Override
  public void setEvaluator(Evaluate.Evaluator<T> evaluator) {
    this.evaluator = evaluator; 
  }
  
  @Override
  public ThresholdedClassifier<T> boost(T[] trainingSamples, boolean[] predicates,
      WeakLearner<T> weakLearner) throws InvalidTrainingSamplesException {
    if (trainingSamples.length != predicates.length) {
      throw new InvalidTrainingSamplesException("Incompatible lengths of training samples and predicates");
    }
    
    StrongClassifier<T> result = new StrongClassifier<T>();
    int numTrainingSamples = trainingSamples.length;
    double[] d = new double[numTrainingSamples];
    ThresholdedClassifier<T> thresholded = null;
    
    //Initialize
    System.out.println("Initial d: " + (1.0 / numTrainingSamples));
    for (int i = 0; i < numTrainingSamples; i++){
      d[i] = 1.0 / numTrainingSamples;
    }
    weakLearner.init(trainingSamples, predicates);
    
    //Iterate
    for (int t = 0; t < iterations; t++) {
      System.out.println("Boosting... " + (t+1) + " of " + iterations);
      System.out.println("Calculating epsilon...");
      BClassifier<T> weakClassifier = weakLearner.learn(d);
      double error = weakLearner.getError();
      double alpha = 0.5 * Math.log((1 - error) / error);
      System.out.println("Looping samples...");
      for (int i = 0; i < numTrainingSamples; i++) {
        //indicator is 1 or -1
        d[i] = (d[i] * Math.exp(alpha * weakLearner.getIndicator(i)));
      }
      normalize(d);
      result.add(weakClassifier, alpha);
      
      //Find optimal threshold for StrongClassifier
      double[] finalD = new double[d.length];
      for (int i=0; i<finalD.length; i++)
        finalD[i] = 1.0 / finalD.length;
      ThresholdTrainer<T> trainer = new ThresholdTrainer<>(result, trainingSamples, predicates, finalD);
      trainer.train();
      thresholded = trainer.getThresholded();
      
      if (evaluator != null) {
        if (evaluator.evaluate(thresholded, trainingSamples, predicates))
          break;
      }
      
      if (quickSaver != null)
        quickSaver.saveClassifier(thresholded);
    }
    return thresholded;
  }  
  
  
  /**
   * Normalizes an array so the sum of all elements equals 1 
   * @param array to normalize
   */
  public static void normalize(double[] array){
    double sum = 0;
    for (int i = 0; i < array.length; i++){
      sum += array[i];
    }
    for (int i = 0; i < array.length; i++){
      array[i] /= sum;
    }
  }
}

