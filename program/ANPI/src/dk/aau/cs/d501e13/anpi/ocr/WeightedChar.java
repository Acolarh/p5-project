package dk.aau.cs.d501e13.anpi.ocr;

/**
 * Weighted character.
 */
public class WeightedChar implements Comparable<WeightedChar> {
  
  /**
   * Character.
   */
  public char c;
  
  /**
   * Weight.
   */
  public double weight;
  
  /**
   * Constructor.
   * @param c The character.
   * @param weight The weight.
   */
  public WeightedChar(char c, double weight) {
    this.c = c;
    this.weight = weight;
  }

  @Override
  public int compareTo(WeightedChar o) {
    if (o.weight < weight)
      return -1;
    else if (o.weight > weight)
      return 1;
    else
      return 0;
  }
}
