package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *  Implementation for a neural network.
 */
public class NeuralNetwork implements Serializable {
  private static final long serialVersionUID = -3907785002718772819L;
  /**
   * List of input neurons.
   */
  private List<InputNeuron> inputNeurons = new ArrayList<InputNeuron>();
  /**
   * List of hidden neurons.
   */
  private List<Neuron> hiddenNeurons = new ArrayList<Neuron>();
  /**
   * List of output neurons.
   */
  private List<Neuron> outputNeurons = new ArrayList<Neuron>();
  /**
   * @param neuron Adds neuron to list of inputNeurons.
   */
  public void addInput(InputNeuron neuron){
    inputNeurons.add(neuron);
  }
  /**
   * @param neuron Adds neuron to list of hiddenNeurons.
   */
  public void addHidden(Neuron neuron){
    hiddenNeurons.add(neuron);
  }
  /**
   * @param neuron Adds neuron to list of outputNeurons.
   */
  public void addOutput(Neuron neuron){
    outputNeurons.add(neuron);
  }
  /**
   * Set input vector, must have same size as input layer
   * @param v Input vector
   * @throws CorruptedInputException 
   */
  public void setInputVector(double ... v) throws CorruptedInputException {
    if (v.length != inputNeurons.size()) {
      throw new CorruptedInputException("Length of inputs " +
        "is not equal to number of input neurons!");
    }
    for (int i = 0; i < v.length; i++) {
      inputNeurons.get(i).setValue(v[i]);
    }
  }
  /**
   * Calculate and return output vector
   * @return Output vector
   */
  public double[] calculate() {
    flush();
    double[] v = new double[outputNeurons.size()];
    for (int i = 0; i < v.length; i++) {
      v[i] = outputNeurons.get(i).calculate();
    }
    return v;
  }
  /**
   * @return List of input neurons.
   */
  public List<InputNeuron> getInputNeurons() {
    return inputNeurons;
  }
  /**
   * @return List of hidden neurons.
   */
  public List<Neuron> getHiddenNeurons() {
    return hiddenNeurons;
  }
  /**
   * @return List of output neurons.
   */
  public List<Neuron> getOutputNeurons() {
    return outputNeurons;
  }
  /**
   * Resets the output and hidden neurons to null.
   */
  public void flush(){
    for(Neuron n : outputNeurons)
      n.flush();
    
    for(Neuron n : hiddenNeurons)
      n.flush();
  }
  
}
