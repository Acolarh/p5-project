package dk.aau.cs.d501e13.anpi.imagetransformation;

import java.util.ArrayList;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.geometry.Line;
import dk.aau.cs.d501e13.anpi.geometry.Point;

public class HoughTransform {
  int thetaSteps;
  int radiusSteps;
  double thetaStepSize;
  double maxRadius;
  double radiusStepSize;
  
  int[][] houghData;
  boolean binarizedImage[][];
  int binarizedWidth;
  int binarizedHeight;
  boolean unbrokenLine;
  int lineNeighborhood;
  
  /*
   * Make a hough transformation on a binarized image)
   */
  public HoughTransform(Image binarizedImg){
    this(binarizedImg, 180, 200, true, 0);
  }
  
  /*
   * Make a hough transformation on a binarized image)
   * thetaSteps is the amount of angles to try
   * radiusSteps is the amount of distances to try from the center of the image
   * unbrokenLine determines whether to match longest unbroken line or longest line which may have gaps
   */
  public HoughTransform(Image binarizedImg, int thetaSteps, int radiusSteps, boolean unbrokenLine, int lineNeighborhood){
    
    binarizedWidth = binarizedImg.getWidth();
    binarizedHeight = binarizedImg.getHeight();
    
    this.thetaSteps = thetaSteps;
    this.radiusSteps = radiusSteps;
    this.unbrokenLine = unbrokenLine;
    this.lineNeighborhood = lineNeighborhood;
    thetaStepSize = Math.PI * 2 / thetaSteps;
    maxRadius = (int)Math.sqrt(Math.pow(binarizedWidth / 2, 2) + Math.pow(binarizedHeight / 2, 2));
    radiusStepSize = maxRadius / radiusSteps;
    makeBinarizedImage(binarizedImg);
    houghData = new int[thetaSteps][radiusSteps];
    calcHoughSpace();
  }
  
  private void makeBinarizedImage(Image img){
    binarizedImage = new boolean[binarizedWidth][binarizedHeight];
    for (int x = 0; x < binarizedWidth; x++){
      for (int y = 0; y < binarizedHeight; y++){
        binarizedImage[x][y] = img.getGray(x, y) == 255;
      }
    }
  }
  
  /*
   * Calculates the hough space from the image given when instantiating this class
   */
  void calcHoughSpace(){
    for (int t = 0; t < thetaSteps; t++){
      for (int r = 0; r < radiusSteps; r++){
        double theta = t * thetaStepSize;
        double radius = r * radiusStepSize;
        Line xyLine = getLineFromHoughSpace(binarizedWidth, binarizedHeight, theta, radius);
        
        //A line is not guaranteed to intersect with the rectangle representing the frame
        if (xyLine != null){
          int lineIntensity = getLineIntensity(xyLine);
          houghData[t][r] = lineIntensity;
        }
      }
    }
    normalize(houghData);
  }
  
  /*
   * Returns a visual representation of the Hough Space as an image
   */
  public Image getHoughSpace(){  
    return getImage(houghData);
  }
   
  /*
   * Converts a point in Hough-space to line in XY-space defined by its intersection
   * with a frame having the dimensions (width, height)
   */
  private Line getLineFromHoughSpace(int width, int height, double theta, double radius){
       
    double centerX = width / 2;
    double centerY = height / 2;
    
    double startX = centerX + Math.cos(theta) * radius;
    double startY = centerY - Math.sin(theta) * radius;
    
    double pointTheta = theta - Math.PI / 2;
    
    double interSectRightY = startY - Math.tan(pointTheta) * (width - startX);
    double interSectLeftY = startY + Math.tan(pointTheta) * (startX);
    double interSectTopX = startX - Math.tan(theta) * (startY);
    double interSectBottomX = startX + Math.tan(theta) * (height - startY);
    
    boolean interSectRight = interSectRightY >= 0 && interSectRightY < height;
    boolean interSectLeft = interSectLeftY >= 0 && interSectLeftY < height;
    boolean interSectTop = interSectTopX >= 0 && interSectTopX < width;
    boolean interSectBottom = interSectBottomX >= 0 && interSectBottomX < width;
    
    if (interSectRight){
      if (interSectLeft) return new Line(0, (int)interSectLeftY, width-1, (int)interSectRightY);
      if (interSectTop) return new Line((int)interSectTopX, 0, width-1, (int)interSectRightY);
      if (interSectBottom) return new Line((int)interSectBottomX, height-1, width-1, (int)interSectRightY);
    }
    else if (interSectLeft){
      if (interSectTop) return new Line((int)interSectTopX, 0, 0, (int)interSectLeftY);
      if (interSectBottom) return new Line((int)interSectBottomX, height-1, 0, (int)interSectLeftY);
    }
    else if (interSectTop){
      if (interSectBottom) return new Line((int)interSectBottomX, height-1, (int)interSectTopX, 0);
    }
    return null;
  }
  
  /*
   * Gets the intensity of the line from (x, y) to (x2, y2) in the image
   */
  private int getLineIntensity(Line line){
    int x = line.x;
    int y = line.y;
    int x2 = line.x2;
    int y2 = line.y2;

    int score = 0;
    int maxScore = 0;

    double difX = x2-x;
    double difY = y2-y;
    double length = Math.sqrt(difX*difX+difY*difY);
    int lastX = -1;
    int lastY = -1;
    for (double pos = 0; pos < length; pos++){
      int posX = (int)Math.floor(x + difX * (pos / length));
      int posY = (int)Math.floor(y + difY * (pos / length));
      if (posX != lastX || posY != lastY){
        if (pointHasContrast(posX, posY)){
          score++;
          if (score > maxScore)
            maxScore = score;
        }
        else if (unbrokenLine)
          score = 0;
        lastX = posX;
        lastY = posY;
      }
    }
    
    return maxScore;
  }
  
  private boolean pointHasContrast(int x, int y){
    int maxX = binarizedWidth - 1;
    int maxY = binarizedHeight - 1;
    int xLeft = x - lineNeighborhood / 2;
    int xRight = x + (lineNeighborhood + 1) / 2;
    int yLeft = y - lineNeighborhood / 2;
    int yRight = y + (lineNeighborhood + 1) / 2;
    for (int i = xLeft; i <= xRight; i++){
      if (i < 0 || i > maxX)
        continue;
      for (int p = yLeft; p <= yRight; p++){
        if (p < 0 || p > maxY)
          continue;
        if (binarizedImage[i][p])
          return true;
      }
    }
    return false;
  }
  
  /*
   * Converts  2-dimensional array of integers to an image
   */
  private Image getImage(int[][] intArray){
    int width = intArray.length;
    int height = intArray[0].length;  
    Image result = new Image(width, height);
    for (int x = 0; x < width; x++)
      for (int y = 0; y < height; y++){
        result.setRed(x,y, intArray[x][y]);
        result.setGreen(x,y, intArray[x][y]);
        result.setBlue(x,y, intArray[x][y]);
      }
      return result;
  }
  
  /*
   * Normalizes the values in a 2-dimensional array of integers to be in the range 0-255
   */
  private void normalize(int[][] intArray){
    int width = intArray.length;
    int height = intArray[0].length;  
    double max = Integer.MIN_VALUE;
    for (int x = 0; x < width; x++)
      for (int y = 0; y < height; y++)
        if (intArray[x][y] > max)
          max = intArray[x][y];
    for (int x = 0; x < width; x++)
      for (int y = 0; y < height; y++)
          intArray[x][y] = (int)(intArray[x][y] / max * 255.0);
  }
 
  /*
   * Finds the relative difference betweem two values in radians.
   * E.g. (2PI, 0) = 0, (0, 3/2PI) = PI, (PI, 0) = PI
   */
  private double radianDifference(double d1, double d2){
    double dif = Math.abs(d1 - d2);
    return Math.min(dif, Math.PI * 2 - dif);
  }
  
  /*
   * Returns the at most numLines strongest lines, where the angle of each
   * line must differ with at least minDegreeDifference from all other
   * lines returned
   */
  public ArrayList<Line> getStrongestLines(int numLines, double minDegreeDifference) {
    
    double minRadianDifference = minDegreeDifference / 180 * Math.PI; 
    ArrayList<Point> bestPoints = new ArrayList<Point>();
    
    for (int i = 0; i < numLines; i++){
      int max = Integer.MIN_VALUE;
      Point bestPoint = new Point(0, 0);
      for (int x = 0; x < houghData.length; x++){
        for (int y = 0; y < houghData[0].length; y++){
          if (houghData[x][y] > max){
            //Make sure that line differs with at least minDegreeDifference degrees from
            //all other lines found
            boolean angleIsDifferentEnough = true;
            for (Point p : bestPoints){
              if (radianDifference(p.x * thetaStepSize, x * thetaStepSize) < minRadianDifference){
                angleIsDifferentEnough = false;
                break;
              }
            }
            if (angleIsDifferentEnough){
              max = houghData[x][y];
              bestPoint.x = x;
              bestPoint.y = y;
            }
          }
        }
      }
      bestPoints.add(bestPoint);
    }
    
    ArrayList<Line> bestLines = new ArrayList<Line>();
    for (Point p : bestPoints)
      bestLines.add(getLineFromHoughSpace(binarizedWidth, binarizedHeight, p.x * thetaStepSize, p.y * radiusStepSize));
    
    return bestLines;
  }
}
