package dk.aau.cs.d501e13.anpi.data.models;

/**
 * Different flags for number plates.
 */
public class NumberPlateFlags {
  /**
   * Flag
   */
  protected int flag = 0;

  /**
   * Initialize to a state where no flags are set
   */
  public NumberPlateFlags() {
    flag = 0;
  }

  /**
   * Initialize to a specific set of flags
   * 
   * @param flags
   *          The flags it should be initialized to
   */
  public NumberPlateFlags(int flags) {
    this.flag = flags;
  }

  /**
   * @return Flags.
   */
  public int getFlags() {
    return flag;
  }

  /**
   * Set flags.
   * @param raw A bit mask
   */
  public void setFlags(int raw) {
    flag = raw;
  }

  /**
   * Clear flags.
   */
  public void clear() {
    flag = 0;
  }

  public static final int WHITE = 0x00000001;
  public static final int YELLOW = 0x00000002;
  public static final int SQUARE = 0x00000004;
  public static final int COUNTRY = 0x00000008;

  public static final int BLURRY = 0x00000100;
  public static final int PARTIAL = 0x00000200;
  public static final int SKEWED = 0x00000400;
  public static final int OCR_DONE = 0x00000800;

  /**
   * Set bit mask
   * @param mask Bit mask as one of the constants above
   * @param value Boolean value 
   */
  protected void setBit(int mask, boolean value) {
    if (value) {
      flag |= mask;
    }
    else {
      flag &= ~mask;
    }
  }

  /**
   * Check if a flag is set.
   * @param mask Bit mask as one of the constants above
   * @return If the bit is set
   */
  protected boolean isBitSet(int mask) {
    return (flag & mask) != 0;
  }

  /**
   * @return True if flag is white, otherwise false
   */
  public boolean isWhite() {
    return isBitSet(WHITE);
  }

  /**
   * @return True if flag is yellow, otherwise false
   */
  public boolean isYellow() {
    return isBitSet(YELLOW);
  }

  /**
   * @return True if flag is square, otherwise false
   */
  public boolean isSquare() {
    return isBitSet(SQUARE);
  }

  /**
   * @return True if flag is country, otherwise false
   */
  public boolean isCountry() {
    return isBitSet(COUNTRY);
  }

  /**
   * @return True if flag is blurry, otherwise false
   */
  public boolean isBlurry() {
    return isBitSet(BLURRY);
  }

  /**
   * @return True if flag is set to isPartial, otherwise false
   */
  public boolean isPartial() {
    return isBitSet(PARTIAL);
  }

  /**
   * @return True if flag is set to isSkewed, otherwise false
   */
  public boolean isSkewed() {
    return isBitSet(SKEWED);
  }

  /**
   * @return True if flag is set to OcrDone, otherwise false
   */
  public boolean isOcrDone() {
    return isBitSet(OCR_DONE);
  }

  /**
   * Set flag to white.
   * @param value Boolean value.
   */
  public void setWhite(boolean value) {
    setBit(WHITE, value);
  }

  /**
   * Set flag to yyellow.
   * @param value Boolean value.
   */
  public void setYellow(boolean value) {
    setBit(YELLOW, value);
  }

  /**
   * Set flag to square.
   * @param value Boolean value.
   */
  public void setSquare(boolean value) {
    setBit(SQUARE, value);
  }

  /**
   * Set flag to country.
   * @param value Boolean value.
   */
  public void setCountry(boolean value) {
    setBit(COUNTRY, value);
  }

  /**
   * Set flag to blurry.
   * @param value Boolean value.
   */
  public void setBlurry(boolean value) {
    setBit(BLURRY, value);
  }

  /**
   * Set flag to partial.
   * @param value Boolean value.
   */
  public void setPartial(boolean value) {
    setBit(PARTIAL, value);
  }

  /**
   * Set flag to skewed.
   * @param value Boolean value.
   */
  public void setSkewed(boolean value) {
    setBit(SKEWED, value);
  }

  /**
   * Set flag to ocr done.
   * @param value Boolean value.
   */
  public void setOcrDone(boolean value) {
    setBit(OCR_DONE, value);
  }

  /**
   * @return True if flag is empty, otherwise false.
   */
  public boolean isEmpty() {
    return flag == 0;
  }
}
