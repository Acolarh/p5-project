package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.Learner;

/**
 * Approximates the optimum threshold for the global feature density variance.
 */
public class DensityVarianceLearner implements Learner<Image> {
  
  private double bestMean = 0.0;
  private int xAxis = 6;
  private int yAxis = 2;

  @Override
  public void configure(Config conf) { 
    xAxis = conf.getInt("xAxis", xAxis);
    yAxis = conf.getInt("yAxis", yAxis);
  }

  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates)
    throws InvalidTrainingSamplesException {
    
    DensityVarianceFeature feature = new DensityVarianceFeature(xAxis, yAxis);
    
    double minNeg = Double.MAX_VALUE,
      minPos = Double.MAX_VALUE, 
      maxNeg = Double.MIN_VALUE, 
      maxPos = Double.MIN_VALUE,
      meanNeg = 0.0,
      meanPos = 0.0;
    double sumNeg = 0.0,
      sumPos = 0.0;
    double pred;
    int i;
    
    if(trainingSamples.length != predicates.length)
      throw new InvalidTrainingSamplesException("Length of arrays are unequal.");
  
    
    try{
      for(i = 0; i < trainingSamples.length; i++){
        if(predicates[i] != true){
          
          
          pred = feature.getDensityVariance(trainingSamples[i]);
          if(minNeg > pred)
            minNeg = pred;
          if(maxNeg < pred)
            maxNeg = pred;
          
          sumNeg += pred;
          
          
          
          System.out.println("Negative: " + feature.getDensityVariance(trainingSamples[i]));
          continue;
        }
  
        System.out.println("Positive: " + feature.getDensityVariance(trainingSamples[i]));
        double imgPixelMean = feature.getDensityVariance(trainingSamples[i]);
        
        
        pred = feature.getDensityVariance(trainingSamples[i]);
        if(minPos > pred)
          minPos = pred;
        if(maxPos < pred)
          maxPos = pred;
        
        sumPos += pred;
        
        
        
        if(bestMean < imgPixelMean)
          bestMean = imgPixelMean;
               
      }
      meanNeg = sumNeg / i;
      meanPos = sumPos / i;
      
      System.out.println("Mean negative: " + meanNeg);
      System.out.println("Mean positive: " + meanPos);
      System.out.println("Max neagtive: " + maxNeg);
      System.out.println("Max positive: " + maxPos);
      System.out.println("Min negative: " + minNeg);
      System.out.println("Min positive: " + minPos);
      feature.setThreshold(minPos);
      System.out.println("Selected: " + feature.getThreshold());
    }
    catch (InvalidParameterException e) {
      e.printStackTrace();
      System.exit(10);
      
    }
    return feature;
  }
  
    
}
