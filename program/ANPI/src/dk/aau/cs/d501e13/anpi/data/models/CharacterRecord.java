package dk.aau.cs.d501e13.anpi.data.models;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.imageio.ImageIO;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.data.Record;

/**
 * Record for table characters
 */
public class CharacterRecord extends Record {
  private static final long serialVersionUID = 6775457853174727970L;

  /**
   * Id
   */
  public int id;
  
  /**
   * Image name
   */
  public int value;
  
  /**
   * The image
   */
  public byte[] image;
  
  /**
   * Construct from data
   * @param model Model
   * @param id Id
   * @param value Value
   * @param image Image
   */
  public CharacterRecord(CharacterModel model, int id, int value, byte[] image) {
    super(model);
    this.id = id;
    this.value = value;
    this.image = image;
  }
  
  /**
   * Construct from result set
   * @param model Model
   * @param result Result set
   * @throws SQLException on SQL error
   */
  public CharacterRecord(CharacterModel model, ResultSet result) throws SQLException {
    this(model, result.getInt("id"), result.getInt("value"), result.getBytes("image"));
  }
  
  /**
   * Set an image.
   * @param image The image.
   */
  public void setImage(Image image) {
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ImageIO.write(image.getBufferedImage(), "png", baos);
      this.image = baos.toByteArray();
      baos.close();
    }
    catch (IOException e) {
    }
  }
  
  /**
   * @return The image.
   */
  public Image getImage() {
    ByteArrayInputStream bais = new ByteArrayInputStream(image);
    try {
      Image image = new Image(ImageIO.read(bais));
      bais.close();
      return image;
    }
    catch (IOException e) {
      return null;
    }
  }
  
  @Override
  public void save() throws SQLException {
    model.execute("save", value, image, id);
  }
  
  @Override
  public void delete() throws SQLException {
    model.execute("delete", id);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof CharacterRecord)) {
      return false;
    }
    CharacterRecord r = (CharacterRecord)obj;
    return r.id == id;
  }
}
