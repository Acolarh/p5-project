package dk.aau.cs.d501e13.anpi;

import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

/**
 * A single number plate
 */
public class Numberplate {
  /**
   * Position and size of number plate as a rectangle
   */
  public Rectangle region;
  
  /**
   * Value of number plate as a string, e.g. "AZ 48 493"
   */
  public String value;

  /**
   * Constructor
   * @param region Region
   * @param value Value
   */
  public Numberplate(Rectangle region, String value) {
    this.region = region;
    this.value = value;
  }
}
