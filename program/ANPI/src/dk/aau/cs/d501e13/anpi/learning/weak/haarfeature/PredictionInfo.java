package dk.aau.cs.d501e13.anpi.learning.weak.haarfeature;

public class PredictionInfo {
  public double d;
  public boolean predicate;
  public double value;
  
  public PredictionInfo(double d, boolean predicate, double value){
    this.d = d;
    this.predicate = predicate;
    this.value = value;
  }
}
