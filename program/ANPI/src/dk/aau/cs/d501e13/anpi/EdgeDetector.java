package dk.aau.cs.d501e13.anpi;

import java.awt.image.Raster;
import java.awt.image.WritableRaster;

/**
 * For edge detection
 */
public interface EdgeDetector {
  /**
   * Calculate edge detection
   * @param input Gray-scale input image
   * @param output Empty gray-scale output image (same size as input)
   */
  public void calculate(Raster input, WritableRaster output);
}
