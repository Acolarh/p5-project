package dk.aau.cs.d501e13.anpi.neuralnetwork.transfer;

/**
 * The sigmoid function as a transfer function.
 */
public class SigmoidFunction implements TransferFunction {
  private static final long serialVersionUID = 4187196932284841585L;

  @Override
  public double activate(double inputSum) {
    double res = 1.0 / (1.0 + Math.exp(-(inputSum)));
    return 1.0 / (1.0 + Math.exp(-(inputSum)));
  }
  
  @Override
  public double getDerivative(double output) {
    return output * (1.0 - output);
  }
}
