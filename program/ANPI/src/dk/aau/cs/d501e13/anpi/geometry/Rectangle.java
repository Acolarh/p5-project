package dk.aau.cs.d501e13.anpi.geometry;

import java.io.Serializable;

/**
 * A rectangle in two-dimensional Euclidean space
 */
public class Rectangle extends Point implements Serializable {
  private static final long serialVersionUID = 408308397485133638L;

  /**
   * Width of rectangle in pixels
   */
  public int width;
  
  /**
   * Height of rectangle in pixels
   */
  public int height;
  
  /**
   * Constructor
   * @param x X coordinate of top left corner
   * @param y Y coordinate of top left corner
   * @param width Width in pixels
   * @param height Height in pixels
   */
  public Rectangle(int x, int y, int width, int height) {
    super(x, y);
    this.width = width;
    this.height = height;
  }
  
  /**
   * Constructor
   * @param p Point of top left corner
   * @param width Width in pixels
   * @param height Height in pixels
   */
  public Rectangle(Point p, int width, int height) {
    super(p);
    this.width = width;
    this.height = height;
  }
  
  /**
   * Cloning constructor
   * @param r Other rectangle
   */
  public Rectangle(Rectangle r) {
    this(r, r.width, r.height);
  }
  
  /**
   * Determines whether or not this Rectangle and the specified Rectangle intersect.
   * @param other Specified rectangle
   * @return True if intersects, false otherwise
   */
  public boolean intersects(Rectangle other) {
    if (x < other.x) {
      if (other.x - x >= width) {
        return false;
      }
    }
    else {
      if (x - other.x >= other.width){
        return false;
      }
    }
    if (y < other.y) {
      if (other.y - y >= height) {
        return false;
      }
    }
    else {
      if (y - other.y >= other.height){
        return false;
      }
    }
    return true;
  }
  
  /**
   * Determine whether or not this Rectangle and the specified rectangles intersect
   * @param others Specified rectangles
   * @return True if intersection
   */
  public boolean intersects(Rectangle[] others) {
    for (Rectangle other : others) {
      if (intersects(other)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Gets the intersected area between two rectangles
   * @param other The rectangle which you want to intersect with 
   * @return A rectangle which represents the intersected area
   */
  public Rectangle getIntersection(Rectangle other) {
    Rectangle sectedRect = new Rectangle(0, 0, 0, 0);
    
    if(this.intersects(other) == true) {
      sectedRect.x = Math.max(this.x, other.x);
      sectedRect.y = Math.max(this.y, other.y);
      sectedRect.width = Math.min((this.x + this.width), (other.x + other.width)) - sectedRect.x;
      sectedRect.height = Math.min((this.y + this.height), (other.y + other.height)) - sectedRect.y;
    }
    else {
      return null;
    }
    return sectedRect;
  }
  
  /**
   * Compares two rectangles
   * @param other The rectangle to compare with
   * @return A percentage of how equal the two rectangles are
   */
  public double compareRectangles(Rectangle other) {
    double sizeOfThisRect = this.width * this.height;
    double sizeOfOtherRect = other.width * other.height;
    
    Rectangle sectedRect = this.getIntersection(other);
    if(sectedRect == null) {
      return 0.0; 
    }
    if ((sizeOfThisRect + sizeOfOtherRect) > 0) {
      return Math.abs((2 * (sectedRect.width * sectedRect.height)) / (sizeOfThisRect + sizeOfOtherRect));
    }
    else
      return 0.0;
  }
  
  /**
   * Checks if two rectangles are equal. This check is based on the x, y, width and height of
   * the two rectangles are equal
   * @return true if equal or false if different
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof Rectangle)) {
      return false;
    }
    Rectangle rhs = (Rectangle) obj;
    if (rhs.x == this.x && rhs.y == this.y &&
        rhs.width == this.width && rhs.height == this.height) {
      return true;
    }
    return false; 
  }
  
  @Override
  public String toString() {
    return width + "x" + height + "[" + x + "x" + y + "]";
  }
}
