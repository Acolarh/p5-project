package dk.aau.cs.d501e13.anpi.detector;

import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ailal.Ailal;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;

/**
 * Find objects in images using an array of WeightedClassifiers
 */
public class Detector {
  /**
   * Classifier used for detecting
   */
  BClassifier<Image> classifier;
  
  /**
   * Minimum width of a number plate to detect
   */
  private int minWidth = 178 / 2;
  
  /**
   * Minimum height of a number plate to detect
   */
  private int minHeight = 49 / 2;
  
  /**
   * Maximum width of a number plate to detect
   */
  private int maxWidth = Integer.MAX_VALUE;
  
  /**
   * Maximum height of a number plate to detect
   */
  private int maxHeight = Integer.MAX_VALUE;
  
  /**
   * The minimum allowed value for (width / height) of a frame
   */
  private double aspectRatioMin = (178.0 / 49.0) * 0.7;
  
  /**
   * The maximum allowed value for (width / height) of a frame
   */
  private double aspectRatioMax = (178.0 / 49.0) * 1.3;
  
  /**
   * scaleFactor The factor by which each dimension of a frame is scaled.
   */
  private double scaleFactor = 0.1;
  
  /**
   * moveFactor A frame can in each step be moved an amount of pixels in one dimension as a fraction of its length in that dimension
   */
  private double moveFactor = 0.05;

  /*
   * Optimization to save that value here instead of calling Image.getWidth() many times
   */
  private int imgWidth;
  
  /*
   * Optimization to save that value here instead of calling Image.getHeight() many times
   */
  private int imgHeight;
  
  /**
   * Constructor. Create a Detector with this set of weightedClassifiers
   * @param weightedClassifiers Classifiers to use for detecting
   */
  public Detector(BClassifier<Image> classifier){
    this.classifier = classifier;
  }
  
  /**
   * Constructor
   * @param conf ANPI configuration loader
   * @throws ClassifiersLoadException if unable to load classifiers
   */
  public Detector(Config conf) throws ClassifiersLoadException {
    Ailal<Image> ailal = new Ailal<Image>(conf);
    classifier = ailal.getClassifier();
    load(conf);
  }
  
  /**
   * Load configuration
   * @param conf ANPI configuration loader
   */
  public void load(Config conf) {
    minWidth = conf.getInt("minWidth", 0);
    minHeight = conf.getInt("minHeight", 0);
    maxWidth = conf.getInt("maxWidth", Integer.MAX_VALUE);
    maxHeight = conf.getInt("maxHeight", Integer.MAX_VALUE);
    aspectRatioMin = conf.getDouble("aspectRatioMin", aspectRatioMin);
    aspectRatioMax = conf.getDouble("aspectRatioMax", aspectRatioMax);
    scaleFactor = conf.getDouble("scaleFactor", 0);
    moveFactor = conf.getDouble("moveFactor", 0);
  }
  
  /**
   * Set classifier
   * @param classifier The classifier
   */
  public void setClassifier(BClassifier<Image> classifier){
    this.classifier = classifier;
  }
  
  /**
   * @return The classifier
   */
  public BClassifier<Image> getClassifier() {
    return classifier;
  }
  
  /**
   * moveFactor A frame can in each step be moved an amount of pixels in one dimension as a fraction of its length in that dimension
   * @param val Value
   */
  public void setMoveFactor(double val){
    this.moveFactor = val;
  }
  
  /**
   * scaleFactor The factor by which each dimension of a frame is scaled.
   * @param val Value
   */
  public void setScaleFactor(double val){
    this.scaleFactor = val;
  }
  
  /**
   * Sets the minimum allowed ratio between width and height (width / height) of an ideal number plate
   * @param val The min aspect ratio
   */
  public void setAspectRatioMin(double val){
    this.aspectRatioMin = val;
  }
  
  /**
   * Sets the maximum allowed ratio between width and height (width / height) of an ideal number plate
   * @param val The max aspect ratio
   */
  public void setAspectRatioMax(double val){
    this.aspectRatioMax = val;
  }
  
  /**
   * Sets the minimum width of a number plate to find
   * @param minWidth Minimum width in pixels
   * @throws DetectorException 
   */
  public void setMinWidth(int minWidth) throws DetectorException{
    if (minWidth == 0)
      throw new DetectorException("Minimum width for detection cannot be 0. It must be at least 1");
    this.minWidth = minWidth;
  }
  
  /**
   * Sets the minimum height of a number plate to find
   * @param minHeight Minimum height in pixels
   * @throws DetectorException 
   */
  public void setMinHeight(int minHeight) throws DetectorException{
    if (minHeight == 0)
      throw new DetectorException("Minimum height for detection cannot be 0. It must be at least 1");
    this.minHeight = minHeight;
  }
  
  /**
   * Sets the maximum width of a number plate to find
   * @param maxWidth Maximum width in pixels
   */
  public void setMaxWidth(int maxWidth){
    this.maxWidth = maxWidth;
  }
  
  /**
   * Sets the maximum height of a number plate to find
   * @param maxHeight Maximum height in pixels
   */
  public void setMaxHeight(int maxHeight){
    this.maxHeight = maxHeight;
  }
  
  private int counter;
  /**
   * Detects regions in an image that contains a number plate
   * @param image Image
   * @return A list of rectangles, representing the number plate's position in the image
   */
  public List<Rectangle> detectPlates(Image image) {
    counter = 0;
    imgWidth = image.getWidth();
    imgHeight = image.getHeight();
    List<Rectangle> numberPlates = new ArrayList<Rectangle>();
    int maxWidth = Math.min(image.getWidth(), this.maxWidth);
    int maxHeight = Math.min(image.getHeight(), this.maxHeight);
    int minWidth = Math.max(0, this.minWidth);
    int minHeight = Math.max(0, this.minHeight);
    for (int width = minWidth; width <= maxWidth; width += 1 + (int)(width * scaleFactor)) {
      for (int height = minHeight; height <= maxHeight; height += 1 + (int) (height * scaleFactor)) {
        double aspectRatio = (double)width / height;
        if (aspectRatio >= aspectRatioMin && aspectRatio <= aspectRatioMax){
          numberPlates.addAll(findNumberPlatesOfSize(image, width, height,
            (int) (1 + width * moveFactor),
            (int) (1 + height * moveFactor)));
        }
      }
    }
    System.out.println("Checked areas: " + counter);
    return numberPlates;
  }

  /**
   * Returns whether a region in an image contains a number plate or not
   * @param image Image
   * @param testRegion Region
   * @return true if the region contains a number plate, false if not.
   */
  public boolean isNumberPlate(Image image, int x, int y, int frameWidth, int frameHeight){
    counter++;
    Rectangle r = new Rectangle(x, y, frameWidth, frameHeight);
    try {
      if(classifier.predict(image.getSubimage(r)))
        return true;
    }
    catch (ImageCloneException e) {
      System.out.println("ERROR: unable to clone");
      e.printStackTrace();
    }
    return false;
  }
  
  /**
   * Finds all number plates in an image of a given size.
   * @param image Image
   * @param frameWidth Frame width
   * @param frameHeight Frame height
   * @param stepX The length a frame is shifted horizontally each step
   * @param stepY The length a frame is shifted vertically each step
   * @return A list of number plates as rectangles
   */
  private List<Rectangle> findNumberPlatesOfSize(Image image, int frameWidth, int frameHeight, int stepX, int stepY) {
    List<Rectangle> foundPlates = new ArrayList<Rectangle>();
    for (int x = 0; x <= imgWidth - frameWidth; x += stepX ){
      for (int y = 0; y <= imgHeight - frameHeight; y+= stepY){
        if (isNumberPlate(image, x, y, frameWidth, frameHeight))
          foundPlates.add(new Rectangle(x,y,frameWidth,frameHeight));
      }
    }
    return foundPlates;
  }
  
  
  
}
