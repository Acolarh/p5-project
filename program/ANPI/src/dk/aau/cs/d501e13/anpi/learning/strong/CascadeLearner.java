package dk.aau.cs.d501e13.anpi.learning.strong;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.detector.Detector;
import dk.aau.cs.d501e13.anpi.edgedetectors.PrewittEdgeDetector;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.CascadeClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.Learner;
import dk.aau.cs.d501e13.anpi.learning.LearnerLoader;
import dk.aau.cs.d501e13.anpi.learning.QuickSave;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.boosting.AdaBooster;

public class CascadeLearner implements Learner<Image>, QuickSave<Image> {
  private LearnerLoader loader;
  private WeakLearner<Image> weakLearner = null;
  private Random rand = new Random(); //TODO: seed?
  
  private int layers;
  private int boostIterations;

  private QuickSaver<Image> quickSaver = null;
  @Override
  public void setQuickSaver(QuickSaver<Image> quickSaver) {
    this.quickSaver = quickSaver;
  }
  
  public CascadeLearner(LearnerLoader loader) {
    this.loader = loader;
  }
  
  @Override
  public void configure(Config conf) {
    String weakLearnerName = conf.getString("weakLearner");
    Learner<Image> learner = loader.getLearner(weakLearnerName);
    if (learner == null || !(learner instanceof WeakLearner)) {
      System.out.println("ERROR: Weak learner '" + weakLearnerName + "' not found");
      return;
    }
    weakLearner = (WeakLearner<Image>)learner;
    weakLearner.configure(conf.getConfig("weakLearnerConfig"));
    
    layers = conf.getInt("layers");
    boostIterations = conf.getInt("iterations");
  }
  
  private List<Image> getNegSamples(Image img, NumberplateRecord[] plates, Detector detector, double toAdd){
    List<Image> results = new ArrayList<Image>();
    List<Rectangle> detected = detector.detectPlates(img);
    
    while(toAdd > 0.0){
      //Make sure there is something to add
      if(detected.size() == 0){
        System.out.println("No more negatives to add");
        break;
      }
      
      //Add a bit of randomness when toAdd is less than 1.0
      if(toAdd < rand.nextDouble())
        break;
      
      //Take a random plate and add it if it is valid
      Rectangle selected = detected.get(rand.nextInt(detected.size()));
      if(subareaValid(img,selected, plates)){
        try {
          results.add(img.getSubimage(selected).getResized(42, 11));
          //TODO: get size from config file
        }
        catch (ImageCloneException e) {
          e.printStackTrace();
          System.exit(-42);
        }
        toAdd -= 1.0;
      }
      
      detected.remove(selected);
    }
    
    return results;
  }
  
  private void addNegatives(TrainingSamples samples, int amount, BClassifier<Image> classifier){
    long time = System.currentTimeMillis();
    
    Detector detector = new Detector(classifier);
    
    //Run detect and add false positives to training samples
    try { 
      
      ImageRecord[] images_old = loader.getImages().findByDataset(ImageRecord.SET_TRAINING);
      List<ImageRecord> images = Arrays.asList(images_old);
      Collections.shuffle(images);
      images = images.subList(0, Math.min(amount / 4, images.size()));
      
      int current = 0;
      for(ImageRecord image : images){
        if(amount <= 0)
          break;
        
        double toAdd = (double)amount / (images.size() - current); 
        System.out.println("Adding from: " + ++current + "/" + images.size() + " - " + toAdd);
        
        //Load and process
        NumberplateRecord[] plates = image.getNumberplates();
        Image loaded = null;
        try {
          Image original = new Image(loader.getDir() + "/" + image.name);
          loaded = original;
          //TODO: edge and resize depending on settings
          loaded = loaded.getEdges(new PrewittEdgeDetector());
          loaded = loaded.getResized(640, 480);

          //Scale number plates
          double widthScale = (double)loaded.getWidth() / original.getWidth();
          double heightScale = (double)loaded.getHeight() / original.getHeight();
          for(NumberplateRecord plate : plates){
            plate.x *= widthScale;
            plate.y *= heightScale;
            plate.width *= widthScale;
            plate.height *= heightScale;
          }
        }
        catch (ImageReadException e) { //TODO: fail on last one //TODO: what?
          System.out.println("Warning, couldn't load: " + image.name);
          continue;
        }
        
        //Create negative samples and add
        List<Image> negs = getNegSamples(loaded, plates, detector, toAdd);
        for(Image img : negs)
          samples.addNegative(img);
        amount -= negs.size();
      }
      System.out.println("Negatives after adding: " + samples.getNegatives().size());
    }
    catch (SQLException e) {
      e.printStackTrace();
      System.exit(-42);
    }
    System.out.println("addNegatives took: " + (System.currentTimeMillis() - time) + " msec");
  }
  
  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates)
    throws InvalidTrainingSamplesException {
    TrainingSamples samples = null;
    try {
      samples = loader.getSamples();
    }
    catch (ImageCloneException | SQLException e) {
      e.printStackTrace();
      System.exit(-42);
    }
    
    CascadeClassifier<Image> cascade = new CascadeClassifier<Image>(); 
    int negAmount = samples.getNegatives().size() / 2;
    
    for(int layer=1; layer<=layers; layer++){
      AdaBooster<Image> booster = new AdaBooster<Image>(boostIterations*layer);
      booster.setEvaluator(new ImageEvaluator(0.95, 0.9));
      
      TrainingSamples.SamplesArray samplesArray = samples.getSamplesArray();
      cascade.add(booster.boost(samplesArray.samples, samplesArray.predicates, weakLearner));
      
      //Remove correctly identified negatives, if not last layer
      if(layer < layers){
        //Safety save
        if (quickSaver != null)
          quickSaver.saveClassifier(cascade);
        
        List<Image> remove = new ArrayList<Image>();
        for(Image img : samples.getNegatives())
          if(cascade.predict(img) == false)
            remove.add(img);
        
        for(Image img : remove)
          samples.removeNegative(img);
        
        System.out.println("Negatives removed: " + remove.size());
        System.out.println("Negatives remaining: " + samples.getNegatives().size());
        
        addNegatives(samples, remove.size() + negAmount, cascade);
        
        if(samples.getNegatives().size() == 0){
          System.out.println("Fits perfectly, stopping!");
          break;
        }
        samples.saveNegatives("cascade_" + layer);
      }
      
    }
    return cascade;
  }

  
  private boolean subareaValid(Image img, Rectangle area, NumberplateRecord[] plates){
    for(NumberplateRecord plate : plates)
      if(area.intersects(new Rectangle(plate.x,plate.y,plate.width,plate.height)))
        return false;
    return true;
  }
}
