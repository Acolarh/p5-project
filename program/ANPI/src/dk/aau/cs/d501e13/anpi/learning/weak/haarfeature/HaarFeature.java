package dk.aau.cs.d501e13.anpi.learning.weak.haarfeature;

import java.util.Arrays;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.Classifier;

/**
 * Haar feature class.
 */
public class HaarFeature implements Classifier<Image>{
  private static final long serialVersionUID = 3106783964771171945L;

  /**
   * Array of rectangle objects.
   */
  Rectangle[] addRectangles;
  
  /**
   * Array of rectangle objects.
   */
  Rectangle[] subtractRectangles;
  
  /**
   * The frame width.
   */
  int frameWidth;
  
  /**
   * The frame height.
   */
  int frameHeight;
  
  /**
   * Used as a cache.
   */
  transient private HaarFeature lastScale = null;
  
  /**
   * A Haar feature can predict if an ImageSample contains a number plate.
   * It predicts by examining the difference in pixel values covered by two sets of rectangles
   * @param addRectangles The rectangles whose underlying pixel values are summed
   * @param subtractRectangles The rectangles whose underlying pixel values are summed
   * @param frameWidth The width of the frame which the Haar feature must lie within
   * @param frameHeight The height of the frame which the Haar feature must lie within
   */
  public HaarFeature(Rectangle[] addRectangles, Rectangle[] subtractRectangles, int frameWidth, int frameHeight){
    this.addRectangles = addRectangles;
    this.subtractRectangles = subtractRectangles;
    this.frameWidth = frameWidth;
    this.frameHeight = frameHeight;
  }
  
  /**
   * Returns a scaled Haar feauture.
   * @param wantedWidth The wanted width.
   * @param wantedHeight The wanted height.
   * @return The scaled Haar feature.
   */
  public HaarFeature getScaled(int wantedWidth, int wantedHeight){
    double widthScale = (double)wantedWidth / frameWidth;
    double heightScale = (double)wantedHeight / frameHeight;
    
    //Create scaled areas to add/subtract
    Rectangle[] adds = new Rectangle[addRectangles.length];
    Rectangle[] subs = new Rectangle[subtractRectangles.length];

    for(int i=0; i<adds.length; i++){
      Rectangle old = addRectangles[i];
      adds[i] = new Rectangle( //TODO: evaluate the use of floor
            (int)Math.floor(old.x * widthScale)
          , (int)Math.floor(old.y * heightScale)
          , (int)Math.floor(old.width * widthScale)
          , (int)Math.floor(old.height * heightScale)
        );
    }
    for(int i=0; i<subs.length; i++){
      Rectangle old = subtractRectangles[i];
      subs[i] = new Rectangle(
            (int)Math.floor(old.x * widthScale)
          , (int)Math.floor(old.y * heightScale)
          , (int)Math.floor(old.width * widthScale)
          , (int)Math.floor(old.height * heightScale)
        );
    }
    
    return new HaarFeature(adds, subs, wantedWidth, wantedHeight);
  }
  
  /**
   * Calculates the difference of pixel intensities in the two set of rectangles when Haar Feature is put on an image 
   * @param img The image
   * @return The difference
   */
  public double predictValue(Image img) {
    if (img.getWidth() == frameWidth && img.getHeight() == frameHeight) {
      int sum = 0;
      for (Rectangle r : addRectangles){
        sum += img.getSum(r.x, r.y, r.width, r.height);
      }
      for (Rectangle r : subtractRectangles){
        sum -= img.getSum(r.x, r.y, r.width, r.height);
      }
      return sum;
    }
    else {
      if(lastScale == null || lastScale.frameWidth != img.getWidth() || lastScale.frameHeight != img.getHeight()){
        lastScale = getScaled(img.getWidth(), img.getHeight());
      }
      
      double scale = ((double)lastScale.frameWidth / frameWidth) * ((double)lastScale.frameHeight / frameHeight);
      return (int)Math.round(lastScale.predictValue(img) / scale);
    }
  }
  
  /**
   * Just a quick way to find a hashCode for a HaarFeature
   * @return A hash code as an integer
   */
  public int hashCode() {
      return this.addRectangles.length * 17 + this.subtractRectangles.length * 119;
  }

  /**
   * Checks if two HaarFeatures are equal not. This check is not just based on containment, 
   * but on an exact sequence of rectangle values.
   * @return True = equal, false = different
   */
  public boolean equals(Object obj) {
      if (obj == null)
          return false;
      if (obj == this)
          return true;
      if (!(obj instanceof HaarFeature))
          return false;

      HaarFeature rhs = (HaarFeature) obj;
      
      //Compare 
      return Arrays.equals(rhs.addRectangles, this.addRectangles)
        && Arrays.equals(rhs.subtractRectangles, this.subtractRectangles);
  }
}
