package dk.aau.cs.d501e13.anpi.learning;

import dk.aau.cs.d501e13.anpi.Config;

/**
 * A learner (produces a classifier)
 * @param <T> Type of training sample
 */
public interface Learner<T> {
  /**
   * Configure learner
   * @param conf Configuration object
   */
  public void configure(Config conf);
  
  /**
   * Create classifier based on training samples
   * @param trainingSamples Training samples
   * @param predicates Predicates
   * @return A boolean classifier
   * @throws InvalidTrainingSamplesException if invalid size of training samples
   * and predicates
   */
  public BClassifier<T> learn(T[] trainingSamples, boolean[] predicates) throws InvalidTrainingSamplesException;
}
