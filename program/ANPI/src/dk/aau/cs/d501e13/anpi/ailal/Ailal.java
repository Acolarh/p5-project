package dk.aau.cs.d501e13.anpi.ailal;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;

/**
 * Class which loads and saves a list of classifiers to the disk
 * @param <T> The type wanted for the WeightedClassifiers
 */
public class Ailal<T> {
  /**
   * The classifier object.
   */
  private BClassifier<T> classifier = null;
  
  /**
   * The file name of the classifier file.
   */
  private String classifiersFile = null;

  /**
   * Constructor
   */
  public Ailal() {
  }

  /**
   * Constructor
   * @param conf ANPI configuration loader
   * @throws ClassifiersLoadException if unable to load classifiers
   */
  public Ailal(Config conf) throws ClassifiersLoadException {
    if (!loadClassifier(Utilities.classifierFileName(conf))) {
      throw new ClassifiersLoadException("Unable to load classifier from " + conf.getString("classifierFile"));
    }
  }
  
  /**
   * Set this object to work with a new classifier (for saving them later)
   * @param classifier The classifier
   */
  public void setClassifier(BClassifier<T> classifier){
    this.classifier = classifier;
  }
  
  /**
   * Retrieve the classifier which were previously set or loaded
   * @return The classifier
   */
  public BClassifier<T> getClassifier(){
    return this.classifier;
  }

  /**
   * Save the classifiers to the file system
   * @param filepath Where to save the file on the file system
   * @return true if file was successfully saved, false if not
   */
  public boolean saveClassifier(String filepath){
    //TODO: throw custom exception instead?
    try{  
      FileOutputStream fos = new FileOutputStream(filepath);
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(classifier);
      oos.close(); //TODO: resource leaks, or unneeded closing?
      fos.close();
    } catch (IOException e){
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Load the classifiers from the file system
   * @param filepath Where the file to load is on the file system
   * @return true if file was successfully loaded, false if not
   */
  @SuppressWarnings("unchecked") 
  //NOTE: There is no better way of doing this, 
  //according to: http://stackoverflow.com/a/3025223/2248153
  public boolean loadClassifier(String filepath){
    //TODO: throw custom exception instead?
    try{  
      FileInputStream fis = new FileInputStream(filepath);
      ObjectInputStream ois = new ObjectInputStream(fis);
      classifier = (BClassifier<T>)ois.readObject();
      ois.close();
      fis.close();
    } catch (IOException | ClassNotFoundException e){
      e.printStackTrace();
      return false;
    }
    return true;
  }
}
