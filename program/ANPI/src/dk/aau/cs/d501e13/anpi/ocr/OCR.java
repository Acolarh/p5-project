package dk.aau.cs.d501e13.anpi.ocr;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 *  The class for optical character recognition. 
 */
public class OCR {
  
  /**
   * The total number of output neurons. 0 - 9 and A - Z.
   */
  private int numOutputs = 36;
  
  /**
   * A neural network. Default is null.
   */
  private NeuralNetwork neuralNetwork = null;
  
  /**
   * Width of an input image
   */
  private int width = 6;
  
  /**
   * Height of an input image.
   */
  private int height = 9;
  
  /**
   * The number of hidden neurons in the neural network..
   */
  private int numHiddenNeurons = 25;
//  private int numHiddenNeurons = (width * height + NUM_OUTPUTS) / 2;
  
  /**
   * The number of hidden layers in the neural network.
   */
  private int numHiddenLayers = 2;
  
  /**
   * The transfer function.
   */
  private TransferFunction transferFunction = new SigmoidFunction();

  /**
   * Threshold used to get character.
   */
  private double threshold = 0.5;
  
  /**
   * The learning rate.
   */
  private double learningRate = 0.25;
  
  /**
   * The momentum to speed up the learning.
   */
  private double momentum = 0.0;
  
  /**
   * Whether or not to have thresholded neurons.
   */
  private boolean thresholdedNeurons = false;
  
  private double maxWeight = 0.5;
  private double minWeight = -0.5;
  
  /**
   * Constructor for class.
   * @param conf The configuration file.
   */
  public OCR(Config conf){
    numOutputs = conf.getInt("ocrOutputs", numOutputs);
    width = conf.getInt("ocrWidth", width);
    height = conf.getInt("ocrHeight", height);
    numHiddenNeurons = conf.getInt("ocrHiddenNeurons", numHiddenNeurons);
    numHiddenLayers = conf.getInt("ocrHiddenLayers", numHiddenLayers);
    threshold = conf.getDouble("ocrThreshold", threshold); 
    learningRate = conf.getDouble("ocrLearningRate", learningRate);
    momentum = conf.getDouble("ocrMomentum", momentum);
    thresholdedNeurons = conf.getBoolean("ocrThresholdedNeurons", thresholdedNeurons);
    maxWeight = conf.getDouble("maxWeight", maxWeight);
    minWeight = conf.getDouble("minWeight", minWeight);
    
    if(!load(Utilities.ocrFileName(conf))){
      neuralNetwork = new NeuralNetwork();
      int numInputs = width * height;
      
      for(int i = 0; i < numInputs; i++){
        neuralNetwork.addInput(new InputNeuron());
      }

      for(int i = 0; i < numOutputs; i++){
        Neuron n = new Neuron(transferFunction);
        if (thresholdedNeurons)
          n.setThreshold(Math.random() - 0.5);
        neuralNetwork.addOutput(n);
      }
      
//      for (int i = 0; i < numHiddenNeurons; i++) {
//        Neuron n = new Neuron(transferFunction);
//        neuralNetwork.addHidden(n);
//        for (Neuron in : neuralNetwork.getInputNeurons()) {
//          n.addInput(in);
//        }
//        for(Neuron on : neuralNetwork.getOutputNeurons()){
//          on.addInput(n);
//        }
//      }
      
      Neuron[] previousLayer = null;
      for(int i = 0; i < numHiddenLayers; i++){
        Neuron[] currentLayer = new Neuron[numHiddenNeurons];
        for(int j = 0; j < numHiddenNeurons; j++){
          Neuron n = new Neuron(transferFunction);
          if (thresholdedNeurons)
            n.setThreshold(Math.random() - 0.5);
          currentLayer[j] = n;
          neuralNetwork.addHidden(n);
          if (i == 0) {
            for(Neuron in : neuralNetwork.getInputNeurons()){
              n.addInput(in, Math.random() - 0.5, learningRate, momentum);
            }
          }
          else {
            for(Neuron hn : previousLayer){
              n.addInput(hn, Math.random() - 0.5, learningRate, momentum);
            }
          }
          if (i == numHiddenLayers - 1) {
            for(Neuron on : neuralNetwork.getOutputNeurons()){
              on.addInput(n, Math.random() - 0.5, learningRate, momentum);
            }
          }
        }
        previousLayer = Arrays.copyOf(currentLayer, numHiddenNeurons);
      }
    }
  }
  
  /**
   * Save the classifiers to the file system.
   * @param filepath Where the file will be saved.
   * @return true if file was successfully loaded, false if not.
   */
  public boolean save(String filepath){
    //TODO: throw custom exception instead?
    try{  
      FileOutputStream fos = new FileOutputStream(filepath);
      ObjectOutputStream oos = new ObjectOutputStream(fos);
      oos.writeObject(neuralNetwork);
      oos.close(); //TODO: resource leaks, or unneeded closing?
      fos.close();
    } catch (IOException e){
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Load the classifiers from the file system.
   * @param filepath Where the file to load is on the file system.
   * @return true if file was successfully loaded, false if not.
   */
  public boolean load(String filepath){
    //TODO: throw custom exception instead?
    try{  
      FileInputStream fis = new FileInputStream(filepath);
      ObjectInputStream ois = new ObjectInputStream(fis);
      neuralNetwork = (NeuralNetwork)ois.readObject();
      ois.close();
      fis.close();
    } catch (IOException | ClassNotFoundException e){
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Get neural network
   * @return Neural network
   */
  public NeuralNetwork getNeuralNetwork() {
    return neuralNetwork;
  }
  
  /**
   * Set neural network.
   * @param neuralNetwork The neural network object.
   */
  public void setNeuralNetwork(NeuralNetwork neuralNetwork) {
    this.neuralNetwork = neuralNetwork;
  }
  
  /**
   * Convert character to offset.
   * @param c The character.
   * @return The offset.
   */
  public int charToOffset(char c) {
    if (c < 'A') {
      return c - '0';
    }
    else {
      return 10 + c - 'A';
    }
  }
  
  /**
   * Convert offset to character.
   * @param offset The offset.
   * @return The character.
   */
  public char offsetToChar(int offset) {
    if (offset < 10 && offset > -1)
      return (char) ('0' + offset);
    else if(offset >= 10 && offset < numOutputs)
      return (char) ('A' + offset - 10);
    else
      return '?';
  }
  
  /**
   * Fill an array with the expected output. 
   * @param c The expected output.
   * @return an array with a probability for which char is the expected one.
   */
  public double[] getExpectedOutputs(char c){
    double[] outputs = new double[numOutputs];
    Arrays.fill(outputs, 0.0);
    if (c < 'A') {
      outputs[c - '0'] = 1.0;
    }
    else {
      outputs[10 + c - 'A'] = 1.0;
    }
    return outputs;
  }
  
  /**
   * @param outputs The calculated outputs.
   * @return The character based on the output.
   */
  public char getCharacter(double[] outputs){
    int bestIndex = -1;
    double bestProbability = 0.0;
    
    for(int i = 0; i < outputs.length; i++){
      if(outputs[i] > bestProbability && outputs[i] > threshold){
        bestIndex = i;
        bestProbability = outputs[i];
      }
    }
    
    return offsetToChar(bestIndex);
  }
  
  /**
   * Get a list of weighted characters instead of just the best one
   * @param outputs Array of outputs.
   * @return List of best weighted characters
   */
  public List<WeightedChar> getCharacters(double[] outputs) {
    List<WeightedChar> all = new ArrayList<WeightedChar>();
    for (int i = 0; i< outputs.length; i++) {
      if (outputs[i] > threshold) {
        all.add(new WeightedChar(offsetToChar(i), outputs[i]));
      }
    }
    Collections.sort(all);
    List<WeightedChar> result = new ArrayList<WeightedChar>();
    double previous = -1.0;
    for (WeightedChar c : all) {
      if (previous >= 0) {
        double diff = previous - c.weight;
        if (diff < threshold) {
          previous = c.weight;
          result.add(c);
        }
        else {
          break;
        }
      }
      else {
        previous = c.weight;
        result.add(c);
      }
    }
    return result;
  }
  
  /**
   * Calculates the input vector based on the image.
   * @param image The image with a character.
   * @return The array of inputs.
   */
  public double[] getInputVector(Image image){
    if (image.getWidth() != width || image.getHeight() != height) {
      image = image.getResized(width, height);
    }
    int size = image.getWidth() * image.getHeight();
    double[] inputs = new double[size];
    int i = 0;
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        inputs[i++] = image.getGray(x, y) / 255.0;
      }
    }
    return inputs;
  }
  
  /**
   * @param image The input image with a character.
   * @return The character.
   * @throws CorruptedInputException 
   */
  public char identify(Image image) throws CorruptedInputException{
    neuralNetwork.setInputVector(getInputVector(image));
    
    return getCharacter(neuralNetwork.calculate());
  }
  
  /**
   * Identify a character and return list of best weighted characters
   * @param image Character image
   * @return List of weighted characters
   * @throws CorruptedInputException
   */
  public List<WeightedChar> identifyWeighted(Image image) throws CorruptedInputException {
    neuralNetwork.setInputVector(getInputVector(image));
    
    return getCharacters(neuralNetwork.calculate());
  }
  
  /**
   * @return The number of outputs.
   */
  public int getNumOutputs() {
    return numOutputs;
  }
  
  /**
   * @return The width
   */
  public int getWidth() {
    return width;
  }
 
  /**
   * @return The height
   */
  public int getHeight() {
    return height;
  }
  
  /**
   * @return The number of hidden neurons
   */
  public int getNumHiddenNeurons() {
    return numHiddenNeurons;
  }
  
  /**
   * @return The number of hidden layers
   */
  public int getNumHiddenLayers() {
    return numHiddenLayers;
  }
  
  /**
   * @return The transfer function
   */
  public TransferFunction getTransferFunction() {
    return transferFunction;
  }
  
  /**
   * @return The threshold
   */
  public double getThreshold() {
    return threshold;
  }
  
  /**
   * @return The learning rate
   */
  public double getLearningRate() {
    return learningRate;
  }
  
  /**
   * @return The momentum
   */
  public double getMomentum() {
    return momentum;
  }
  
}
