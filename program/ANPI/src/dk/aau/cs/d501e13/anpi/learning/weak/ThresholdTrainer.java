package dk.aau.cs.d501e13.anpi.learning.weak;

import java.util.Arrays;
import java.util.Comparator;

import dk.aau.cs.d501e13.anpi.learning.Classifier;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.PredictionInfo;

public class ThresholdTrainer<T> {
  private Classifier<T> classifier;
  
  private PredictionInfo[] infos;
  
  private double threshold = 0.0;
  private boolean greater = true;
  
  public double getThreshold(){
    return threshold;
  }
  public boolean getSign(){
    return greater;
  }
  
  
  private void init(double[] values, boolean[] predicates, double[] distribution){
    infos = new PredictionInfo[values.length];
    for(int i=0; i<infos.length; i++){
      infos[i] = new PredictionInfo(distribution[i],predicates[i],values[i]);
    }
    
    Arrays.sort(infos, new Comparator<PredictionInfo>(){
      @Override
      public int compare(PredictionInfo o1, PredictionInfo o2) {
        if(o1.value > o2.value) return 1;
        if(o1.value < o2.value) return -1;
        return 0;
      }
    });
  }
  
  public ThresholdedClassifier<T> getThresholded(){
    return new ThresholdedClassifier<T>(classifier, threshold, greater);
  }
  
  public ThresholdTrainer(Classifier<T> classifier, T[] samples, boolean[] predicates, double[] distribution){
    this.classifier = classifier;
    
    //Fill values
    double[] values = new double[samples.length];
    for(int i=0; i<samples.length; i++)
      values[i] = classifier.predictValue(samples[i]);
    
    init(values, predicates, distribution);
  }
  
  public ThresholdTrainer(Classifier<T> classifier, double[] values, boolean[] predicates, double[] distribution){
    this.classifier = classifier;
    init(values, predicates, distribution);
  }
  public double train(){
    //Find initial error
    double falseNeg = 0;
    double falsePos = 0;
    for(PredictionInfo info : infos)
      falseNeg += info.predicate ? info.d : 0;
    for(PredictionInfo info : infos)
      falsePos += info.predicate ? 0 : info.d;
    
    //Initial error for Less-than
    double errorLess = falseNeg;
    double thresholdLess = Double.NEGATIVE_INFINITY;
    
    //Find the smallest error for Less-than
    for(int i=0; i<infos.length; i++){
      if(infos[i].predicate)
        falseNeg -= infos[i].d;
      else
        falseNeg += infos[i].d;
      
      if(errorLess > Math.abs(falseNeg)){
        errorLess = Math.abs(falseNeg);
        thresholdLess = i+1<infos.length ? infos[i+1].value : Double.NEGATIVE_INFINITY;
      }
    }
    
    //Initial error for Greater-than
    double errorGreater = falsePos;
    double thresholdGreater = Double.NEGATIVE_INFINITY;
    
    //Find the smallest error for Greater-than
    for(int i=0; i<infos.length; i++){
      if(infos[i].predicate)
        falsePos += infos[i].d;
      else
        falsePos -= infos[i].d;
      
      if(errorGreater > Math.abs(falsePos)){
        errorGreater = Math.abs(falsePos);
        thresholdGreater = infos[i].value;
      }
    }
    
    //Use the best one
    greater = errorGreater < errorLess;
    threshold = greater ? thresholdGreater : thresholdLess;
    return greater ? errorGreater : errorLess;
  }
  
  public double train(double positiveRatio){
    //Find initial error
    double falseNeg = 0;
    double falsePos = 0;
    for(PredictionInfo info : infos)
      falseNeg += info.predicate ? info.d : 0;
    for(PredictionInfo info : infos)
      falsePos += info.predicate ? 0 : info.d;
    
    //Reset weights to fit the positives/negatives ratio
    double posWeight = positiveRatio;
    double negWeight = 1.0;
    //Normalize, and make it sum to 2.0
    double totalWeight = posWeight + negWeight;
    posWeight = posWeight / totalWeight * 2.0;
    negWeight = negWeight / totalWeight * 2.0;
    
    //Initial error for Less-than
    double errorLess = falseNeg*posWeight;
    double thresholdLess = Double.NEGATIVE_INFINITY;
    
    //Find the smallest error for Less-than
    double falseNegPos = 0;
    for(int i=0; i<infos.length; i++){
      if(infos[i].predicate)
        falseNeg -= infos[i].d;
      else
        falseNegPos += infos[i].d;
      
      double error = Math.abs(falseNeg*posWeight + falseNegPos*negWeight);
      if(errorLess > error){
        errorLess = error;
        thresholdLess = i+1<infos.length ? infos[i+1].value : Double.NEGATIVE_INFINITY;
      }
    }
    
    //Initial error for Greater-than
    double errorGreater = falsePos*negWeight;
    double thresholdGreater = Double.NEGATIVE_INFINITY;
    
    //Find the smallest error for Greater-than
    double falsePosNeg = 0;
    for(int i=0; i<infos.length; i++){
      if(infos[i].predicate)
        falsePosNeg += infos[i].d;
      else
        falsePos -= infos[i].d;
      
      double error = Math.abs(falsePosNeg*posWeight + falsePos*negWeight);
      if(errorGreater > error){
        errorGreater = error;
        thresholdGreater = infos[i].value;
      }
    }
    
    //Use the best one
    greater = errorGreater < errorLess;
    threshold = greater ? thresholdGreater : thresholdLess;
    return greater ? errorGreater : errorLess;
  }
}
