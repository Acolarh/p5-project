package dk.aau.cs.d501e13.anpi;

/**
 * A collection of static methods to ensure naming of files.
 */
public class Utilities {
  
  /**
   * Returns the file name as a hash.
   * @param conf The config file.
   * @return A name for the file.
   */
  public static String getFileNameHash(Config conf) {
    String hash = "_" + conf.getInt("frameWidth") + "_" + conf.getInt("frameHeight") + "_";
    hash += conf.getBoolean("isWhitelist") ? 'W' : 'B';
    hash += conf.getInt("numberplateFlags");
    hash += "_" + conf.getLong("seed");
    if (conf.getBoolean("useEdgeDetection")) {
      hash += "_E" + (conf.getBoolean("edgeDetectBeforeResize") ? "B" : "A");
    }
    return hash;
  }
  
  /**
   * Returns the name for the file containing positive samples.
   * @param conf The config file.
   * @return A name for the positive sample's file.
   */
  public static String positivesFileName(Config conf) {
    if (conf.isSet("positivesFile"))
      return conf.getString("positivesFile");
    return conf.getString("baseDir", ".") + "/npr/positives" + getFileNameHash(conf) + "_" + conf.getInt("limitPositives") + ".png";
  }
  /**
   * Returns the name for the file containing negative samples.
   * @param conf The config file.
   * @return A name for the negative sample's file.
   */
  public static String negativesFileName(Config conf) {
    if (conf.isSet("negativesFile"))
      return conf.getString("negativesFile");
    String negativesFile = conf.getString("baseDir", ".") + "/npr/negatives" + getFileNameHash(conf) + "_";
    if (conf.getBoolean("useRatio")) {
      negativesFile += "R" + conf.getDouble("negativesRatio");
    }
    else {
      negativesFile += "N" + conf.getInt("numNegatives");
    }
    negativesFile += "_" + conf.getInt("limitNegatives") + ".png";
    return negativesFile;
  }
  
  /**
   * Returns the name for classifiers.
   * @param conf The config file.
   * @return A name for a classifier file.
   */
  public static String classifierFileName(Config conf) {
    if (conf.isSet("classifierFile"))
      return conf.getString("classifierFile");
    return conf.getString("baseDir", ".") + "/npr/classifier" + getFileNameHash(conf);
  }
  
  /**
   * Returns the file name for neural networks.
   * @param conf The config file.
   * @return A name for ocr file.
   */
  public static String ocrFileName(Config conf) {
    if (conf.isSet("ocrFile"))
      return conf.getString("ocrFile");
    return conf.getString("baseDir", ".") + "/ocr/ocr_" + conf.getInt("ocrWidth")
      + "_" + conf.getInt("ocrHeight")
      + "_" + conf.getInt("ocrOutputs")
      + "_" + conf.getInt("ocrHiddenLayers")
      + "_" + conf.getInt("ocrHiddenNeurons")
      + "_" + conf.getDouble("ocrLearningRate")
      + "_" + conf.getDouble("ocrMomentum")
      + (conf.getBoolean("ocrThresholdedNeurons", false) ? "_T" : "");
  }
}
