package dk.aau.cs.d501e13.anpi.testing;

import java.io.IOException;

import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.ailal.Ailal;
import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.NeuralNetworkClassifier;
import dk.aau.cs.d501e13.anpi.learning.strong.NeuralNetworkLearner;

public class ClassifierTest {
  public static void main(String[] args) throws IOException, ClassifiersLoadException{
    if(args.length < 1){
      System.out.println("ClassfierTest CONFIG_FILE");
      System.exit(-1);
    }
    
    String configPath = args[0];
    Config config = new Config(configPath);
    TrainingSamples samples = new TrainingSamples(config);
    System.out.println(Utilities.negativesFileName(config));
    System.out.println(Utilities.positivesFileName(config));
    
    BClassifier<Image> classifier = new Ailal<Image>(config).getClassifier();
    

    if (classifier instanceof NeuralNetworkClassifier) {
      if (((NeuralNetworkClassifier)classifier).nnl == null) {
        System.out.println("Fixing incomplete classifier...");
        ((NeuralNetworkClassifier)classifier).nnl = new NeuralNetworkLearner();
        ((NeuralNetworkClassifier)classifier).nnl.configure(config.getConfig("learnerConfig"));
      }
    }
    
    int positives = 0;
    int negatives = 0;
    int falsePositives = 0;
    int falseNegatives = 0;
    for(Image pos : samples.getPositives()){
      if(classifier.predict(pos))
        positives++;
      else
        falseNegatives++;
    }
    for(Image pos : samples.getNegatives()){
      if(classifier.predict(pos))
        falsePositives++;
      else
        negatives++;
    }
    System.out.println("Positives: " + positives);
    System.out.println("negatives: " + negatives);
    System.out.println("False positives: " + falsePositives);
    System.out.println("False negatives: " + falseNegatives);
  }
}
