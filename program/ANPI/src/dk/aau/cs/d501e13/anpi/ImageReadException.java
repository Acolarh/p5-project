package dk.aau.cs.d501e13.anpi;

public class ImageReadException extends Exception {
  private static final long serialVersionUID = 1815566237356936257L;

  public ImageReadException() {
    // TODO Auto-generated constructor stub
  }

  public ImageReadException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public ImageReadException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public ImageReadException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public ImageReadException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

}
