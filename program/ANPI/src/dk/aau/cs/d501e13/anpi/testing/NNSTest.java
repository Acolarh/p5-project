package dk.aau.cs.d501e13.anpi.testing;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;
import dk.aau.cs.d501e13.anpi.ocr.OCR;

public class NNSTest {
  public static void main(String[] args) throws IOException, ClassNotFoundException {
    // BEGIN CREATE NETWORK
    TransferFunction transferFunction = new SigmoidFunction();
    NeuralNetwork nn = new NeuralNetwork();
    InputNeuron inputA = new InputNeuron();
    InputNeuron inputB = new InputNeuron();
    nn.addInput(inputA);
    nn.addInput(inputB);
    Neuron hiddenA = new Neuron(transferFunction);
    hiddenA.addInput(inputA, Math.random());
    hiddenA.addInput(inputB, Math.random());
    nn.addHidden(hiddenA);
    Neuron hiddenB = new Neuron(transferFunction);
    hiddenB.addInput(inputA, Math.random());
    hiddenB.addInput(inputB, Math.random());
    nn.addHidden(hiddenB);
    InputNeuron hiddenC = new InputNeuron();
    hiddenC.setValue(1.0);
    nn.addHidden(hiddenC);
    Neuron output = new Neuron(transferFunction);
    output.addInput(hiddenA, Math.random());
    output.addInput(hiddenB, Math.random());
    output.addInput(hiddenC, Math.random());
    nn.addOutput(output);
    // END CREATE NETWORK
    
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    ObjectOutputStream oos = new ObjectOutputStream(out);
    oos.writeObject(nn);
    oos.close();
    
    ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
    ObjectInputStream ois = new ObjectInputStream(in);
    NeuralNetwork nn2 = (NeuralNetwork) ois.readObject();
    ois.close();

    System.out.println(nn.getHiddenNeurons().get(0).getInputs().get(0).weight);
    System.out.println(nn2.getHiddenNeurons().get(0).getInputs().get(0).weight);
    
    out.close();
    in.close();
    
    OCR ocr = new OCR(new Config());
    ocr.save("ocr");
    
    
  }
}
