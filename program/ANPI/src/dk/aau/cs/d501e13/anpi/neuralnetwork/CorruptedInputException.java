package dk.aau.cs.d501e13.anpi.neuralnetwork;

public class CorruptedInputException extends Exception {
  private static final long serialVersionUID = 4760528063574950007L;

  public CorruptedInputException() {
    super();
    // TODO Auto-generated constructor stub
  }

  public CorruptedInputException(String message, Throwable cause, 
      boolean nableSuppression, boolean writableStackTrace) {
    super(message, cause, nableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public CorruptedInputException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public CorruptedInputException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public CorruptedInputException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
