package dk.aau.cs.d501e13.anpi;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dk.aau.cs.d501e13.anpi.cornerdetector.CornerDetector;
import dk.aau.cs.d501e13.anpi.deskewing.Deskew;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.detector.Detector;
import dk.aau.cs.d501e13.anpi.edgedetectors.PrewittEdgeDetector;
import dk.aau.cs.d501e13.anpi.edgedetectors.SobelEdgeDetector;
import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.ocr.OCR;
import dk.aau.cs.d501e13.anpi.ocr.WeightedChar;
import dk.aau.cs.d501e13.anpi.ocr.WeightedString;

/**
 * ANPI abstraction
 */
public class Anpi {

  /**
   * Factor used for segmenting characters in image.
   */
  static final double SEGMENT_FACTOR = 0.53;
  
  /**
   * Deskewer object
   */
  private Deskew deskewer; 

  /**
   * Detector object
   */
  private Detector detector;
  
  /**
   * Optical Character Recognition object
   */
  private OCR ocr;
  
  /**
   * ANPI configuration loader
   */
  private Config conf;
  
  /**
   * Map of edge detectors
   */
  private Map<String, EdgeDetector> edgeDetectors = new HashMap<String, EdgeDetector>();
  
  /**
   * Constructor
   */
  public Anpi(Config conf) {
    this.conf = conf;
    addEdgeDetector(new PrewittEdgeDetector());
    addEdgeDetector(new SobelEdgeDetector());
    ocr = new OCR(conf);
  }
  
  /**
   * @return The ocr object.
   */
  public OCR getOcr() {
    return ocr;
  }
  
  /**
   * @return The detector object.
   */
  public Detector getDetector() {
    return detector;
  }
  
  /**
   * Set the detector object.
   * @param detector The detector.
   */
  public void setDetector(Detector detector) {
    this.detector = detector;  
  }
  
  /**
   * Returns the edge detector object.
   * @param name The name of the edge detector.
   * @return The edge detector with the given name.
   */
  public EdgeDetector getEdgeDetector(String name) {
    return edgeDetectors.get(name);
  }
  
  /**
   * Adds an edge detector.
   * @param edgeDetector The edge detector object.
   */
  public void addEdgeDetector(EdgeDetector edgeDetector) {
    edgeDetectors.put(edgeDetector.getClass().getSimpleName(), edgeDetector);
  }
  
  /**
   * Detect and identify all number plates in image
   * @param image Image of car(s) (or similar)
   * @return List of detected number plates
   * @throws ClassifiersLoadException
   * @throws InvalidParameterException
   * @throws CorruptedInputException
   */
  public List<Numberplate> detectPlates(Image image) throws ClassifiersLoadException, 
                                      InvalidParameterException, CorruptedInputException {
    try {
      List<Rectangle> rectangles = detectPlatePositions(image);
      List<Numberplate> numberplates = new ArrayList<Numberplate>();
      for (Rectangle r : rectangles) {
        Image numberplate;
        numberplate = image.getSubimage(r);
        String value = identifyCharacters(numberplate);
        numberplates.add(new Numberplate(r, value));
      }
      return numberplates;
    }
    catch (ImageCloneException e1) {
      e1.printStackTrace();
    }
    return null;
  }
  
  /**
   * Detect all number plates in image
   * @param image Image of car(s)
   * @return List of positions
   * @throws ClassifiersLoadException if unable to load classifiers
   */
  public List<Rectangle> detectPlatePositions(Image image) throws ClassifiersLoadException {
    Image resized = image;
    if (conf.getBoolean("detectorResize")) {
      resized = image.getResized(conf.getInt("detectorWidth"), conf.getInt("detectorHeight"), true);
    }
    if (conf.getBoolean("useEdgeDetection", true)) {
      resized = resized.getEdges(getEdgeDetector(
        conf.getString("edgeDetector", "PrewittEdgeDetector")
      ));
    }
    double xScale = image.getWidth() / resized.getWidth();
    double yScale = image.getHeight() / resized.getHeight();
    if (detector == null) {
      detector = new Detector(conf);
    }
    List<Rectangle> rectangles = detector.detectPlates(resized);
    for (Rectangle r : rectangles) {
      r.width *= xScale;
      r.height *= yScale;
      r.x *= xScale;
      r.y *= yScale;
    }
    return rectangles;
  }
  
  /**
   * Identify characters on number plate
   * @param numerplate Image of number plate
   * @return A string of characters
   * @throws InvalidParameterException 
   * @throws CorruptedInputException 
   */
  public String identifyCharacters(Image numberplate) throws InvalidParameterException, CorruptedInputException {
    List<Point> corners = detectCorners(numberplate);
    numberplate = skewPlate(numberplate, corners);
    List<Image> characters = segmentPlate(numberplate);
    characters = resizeCharacters(characters, ocr.getWidth(), ocr.getHeight());
    String value = "";
    for (Image character : characters) {
      value += identifyCharacter(character);
    }
    return value;
  }
  
  /**
   * Detect corners of a single number plate
   * @param numberplate Image of number plate
   * @return List of corners
   */
  public List<Point> detectCorners(Image numberplate) {
    CornerDetector cd = new CornerDetector(numberplate);
    return cd.getNumberplateCorners();
  }
  
  /**
   * Deskew a number plate based on the position of its corners
   * @param numberplate Image of number plate
   * @param corners List of Points
   * @return The deskewed image of number plate
   * @throws InvalidParameterException 
   */
  public Image skewPlate(Image numberplate, List<Point> corners) throws InvalidParameterException {
    deskewer = new Deskew(numberplate, corners); 
    return deskewer.deskew();
  }
  
  /**
   * Segment plate into individual characters
   * @param numberplate Picture of number plate
   * @return List of characters
   */
  public List<Image> segmentPlate(Image numberplate) {
    return segmentPlate(numberplate, SEGMENT_FACTOR);
  }
  
  /**
   * Segment number plate into individual characters
   * @param numberplate Image of deskewed number plate
   * @param a The threshold factor
   * @return List of characters
   */
  public List<Image> segmentPlate(Image numberplate, double a) {
    List<Image> characters = new ArrayList<Image>();
    Image binarized = numberplate.getBinarized();
    long[] histogram = binarized.getVHistogram();
    long max = 0;
    long min = Long.MAX_VALUE;
    long sum = 0;
    for (long l : histogram) {
      if (max < l) {
        max = l;
      }
      if (min > l) {
        min = l;
      }
      sum += l;
    }
    long avg = sum / histogram.length;
//    long threshold = max - (long)((max - min) * a);
    long threshold = avg + (long)((max - avg) * a);
//    System.out.println("min: " + min + " max: " + max + " threshold: " + threshold);
    
    int start = 0;
    boolean first = true;

    long rsum = sum;
    int rlength = histogram.length;
    
    try {
      for (int i = 0; i < histogram.length; i++) {
        long val = histogram[i];
        rsum -= val;
        rlength--;
        if (val > threshold) {
          if (start >= 0) {
            if (first) {
              first = false;
            }
            else {
              Image subImage = numberplate.getSubimage(new Rectangle(
                start, 0,
                i - start, numberplate.getHeight()
              ));
              characters.add(subImage);
            }
            start = -1;
            // update threshold
            if (rlength > 0) {
              avg = rsum / rlength;
              threshold = avg + (long)((max - avg) * a);
            }
          }
        }
        else {
          if (start < 0) {
            start = i;
          }
        }
      }
//      if (start >= 0) {
//        int width = numberplate.getWidth() - start;
//        double aspect = width / (double)numberplate.getHeight();
//        if (aspect < 5.0) {
//          try {
//            characters.add(numberplate.getSubimage(new Rectangle(
//              start, 0,
//              numberplate.getWidth() - start, numberplate.getHeight()
//            )));
//          }
//          catch (ImageCloneException e) {}
//        }
//      }
    }
    catch (ImageCloneException e) {}
//    if (characters.size() <= 1) {
//      return segmentPlate(numberplate, a * 1.5);
//    }
    return verifyCharacters(characters);
  }
  
  /**
   * Verify and crop list of characters
   * @param characters List of characters
   * @return List of characters
   */
  private List<Image> verifyCharacters(List<Image> characters) {
    List<Image> verified = new ArrayList<Image>();
    
    int heightSum = 0;
    
    for (Image character : characters) {
      Image binarized = character.getBinarized();
      long[] histogram = binarized.getHHistogram();
      long max = 0;
      long min = Long.MAX_VALUE;
      for (long l : histogram) {
        if (max < l) {
          max = l;
        }
        if (min > l) {
          min = l;
        }
      }
      long threshold = max;
      int start = -1;
      int bestStart = 0;
      int bestHeight = 0;
      for (int i = 0; i < histogram.length; i++) {
        long val = histogram[i];
        if (val < threshold) {
          if (start < 0) {
            start = i;
          }
        }
        else {
          if (start >= 0) {
            if (i - start > bestHeight) {
              bestStart = start;
              bestHeight = i - start;
            }
            start = -1;
          }
        }
      }
      if (start >= 0) {
        if (character.getHeight() - start > bestHeight) {
          bestStart = start;
          bestHeight = character.getHeight() - start;
        }
      }
      if (bestHeight < character.getHeight() / 3) {
        continue;
      }
      if (bestHeight / (double)character.getWidth() > 10.0) {
        continue;
      }
      try {
        verified.add(character.getSubimage(new Rectangle(
          0, bestStart,
          character.getWidth(), bestHeight
        )));
        heightSum += bestHeight;
      }
      catch (ImageCloneException e) { }
    }
    if (verified.size() == 0) {
      return verified;
    }
    int heightAvg = heightSum / verified.size();
    List<Image> verifiedTwice = new ArrayList<Image>();
    for (Image character : verified) {
      int diff = Math.abs(heightAvg - character.getHeight());
      double d = diff / (double)heightAvg;
      if (d < 0.2) {
        verifiedTwice.add(character);
      }
    }
    return verifiedTwice;
  }
  
  /**
   * Resize all characters in a list to fit within a specified width and height
   * @param characters List of characters
   * @param width Width of character
   * @param height Height of character
   * @return List of characters
   */
  public List<Image> resizeCharacters(List<Image> characters, int width, int height) {
    List<Image> result = new ArrayList<Image>();
    for (Image character : characters) {
      Image c = new Image(width, height);
      Image resized = character.getResized(width, height, true)
        .getGrayscale().getNormalized();
      int left = (width - resized.getWidth()) / 2;
      int top = (height - resized.getHeight()) / 2;
      Graphics2D g = c.createGraphics();
      g.setColor(Color.white);
      g.fillRect(0, 0, width, height);
      g.drawImage(resized.getBufferedImage(), left, top, null);
      g.dispose();
      result.add(c);
    }
    return result;
  }
  
  /**
   * Identify a single character 
   * @param character Image of character
   * @return Character
   * @throws CorruptedInputException 
   */
  public char identifyCharacter(Image character) throws CorruptedInputException {
    return ocr.identify(character);
  }
  
  /**
   * Add a character with weight.
   * @param stringList The list of strings
   * @param c The character
   * @param weight The weight
   */
  private static void addWeightedChar(List<WeightedString> stringList,
      char c, double weight) {
    for (WeightedString string : stringList) {
      string.s += c;
      string.weight += weight;
    }
  }
  
  /**
   * Copy a list of strings.
   * @param stringList The initial list of strings.
   * @return A list of the copy of the weighted strings.
   */
  private static List<WeightedString> makeCopy(List<WeightedString> stringList) {
    List<WeightedString> copy = new ArrayList<WeightedString>();
    for (WeightedString string : stringList) {
      copy.add(new WeightedString(string));
    }
    return copy;
  }
  
  /**
   * Make multiple copies of a list of strings.
   * @param stringList The initial list of strings.
   * @param numCopies The number of copies.
   * @return A list in a list of the copy of the weighted strings.
   */
  private static List<List<WeightedString>> makeCopies(List<WeightedString> stringList,
      int numCopies) {
    List<List<WeightedString>> copies = new ArrayList<List<WeightedString>>();
    copies.add(makeCopy(stringList));
    for (int i = 1; i < numCopies; i++) {
      copies.add(makeCopy(stringList));
    }
    return copies;
  }
  
  /**
   * Add weighted list.
   * @param stringList The list of weighted strings.
   * @param charList The list of weighted characters.
   */
  private static void addWeightedList(List<WeightedString> stringList,
      List<WeightedChar> charList) {
    int numCopies = charList.size();
    List<List<WeightedString>> copies = makeCopies(stringList, numCopies);
    for (int i = 0; i < numCopies; i++) {
      WeightedChar wc = charList.get(i);
      addWeightedChar(copies.get(i), wc.c, wc.weight);
    }
    stringList.clear();
    for (List<WeightedString> copy : copies) {
      stringList.addAll(copy);
    }
  }
  
  /**
   * Add weighted characters.
   * @param stringList The list of weighted strings.
   * @param charList The list of weighted characters.
   */
  private static void addWeightedChars(List<WeightedString> stringList,
      List<WeightedChar> charList) {
    if (charList.size() == 0) {
      addWeightedChar(stringList, '?', 0.0);
    }
    else if (charList.size() == 1) {
      WeightedChar wc = charList.get(0);
      addWeightedChar(stringList, wc.c, wc.weight);
    }
    else {
      addWeightedList(stringList, charList);
    }
  }
  
  /**
   * Identify multiple characters and return list of best fitting strings
   * @param characters List of character images
   * @return List of weighted strings (not sorted)
   * @throws CorruptedInputException
   */
  public List<WeightedString> identifyCharacters(List<Image> characters) throws CorruptedInputException {
    List<WeightedString> stringList = new ArrayList<WeightedString>();
    stringList.add(new WeightedString("", 0.0));
    for (Image character : characters) {
      List<WeightedChar> charList = ocr.identifyWeighted(character);
      addWeightedChars(stringList, charList);
    }
    for (WeightedString string : stringList) {
      string.weight /= string.s.length();
    }
    return stringList;
  }
}
