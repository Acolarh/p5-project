package dk.aau.cs.d501e13.anpi.geometry;

/**
 * A corner of a polygon
 */
public class Corner extends Point {
  private static final long serialVersionUID = 6378857939095589263L;

  /**
   * Angle in radians
   */
  public double angle;
  
  /**
   * Constructor
   * @param x X coordinate
   * @param y Y coordinate
   * @param angle Angle of corner in radians
   */
  public Corner(int x, int y, double angle) {
    super(x, y);
    this.angle = angle;
  }
  
  /**
   * Constructor
   * @param p Point of corner
   * @param angle Angle of corner in radians
   */
  public Corner(Point p, double angle) {
    super(p);
    this.angle = angle;
  }
  
  /**
   * Cloning constructor
   * @param c Other corner
   */
  public Corner(Corner c) {
    this(c, c.angle);
  }
  
}
