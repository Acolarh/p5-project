package dk.aau.cs.d501e13.anpi.ailal;

import java.awt.Graphics2D;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.EdgeDetector;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;

/**
 * Load and save training samples to disk
 */
public class TrainingSamples {
  
  /**
   * Represents an array of samples and predicates
   */
  public class SamplesArray {
    /**
     * Array of image samples
     */
    public Image[] samples;
    
    /**
     * Array of predicates
     */
    public boolean[] predicates;
    
    /**
     * Constructor.
     * @param samples Array of image samples
     * @param predicates Array of predicates
     * @throws InvalidTrainingSamplesException if uneven sizes
     */
    public SamplesArray(Image[] samples, boolean[] predicates) throws InvalidTrainingSamplesException {
      if (samples.length != predicates.length) {
        throw new InvalidTrainingSamplesException("Incompatible lengths of training samples and predicates");
      }
      this.samples = samples;
      this.predicates = predicates;
    }
  }
  
  /**
   * Width of a sample in pixels
   */
  private int sampleWidth;
  
  /**
   * Height of a sample in pixels
   */
  private int sampleHeight;
  
  /**
   * Positives limit
   */
  private int limitPositives = -1;
  
  /**
   * Negatives limit
   */
  private int limitNegatives = -1;

  /**
   * List of positives
   */
  private List<Image> positives = new ArrayList<Image>();
  
  /**
   * List of negatives
   */
  private List<Image> negatives = new ArrayList<Image>();  

  /**
   * Constructor.
   * @param sampleWidth Width of sample in pixels
   * @param sampleHeight Height of sample in pixels
   */
  public TrainingSamples(int sampleWidth, int sampleHeight) {
    this.sampleWidth = sampleWidth;
    this.sampleHeight = sampleHeight;
  }
  
  /**
   * Constructor.
   * @param sampleWidth Width of sample
   * @param sampleHeight Height of sample
   * @param limitPositives Limit for positives
   * @param limitNegatives Limit for negatives
   */
  public TrainingSamples(int sampleWidth, int sampleHeight, 
      int limitPositives, int limitNegatives) {
    this.sampleWidth = sampleWidth;
    this.sampleHeight = sampleHeight;
    this.limitPositives = limitPositives;
    this.limitNegatives = limitNegatives;
  }
  
  /**
   * Constructor
   * @param conf ANPI configuration loader
   */
  public TrainingSamples(Config conf) {
    sampleWidth = conf.getInt("frameWidth");
    sampleHeight = conf.getInt("frameHeight");
    limitNegatives = conf.getInt("limitNegatives", limitNegatives);
    limitPositives = conf.getInt("limitPositives", limitPositives);
    load(Utilities.positivesFileName(conf), Utilities.negativesFileName(conf));
  }
  
  /**
   * Load positives and negatives from files
   * @param positivesFile Path to png file
   * @param negativesFile Path to png file
   * @return True if file exists and successful, false otherwise
   */
  public boolean load(String positivesFile, String negativesFile) {
    return loadPositives(positivesFile) && loadNegatives(negativesFile);
  }
  
  /**
   * Load positives from file
   * @param positivesFile Path to png file
   * @return True if successful, false otherwise
   */
  public boolean loadPositives(String positivesFile) {
    try {
      Image positivesImage = new Image(positivesFile);
      System.out.println("Loaded positives: " + positivesImage.getWidth() + "x" + positivesImage.getHeight());
      
      int numPlates = positivesImage.getWidth() / sampleWidth;
      
      for (int i = 0; i < numPlates; i++) {
        System.out.println("Adding positive " + (i + 1) + " of " + numPlates);
        positives.add(
          positivesImage.getSubimage(new Rectangle(i * sampleWidth, 0, sampleWidth, sampleHeight))
        );
      }
    }
    catch (ImageReadException | ImageCloneException e) {
      return false;
    }
    return true;
  }

  /**
   * Load negatives from file
   * @param negativesFile Path to png file
   * @return True if successful, false otherwise
   */
  public boolean loadNegatives(String negativesFile) {
    try {
      Image negativesImage = new Image(negativesFile);
      System.out.println("Loaded negatives: " + negativesImage.getWidth() + "x" + negativesImage.getHeight());
      
      int numPlates = negativesImage.getWidth() / sampleWidth;
      
      for (int i = 0; i < numPlates; i++) {
        System.out.println("Adding negative " + (i + 1) + " of " + numPlates);
        negatives.add(
          negativesImage.getSubimage(new Rectangle(i * sampleWidth, 0, sampleWidth, sampleHeight))
        );
      }
    }
    catch (ImageReadException | ImageCloneException e) {
      return false;
    }
    return true;
  }
  
  /**
   * Save positives and negatives to files
   * @param positivesFile Path to PNG file
   * @param negativesFile Path to PNG file
   * @return True if successful false otherwise
   */
  public boolean save(String positivesFile, String negativesFile) {
    return savePositives(positivesFile) && saveNegatives(negativesFile);
  }
  
  /**
   * Save positives to file
   * @param positivesFile Path to PNG file
   * @return True if successful, false otherwise
   */
  public boolean savePositives(String positivesFile) {
    System.out.println("Creating positives picture: " + (sampleWidth * positives.size()) + "x" + (sampleHeight));
    try {
      Image positivesImage = new Image(sampleWidth * positives.size(), sampleHeight);
      Graphics2D positiveG = positivesImage.createGraphics();
      
      int positiveOffset = 0;
      for (Image positive : positives) {
        positiveG.drawImage(
          positive.getBufferedImage(),
          positiveOffset,
          0,
          null
        );
        positiveOffset += sampleWidth;
      }
      positiveG.dispose();
      System.out.println("Saving positives...");
      positivesImage.save(positivesFile, "png");
    }
    catch (IllegalArgumentException e) {
      System.out.println("ERROR: No positives to save");
      return false;
    }
    catch (IOException e) {
      System.out.println("ERROR: Unable to save positives");
      return false;
    }
    return true;
  }
  
  /**
   * Save negatives to file
   * @param negativesFile Path to PNG file
   * @return True if successful, false otherwise
   */
  public boolean saveNegatives(String negativesFile) {
    System.out.println("Creating negatives picture: " + (sampleWidth * positives.size()) + "x" + (sampleHeight));
    try {
      Image negativesImage = new Image(sampleWidth * negatives.size(), sampleHeight);
      Graphics2D negativeG = negativesImage.createGraphics();
      
      int negativeOffset = 0;
      for (Image negative : negatives) {
        negativeG.drawImage(
          negative.getBufferedImage(),
          negativeOffset,
          0,
          null
        );
        negativeOffset += sampleWidth;
      }
      negativeG.dispose();
      System.out.println("Saving negatives...");
      negativesImage.save(negativesFile, "png");
    }
    catch (IllegalArgumentException e) {
      System.out.println("ERROR: No negatives to save");
      return false;
    }
    catch (IOException e) {
      System.out.println("ERROR: Unable to save negatives");
      return false;
    }
    return true;
  }
  
  /**
   * @return List of positives
   */
  public List<Image> getPositives() {
    return positives;
  }
  
  /**
   * @return List of negatives
   */
  public List<Image> getNegatives() {
    return negatives;
  }
  
  /**
   * Add positive
   * @param img A positive sample
   */
  public void addPositive(Image img) {
    if (limitPositives >= 0 && positives.size() >= limitPositives) {
      return;
    }
    positives.add(img);
  }
  
  /**
   * Add negative
   * @param img A negative sample
   */
  public void addNegative(Image img) {
    if (limitNegatives >= 0 && negatives.size() >= limitNegatives) {
      return;
    }
    negatives.add(img);
  }
  
  /**
   * Remove a positive
   * @param img Image to remove (must be reference equal)
   */
  public void removePositive(Image img){
    positives.remove(img);
  }
  
  /**
   * Removes a negative
   * @param img Image to remove (must be reference equal)
   */
  public void removeNegative(Image img){
    negatives.remove(img);
  }
  
  /**
   * Add positives from database
   * @param dir Sample image dir
   * @param images Image model
   * @param numberplates Number plate model
   * @param numberplateFlags Flags (see {@see NumberplateModel})
   * @param isWhitelist Whether or not flags is a white list
   * @param edgeDetector Edge detector object
   * @param edgeDetectBeforeResize Whether or not to edge detect before resizing
   * @throws SQLException on sql error
   * @throws ImageCloneException on image clone error
   */
  public void addPositivesFromDb(String dir, ImageModel images, NumberplateModel numberplates,
      int numberplateFlags, boolean isWhitelist, EdgeDetector edgeDetector,
      boolean edgeDetectBeforeResize) throws SQLException, ImageCloneException {
    System.out.println("Getting list of images...");
    if (isWhitelist) {
      numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
        + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = " + numberplateFlags);
    }
    else {
      numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
        + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = 0");
    }
    
    ImageRecord[] records = images.findByDataset(ImageRecord.SET_TRAINING);
    
    int i = 1;
    int of = records.length;
    for (ImageRecord record : records) {
      if (limitPositives >= 0 && positives.size() >= limitPositives) {
        break;
      }
      NumberplateRecord[] plates = (NumberplateRecord[])numberplates.queryMultiple("select-flags", record.id);
//      NumberplateRecord[] plates = record.getNumberplates();
      if (plates.length == 0) {
        continue;
      }
      Image img;
      System.out.println("Loading image " + (i++) + " of " + of + ": " + dir + "/" + record.name);
      try {
        img = new Image(dir + "/" + record.name);
      }
      catch (ImageReadException e) {
        System.out.println("ERROR: Unable to read image: " + dir + "/" + record.name);
        continue;
      }
      
      for (int j = 0; j < plates.length; j++) {
        if (limitPositives >= 0 && positives.size() >= limitPositives) {
          break;
        }
        NumberplateRecord plate = plates[j];
        Rectangle r = new Rectangle(plate.x, plate.y, plate.width, plate.height);
        if (edgeDetector == null) {
          positives.add(img.getSubimage(r).getResized(sampleWidth, sampleHeight));
        }
        else if (edgeDetectBeforeResize) {
          positives.add(img.getSubimage(r)
            .getEdges(edgeDetector).getResized(sampleWidth, sampleHeight));
        }
        else {
          positives.add(img.getSubimage(r)
            .getResized(sampleWidth, sampleHeight).getEdges(edgeDetector));
        }
      }
    }

  }
  /**
   * Add positives from database (don't use edge detection)
   * @param dir Sample image dir
   * @param images Image model
   * @param numberplates Number plate model
   * @param numberplateFlags Flags (see {@see NumberplateModel})
   * @param isWhitelist Whether or not flags is a white list
   * @throws SQLException on sql error
   * @throws ImageCloneException on image clone error
   */
  public void addPositivesFromDb(String dir, ImageModel images, NumberplateModel numberplates,
      int numberplateFlags, boolean isWhitelist) throws SQLException, ImageCloneException {
    addPositivesFromDb(dir, images, numberplates, numberplateFlags, isWhitelist, null, false);
  }
  
  /**
   * Add positives from database
   * @param dir Image sample dir
   * @param images Image model
   * @param numberplates Number plate mode
   * @param numberplateFlags Number plate flags
   * @throws SQLException on SQL error 
   * @throws ImageCloneException on clone error
   */
  public void addPositivesFromDb(String dir, ImageModel images, NumberplateModel numberplates,
      int numberplateFlags) throws SQLException, ImageCloneException {
    addPositivesFromDb(dir, images, numberplates, numberplateFlags, false);
  }
  
  /**
   * Add positives from database
   * @param dir Image sample dir
   * @param images Image model
   * @param numberplates Number plate model
   * @throws SQLException on SQL error 
   * @throws ImageCloneException on clone error
   */
  public void addPositivesFromDb(String dir, ImageModel images,
      NumberplateModel numberplates) throws SQLException, ImageCloneException {
    addPositivesFromDb(dir, images, numberplates, 0, false);
  }
  
  /**
   * @return A tuple of samples and predicates
   */
  public SamplesArray getSamplesArray() {
    int numSamples = positives.size() + negatives.size();
    Image[] samples = new Image[numSamples];
    boolean[] predicates = new boolean[numSamples];
    
    int i = 0;
    
    for (Image positive : positives) {
      samples[i] = positive;
      predicates[i] = true;
      i++;
    }
    for (Image negative : negatives) {
      samples[i] = negative;
      predicates[i] = false;
      i++;
    }
    
    try {
      return new SamplesArray(samples, predicates);
    }
    catch (InvalidTrainingSamplesException e) {
      System.out.println("I'm sorry. This musn't happen.");
      e.printStackTrace();
    }
    return null;
  }
  
  /**
   * @return Size of training set
   */
  public int size() {
    return positives.size() + negatives.size();
  }

}
