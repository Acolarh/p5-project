package dk.aau.cs.d501e13.anpi.learning;

/**
 * A binary classifier
 * @param <T> the type of sample to classify
 */
public interface BClassifier<T> extends java.io.Serializable {
  
  /**
   * Predicts the classification of a sample (example: predicts if a picture has a number plate),
   * @param p The sample
   * @return The predicted classification
   */
  public abstract boolean predict(T t);
    
}
