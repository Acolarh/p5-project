package dk.aau.cs.d501e13.anpi.data.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.Model;

/**
 * Model for table number plates
 */
public class NumberplateModel extends Model {
  
  /**
   * Constructor
   * @param db Database object
   * @throws SQLException on SQL error
   */
  public NumberplateModel(Database db) throws SQLException {
    super(db, "numberplates", new String[]{"image_id", "x", "y", "width",
        "height", "flags", "value"});
    
    prepare("find", "SELECT * FROM " + name() + " WHERE id = ?");
    prepare("findByImageId", "SELECT * FROM " + name() + " WHERE image_id = ?");
    prepare("findByValue", "SELECT * FROM " + name() + " WHERE value = ?");
    
    prepare("save", "UPDATE " + name() + " SET image_id = ?, x = ?, y = ?,"
        + " width = ?, height = ?, flags = ?, value = ? WHERE id = ?");
    prepare("delete", "DELETE FROM " + name() + " WHERE id = ?");
  }
  
  /**
   * Create a record
   * @param imageId Image id
   * @param x X coordinate
   * @param y Y coordinate
   * @param width Width
   * @param height Height 
   * @param flags Flags (see {@link NumberplateRecord})
   * @param value Value
   * @return New record
   * @throws SQLException on SQL error
   */
  public NumberplateRecord create(int imageId, int x, int y, int width,
      int height, int flags, String value) throws SQLException {
    int id = executeCreate(imageId, x, y, width, height, flags, value);
    if (id > 0) {
      return new NumberplateRecord(this, id, imageId, x, y, width, height,
          new NumberPlateFlags(flags), value);
    }
    return null;
  }
  
  /**
   * Create a record
   * @param image Image
   * @param x X coordinate
   * @param y Y coordinate
   * @param width Width
   * @param height Height 
   * @param flags Flags (see {@link NumberplateRecord})
   * @param value Value
   * @return New record
   * @throws SQLException on SQL error
   */
  public NumberplateRecord create(ImageRecord image, int x, int y, int width,
      int height, int flags, String value) throws SQLException {
    return create(image.id, x, y, width, height, flags, value);
  }

  /**
   * Find record by id
   * @param id Id
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public NumberplateRecord find(int id) throws SQLException {
    return (NumberplateRecord)querySingle("find", id);
  }
  
  /**
   * Find records by imageId
   * @param imageId Image id
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public NumberplateRecord[] findByImageId(int imageId) throws SQLException {
    return (NumberplateRecord[])queryMultiple("findByImageId", imageId);
  }
  
  /**
   * Find records by value
   * @param imageId Image id
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public NumberplateRecord[] findByValue(String value) throws SQLException {
    return (NumberplateRecord[])queryMultiple("findByValue", value);
  }
  
  @Override
  public NumberplateRecord makeRecord(ResultSet r) throws SQLException {
    NumberplateRecord record = null;
    if (r.next()) {
      record = new NumberplateRecord(this, r);
    }
    r.close();
    return record;
  }

  @Override
  public NumberplateRecord[] makeRecordList(ResultSet r) throws SQLException {
    List<NumberplateRecord> records = new ArrayList<NumberplateRecord>();
    while (r.next()) {
      records.add(new NumberplateRecord(this, r));
    }
    r.close();
    return records.toArray(new NumberplateRecord[0]);
  }
  
  @Override
  public NumberplateRecord[] all() throws SQLException {
    return (NumberplateRecord[])super.all();
  }
}
