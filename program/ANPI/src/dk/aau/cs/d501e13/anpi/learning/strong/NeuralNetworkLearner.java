package dk.aau.cs.d501e13.anpi.learning.strong;

import java.io.Serializable;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.Learner;
import dk.aau.cs.d501e13.anpi.learning.LearnerLoader;
import dk.aau.cs.d501e13.anpi.learning.NeuralNetworkClassifier;
import dk.aau.cs.d501e13.anpi.learning.QuickSave;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.boosting.AdaBooster;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkTrainingSample;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 * A learner using AdaBoost boosting algorithm
 */
public class NeuralNetworkLearner implements Learner<Image>, QuickSave<Image>, Serializable {
  private static final long serialVersionUID = -7967128162102220131L;

  /**
   * The training sample.
   */
  public class TrainingSample implements NeuralNetworkTrainingSample {

    /**
     * Inputs.
     */
    private double[] inputs;
    
    /**
     * Outputs.
     */
    private double[] outputs = new double[1];
    
    /**
     * Constructor.
     * @param input The input image.
     * @param predicate A predicate.
     */
    public TrainingSample(Image input, boolean predicate) {
      inputs = getInputsFor(input);
      outputs[0] = predicate ? 1.0 : 0.0;
    }
    
    @Override
    public double[] getInputs() {
      return inputs;
    }

    @Override
    public double[] getExpectedOutputs() {
      return outputs;
    }

    
  }
  /**
   * Number of neural network iterations
   */
  private int maxEpochs = -1;
  
  /**
   * Width of number plate.
   */
  private int width = 34;
  
  /**
   * Height of number plate.
   */
  private int height = 9;
  
  /**
   * Number of hidden neurons.
   */
  private int numHidden;
  
  /**
   * Whether or not neurons are thresholded.
   */
  private boolean thresholdedNeurons = false;
  
  /**
   * Learning rate.
   */
  private double learningRate = 0.2;
  
  /**
   * The momentum.
   */
  private double momentum = 0.0;
  
  /**
   * The threshold.
   */
  private double threshold = 0.5;
  
  /**
   * Maximum error.
   */
  private double maxError = 1.0;
  
  /**
   * Outputs at every number of iteration.
   */
  private int outputEvery = 1;
  
  /**
   * Quick saver
   */
  transient private QuickSaver<Image> quickSaver = null;

  /**
   * Returns the inputs.
   * @param image The image.
   * @return The inputs.
   */
  public double[] getInputsFor(Image image) {
    if (image.getWidth() != width || image.getHeight() != this.height) {
      image = image.getResized(width, height);
    }
    double[] inputs = new double[width * height];
    int i = 0;
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        inputs[i++] = image.getGray(x, y) / 255.0;
      }
    }
    return inputs;
  }

  /**
   * Calculates the sum of sqaured errors.
   * @param nn The neural network.
   * @param samples The samples.
   * @return The sum of the squared errors.
   * @throws CorruptedInputException
   */
  public static double SSE(NeuralNetwork nn, NeuralNetworkTrainingSample[] samples) throws CorruptedInputException {
    double errorSum = 0.0;
    for (NeuralNetworkTrainingSample ts : samples) {
      nn.setInputVector(ts.getInputs());
      double[] outputs = nn.calculate();
      double[] expected = ts.getExpectedOutputs();
      for (int i = 0; i < outputs.length; i++) {
        double error = outputs[i] - expected[i];
        errorSum += error * error;
      }
    }
    return errorSum;
  }
  
  @Override
  public void configure(Config conf) {
    maxEpochs = conf.getInt("maxEpochs", maxEpochs);
    width = conf.getInt("width", width);
    height = conf.getInt("height", height);
    numHidden = conf.getInt("numHidden", width * height / 2);
    maxError = conf.getDouble("maxError", maxError);
    learningRate = conf.getDouble("learningRate", learningRate);
    momentum = conf.getDouble("momentum", momentum);
    threshold = conf.getDouble("threshold", threshold);
    thresholdedNeurons = conf.getBoolean("thresholdedNeurons", thresholdedNeurons);
    outputEvery = conf.getInt("outputEvery", outputEvery);
  }

  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates) throws InvalidTrainingSamplesException {
    NeuralNetwork nn = new NeuralNetwork();
    TransferFunction transferFunction = new SigmoidFunction();
    int numInputs = width * height;
    Neuron output = new Neuron(transferFunction);
    nn.addOutput(output);
    for (int i = 0; i < numInputs; i++) {
      nn.addInput(new InputNeuron());
    }
    for (int i = 0; i < numHidden; i++) {
      Neuron n = new Neuron(transferFunction);
      if (thresholdedNeurons)
        n.setThreshold(Math.random() - 0.5);
      nn.addHidden(n);
      output.addInput(n, Math.random() - 0.5, learningRate, momentum);
      for (InputNeuron in : nn.getInputNeurons()) {
        n.addInput(in, Math.random() - 0.5, learningRate, momentum);
      }
    }
    TrainingSample[] samples = new TrainingSample[trainingSamples.length];
    for (int i = 0; i < samples.length; i++) {
      samples[i] = new TrainingSample(trainingSamples[i], predicates[i]);
    }
    double error;
    try {
      error = SSE(nn, samples);
      dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner learner =
        new dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetworkLearner(nn);
      int i = 0;
      while (error > maxError) {
        if (maxEpochs >= 0 && i > maxEpochs) {
          break;
        }
        learner.learn(samples);
        error = SSE(nn, samples);
        if (i % outputEvery == 0) {
          System.out.println("iteration " + i + ": " + error);
          quickSaver.saveClassifier(new NeuralNetworkClassifier(this, nn, threshold));
        }
        i++;
      }
      System.out.println("iteration " + i + ": " + error);
    }
    catch (CorruptedInputException e) {
      throw new InvalidTrainingSamplesException(e);
    }
    return new NeuralNetworkClassifier(this, nn, threshold);
  }
  
  @Override
  public void setQuickSaver(QuickSaver<Image> quickSaver) {
    this.quickSaver = quickSaver;
  }

}
