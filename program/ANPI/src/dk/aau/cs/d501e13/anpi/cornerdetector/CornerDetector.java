package dk.aau.cs.d501e13.anpi.cornerdetector;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import dk.aau.cs.d501e13.anpi.EdgeDetector;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.edgedetectors.CannyEdgeDetector;
import dk.aau.cs.d501e13.anpi.edgedetectors.PrewittEdgeDetector;
import dk.aau.cs.d501e13.anpi.edgedetectors.SobelEdgeDetector;
import dk.aau.cs.d501e13.anpi.geometry.Line;
import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.imagetransformation.HoughTransform;

/**
 * Class for corner detection.
 */
public class CornerDetector {
  
  /**
   * original image.
   */
  Image originalImage;
  
  /**
   * Binarized image.
   */
  public Image binarizedImage;
  
  /**
   * Hough transform object.
   */
  HoughTransform ht;
  
  /**
   * Makes continuous line with a span of this number of pixels 
   */
  int lineNeighborhood = 2;
  
  /**
   * Minimum different in degrees
   */
  int minLineDegreeDifference = 50;
  
  /**
   * Extend factor
   */
  double lineExtendFactor = 1.3;
  
  /**
   * If line is continuous.
   */
  boolean unbrokenLine = true;
  
  /**
   * Edge detector object.
   */
  EdgeDetector edgeDetector = new CannyEdgeDetector();
  
  /**
   * The number of different degrees tested on image.
   * Calculated as thetaSteps / 360.
   * 180 / 360 means that every other degree will be tested. 
   */
  int thetaSteps = 180;
  
  /**
   * The number of different radius' to be tried on image.
   */
  int radiusSteps = 200;
  
  /**
   * Initialises corner detection on an original image which is not edge detected
   * @param image The image
   */
  public CornerDetector(Image image){
    originalImage = image;
    binarizedImage = image.getEdges(edgeDetector).getBinarized();    
    ht = new HoughTransform(binarizedImage, thetaSteps, radiusSteps, unbrokenLine, lineNeighborhood);
  }
  
  /**
   * @return A list of corners as points.
   */
  public ArrayList<Point> getNumberplateCorners(){
    //Find the 4 strongest lines with a minimum angle difference of x degrees between any two lines.
    ArrayList<Line> bestLines = ht.getStrongestLines(4, minLineDegreeDifference);
    
    //Extend the length of the lines as they might not intersect each other
    //because they are cut off to fit the frame size
    for (Line line : bestLines)
      line.extend(lineExtendFactor);
    
    
    //Find the corners of the number plate, which is where the lines intersect each other
    ArrayList<Point> points = Line.getIntersections(bestLines);
    if (points.size() > 4){
      points = get4BestPoints(points);
    }
    else if (points.size() < 4){
      points = useFullPicture();
    }
    return points;   
  }
  
  
  /**
   * Gets all four points representing the corner of the full number plate image. 
   * (not only the plate)
   * @return Four points.
   */
  private ArrayList<Point> useFullPicture() {
    int imgX = originalImage.getWidth();
    int imgY = originalImage.getHeight();
    ArrayList<Point> points = new ArrayList<Point>();
    points.add(new Point(0, 0));
    points.add(new Point(imgX, 0));
    points.add(new Point(0, imgY));
    points.add(new Point(imgX, imgY));
    return points;
  }
  
  /**
   * For each four corners in the original image frame, select the point closest to each corner,
   * which have not previously been selected.
   * @param points A list of corners as points.
   * @return The four selected points.
   */
  public ArrayList<Point> get4BestPoints(ArrayList<Point> points) {
    int imgX = originalImage.getWidth();
    int imgY = originalImage.getHeight();
    int[] positionsX = new int[]{0, imgX};
    int[] positionsY = new int[]{0, imgY};
    ArrayList<Point> pointsToKeep = new ArrayList<Point>();
    
    for (int i = 0; i < positionsX.length; i++){
      for (int j = 0; j < positionsY.length; j++){
        
        int pX = positionsX[i];
        int pY = positionsY[j];
        
        //find closest point
        Point closestPoint = null;
        double shortestDist = Double.POSITIVE_INFINITY;
        for (int p = 0; p < points.size(); p++){
          double dist = Math.sqrt(Math.pow(points.get(p).x - pX , 2) + 
            Math.pow(points.get(p).y - pY, 2));
          if (dist < shortestDist){
            shortestDist = dist;
            closestPoint = points.get(p);
          }
        }
        
        //keep closest point and remove it from list
        pointsToKeep.add(closestPoint);
        points.remove(closestPoint);
        
      }
    }
    
    return pointsToKeep;
  }
    
  /**
   * @return The original image with the edges as lines and corners (points) as circles. 
   */
  public Image getDebugImage(){   
    Image copyOfImage = 
      new Image(originalImage.getWidth(), originalImage.getHeight());
    Graphics2D g = copyOfImage.createGraphics();
    g.drawImage(originalImage.getBufferedImage(), 0, 0, null);
    
    //Show the 4 lines found
    g.setColor(Color.BLUE);   
    ArrayList<Line> lines = ht.getStrongestLines(4, minLineDegreeDifference);
    g.setStroke(new BasicStroke(5));
    for (Line l : lines)
      g.drawLine(l.x, l.y, l.x2, l.y2);
    
    //Draw the 4 corners found
    g.setColor(Color.RED);
    ArrayList<Point> numberPlateCorners = getNumberplateCorners();   
    for (Point p : numberPlateCorners)
      g.drawOval(p.x-6, p.y-6, 12, 12);

    return copyOfImage;
  }
  
  /**
   * Used only for debugging purposes.
   * @return An image representing Hough space.
   */
  public Image getHoughSpace(){
    return ht.getHoughSpace();
  }
  
  public static void main(String[] args) throws ImageReadException, IOException{

      Image testImage = new Image("C:\\Users\\Kent\\Desktop\\skewedplate2.png");

      CornerDetector cd = new CornerDetector(testImage);
      //cd.getDebugImage().save("C:\\Users\\SimonBuus\\Documents\\hjørnedetektion\\debug2.png", "png");
      //cd.getHoughSpace().save("C:\\Users\\SimonBuus\\Documents\\hjørnedetektion\\hough2.png", "png");
      cd.binarizedImage.save("C:\\Users\\Kent\\Desktop\\binarized2.png", "png");
  }
}
