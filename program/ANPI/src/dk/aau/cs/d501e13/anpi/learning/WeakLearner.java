package dk.aau.cs.d501e13.anpi.learning;

/**
 * A generic weak learning algorithm
 * @param <T1> Sample type
 */
public interface WeakLearner<T1> extends Learner<T1> {
  /**
   * Initialize weak learner
   * @param trainingSamples Array of training samples
   * @param predicates Array of predicates
   */
  public void init(T1[] trainingSamples, boolean[] predicates);
  
  /**
   * Return a classifier based on the training samples and predicates based on
   * a distribution
   * @param distribution Distribution of samples
   * @return A single classifier
   */
  public BClassifier<T1> learn(double[] distribution);
  
  /**
   * Get error for returned classifier
   * @return Error
   */
  public double getError();
  
  /**
   * Get indicator for returned classifier on a training sample. I.e. 1 if 
   * classifier does not match training sample, -1 otherwise
   * @param trainingSample Sample id
   * @return Indicator
   */
  public int getIndicator(int trainingSample);
}
