package dk.aau.cs.d501e13.anpi.learning.weak.haarfeature;

import java.util.ArrayList;

import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

/**
 * Provides static methods for generating different types of HaarFeatures within a specified frame size
 */
public final class HaarFeatureGenerator {
  
  public final static int FEATURE_TYPE_1 = 1 << 0;
  public final static int FEATURE_TYPE_2 = 1 << 1;
  public final static int FEATURE_TYPE_3 = 1 << 2;
  public final static int FEATURE_TYPE_4 = 1 << 3;
  public final static int FEATURE_TYPE_5 = 1 << 4;
  public final static int FEATURE_TYPE_6 = 1 << 5;
  public final static int FEATURE_TYPE_ALL = FEATURE_TYPE_1 | FEATURE_TYPE_2
    | FEATURE_TYPE_3 | FEATURE_TYPE_4 | FEATURE_TYPE_5 | FEATURE_TYPE_6;
  
  private HaarFeatureGenerator(){}
  
  /**
   * Generates all types of HaarFeatures that can lie within a given frame
   * haarFeatureTypes can be set with a constant named FEATURE_TYPE_*
   * @param frameWidth Width of the frame
   * @param frameHeight Height of the frame
   * @param haarFeatureTypes Bit flags representing which types of HaarFeatures to be generated
   * @return A list of HaarFeatures that can be generated within the frame
   */
  public static ArrayList<HaarFeature> generateHaarFeatures(byte frameWidth, byte frameHeight, int haarFeatureTypes){
    ArrayList<HaarFeature> generated = new ArrayList<HaarFeature>();
    if ((haarFeatureTypes & FEATURE_TYPE_1) != 0)
      generated.addAll(generateType1HaarFeatures(frameWidth, frameHeight));
    if ((haarFeatureTypes & FEATURE_TYPE_2) != 0)
      generated.addAll(generateType2HaarFeatures(frameWidth, frameHeight));
    if ((haarFeatureTypes & FEATURE_TYPE_3) != 0)
      generated.addAll(generateType3HaarFeatures(frameWidth, frameHeight));
    if ((haarFeatureTypes & FEATURE_TYPE_4) != 0)
      generated.addAll(generateType4HaarFeatures(frameWidth, frameHeight));
    if ((haarFeatureTypes & FEATURE_TYPE_5) != 0)
      generated.addAll(generateType5HaarFeatures(frameWidth, frameHeight));
    if ((haarFeatureTypes & FEATURE_TYPE_6) != 0)
      generated.addAll(generateType6HaarFeatures(frameWidth, frameHeight));
    return generated;
  }
  
  /**
   * Generates all types of HaarFeatures that can lie within a given frame 
   * @param frameWidth Width of the frame
   * @param frameHeight Height of the frame
   * @return A list of HaarFeatures that can be generated within the frame
   */
  public static ArrayList<HaarFeature> generateHaarFeatures(byte frameWidth, byte frameHeight){
    return generateHaarFeatures(frameWidth, frameHeight, FEATURE_TYPE_ALL);
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * ----
   * |01|
   * ----
   * The width and height of "0" and "1" are integers greater than 0.
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType1HaarFeatures(byte frameWidth, byte frameHeight){
    ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
    for (int x = 0; x < frameWidth - 1; x++){
      for (int splitX = x+1; splitX < frameWidth; splitX++){
        for (int x2 = splitX + 1; x2 <= frameWidth; x2++){
          for (int y = 0; y < frameHeight; y++){
            for (int y2 = y + 1; y2 <= frameHeight; y2++){
              Rectangle rec1 = new Rectangle(x, y, splitX - x, y2 - y);
              Rectangle rec2 = new Rectangle(splitX, y, x2 - splitX, y2 - y);
              list.add(new HaarFeature(new Rectangle[]{rec1}, new Rectangle[]{rec2}, frameWidth, frameHeight));
            }
          }
        }
      }
    }
    return list;
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * ----
   * |0|
   * |1|
   * ----
   * The width and height of "0" and "1" are integers greater than 0.
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType2HaarFeatures(byte frameWidth, byte frameHeight){
    ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
    for (int y = 0; y < frameHeight - 1; y++){
      for (int splitY = y+1; splitY < frameHeight; splitY++){
        for (int y2 = splitY + 1; y2 <= frameHeight; y2++){
          for (int x = 0; x < frameWidth; x++){
            for (int x2 = x + 1; x2 <= frameWidth; x2++){
              Rectangle rec1 = new Rectangle(x, y, x2 - x, splitY - y);
              Rectangle rec2 = new Rectangle(x, splitY, x2 - x, y2 - splitY);
              list.add(new HaarFeature(new Rectangle[]{rec1}, new Rectangle[]{rec2}, frameWidth, frameHeight));
            }
          }
        }
      }
    }
    return list;
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * ----
   * |010|
   * ----
   * The width and height of "0" and "1" are integers greater than 0.
   * The height of "0" and "1" are the same.
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType3HaarFeatures(byte frameWidth, byte frameHeight){
    ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
    for (int x = 0; x <= frameWidth-3; x++){
      for (int x2 = x + 2; x2 < frameWidth; x2++){
        for (int x0 = x + 1; x0 - x <= (x2 - x) / 2; x0++){
          for (int y = 0; y < frameHeight; y++){
            for (int y2 = y + 1; y2 < frameHeight + 1; y2++){
                Rectangle rec1 = new Rectangle(x, y, x0-x, y2 - y);
                Rectangle rec2 = new Rectangle(x0, y, x2+x+1-2*x0, y2 - y);
                Rectangle rec3 = new Rectangle(x2+x+1-x0, y, x0-x, y2 - y);
                list.add(new HaarFeature(new Rectangle[]{rec1, rec3}, new Rectangle[]{rec2}, frameWidth, frameHeight));
            }
          }
        }
      }
    }
    return list;
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * ---
   * |0|
   * |1|
   * |0|
   * ---
   * The width and height of "0" and "1" are integers greater than 0.
   * The width of "0" and "1" are the same.
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType4HaarFeatures(byte frameWidth, byte frameHeight){
	  ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
    for (int y = 0; y <= frameHeight-3; y++){
      for (int y2 = y + 2; y2 < frameHeight; y2++){
        for (int y0 = y + 1; y0 - y <= (y2 - y) / 2; y0++){
          for (int x = 0; x < frameWidth; x++){
            for (int x2 = x + 1; x2 < frameWidth + 1; x2++){
            	Rectangle rec1 = new Rectangle(x, y, x2 - x, y0-y);
            	Rectangle rec2 = new Rectangle(x, y0, x2 - x, y2+y+1-2*y0);
            	Rectangle rec3 = new Rectangle(x, y2+y+1-y0, x2 - x, y0-y);
                list.add(new HaarFeature(new Rectangle[]{rec1, rec3}, new Rectangle[]{rec2}, frameWidth, frameHeight));
            }
          }
        }
      }
    }
    return list;
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * -----
   * |131|
   * |202|
   * |131|
   * -----
   * The width and height of "0", "1", "2" and "3" are integers greater than 0 
   * "1", "2" and "3" are grouped against "0"
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType5HaarFeatures(byte frameWidth, byte frameHeight){
	  ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
	  for (int x = 0; x < frameWidth - 2; x++){
	  	for (int y = 0; y < frameHeight - 2; y++){
	  		for (int innerX = x + 1; innerX < frameWidth; innerX++){
	  			for (int innerY = y + 1; innerY < frameHeight; innerY++){
	  				for (int innerX2 = innerX + 1; innerX2 + (innerX - x) <= frameWidth; innerX2++){
	  					for (int innerY2 = innerY + 1; innerY2 + (innerY - y) <= frameHeight; innerY2++){
	  						Rectangle recInner = new Rectangle(innerX, innerY, innerX2 - innerX, innerY2 - innerY);
		  					Rectangle recFull = new Rectangle(x, y, 2 * (innerX - x) + (innerX2 - innerX),  2 * (innerY - y) + (innerY2 - innerY));
	                        list.add(new HaarFeature(new Rectangle[]{recInner, recInner}, new Rectangle[]{recFull}, frameWidth, frameHeight));
	                    }
	  				}
	  			}
	  		}
	  	}
	  }
    return list;
  }
  
  /**
   * Generates all HaarFeatures on the form (including the inverted ones):
   * ----
   * |10|
   * |01|
   * ----
   * The width and height of "0" and "1" are integers greater than 0 
   * @return A list of Haar features.
   */
  private static ArrayList<HaarFeature> generateType6HaarFeatures(byte frameWidth, byte frameHeight){
      ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();
      for (int x = 0; x < frameWidth; x++){
        for (int y = 0; y < frameHeight; y++){
          for (int x2 = x+1; 2*x2-x <= frameWidth; x2++){
            for (int y2 = y+1; 2*y2-y <= frameHeight; y2++){
              Rectangle recLT = new Rectangle(x, y, x2-x, y2-y); //left top
              Rectangle recRT = new Rectangle(x2, y, x2-x, y2-y); //right top
              Rectangle recLB = new Rectangle(x, y2, x2-x, y2-y); //left bottom
              Rectangle recRB = new Rectangle(x2, y2, x2-x, y2-y); //right bottom
              list.add(new HaarFeature(new Rectangle[]{recLT, recRB}, new Rectangle[]{recRT, recLB}, frameWidth, frameHeight));
            }
          }
        }
      }
    return list;
  }
}
