package dk.aau.cs.d501e13.anpi;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;

import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

/**
 * Image abstraction
 */
public class Image implements Cloneable {
  /**
   * Buffered image (RGB)
   */
  private BufferedImage input;
  
  /**
   * Gray scale image
   */
  private BufferedImage grayscale = null;
  
  /**
   * Raster for input image
   */
  private WritableRaster inputRaster = null;
  
  /**
   * Raster for gray scale image
   */
  private WritableRaster grayscaleRaster = null;
  
  /**
   * Integral image
   */
  private long[][] integralImage = null;
  
  /**
   * Root parent image
   */
  private Image root = null;
  
  /**
   * Region for sub-image
   */
  private Rectangle region = null;
  
  /**
   * Load an image
   * @param file Image file
   * @throws ImageReadException if unable to load image
   */
  public Image(File file) throws ImageReadException {
    try {
      input = ImageIO.read(file);
      if (input == null) {
        throw new ImageReadException("Unable to read file format: " + file);
      }
      try {
        Metadata metadata = ImageMetadataReader.readMetadata(file);
        ExifIFD0Directory exifDir = metadata.getDirectory(ExifIFD0Directory.class);
        if (exifDir == null) {
          throw new MetadataException("No exif IFD0 directory");
        }
        int orientation = exifDir.getInt(ExifIFD0Directory.TAG_ORIENTATION);
        switch (orientation) {
          case 3:
          case 6:
          case 8:
            BufferedImage rotated;
            AffineTransform at = new AffineTransform();
            if (orientation == 3) {
              rotated = new BufferedImage(input.getWidth(), input.getHeight(), input.getType());
              at.translate(input.getWidth() / 2, input.getHeight() / 2);
              at.rotate(Math.PI);
              at.translate(- input.getWidth() / 2, - input.getHeight() / 2);
            }
            else {
              at.translate(input.getHeight() / 2, input.getWidth() / 2);
              rotated = new BufferedImage(input.getHeight(), input.getWidth(), input.getType());
              if (orientation == 6) {
                at.rotate(Math.PI / 2.0);
              }
              else {
                at.rotate(3.0 * Math.PI / 2.0);
              }
              at.translate(- input.getWidth() / 2, - input.getHeight() / 2);
            }
            Graphics2D g = rotated.createGraphics();
            g.drawImage(input, at, null);
            g.dispose();
            input = rotated;
            break;
        }
      }
      catch (ImageProcessingException e) { }
      catch (MetadataException e) { }
      inputRaster = input.getRaster();
      region = new Rectangle(0, 0, input.getWidth(), input.getHeight());
    }
    catch (IOException e) {
      throw new ImageReadException("Unable to open file: " + file, e);
    }
  }
  
  /**
   * Load an image
   * @param filename Image file name
   * @throws ImageReadException if unable to load image
   */
  public Image(String filename) throws ImageReadException {
    this(new File(filename));
  }
  
  /**
   * Save image
   * @param file File object
   * @param type File type ("png" etc.)
   * @throws IOException if unable to save
   */
  public void save(File file, String type) throws IOException {
	convertToRoot();
    ImageIO.write(input, type, file);
  }

  /**
   * Save image
   * @param fileName File name
   * @param type File type ("png" etc.)
   * @throws IOException if unable to save
   */
  public void save(String fileName, String type) throws IOException {
    save(new File(fileName), type);
  }
  
  /**
   * Create image from buffered image
   * @param image Buffered image
   */
  public Image(BufferedImage image) {
    input = image;
    inputRaster = input.getRaster();
    region = new Rectangle(0, 0, input.getWidth(), input.getHeight());
  }
  
  /**
   * Create an empty canvas
   * @param width Width in pixels
   * @param height Height in pixels
   */
  public Image(int width, int height) {
    input = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    inputRaster = input.getRaster();
    region = new Rectangle(0, 0, width, height);
  }
  
  /**
   * Convert sub-image to root image
   */
  private void convertToRoot() {
    if (root != null) {
      BufferedImage bimg = new BufferedImage(region.width, region.height, input.getType());
      input = copyToBufferedImage(bimg);
      inputRaster = input.getRaster();
      root = null;
      region = new Rectangle(0, 0, region.width, region.height);
    }
    grayscale = null;
    grayscaleRaster = null;
    integralImage = null;
  }
  
  /**
   * Create graphics object for drawing
   * @return Graphics object
   */
  public Graphics2D createGraphics() {
    convertToRoot();
    return input.createGraphics();
  }
  
  /**
   * Create a writable raster for quick pixel access
   * @return Writable raster
   */
  public WritableRaster getRaster() {
    convertToRoot();
    return inputRaster;
  }
  
  /**
   * Create gray-scale buffered image
   */
  private void createGrayscale() {
    if (root != null) {
      if (root.grayscale == null) {
        root.createGrayscale();
      }
      grayscale = root.grayscale;
      grayscaleRaster = root.grayscaleRaster;
      return;
    }
    if (input.getType() == BufferedImage.TYPE_BYTE_GRAY) {
      grayscale = input;
      grayscaleRaster = inputRaster;
      return;
    }
    grayscale = new BufferedImage(input.getWidth(), input.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
    Graphics2D g = grayscale.createGraphics();
    g.drawImage(input, 0, 0, null);
    g.dispose();
    grayscaleRaster = grayscale.getRaster();
  }
  
  /**
   * Create integral image
   */
  private void createIntegralImage() {
    if (root != null) {
      if (root.integralImage == null) {
        root.createIntegralImage();
      }
      integralImage = root.integralImage;
      return;
    }
    int width = region.width;
    int height = region.height;
    if (grayscale == null) {
      createGrayscale();
    }
    Raster r = grayscaleRaster;
    integralImage = new long[width][height];
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        int value = r.getSample(region.x + x, region.y + y, 0);
        if (x == 0 && y == 0) {
          integralImage[0][0] = value;
        }
        else if (x == 0) {
          integralImage[0][y] = value + integralImage[0][y - 1];
        }
        else if (y == 0) {
          integralImage[x][0] = value + integralImage[x - 1][0];
        }
        else {
          integralImage[x][y] = value + integralImage[x - 1][y]
              + integralImage[x][y - 1] - integralImage[x - 1][y - 1];
        }
      }
    }
  }
  
  /**
   * Create edge detection image
   * @param edgeDetector An edge detector
   * @return Edges
   */
  public Image getEdges(EdgeDetector edgeDetector) {
    BufferedImage edges = new BufferedImage(region.width, region.height, BufferedImage.TYPE_BYTE_GRAY);
    WritableRaster edgesRaster = edges.getRaster();
    edgeDetector.calculate(getGrayscale().getBufferedImage().getRaster(), edgesRaster);
    return new Image(edges);
  }
  
  /**
   * @return Inverted image
   */
  public Image getInverted() {
    BufferedImage inverted = new BufferedImage(region.width, region.height, input.getType());
    WritableRaster invertedRaster = inverted.getRaster();
    int numBands = invertedRaster.getNumBands();
    for (int x = 0; x < region.width; x++) {
      for (int y = 0; y < region.height; y++) {
        for (int b = 0; b < numBands; b++) {
          int value = inputRaster.getSample(region.x + x, region.y + y, b);
          invertedRaster.setSample(x, y, b, 255 - value);
        }
      }
    }
    return new Image(inverted);
  }
  
  /**
   * @return Normalized image
   */
  public Image getNormalized() {
    int numBands = inputRaster.getNumBands();
    int[] min = new int[numBands];
    Arrays.fill(min, 255);
    int[] max = new int[numBands];
    Arrays.fill(max, 0);
    for (int x = 0; x < region.width; x++) {
      for (int y = 0; y < region.height; y++) {
        for (int b = 0; b < numBands; b++) {
          int value = inputRaster.getSample(region.x + x, region.y + y, b);
          if (value > max[b]) {
            max[b] = value;
          }
          if (value < min[b]) {
            min[b] = value;
          }
        }
      }
    }
    double[] a = new double[numBands];
    for (int b = 0; b < numBands; b++) {
      int diff = max[b] - min[b];
      a[b] = diff / 255.0;
    }
    BufferedImage normalized = new BufferedImage(region.width, region.height, input.getType());
    WritableRaster normalizedRaster = normalized.getRaster();
    for (int x = 0; x < region.width; x++) {
      for (int y = 0; y < region.height; y++) {
        for (int b = 0; b < numBands; b++) {
          int value = inputRaster.getSample(region.x + x, region.y + y, b);
          normalizedRaster.setSample(x, y, b, (value - min[b]) / a[b]);
        }
      }
    }
    return new Image(normalized);
  }
  
  /**
   * @return Binarized image
   */
  public Image getBinarized() {
    if (grayscaleRaster == null) {
      createGrayscale();
    }
    long sum = getSum(0, 0, region.width, region.height);
    long avg = sum / (region.width * region.height);
    BufferedImage normalized = new BufferedImage(region.width, region.height, BufferedImage.TYPE_BYTE_GRAY);
    WritableRaster normalizedRaster = normalized.getRaster();
    for (int x = 0; x < region.width; x++) {
      for (int y = 0; y < region.height; y++) {
        int value = grayscaleRaster.getSample(region.x + x, region.y + y, 0);
        if (value > avg) {
          normalizedRaster.setSample(x, y, 0, 255);
        }
        else {
          normalizedRaster.setSample(x, y, 0, 0);
        }
      }
    }
    return new Image(normalized);
  }
  
  /**
   * Make a copy of a buffered image
   * @param bim The buffered image
   * @return Copy of buffered image
   */
  private BufferedImage copyToBufferedImage(BufferedImage bim) {
    Graphics2D g = bim.createGraphics();
    g.drawImage(
      input, 0, 0, bim.getWidth(), bim.getHeight(),
      region.x, region.y, region.x + region.width, region.y + region.height,
      null
    );
    g.dispose();
    return bim;
  }
  
  /**
   * Get image as a Java buffered image
   * @return Buffered image
   */
  public BufferedImage getBufferedImage() {
    if (root == null) {
      return input;
    }
    BufferedImage bimg = new BufferedImage(region.width, region.height, input.getType());
    bimg = copyToBufferedImage(bimg);
    return bimg;
  }
  
  /**
   * @return Grayscale image
   */
  public Image getGrayscale() {
    if (root == null) {
      if (grayscale == null) {
        createGrayscale();
      }
      return new Image(grayscale);
    }
    BufferedImage bimg = new BufferedImage(region.width, region.height, BufferedImage.TYPE_BYTE_GRAY);
    bimg = copyToBufferedImage(bimg);
    return new Image(bimg);
  }
  
  /**
   * Get a sub image
   * @param region Region of image to return as a sub image
   * @return Sub image
   * @throws ImageCloneException if unable to clone
   */
  public Image getSubimage(Rectangle region) throws ImageCloneException {
    int width = root!=null ? root.getWidth() : getWidth();
    int height = root!=null ? root.getHeight() : getHeight();
    if(region.x + region.width > width
      || region.y + region.height > height){
      throw new ImageCloneException("Subimages goes out of boundss");
    }
    
    Image subimage;
    try {
      subimage = (Image)this.clone();
    }
    catch (CloneNotSupportedException e) {
      throw new ImageCloneException("Could not clone image", e);
    }
    subimage.region = new Rectangle(
      subimage.region.x + region.x,
      subimage.region.y + region.y,
      region.width,
      region.height
    );
    if (subimage.root == null) {
      subimage.root = this;
    }
    return subimage;
  }
  
  /**
   * Resize image. Edge detection data is not resized, i.e.
   * {@link #applyEdgeDetection(EdgeDetector)} must be called on the new image
   * @param width New width
   * @param height New height
   * @return Resized image
   */
  public Image getResized(int width, int height) {
    return getResized(width, height, false);
  }

  /**
   * Resize image. Edge detection data is not resized, i.e.
   * {@link #applyEdgeDetection(EdgeDetector)} must be called on the new image
   * @param width New width
   * @param height New height
   * @param keepAspectRatio Whether or not to preserve the aspect ratio
   * (resulting image may be smaller)
   * @return Resized image
   */
  public Image getResized(int width, int height, boolean keepAspectRatio) {
    if (keepAspectRatio) {
      double aspectRatio = (double)region.height / (double)region.width;
      int newHeight = (int)(width * aspectRatio);
      if (newHeight > height) {
        width = (int)(height / aspectRatio);
      }
      else {
        height = newHeight;
      }
    }
    Image resized = new Image(width, height);
    Graphics2D g = resized.input.createGraphics();
    g.drawImage(
        input, 0, 0, width, height,
        region.x, region.y, region.x + region.width, region.y + region.height,
        null
    );
    //g.drawImage(input, 0, 0, width, height, null);
    g.dispose();
    resized.inputRaster = resized.input.getRaster();
    if (grayscale != null) {
      resized.grayscale = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
      g = resized.grayscale.createGraphics();
      g.drawImage(
          grayscale, 0, 0, width, height,
          region.x, region.y, region.x + region.width, region.y + region.height,
          null
      );
//      g.drawImage(grayscale, 0, 0, width, height, null);
      g.dispose();
      resized.grayscaleRaster = resized.grayscale.getRaster();
    }
    return resized;
  }
  
  /**
   * @return Width of image in pixels
   */
  public int getWidth() {
    return region.width;
  }
  
  /**
   * @return Height of image in pixels
   */
  public int getHeight() {
    return region.height;
  }
  
  /**
   * Get red value of pixel
   * @param x X
   * @param y Y
   * @return Value from 0 to 255
   */
  public int getRed(int x, int y) {
    return inputRaster.getSample(region.x + x, region.y + y, 0);
  }
  
  /**
   * Get green value of pixel
   * @param x X
   * @param y Y
   * @return Value from 0 to 255
   */
  public int getGreen(int x, int y) {
    return inputRaster.getSample(region.x + x, region.y + y, 1);
  }

  /**
   * Get blue value of pixel
   * @param x X
   * @param y Y
   * @return Value from 0 to 255
   */
  public int getBlue(int x, int y) {
    return inputRaster.getSample(region.x + x, region.y + y, 2);
  }

  /**
   * Get grayscale value of pixel
   * @param x X
   * @param y Y
   * @return Value from 0 to 255
   */
  public int getGray(int x, int y) {
    if (grayscaleRaster == null) {
      createGrayscale();
    }
    return grayscaleRaster.getSample(region.x + x, region.y + y, 0);
  }

  /**
   * Set red value of pixel
   * @param x X
   * @param y Y
   * @param value Value from 0 to 255
   */
  public void setRed(int x, int y, int value) {
    inputRaster.setSample(region.x + x, region.y + y, 0, value);
  }

  /**
   * Set green value of pixel
   * @param x X
   * @param y Y
   * @param value Value from 0 to 255
   */
  public void setGreen(int x, int y, int value) {
    inputRaster.setSample(region.x + x, region.y + y, 1, value);
  }

  /**
   * Set blue value of pixel
   * @param x X
   * @param y Y
   * @param value Value from 0 to 255
   */
  public void setBlue(int x, int y, int value) {
    inputRaster.setSample(region.x + x, region.y + y, 2, value);
  }

  /**
   * Set grayscale value of pixel
   * @param x X
   * @param y Y
   * @param value Value from 0 to 255
   */
  public void setGray(int x, int y, int value) {
    if (grayscaleRaster == null) {
      createGrayscale();
    }
    grayscaleRaster.setSample(region.x + x, region.y + y, 0, value);
  }
  
  /**
   * Get integral image sum of pixel
   * @param x X
   * @param y Y
   * @return Sum of pixel
   */
  public long getSum(int x, int y) {
    if (integralImage == null) {
      createIntegralImage();
    }
    return integralImage[region.x + x][region.y + y];
  }
  
  /**
   * Get integral image sum of region
   * @param x X
   * @param y Y
   * @param width Width
   * @param height Height
   * @return Sum of pixels
   */
  public long getSum(int x, int y, int width, int height) {
    if (integralImage == null) {
      createIntegralImage();
    }
    x += region.x;
    y += region.y;
    int x1 = x - 1;
    int y1 = y - 1;
    int x2 = x1 + width;
    int y2 = y1 + height;
    if (x1 < 0 && y1 < 0) {
      return integralImage[x2][y2];
    }
    else if (x1 < 0) {
      return integralImage[x2][y2] - integralImage[x2][y1];
    }
    else if (y1 < 0) {
      return integralImage[x2][y2] - integralImage[x1][y2];
    }
    return integralImage[x2][y2] - integralImage[x1][y2] - integralImage[x2][y1]
        + integralImage[x1][y1];
  }
  
  /**
   * Get integral image sum of region
   * @param r Region
   * @return Sum of pixels
   */
  public long getSum(Rectangle r) {
    return getSum(r.x, r.y, r.width, r.height);
  }
  
  /**
   * @return Vertical histogram of image
   */
  public long[] getVHistogram() {
    long[] histogram = new long[region.width];
    for (int x = 0; x < region.width; x++) {
      histogram[x] = getSum(x, 0, 1, region.height);
    }
    return histogram;
  }

  /**
   * @return Horizontal histogram of image
   */
  public long[] getHHistogram() {
    long[] histogram = new long[region.height];
    for (int y = 0; y < region.height; y++) {
      histogram[y] = getSum(0, y, region.width, 1);
    }
    return histogram;
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Image)) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    
    Image other = (Image) obj;
    
    if (other.getWidth() != getWidth()) {
      return false;
    }
    if (other.getHeight() != getHeight()) {
      return false;
    }
    
    WritableRaster otherRaster = other.inputRaster;
    int bands = otherRaster.getNumBands();
    
    if (bands != inputRaster.getNumBands()) {
      return false;
    } 
    
    for (int x = 0; x < region.width; x++) {
      for (int y = 0; y < region.height; y++) {
        for (int b = 0; b < bands; b++) {
          if (inputRaster.getSample(region.x + x, region.y + y, b) 
              != otherRaster.getSample(other.region.x + x, other.region.y + y, b)){
            return false;
          }
        }
      }
    }
    return true;
  }
}
