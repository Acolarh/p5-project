package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 * An input neuron.
 */
public class InputNeuron extends Neuron {
  private static final long serialVersionUID = -705471495201263570L;
  /**
   * Constructor for class.
   */
  public InputNeuron() {
    super(null);
  }
  /**
   * A list of output neurons.
   */
  @SuppressWarnings("unused")
  private List<Connection> outputs = new ArrayList<Connection>();
  /**
   * The value associated with the neuron.
   */
  private double value;
  /**
   * Sets the value for a neuron.
   * @param value The value of the neuron.
   */
  public void setValue(double value) {
    this.value = value;
  }

  @Override
  public double calculate() {
    return value;
  }

}
