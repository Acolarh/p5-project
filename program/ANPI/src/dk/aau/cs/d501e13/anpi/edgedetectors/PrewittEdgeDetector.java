package dk.aau.cs.d501e13.anpi.edgedetectors;

import java.awt.image.Raster;
import java.awt.image.WritableRaster;

/**
 * Prewitt operator
 */
public class PrewittEdgeDetector extends DDOEdgeDetector {
  /**
   * Horizontal 3x3 kernel
   */
  private static int[][] kernelx = {{-1, 0, 1}, {-1, 0, 1}, {-1, 0, 1}};
  
  /**
   * Vertical 3x3 kernel
   */
  private static int[][] kernely = {{1, 1, 1}, {0, 0, 0}, {-1, -1, -1}};
  
  @Override
  public void calculate(Raster input, WritableRaster output) {
    calculate(input, output, kernelx, kernely);
  }

}
