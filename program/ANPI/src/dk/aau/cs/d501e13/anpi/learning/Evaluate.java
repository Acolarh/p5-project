package dk.aau.cs.d501e13.anpi.learning;

import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples;

/**
 * Interface to evaluate and stop a iterating learner
 * @param <T> Type of sample
 */
public interface Evaluate<T> {
  
  /**
   * An evaluator
   * @param <T> Type of sample
   */
  public interface Evaluator<T>{
  /**
   * Evaluate a classifier, and tell if it is good enough
   * @param classifier The classifier to evaluate
   * @param samples The TrainingSamples to evaluate upon
   * @return true if the learner should stop
   */
  boolean evaluate(BClassifier<T> classifier, T[] trainingSamples, boolean[] predicates);
  }
  
  /**
   * Set the evaluator
   * @param evaluator The evaluator object
   */
  public void setEvaluator(Evaluator<T> evaluator);
}
