package dk.aau.cs.d501e13.anpi;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

/**
 * A configuration object
 */
public class Config {
  /**
   * Map of keys/values
   */
  private Map<String, Object> map;
  
  /**
   * Construct empty configuration.
   */
  public Config() {
    map = new HashMap<String, Object>();
  }
  
  /**
   * Construct from file 
   * @param file YAML file
   * @throws IOException if unable to open/read file
   */
  @SuppressWarnings("unchecked")
  public Config(String file) throws IOException {
    Yaml yaml = new Yaml();
    FileInputStream fis = new FileInputStream(file);
    map = (Map<String, Object>) yaml.load(fis);
    fis.close();
  }
  
  /**
   * Construct from existing map
   * @param map Map
   */
  public Config(Map<String, Object> map) {
    this.map = map;
  }
  
  /**
   * Save configuration to YAML file
   * @param file YAML file
   * @return True on success, false on failure
   * @todo implement.
   */
  public boolean save(String file) {
    return false;
  }
  
  /**
   * Get value of key
   * @param key Key
   * @return Value
   */
  public Object get(String key) {
    return map.get(key);
  }
  
  /**
   * Whether or not a key is set
   * @param key Key
   * @return True if set, false if not
   */
  public boolean isSet(String key) {
    return map.containsKey(key);
  }
  
  /**
   * Get value of key as byte
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Byte value
   */
  public byte getByte(String key, byte defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Integer) {
      return ((Integer)val).byteValue();
    }
    return defaultValue;
  }
  
  /**
   * Get value of key as short
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Short value
   */
  public short getShort(String key, short defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Integer) {
      return ((Integer)val).shortValue();
    }
    return defaultValue;
  }

  /**
   * Get value of key as integer
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Integer value
   */
  public int getInt(String key, int defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Integer) {
      return (int)val;
    }
    return defaultValue;
  }

  /**
   * Get value of key as long
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Long value
   */
  public long getLong(String key, long defaultValue) {
    Object val = get(key);
    if (val != null) {
      if (val instanceof Integer) {
        return (int)val;
      }
      if (val instanceof Long) {
        return (long)val;
      }
      if (val instanceof BigInteger) {
        return ((BigInteger)val).longValue();
      }
    }
    return defaultValue;
  }

  /**
   * Get value of key as double
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Double value
   */
  public double getDouble(String key, double defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Number) {
      return ((Number)val).doubleValue();
    }
    return defaultValue;
  }

  /**
   * Get value of key as boolean
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Boolean value
   */
  public boolean getBoolean(String key, boolean defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Boolean) {
      return (boolean)val;
    }
    return defaultValue;
  }

  /**
   * Get value of key as string
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return String value
   */
  public String getString(String key, String defaultValue) {
    Object val = get(key);
    if (val != null) {
      if (val instanceof String) {
        return (String)val;
      }
      return val.toString();
    }
    return defaultValue;
  }

  /**
   * Get value of key as a configuration (sub-configuration)
   * @param key Key
   * @param defaultValue Default value (if key not set)
   * @return Config value
   */
  @SuppressWarnings("unchecked")
  public Config getConfig(String key, Config defaultValue) {
    Object val = get(key);
    if (val != null && val instanceof Map) {
      return new Config((Map<String, Object>)val);
    }
    return defaultValue;
  }

  /**
   * Get value of key as byte
   * @param key Key
   * @return Byte value
   */
  public byte getByte(String key) {
    return getByte(key, (byte)0);
  }

  /**
   * Get value of key as short
   * @param key Key
   * @return Short value
   */
  public short getShort(String key) {
    return getShort(key, (short)0);
  }

  /**
   * Get value of key as Integer
   * @param key Key
   * @return Integer value
   */
  public int getInt(String key) {
    return getInt(key, 0);
  }

  /**
   * Get value of key as Long
   * @param key Key
   * @return Long value
   */
  public long getLong(String key) {
    return getLong(key, 0);
  }

  /**
   * Get value of key as double
   * @param key Key
   * @return Double value
   */
  public double getDouble(String key) {
    return getDouble(key, Double.NaN);
  }

  /**
   * Get value of key as boolean
   * @param key Key
   * @return Boolean value
   */
  public boolean getBoolean(String key) {
    return getBoolean(key, false);
  }

  /**
   * Get value of key as string
   * @param key Key
   * @return String value
   */
  public String getString(String key) {
    return getString(key, "");
  }

  /**
   * Get value of key as a configuration (sub-configuration)
   * @param key Key
   * @return Config value
   */
  public Config getConfig(String key) {
    return getConfig(key, new Config());
  }
}
