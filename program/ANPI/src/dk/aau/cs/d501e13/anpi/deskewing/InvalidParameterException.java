package dk.aau.cs.d501e13.anpi.deskewing;

public class InvalidParameterException extends Exception  {
 
  private static final long serialVersionUID = -364386737310151385L;

  public InvalidParameterException() {
    // TODO Auto-generated constructor stub
  }
  
  public InvalidParameterException(String message) {
    super(message);
  }

  public InvalidParameterException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public InvalidParameterException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public InvalidParameterException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }
} 
