package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;

/**
 * Predicts if a sub-image is a plate or not based on density variance global feature.
 */
public class DensityVarianceFeature implements BClassifier<Image> {
  
  private static final long serialVersionUID = -3550114486010727328L;
  /**
   * The number of sub images split vertically (the x-axis).
   */
  private int splitVertical;
  /**
   * The number of sub images split horizontally (the y-axis).
   */
  private int splitHorizontal;
  /**
   * The threshold.
   */
  private double threshold = 0.5;
  
  /**
   * Constructor.
   * @param v The x-axis.
   * @param h The y-axis.
   */
  public DensityVarianceFeature(int v, int h) {
    this.splitVertical = v;
    this.splitHorizontal = h;
  }
  
  /**
   * @return The number of sub images, counting vertically.
   */
  public int getSplitVertical() {
    return splitVertical;
  }

  /**
   * Set the number of sub images, counting vertically.
   * @param sImageVer The number of sub images.
   */
  public void setSplitVertical(int v) {
    this.splitVertical = v;
  }

  /**
   * @return The number of sub images, counting horizontally.
   */
  public int getSplitHorizontal() {
    return splitHorizontal;
  }

  /**
   * Set the number of sub images, counting horizontally.
   * @param sImageHor The number of sub images.
   */
  public void setSplitHorizontal(int h) {
    this.splitHorizontal = h;
  }

  /**
   * Set the threshold.
   * @param threshold The threshold.
   */
  public void setThreshold(double threshold) {
    this.threshold = threshold;
  }
  
  /**
   * @return The threshold.
   */
  public double getThreshold() {
    return this.threshold;
  }
  
  /**
   * Calculates the density variance.
   * @param img The image to calculate on.
   * @return The density variance.
   * @throws InvalidParameterException
   */
  public double getDensityVariance(Image img) throws InvalidParameterException {
    if(splitVertical < 0 || splitHorizontal < 0)
      throw new InvalidParameterException("The number of sub-images on the x-axis or y-axis is set below 0");
    
    // Variables for the entire image.
    double pix = img.getWidth() * img.getHeight();
    double gradientStrength = img.getSum(0, 0, img.getWidth(), img.getHeight()) / pix;
    double sumGradientStrength = 0;
    // Variables for the sub images.
    int wSplitImg = (img.getWidth() / splitVertical);
    int hSplitImg = (img.getHeight() / splitHorizontal);
    double pixSplitImg = wSplitImg * hSplitImg;
    double splitGradientStrength = 0;
    
    for(int j = 0; j < splitHorizontal; j++) {
      for(int i = 0; i < splitVertical; i++) {  
        splitGradientStrength = img.getSum((i * wSplitImg), (j * hSplitImg), 
          wSplitImg, hSplitImg) / pixSplitImg;
        sumGradientStrength += Math.abs(splitGradientStrength - gradientStrength); 
      }
    }
    return gradientStrength > 0 ? 
      sumGradientStrength / ((splitVertical * splitHorizontal) * gradientStrength) :
      sumGradientStrength / (splitVertical * splitHorizontal);
    
  }
  
  @Override
  public boolean predict(Image img) {
    double densityVariance = 0.0;
    try {
      densityVariance = getDensityVariance(img);
    }
    catch (InvalidParameterException e) {
      e.printStackTrace();
      System.exit(10);
    }
    
    // can't remember if densityVariance >= threshold or densityVariance <= threshold
    return densityVariance >= threshold;
  }
}
