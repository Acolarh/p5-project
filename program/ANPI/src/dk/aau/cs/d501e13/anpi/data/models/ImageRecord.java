package dk.aau.cs.d501e13.anpi.data.models;

import java.sql.ResultSet;
import java.sql.SQLException;

import dk.aau.cs.d501e13.anpi.data.Record;

/**
 * Record for table images
 */
public class ImageRecord extends Record {
  private static final long serialVersionUID = -9037098567673806006L;

  /**
   * Id
   */
  public int id;
  
  /**
   * Image name
   */
  public String name;
  
  /**
   * The set this image belongs to
   */
  public int set;

  public static final int SET_UNSET = 0;
  public static final int SET_TRAINING = 1;
  public static final int SET_TEST = 2;
  
  /**
   * Numberplate model
   */
  public transient NumberplateModel numberplateModel;
  
  /**
   * Construct from data
   * @param model Model
   * @param id Id
   * @param name Image name
   * @param set One of the constants (0, 1, or 2)
   */
  public ImageRecord(ImageModel model, int id, String name, int set) {
    super(model);
    this.id = id;
    this.name = name;
    this.set = set;
    // Dependencies
    numberplateModel = (NumberplateModel)model.getDb().getModel("NumberplateModel");
  }
  
  /**
   * Construct from result set
   * @param model Model
   * @param result Result set
   * @throws SQLException on SQL error
   */
  public ImageRecord(ImageModel model, ResultSet result) throws SQLException {
    this(model, result.getInt("id"), result.getString("name"), result.getInt("dataset"));
  }

  /**
   * Find all associated number plates
   * @return Array of number plate records
   * @throws SQLException on SQL error
   */
  public NumberplateRecord[] getNumberplates() throws SQLException {
    return numberplateModel.findByImageId(id);
  }
  
  @Override
  public void save() throws SQLException {
    model.execute("save", name, set, id);
  }
  
  @Override
  public void delete() throws SQLException {
    model.execute("delete", id);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ImageRecord)) {
      return false;
    }
    ImageRecord r = (ImageRecord)obj;
    return r.id == id;
  }
  
}
