package dk.aau.cs.d501e13.anpi.learning;

/**
 * Learners implementing this interface can quick save result
 * @param <T> Type of training sample
 */
public interface QuickSave<T> {
  /**
   * A quick saver
   * @param <T> Type of training sample
   */
  public interface QuickSaver<T> {
    /**
     * Save a classifier
     * @param classifier Classifier
     */
    public void saveClassifier(BClassifier<T> classifier);
  }
  
  /**
   * Set quick saver
   * @param quickSaver Quick saver
   */
  public void setQuickSaver(QuickSaver<T> quickSaver);
}
