package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

/**
 * A neuron / unit with associated methods.
 */
public class Neuron implements Serializable {
  private static final long serialVersionUID = -6301744530207513790L;
  
  /**
   * Static count for giving unique ids.
   */
  private static int count = 0;
  
  /**
   * Gives a unique id for every neuron.
   */
  private int id = count++;
  
  /**
   * The error.
   */
  private double error = 1;
  
  /**
   * A previously saved value of the neuron.
   */
  private Double savedResult = null;
  
  /**
   * A list of the input neurons.
   */
  private List<Connection> inputs = new ArrayList<Connection>();
  
  /**
   * A list of the output neurons.
   */
  private List<Connection> outputs = new ArrayList<Connection>();
  
  /**
   * A specified transfer function.
   */
  private TransferFunction transferFunction;
  
  /*
   * A connection to an input node with constant value 1
   */
  private Connection threshold;

  /**
   * @return The list of input neurons.
   */
  public List<Connection> getInputs() {
    return inputs;
  }

  /**
   * @return The list of output neurons.
   */
  public List<Connection> getOutputs() {
    return outputs;
  }

  /**
   * @return The error.
   */
  public double getError() {
    return error;
  }
  
  /**
   * @return The threshold.
   */
  public double getThreshold() {
    return threshold == null ? 0 : threshold.weight;
  }

  /**
   * @param error Set a new error value.
   */
  public void setError(double error) {
    this.error = error;
  }

  /**
   * Constructor for class.
   * @param transferFunction The activation function.
   */
  public Neuron(TransferFunction transferFunction) {
    this.transferFunction = transferFunction;
  }
  
  /**
   * Constructor for class.
   * @param transferFunction  The activation function.
   * @param threshold The threshold for the neuron
   */
  public Neuron(TransferFunction transferFunction, double threshold) {
    this.transferFunction = transferFunction;
    setThreshold(threshold);
  }

  /**
   * @return The transfer function.
   */
  public TransferFunction getTransferFunction() {
    return transferFunction;
  }

  /**
   * Add a new Connection to the list of inputs. Weight is set to 1.0 by default.
   * @param neuron The neuron which will be added to the list of inputs.
   */
  public void addInput(Neuron neuron) {
    addInput(neuron, Math.random() - 0.5);
  }

  /**
   * Add a new Connection to the list of inputs.
   * @param neuron The neuron which will be added to the list of inputs.
   * @param weight The arc's weight to be added.
   */
  public void addInput(Neuron from, double weight) {
    Connection c = new Connection(from, this, weight);
    from.addConnection(c);
    inputs.add(c);
  }
  
  /**
   * 
   * Add a new Connection to the list of inputs.
   * @param neuron The neuron which will be added to the list of inputs.
   * @param weight The arc's weight to be added.
   * @param learningRate The learning rate.
   */
  public void addInput(Neuron from, double weight, double learningRate) {
    Connection c = new Connection(from, this, weight, learningRate);
    from.addConnection(c);
    inputs.add(c);
  }
  
  /**
   * 
   * Add a new Connection to the list of inputs.
   * @param neuron The neuron which will be added to the list of inputs.
   * @param weight The arc's weight to be added.
   * @param learningRate The learning rate.
   * @param momentum The momentum.
   */
  public void addInput(Neuron from, double weight, double learningRate, double momentum) {
    Connection c = new Connection(from, this, weight, learningRate, momentum);
    from.addConnection(c);
    inputs.add(c);
  }

  /**
   * Add a connection. If the connection is incoming, the connection will be
   * added to inputs. If the connection is outgoing, the connection will be
   * added to outputs.
   * @param connection The connection to be added.
   */
  public void addConnection(Connection connection) {
    if (connection.to == this)
      inputs.add(connection);
    else if (connection.from == this)
      outputs.add(connection);
  }

  /**
   * When weight is updated.
   */
  public void flush() {
    savedResult = null;
  }

  /**
   * Calculate the associated value of a given neuron.
   * @return The value associated with a neuron.
   */
  public double calculate() {
    if (savedResult != null)
      return savedResult;

    double result = 0.0;

    for (Connection c : inputs) {
      result += c.from.calculate() * c.weight;
    }
    return savedResult = transferFunction.activate(result);
  }
  
  /**
   * Adds an input neuron with constant value 1 and a connection with weight.
   * @param val The weight.
   */
  public void setThreshold(double val){
    if (threshold == null){
      InputNeuron tNeuron = new InputNeuron();
      tNeuron.setValue(1);
      Connection con = new Connection(tNeuron, this, val);
      addConnection(con);
      tNeuron.addConnection(con);  
      threshold = con;
    }
    else
      threshold.weight = val;
  }

  @Override
  public int hashCode() {
    return id;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Neuron) {
      return obj.hashCode() == id;
    }
    return false;
  }

}
