package dk.aau.cs.d501e13.anpi.learning.strong;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.Evaluate;

/**
 * The image evaluator.
 */
public class ImageEvaluator implements Evaluate.Evaluator<Image> {
  /**
   * Minimum positive ratio.
   */
  private double minPosRatio;
  
  /**
   * Minimum negative ratio.
   */
  private double minNegRatio;

  /**
   * Constructor.
   * @param posRatio Positive ratio.
   * @param negRatio Negative ratio.
   */
  public ImageEvaluator(double posRatio, double negRatio){
    this.minPosRatio = posRatio;
    this.minNegRatio = negRatio;
  }
  
  
  @Override
  public boolean evaluate(BClassifier<Image> classifier, Image[] trainingSamples, boolean[] predicates) {
    int positives = 0;
    int negatives = 0;
    int falsePositives = 0;
    int falseNegatives = 0;
    
    for(int i=0; i<trainingSamples.length; i++){
      if(classifier.predict(trainingSamples[i])){
        if(predicates[i])
          positives++;
        else
          falsePositives++;
      }
      else{
        if(predicates[i])
          falseNegatives++;
        else
          negatives++;
      }
    }
    System.out.println("Positives: " + positives);
    System.out.println("Negatives: " + negatives);
    System.out.println("False positives: " + falsePositives);
    System.out.println("False negatives: " + falseNegatives);

    double posRatio = (double)positives / (positives + falseNegatives);
    double negRatio = (double)negatives / (negatives + falsePositives);
    System.out.println("Ratios: " + posRatio + " / " + negRatio);
    return posRatio >= minPosRatio && negRatio >= minNegRatio;
  }

}
