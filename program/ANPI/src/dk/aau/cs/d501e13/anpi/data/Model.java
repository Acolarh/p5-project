package dk.aau.cs.d501e13.anpi.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 * A model/representation of a single database table
 */
public abstract class Model {
  /**
   * Map of prepared statements
   */
  private HashMap<String, PreparedStatement> statements = new HashMap<String, PreparedStatement>();
  
  /**
   * Current connection
   */
  private Connection connection;
  
  /**
   * Current table name
   */
  private String table;
  
  /**
   * Database object
   */
  private Database db;

  /**
   * Constructor
   * @param db Database object
   * @param table Table name
   * @param columns Array of column names for use in default insert-query, has
   * to at least include columns without default values
   * @throws SQLException on SQL error
   */
  public Model(Database db, String table, String[] columns) throws SQLException {
    this.db = db;
    connection = db.getConnection();
    this.table = db.tableName(table);
    
    String sql = "INSERT INTO " + this.table + " (";
    String values = "";
    for (int i = 0; i < columns.length; i++) {
      sql += columns[i];
      values += "?";
      if (i < columns.length - 1) {
        sql += ", ";
        values += ", ";
      }
    }
    sql += ") VALUES (" + values + ")";
    prepare("create", sql, true);
    
    prepare("truncate", "TRUNCATE " + this.table);
    
    db.addModel(this);
    prepare("all", "SELECT * FROM " + this.table);
    prepare("count", "SELECT COUNT(*) FROM " + this.table);
  }
  
  /**
   * @return The name of the table
   */
  public String name() {
    return table;
  }
  
  /**
   * @return The Database object
   */
  public Database getDb() {
    return db;
  }
  
  /**
   * Return all records in table
   * @return List of records
   * @throws SQLException on SQL error
   */
  public Record[] all() throws SQLException {
    return queryMultiple("all");
  }
  
  /**
   * Get number of records in table
   * @return Number of records
   * @throws SQLException on SQL error
   */
  public int count() throws SQLException {
    PreparedStatement s = statement("count");
    ResultSet r = s.executeQuery();
    int number = 0;
    if (r.next()) {
      number = r.getInt(1);
    }
    r.close();
    return number;
  }
  
  /**
   * Execute the default insert-query
   * @param values List of values for insert query (must match list of columns
   * passed to the constructor)
   * @return The auto-incremented insert id if available, 0 if not
   * @throws SQLException on SQL error
   */
  protected int executeCreate(Object ... values) throws SQLException {
    PreparedStatement s = statement("create");
    for (int i = 1; i <= values.length; i++) {
      s.setObject(i, values[i - 1]);
    }
    s.execute();
    ResultSet r = s.getGeneratedKeys();
    int id = 0;
    if (r.next()) {
      id = r.getInt(1);
    }
    r.close();
    return id;
  }
  
  /**
   * Empty the table
   * @throws SQLException on SQL error
   */
  public void truncate() throws SQLException {
    execute("truncate");
  }
  
  /**
   * Make a single record object
   * @param r Result set from PreparedStatement.executeQuery()
   * @return A single record or null if empty ResultSet
   * @throws SQLException on SQL error
   */
  public abstract Record makeRecord(ResultSet r) throws SQLException;
  
  /**
   * Make an array of record objects
   * @param r Result set from PreparedStatement.executeQuery()
   * @return An array of record objects in ResultSet
   * @throws SQLException on SQL error
   */
  public abstract Record[] makeRecordList(ResultSet r) throws SQLException;
  
  /**
   * Prepare a statement
   * @param statementName Name of statement
   * @param sql SQL (? for placeholders)
   * @param returnKeys Whether or not to return auto incremented keys (needed
   * for INSERT-queries)
   * @throws SQLException on SQL error
   */
  public void prepare(String statementName, String sql, boolean returnKeys) throws SQLException {
    if (returnKeys) {
      statements.put(statementName, connection.prepareStatement(
          sql,
          Statement.RETURN_GENERATED_KEYS
      ));
    }
    else {
      statements.put(statementName, connection.prepareStatement(sql));
    }
  }
  
  /**
   * Prepare a statement
   * @param statementName Name of statement
   * @param sql SQL (? for placeholders)
   * @throws SQLException on SQL error
   */
  public void prepare(String statementName, String sql) throws SQLException {
    prepare(statementName, sql, false);
  }
  
  /**
   * Return a previously prepared statement
   * @param name Name of statement
   * @return Prepared statement
   */
  public PreparedStatement statement(String name) {
    return statements.get(name);
  }
  
  /**
   * Execute a prepared statement
   * @param statementName Name of statement
   * @param values Values to use in prepared statement, number of values must
   * match number of placeholders in prepared statement
   * @throws SQLException on SQL error
   */
  public void execute(String statementName, Object ... values) throws SQLException {
    PreparedStatement s = statement(statementName);
    for (int i = 1; i <= values.length; i++) {
      s.setObject(i, values[i - 1]);
    }
    s.execute();
  }
  
  /**
   * Execute a prepared statement, and return the first cell as an integer
   * @param statementName Name of statement
   * @param values Values to use in prepared statement, number of values must
   * match number of placeholders in prepared statement
   * @return A number
   * @throws SQLException on SQL error
   */
  public int queryCount(String statementName, Object ... values) throws SQLException {
    PreparedStatement s = statement(statementName);
    for (int i = 1; i <= values.length; i++) {
      s.setObject(i, values[i - 1]);
    }
    ResultSet r = s.executeQuery();
    int number = 0;
    if (r.next()) {
      number = r.getInt(1);
    }
    r.close();
    return number;
  }
  
  /**
   * Execute a prepared statement, and return one record
   * @param statementName Name of statement
   * @param values Values to use in prepared statement, number of values must
   * match number of placeholders in prepared statement
   * @return One record or null if none matches query
   * @throws SQLException on SQL error
   */
  public Record querySingle(String statementName, Object ... values) throws SQLException {
    PreparedStatement s = statement(statementName);
    for (int i = 1; i <= values.length; i++) {
      s.setObject(i, values[i - 1]);
    }
    return makeRecord(s.executeQuery());
  }

  /**
   * Execute a prepared statement, and return one record
   * @param statementName Name of statement
   * @param values Values to use in prepared statement, number of values must
   * match number of placeholders in prepared statement
   * @return An array of records
   * @throws SQLException on SQL error
   */
  public Record[] queryMultiple(String statementName, Object ... values) throws SQLException {
    PreparedStatement s = statement(statementName);
    for (int i = 1; i <= values.length; i++) {
      s.setObject(i, values[i - 1]);
    }
    return makeRecordList(s.executeQuery());
  }
  
  /**
   * Execute a raw SQL query on database
   * @param sql SQL query
   * @throws SQLException on SQL error
   */
  public void executeRaw(String sql) throws SQLException {
    connection.prepareStatement(sql).execute();
  }
  
  /**
   * Execute a raw SQL query on database and return a ResultSet
   * @param sql SQL query
   * @return A result set
   * @throws SQLException on SQL error
   */
  public ResultSet queryRaw(String sql) throws SQLException {
    return connection.prepareStatement(sql).executeQuery();
  }
}
