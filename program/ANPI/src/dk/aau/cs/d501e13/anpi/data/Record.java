package dk.aau.cs.d501e13.anpi.data;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * A single table record
 */
public abstract class Record implements Serializable {
  private static final long serialVersionUID = -4839086472944290957L;
  
  /**
   * The model
   */
  protected transient Model model;
  
  /**
   * Constructor
   * @param model Model
   */
  public Record(Model model) {
    this.model = model;
  }
  
  /**
   * @return The model
   */
  public Model getModel() {
    return model;
  }
  
  /**
   * Save record in its current state
   * @throws SQLException on SQL error
   */
  public abstract void save() throws SQLException;
  
  /**
   * Delete record
   * @throws SQLException on SQL error
   */
  public abstract void delete() throws SQLException;
}
