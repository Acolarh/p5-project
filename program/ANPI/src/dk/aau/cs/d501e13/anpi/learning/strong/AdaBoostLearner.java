package dk.aau.cs.d501e13.anpi.learning.strong;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.Learner;
import dk.aau.cs.d501e13.anpi.learning.LearnerLoader;
import dk.aau.cs.d501e13.anpi.learning.QuickSave;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.boosting.AdaBooster;

/**
 * A learner using AdaBoost boosting algorithm
 */
public class AdaBoostLearner implements Learner<Image>, QuickSave<Image> {
  /**
   * Learner loader
   */
  private LearnerLoader loader;
  
  /**
   * Number of boost iterations
   */
  private int iterations = 0;
  
  /**
   * Weak learner to boost
   */
  private WeakLearner<Image> weakLearner = null;
  
  /**
   * Quick saver
   */
  private QuickSaver<Image> quickSaver = null;
  
  /**
   * Construct
   * @param loader Learner loader
   */
  public AdaBoostLearner(LearnerLoader loader) {
    this.loader = loader;
  }
  
  @Override
  public void configure(Config conf) {
    iterations = conf.getInt("iterations");
    String weakLearnerName = conf.getString("weakLearner");
    Learner<Image> learner = loader.getLearner(weakLearnerName);
    if (learner == null || !(learner instanceof WeakLearner)) {
      System.out.println("ERROR: Weak learner '" + weakLearnerName + "' not found");
      return;
    }
    weakLearner = (WeakLearner<Image>)learner;
    weakLearner.configure(conf.getConfig("weakLearnerConfig"));
  }

  @Override
  public BClassifier<Image> learn(Image[] trainingSamples, boolean[] predicates) throws InvalidTrainingSamplesException {
    AdaBooster<Image> booster = new AdaBooster<Image>(iterations);
    booster.setEvaluator(new ImageEvaluator(1.0, 1.0)); //Never stops it, just for the debug info
    if (quickSaver != null) {
      booster.setQuickSaver(quickSaver);
    }
    return booster.boost(trainingSamples, predicates, weakLearner);
  }
  
  @Override
  public void setQuickSaver(QuickSaver<Image> quickSaver) {
    this.quickSaver = quickSaver;
  }

}
