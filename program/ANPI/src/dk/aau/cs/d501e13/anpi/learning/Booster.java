package dk.aau.cs.d501e13.anpi.learning;

/**
 * A boosting algorithm for boosting weak learners
 * @param <T1> Sample type
 */
public interface Booster<T1> {
  /**
   * Boost learner
   * @param trainingSamples Array of training samples
   * @param predicates Array of predicates
   * @param weakLearner Weak learner
   * @return A strong classifier
   * @throws InvalidTrainingSamplesException if trainingSamples and predicates
   * differ in size
   */
  public ThresholdedClassifier<T1> boost(T1[] trainingSamples, boolean[] predicates,
    WeakLearner<T1> weakLearner) throws InvalidTrainingSamplesException;
}
