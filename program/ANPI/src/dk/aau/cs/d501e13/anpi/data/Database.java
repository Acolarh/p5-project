package dk.aau.cs.d501e13.anpi.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import dk.aau.cs.d501e13.anpi.Config;

/**
 * Creating and maintaining a database connection.
 */
public class Database {
  /**
   * Database properties
   */
  private Properties properties;
  
  /**
   * Current connection
   */
  private Connection connection;
  
  /**
   * Table prefix
   */
  private String tablePrefix = "";
  
  /**
   * Models
   */
  private HashMap<String, Model> models = new HashMap<String, Model>();
  
  /**
   * Crate a connection based on a properties-object. Expects the following
   * properties keys: "driverClass", "driverName", "server", "database", "user"
   * and "pass". "port" and "tablePrefix" are optional.
   * @param properties Properties
   * @throws ClassNotFoundException if database driver not found
   * @throws SQLException if connection failed
   */
  public Database(Properties properties) throws ClassNotFoundException, SQLException {
    this.properties = properties;
    open();
  }
  
  /**
   * Crate a connection based on a database-object.
   * @param conf The config file
   * @throws ClassNotFoundException if database driver not found
   * @throws SQLException if connection failed
   */
  public Database(Config conf) throws ClassNotFoundException, SQLException {
    Class.forName(conf.getString("driverClass"));
    String driver = conf.getString("driverName");
    String server = conf.getString("server");
    String port = conf.getString("port", "3306");
    String db = conf.getString("database");
    String url = "jdbc:" + driver + "://" + server + ":" + port + "/" + db;
    String user = conf.getString("user");
    String pass = conf.getString("pass");
    tablePrefix = conf.getString("tablePrefix", "");
    connection = DriverManager.getConnection(url, user, pass);
  }
  
  /**
   * Create a connection based on a properties file path
   * @param propertiesFile Path to file
   * @throws IOException if file does not exist or is unreadable
   * @throws ClassNotFoundException if database driver not found
   * @throws SQLException if connection failed
   */
  public Database(String propertiesFile) throws IOException, ClassNotFoundException, SQLException {
    File file = new File(propertiesFile);
    FileInputStream fis = new FileInputStream(file);
    properties = new Properties();
    properties.load(fis);
    fis.close();
    open();
  }
  
  /**
   * Open connection
   * @throws ClassNotFoundException on driver not found
   * @throws SQLException on SQL error
   */
  private void open() throws ClassNotFoundException, SQLException {
    Class.forName(properties.getProperty("driverClass"));
    String driver = properties.getProperty("driverName");
    String server = properties.getProperty("server");
    String port = properties.getProperty("port", "3306");
    String db = properties.getProperty("database");
    String url = "jdbc:" + driver + "://" + server + ":" + port + "/" + db;
    String user = properties.getProperty("user");
    String pass = properties.getProperty("pass");
    tablePrefix = properties.getProperty("tablePrefix", "");
    connection = DriverManager.getConnection(url, user, pass);
  }
  
  /**
   * Close connection
   * @throws SQLException on SQL error
   */
  public void close() throws SQLException {
    connection.close();
  }
  
  /**
   * Add model (called by Model constructor)
   * @param model Model object
   */
  public void addModel(Model model) {
    models.put(model.getClass().getSimpleName(), model);
  }
  
  /**
   * Get model 
   * @param modelName Model name
   * @return Model object
   */
  public Model getModel(String modelName) {
    return models.get(modelName);
  }
  
  /**
   * Get table name with prefix
   * @param table Table
   * @return Table name
   */
  public String tableName(String table) {
    return tablePrefix + table;
  }
  
  /**
   * Get connection object
   * @return Connection object for current connection
   */
  public Connection getConnection() {
    return connection;
  }
}
