package dk.aau.cs.d501e13.anpi.testing;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.detector.Detector;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.NeuralNetworkClassifier;
import dk.aau.cs.d501e13.anpi.learning.strong.NeuralNetworkLearner;


public class DetectionTest {
  
  private static long start;
  
  private static class VideoPanel extends JPanel {
    
    private BufferedImage image = null;
    
    public VideoPanel() {
    }
    
    public VideoPanel(BufferedImage image) {
      this.image = image;
    }
    
    public void setImage(BufferedImage image) {
      this.image = image;
      repaint();
    }
    
    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }
  }
  
  
  public static BufferedImage intBuf(Image i) {
    BufferedImage im = new BufferedImage(i.getWidth(), i.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
    WritableRaster r = im.getRaster();
    double high = i.getSum(i.getWidth() - 1, i.getHeight() - 1);
    for (int x = 0; x < i.getWidth(); x++) {
      for (int y = 0; y < i.getHeight(); y++) {
        double value = i.getSum(x, y) / high;
        r.setSample(x, y, 0, (int)(255 * value));
      }
    }
    return im;
  }

  public static void main(String[] args) throws ClassNotFoundException, IOException, SQLException, ImageCloneException, ClassifiersLoadException {
    try {
      if (args.length != 4) {
        System.out.println("usage: tester CONFIG_FILE FORMAT START END");
        System.out.println("example: tester my.yml \"CAM%05d.jpg\" 200 250");
        System.exit(1);
      }
//      String format = "C:/Users/Kent/Desktop/p5-nummerplader/CAM%05d.jpg";
      String format = args[1];
      int start = Integer.parseInt(args[2]);
      int end = Integer.parseInt(args[3]);
      
      
//      Image test2 = new Image("C:/Users/Kent/Desktop/p5-nummerplader/elias (36).jpg");
      // 127 and 130 !!!!
      // elias (36).JPG er heller ikke ringe
      JFrame f = new JFrame("Image test");
      
      Config conf = new Config(args[0]);
      
      System.out.println("Loading classifiers...");

      Anpi anpi = new Anpi(conf);
      
      Detector d = new Detector(conf);
      
      BClassifier<Image> classifier = d.getClassifier();
      
      if (classifier instanceof NeuralNetworkClassifier) {
        if (((NeuralNetworkClassifier)classifier).nnl == null) {
          System.out.println("Fixing incomplete classifier...");
          ((NeuralNetworkClassifier)classifier).nnl = new NeuralNetworkLearner();
          ((NeuralNetworkClassifier)classifier).nnl.configure(conf.getConfig("learnerConfig"));
        }
      }
      
      VideoPanel p = new VideoPanel();
      f.add(p);
      f.setSize(800, 600);
      f.setVisible(true);
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      for (int i = start; i <= end; i++) {
        Image input = new Image(String.format(format, i));
        Image resized = input;
        if (conf.getBoolean("detectorResize")) {
          resized = input.getResized(conf.getInt("detectorWidth"), conf.getInt("detectorHeight"), true);
        }
        Image other = resized;
        if (conf.getBoolean("useEdgeDetection")) {
          resized = resized.getEdges(anpi.getEdgeDetector(
            conf.getString("edgeDetector", "PrewittEdgeDetector")
          ));
        }


        reset("Detecting plates");
        List<Rectangle> rectangles = d.detectPlates(resized);
        pread();
        System.out.println(rectangles.size() + " plates detected!");
        
        
        Graphics2D g = other.createGraphics();
        g.setColor(new Color(255, 255, 255, 255));
        for (Rectangle r : rectangles) {
          g.drawRect(r.x, r.y, r.width, r.height);
        }
        g.setColor(Color.RED);
        g.dispose();
        
        p.setImage(other.getBufferedImage());
      }
      
    }
    catch (ImageReadException e) {
      e.printStackTrace();
    }
  }
  
  public static void reset(String action) {
    System.out.print(action + "...");
    reset();
  }
  
  public static void reset() {
    start = System.currentTimeMillis();
  }
  
  public static long read() {
    return System.currentTimeMillis() - start;
  }
  
  public static void pread() {
    System.out.println(read() + "ms");
  }

}
