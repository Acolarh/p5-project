package dk.aau.cs.d501e13.anpi.learning;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.EdgeDetector;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.Utilities;
import dk.aau.cs.d501e13.anpi.ailal.Ailal;
import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.globalfeatures.DensityVarianceLearner;
import dk.aau.cs.d501e13.anpi.learning.globalfeatures.GradientDensityLearner;
import dk.aau.cs.d501e13.anpi.learning.strong.AdaBoostLearner;
import dk.aau.cs.d501e13.anpi.learning.strong.CascadeLearner;
import dk.aau.cs.d501e13.anpi.learning.strong.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.learning.weak.ThresholdedHaarFeatureLearner;

/**
 * Learning
 */
public class LearnerLoader implements QuickSave<Image> {
  /**
   * Image model
   */
  private ImageModel images;
  
  /**
   * Number plate model
   */
  private NumberplateModel numberplates;
  
  /**
   * Learning data location
   */
  private String dir;
  
  /**
   * Random number generator
   */
  private Random rand = new Random();
  
  /**
   * Width of the frame HaarFeatures must lie within
   */
  private byte width = 44;
  
  /**
   * Height of the frame HaarFeatures must lie within
   */
  private byte height = 11;
  
  /**
   * Use negativesRatio instead of numNegatives
   */
  private boolean useRatio = true;
  
  /**
   * Number of negatives (if useRatio = false)
   */
  private int numNegatives = 100;
  
  /**
   * Negatives per positive (if useRatio = true)
   */
  private double negativesRatio = 1.0;
  
  /**
   * Whether or not to regenerate huge images
   */
  private boolean regenerate = false;
  
  /**
   * Flags to match
   */
  private int numberplateFlags = 0;
  
  /**
   * Whether or not flags are whitelist (include only plates matching flags)
   * or blacklist (include only plates not matchin flags)
   */
  private boolean isWhitelist = false;
  
  /**
   * Name of positives file
   */
  private String positivesFile = null;
  
  /**
   * Name of negatives file
   */
  private String negativesFile = null;
  
  /**
   * Limit positives
   */
  private int limitPositives = -1;
  
  /**
   * Limit negatives
   */
  private int limitNegatives = -1;

  /**
   * Seed
   */
  private long seed = rand.nextInt();
  
  /**
   * Learner
   */
  private String learner = "AdaBoostLearner";
  
  /**
   * Learner configuration
   */
  private Config learnerConfig = new Config();
  
  /**
   * Learners
   */
  private Map<String, Learner<Image>> learners
      = new HashMap<String, Learner<Image>>();
  
  /**
   * Anpi
   */
  private Anpi anpi;
  
  /**
   * Edge detect before resizing
   */
  private boolean edgeDetectBeforeResize = true;
  
  /**
   * Edge detector
   */
  private EdgeDetector edgeDetector = null;
  
  /**
   * Quick saver
   */
  private QuickSaver<Image> quickSaver = null;
  
  /**
   * Constructor
   * @param images Image model
   * @param numberplates Number plate model
   * @param conf Configuration
   */
  public LearnerLoader(ImageModel images, NumberplateModel numberplates, Config conf) {
    this(images, numberplates, conf, new Anpi(conf));
  }

  /**
   * Constructor
   * @param images Image model
   * @param numberplates Number plate model
   * @param conf Configuration
   */
  public LearnerLoader(ImageModel images, NumberplateModel numberplates, Config conf, Anpi anpi) {
    this.images = images;
    this.numberplates = numberplates;
    this.anpi = anpi;
    load(conf);
  }
  
  /**
   * Load configuration
   * @param conf Configuration
   */
  public void load(Config conf) {
    dir = conf.getString("imagesDir");
    width = conf.getByte("frameWidth", width);
    height = conf.getByte("frameHeight", height);
    positivesFile = Utilities.positivesFileName(conf);
    negativesFile = Utilities.negativesFileName(conf);
    useRatio = conf.getBoolean("useRatio", useRatio);
    numNegatives = conf.getInt("numNegatives", numNegatives);
    negativesRatio = conf.getDouble("negativesRatio", negativesRatio);
    numberplateFlags = conf.getInt("numberplateFlags", numberplateFlags);
    isWhitelist = conf.getBoolean("isWhitelist", isWhitelist);
    seed = conf.getLong("seed", seed);
    learner = conf.getString("learner", learner);
    learnerConfig = conf.getConfig("learnerConfig", learnerConfig);
    edgeDetectBeforeResize = conf.getBoolean("edgeDetectBeforeResize", edgeDetectBeforeResize);
    limitNegatives = conf.getInt("limitNegatives", limitNegatives);
    limitPositives = conf.getInt("limitPositives", limitPositives);
    if (conf.getBoolean("useEdgeDetection")) {
      edgeDetector = anpi.getEdgeDetector(conf.getString("edgeDetector", "PrewittEdgeDetector"));
    }
  }
  
  /**
   * Number plate flags to match
   * @see LearnerLoader#getWhitelist(boolean)
   * @return the numberplateFlags
   */
  public int getNumberplateFlags() {
    return numberplateFlags;
  }

  /**
   * Set number plate bit flags
   * @see NumberplateRecord
   * @see LearnerLoader#setWhitelist(boolean)
   * @param numberplateFlags the numberplateFlags to set
   */
  public void setNumberplateFlags(int numberplateFlags) {
    this.numberplateFlags = numberplateFlags;
  }

  /**
   * Whether or not whitelist
   * @return the isWhitelist
   */
  public boolean isWhitelist() {
    return isWhitelist;
  }

  /**
   * Set whether or not to whitelist (only numberplates matching all the flags are
   * found) or blacklist (only numberplates not matching any flags are found).
   * @see LearnerLoader#setNumberplateFlags(int)
   * @param isWhitelist True for whitelisting, false for blacklisting
   */
  public void setWhitelist(boolean isWhitelist) {
    this.isWhitelist = isWhitelist;
  }

  /**
   * Whether or not to reload huge images
   * @param regenerate
   */
  public void setRegenerate(boolean regenerate) {
    this.regenerate = regenerate;
  }

  /**
   * Whether or not big pictures are regenerated
   * @return whether or not big pictures are regenerated
   */
  public boolean isRegenerating() {
    return regenerate;
  }
  
  /**
   * Get width of frame
   * @return Width
   */
  public byte getWidth() {
    return width;
  }

  /**
   * Set width of frame
   * @param width New width
   */
  public void setWidth(byte width) {
    this.width = width;
  }

  /**
   * Set height of frame
   * @return Height
   */
  public byte getHeight() {
    return height;
  }

  /**
   * Set height of frame
   * @param height New height
   */
  public void setHeight(byte height) {
    this.height = height;
  }
  
  /**
   * Get map of learners
   * @return Map of learners
   */
  public Map<String, Learner<Image>> getLearners() {
    return learners;
  }
  
  /**
   * Add a new learner
   * @param learner Learner
   */
  public void addLearner(Learner<Image> learner) {
    learners.put(learner.getClass().getSimpleName(), learner);
  }
  
  /**
   * Get a learner
   * @param name Learner class name
   * @return Learner or null if not found
   */
  public Learner<Image> getLearner(String name) {
    return learners.get(name);
  }
  
  public String getDir(){
    return dir;
  }
  public Anpi getAnpi(){
    return anpi;
  }
  public ImageModel getImages(){
    return images;
  }
  public NumberplateModel getNumberplates(){
    return numberplates;
  }
  
  /**
   * @return The training samples
   * @throws ImageCloneException
   * @throws SQLException
   */
  public TrainingSamples getSamples() throws ImageCloneException, SQLException{
    TrainingSamples trainingSamples = new TrainingSamples(
      width, height, limitPositives, limitNegatives
    );
    
    if (!trainingSamples.loadPositives(positivesFile)) {
      trainingSamples.addPositivesFromDb(
        dir, images, numberplates, numberplateFlags, isWhitelist,
        edgeDetector, edgeDetectBeforeResize
      );
      trainingSamples.savePositives(positivesFile);
    }
    
    if (!trainingSamples.loadNegatives(negativesFile)) {
      // Generate negatives
      if (useRatio) {
        numNegatives = (int)(trainingSamples.getPositives().size() * negativesRatio);
      }
      if (limitNegatives >= 0 && limitNegatives < numNegatives) {
        numNegatives = limitNegatives;
      }
      System.out.println("Generating " + numNegatives + " negatives using seed " + seed);
      rand.setSeed(seed);
      
      images.prepare("select-negatives", "SELECT * FROM images WHERE (id, 0)" +
        " IN (SELECT images.id, COUNT(numberplates.id) AS num_plates FROM " +
        images.name() + " LEFT JOIN " + numberplates.name() + " ON " +
        images.name() + ".id = image_id GROUP BY " + images.name() +
        ".id) AND dataset = " + ImageRecord.SET_TRAINING);
      
      ImageRecord[] records = (ImageRecord[])images.queryMultiple("select-negatives");
      
      
      int negativesPerImage = numNegatives / records.length + 1;
      int negativesAdded = 0;

      System.out.println(records.length + " negative images found, need " +
        negativesPerImage + " negatives per image");

      for (ImageRecord record : records) {
        if (negativesAdded >= numNegatives) {
          break;
        }
        try {
          Image img = new Image(dir + "/" + record.name);
          for (int i = 0; i < negativesPerImage && negativesAdded < numNegatives; i++) {
            int randomW = rand.nextInt(img.getWidth() - width) + 1 + width;
            int randomH = rand.nextInt(img.getHeight() - height) + 1 + height;
            int randomX = rand.nextInt(img.getWidth() - randomW + 1);
            int randomY = rand.nextInt(img.getHeight() - randomH + 1);
            Rectangle r = new Rectangle(randomX, randomY, randomW, randomH);
            if (edgeDetector == null) {
              trainingSamples.addNegative(
                img.getSubimage(r).getResized(width, height)
              );
            }
            else if (edgeDetectBeforeResize) {
              trainingSamples.addNegative(
                img.getSubimage(r).getEdges(edgeDetector).getResized(width, height)
              );
            }
            else {
              trainingSamples.addNegative(
                img.getSubimage(r).getResized(width, height).getEdges(edgeDetector)
              );
            }
            negativesAdded++;
            System.out.println("Negative " + negativesAdded + " of " + numNegatives + " added");
          }
        }
        catch (ImageReadException e) {
          System.out.println("ERROR: Error reading image: " + dir + "/" + record.name);
        }
      }
      
      trainingSamples.saveNegatives(negativesFile);
    }
    return trainingSamples;
  }

  /**
   * Generate strong classifier
   * @return Strong classifier
   * @throws SQLException on SQL error
   * @throws ImageReadException if image does not exist
   * @throws ImageCloneException if shit happens
   * @throws IOException 
   */
  public BClassifier<Image> learn() throws SQLException, ImageCloneException, IOException {
    TrainingSamples.SamplesArray samplesArray = getSamples().getSamplesArray();

    System.out.println("Configuring learner: " + learner);
    Learner<Image> learnerObject = getLearner(learner);
    if (learnerObject == null) {
      System.out.println("ERROR: Learner '" + learner + "' not found");
      return null;
    }
    learnerObject.configure(learnerConfig);
    
    if (quickSaver != null && learnerObject instanceof QuickSave) {
      ((QuickSave<Image>)learnerObject).setQuickSaver(quickSaver);
    }
    
    System.out.println("Learning...");
    try {
      return learnerObject.learn(samplesArray.samples, samplesArray.predicates);
    }
    catch (InvalidTrainingSamplesException e) {
      System.out.println("Invalid size of training samples");
      return null;
    }
  }

  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("usage: learner CONFIG_FILE");
      System.exit(1);
    }
    try {
      Config conf = new Config(args[0]);

      System.out.print("Connection to database...");
      Database db;
      if (conf.get("database") instanceof String) {
        db = new Database(new Config(conf.getString("database")));
      }
      else {
        db = new Database(conf.getConfig("database"));
      }
      System.out.println("DONE");
      ImageModel images = new ImageModel(db);
      NumberplateModel numberplates = new NumberplateModel(db);

      if (conf.get("imagesDir") == null) {
        System.out.println("error: property 'imagesDir' not set");
        System.exit(4);
      }
      LearnerLoader l = new LearnerLoader(images, numberplates, conf);

      l.addLearner(new AdaBoostLearner(l));
      l.addLearner(new CascadeLearner(l));
      l.addLearner(new ThresholdedHaarFeatureLearner());
      l.addLearner(new GradientDensityLearner());
      l.addLearner(new DensityVarianceLearner());
      l.addLearner(new NeuralNetworkLearner());
      
      System.out.println("Learning from images in " + conf.getString("imagesDir") + "...");

      final Ailal<Image> ailal = new Ailal<Image>();

      final String classifierFile = Utilities.classifierFileName(conf);
      
      l.setQuickSaver(new QuickSaver<Image>() {
        @Override
        public void saveClassifier(BClassifier<Image> classifier) {
          ailal.setClassifier(classifier);
          System.out.print("Quick saving classifier to " + classifierFile + "...");
          if (ailal.saveClassifier(classifierFile)) {
            System.out.println("OK");
          }
          else {
            System.out.println("FAIL");
          }
        }
      });
      
      BClassifier<Image> classifier = l.learn();

      ailal.setClassifier(classifier);
      
      System.out.println("Saving classifier to " + classifierFile + "...");

      if (!ailal.saveClassifier(classifierFile)) {
        System.out.println("Could not save classifier!");
        BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
        boolean ask = true;
        while (ask) {
          System.out.print("Abort, Retry, Fail?");
          String input = bin.readLine();
          if (input.length() < 1) {
            continue;
          }
          switch (input.toLowerCase().charAt(0)) {
            case 'a':
              System.exit(1);
              break;
            case 'r':
              if (ailal.saveClassifier(classifierFile)) {
                System.out.println("Success");
              }
              break;
            case 'f':
              System.exit(1);
              break;
            default:
              break;
          }
        }
      }
      
      System.exit(0);
    }
    catch (IOException e1) {
      System.out.println("error: unable to read properties from " + args[0]);
      System.exit(2);
    }
    catch (ClassNotFoundException | SQLException e) {
      System.out.println("error: unable to connect to database");
      e.printStackTrace();
      System.exit(3);
    }
    catch (ImageCloneException e) {
      System.out.println("error: cloning failed");
      e.printStackTrace();
      System.exit(5);
    }
  }

  @Override
  public void setQuickSaver(QuickSaver<Image> quickSaver) {
    this.quickSaver = quickSaver;
  }
}
