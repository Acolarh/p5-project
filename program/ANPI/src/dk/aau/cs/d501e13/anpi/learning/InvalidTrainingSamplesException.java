package dk.aau.cs.d501e13.anpi.learning;

public class InvalidTrainingSamplesException extends Exception {

  public InvalidTrainingSamplesException() {
    // TODO Auto-generated constructor stub
  }

  public InvalidTrainingSamplesException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public InvalidTrainingSamplesException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public InvalidTrainingSamplesException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public InvalidTrainingSamplesException(String message, Throwable cause,
    boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

}
