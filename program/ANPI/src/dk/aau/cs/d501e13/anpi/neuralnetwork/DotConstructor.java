package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Constructs dot file.
 */
public class DotConstructor {
  
  /**
   * Neural network object.
   */
  private NeuralNetwork nn = null;
  
  /**
   * Whether or not to show weights.
   */
  private boolean showWeights = false;
  
  /**
   * Constructor.
   * @param nn Neural network object.
   */
  public DotConstructor(NeuralNetwork nn){
    this.nn = nn;
  }
  
  /**
   * Set show weights field.
   * @param showWeights The value.
   */
  public void setShowWeights(boolean showWeights) {
    this.showWeights = showWeights;
  }
  
  /**
   * Construct the dot file.
   * @param os Output stream.
   */
  public void construct(OutputStream os){
    PrintWriter pw = new PrintWriter(os);
    pw.println("digraph {");
    
    
    for(InputNeuron in : nn.getInputNeurons()){
      pw.println("N" + in.hashCode() + " [label=\"I\"]");
    }
    for(Neuron hn : nn.getHiddenNeurons()){
      pw.println("N" + hn.hashCode() + " [label=\"H\"]");
      for(Connection cn : hn.getInputs()){
        pw.println("N" + cn.from.hashCode() + " -> N" + hn.hashCode()
            + (showWeights ?
              " [label=\"" + String.format("%.2f", cn.weight) + "\"]" : ""));
      }
    }
    for(Neuron on : nn.getOutputNeurons()){
      pw.println("N" + on.hashCode() + " [label=\"O\"]");
      for(Connection cn : on.getInputs()){
        pw.println("N" + cn.from.hashCode() + " -> N" + on.hashCode()
          + (showWeights ?
            " [label=\"" + String.format("%.2f", cn.weight) + "\"]" : ""));
      }
    }
          
    pw.println("}");
    pw.close();
  }

}
