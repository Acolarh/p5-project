package dk.aau.cs.d501e13.anpi.edgedetectors;

import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import dk.aau.cs.d501e13.anpi.EdgeDetector;

/**
 * Edge detection based on discrete differentiation operation, i.e. abstract
 * class for prewitt and sobel
 */
public abstract class DDOEdgeDetector implements EdgeDetector {

  /**
   * Perform edge detection
   * @param input Input raster
   * @param output Output raster
   * @param kernelx Horizontal 3x3 kernel
   * @param kernely Vertical 3x3 kernel
   */
  public void calculate(Raster input, WritableRaster output,
      int[][] kernelx, int[][] kernely) {
    int width = input.getWidth();
    int height = input.getHeight();
    for (int x = 0; x < input.getWidth(); x++) {
      for (int y = 0; y < input.getHeight(); y++) {
        int level = 255;
        if ((x > 0) && (x < (width - 1)) && (y > 0) && (y < (height - 1))) {
          int sumX = 0;
          int sumY = 0;
          for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
              int value = input.getSample(x+i, y+j, 0);
              sumX += value * kernelx[i+1][j+1];
              sumY += value * kernely[i+1][j+1];
            }
          }
          level = Math.abs(sumX) + Math.abs(sumY);
          if (level < 0) {
            level = 0;
          }
          else if (level > 255) {
            level = 255;
          }
          level = 255 - level;
        }
        output.setSample(x, y, 0, level);
      }
    }
  }

}
