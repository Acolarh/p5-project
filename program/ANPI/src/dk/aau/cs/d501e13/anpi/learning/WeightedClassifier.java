package dk.aau.cs.d501e13.anpi.learning;

import java.io.Serializable;

/**
 * Weighted classifier
 * @param <T> The type
 */
public class WeightedClassifier<T> implements Serializable {
  private static final long serialVersionUID = 723574269061523467L;

  /**
   * A binary classifier.
   */
  BClassifier<T> c;

  /**
   * Weight.
   */
  double weight;

  /**
   * Constructor.
   * 
   * @param c The classifier
   * @param weight The weight
   */
  public WeightedClassifier(BClassifier<T> c, double weight) {
    this.c = c;
    this.weight = weight;
  }

  /**
   * @return The classifier.
   */
  public BClassifier<T> getClassifier() {
    return this.c;
  }

  /**
   * @return The weight.
   */
  public double getWeight() {
    return this.weight;
  }
}
