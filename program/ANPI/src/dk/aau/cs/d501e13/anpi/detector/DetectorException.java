package dk.aau.cs.d501e13.anpi.detector;

public class DetectorException extends Exception {

  private static final long serialVersionUID = -2238017432303050405L;

  public DetectorException() {
    // TODO Auto-generated constructor stub
  }

  public DetectorException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public DetectorException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  public DetectorException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public DetectorException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

}
