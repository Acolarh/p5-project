package dk.aau.cs.d501e13.anpi.data.models;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.Model;

/**
 * Model for table images
 */
public class CharacterModel extends Model {
  /**
   * Constructor
   * @param db Database object
   * @throws SQLException on SQL error
   */
  public CharacterModel(Database db) throws SQLException {
    super(db, "characters", new String[]{"value", "image"});
    prepare("find", "SELECT * FROM " + name() + " WHERE id = ?");
    prepare("save", "UPDATE " + name() + " SET value = ?, image = ? WHERE id = ?");
    prepare("delete", "DELETE FROM " + name() + " WHERE id = ?");
  }
  
  /**
   * Create a record
   * @param value Value
   * @param image Image
   * @return New record
   * @throws SQLException on SQL error
   */
  public CharacterRecord create(int value, byte[] image) throws SQLException {
    int id = executeCreate(value, image);
    if (id > 0) {
      return new CharacterRecord(this, id, value, image);
    }
    return null;
  }
  
  /**
   * Create a record
   * @param value Value
   * @param image Image
   * @return New record
   * @throws SQLException
   */
  public CharacterRecord create(int value, Image image) throws SQLException {
    try {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ImageIO.write(image.getBufferedImage(), "png", baos);
      CharacterRecord r = create(value, baos.toByteArray());
      baos.close();
      return r;
    }
    catch (IOException e) {
      return null;
    }
  }
  
  /**
   * Find record by id
   * @param id Id
   * @return Record or null if not found
   * @throws SQLException on SQL error
   */
  public CharacterRecord find(int id) throws SQLException {
    return (CharacterRecord)querySingle("find", id);
  }

  @Override
  public CharacterRecord makeRecord(ResultSet r) throws SQLException {
    CharacterRecord record = null;
    if (r.next()) {
      record = new CharacterRecord(this, r);
    }
    r.close();
    return record;
  }

  @Override
  public CharacterRecord[] makeRecordList(ResultSet r) throws SQLException {
    List<CharacterRecord> records = new ArrayList<CharacterRecord>();
    while (r.next()) {
      records.add(new CharacterRecord(this, r));
    }
    r.close();
    return records.toArray(new CharacterRecord[0]);
  }

  @Override
  public CharacterRecord[] all() throws SQLException {
    return (CharacterRecord[])super.all();
  }

}
