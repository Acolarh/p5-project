package dk.aau.cs.d501e13.anpi.ocr;

import java.util.List;

/**
 * Weighted string.
 */
public class WeightedString implements Comparable<WeightedString> {
  
  /**
   * The string.
   */
  public String s;
  
  /**
   * The weight.
   */
  public double weight;
  
  /**
   * Constructor.
   * @param s The string.
   * @param weight The weight.
   */
  public WeightedString(String s, double weight) {
    this.s = s;
    this.weight = weight;
  }
  
  /**
   * Constructor.
   * @param characters List of weighted characters.
   */
  public WeightedString(List<WeightedChar> characters) {
    int size = characters.size();
    weight = 0.0;
    s = "";
    for (WeightedChar c : characters) {
      s += c.c;
      weight += c.weight;
    }
    weight /= size;
  }
  
  /**
   * Constructor.
   * @param ws Object of weighted string.
   */
  public WeightedString(WeightedString ws) {
    s = ws.s;
    weight = ws.weight;
  }

  @Override
  public int compareTo(WeightedString o) {
    if (o.weight < weight)
      return -1;
    else if (o.weight > weight)
      return 1;
    else
      return 0;
  }
}
