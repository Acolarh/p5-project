package dk.aau.cs.d501e13.anpi.learning;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.strong.NeuralNetworkLearner;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;

/**
 * Neural network classifier
 */
public class NeuralNetworkClassifier implements BClassifier<Image> {
  private static final long serialVersionUID = -2881709236308405094L;
  
  /**
   * Neural network object.
   */
  private NeuralNetwork nn;
  /**
   * Threshold
   */
  private double threshold;

  /**
   * Neural network learner object.
   */
  public NeuralNetworkLearner nnl = null;
  
  /**
   * Constructor.
   * @param nnl The learner object.
   * @param nn The neural network object.
   * @param threshold The threshold.
   */
  public NeuralNetworkClassifier(NeuralNetworkLearner nnl, NeuralNetwork nn, double threshold) {
    this.nnl = nnl;
    this.nn = nn;
    this.threshold = threshold;
  }
  
  @Override
  public boolean predict(Image t) {
    try {
      nn.setInputVector(nnl.getInputsFor(t));
    }
    catch (CorruptedInputException e) {
      e.printStackTrace();
      return false;
    }
    return nn.calculate()[0] > threshold;
  }
}