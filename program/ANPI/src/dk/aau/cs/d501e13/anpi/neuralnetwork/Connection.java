package dk.aau.cs.d501e13.anpi.neuralnetwork;

import java.io.Serializable;

/**
 * A triple with neurons (from and to) and its associated weight.
 */
public class Connection implements Serializable {
  private static final long serialVersionUID = -4571710343628042257L;
  /**
   * The incoming neuron.
   */
  public Neuron from;
  /**
   * The connection of the actual neuron. What is the actual neuron connected to.
   */
  public Neuron to;
  /**
   * A weight associated with the neurons arc.
   */
  public double weight;
  
  /**
   * The learning rate used to teach.
   */
  public double learningRate;
  
  /**
   * A momentum used to speed up the learning.
   */
  public double momentum;
  
  /**
   * Previous delta value.
   */
  public double previousDelta = 0.0;
  
  /**
   * Weight change value. Is calculated from momentum and previousDelta.
   */
  public double weightChange = 0.0;

  /**
   * Constructor for class.
   * @param from The incoming neuron connection.
   * @param to The outgoing neuron connection.
   * @param weight The associated weight.
   */
  public Connection(Neuron from, Neuron to, double weight) {
    this(from, to, weight, 0.25);
  }

  /**
   * Constructor for class.
   * @param from The incoming neuron connection.
   * @param to The outgoing neuron connection.
   * @param weight The associated weight.
   * @param learningRate Learning rate
   */
  public Connection(Neuron from, Neuron to, double weight, double learningRate) {
    this(from, to, weight, learningRate, 0.0);
  }

  /**
   * Constructor for class.
   * 
   * @param from The incoming neuron connection.
   * @param to The outgoing neuron connection.
   * @param weight The associated weight.
   * @param learningRate Learning rate
   * @param momentum Momentum
   */
  public Connection(Neuron from, Neuron to, double weight, double learningRate, double momentum) {
    this.from = from;
    this.to = to;
    this.weight = weight;
    this.learningRate = learningRate;
    this.momentum = momentum;
  }
  
  /**
   * Updates the weights.
   * @param errorTerm The error term.
   */
  public void updateWeight(double errorTerm) {
    double delta = learningRate * errorTerm * from.calculate();
    if (momentum > 0.0) {
      delta += momentum * previousDelta;
    }
//    if ((delta < 0) == (previousDelta < 0)) {
//      learningRate = learningRate * increaseFactor;
//    }
//    else if ((delta >= 0) != (previousDelta >= 0)){
//      learningRate = learningRate * decreaseFactor;
//      delta = 0.0;
//    }
    previousDelta = delta;
    weightChange += delta;
  }

  /**
   * Updates the weights.
   * @param errorTerm The error term.
   * @param learningRate The learning rate.
   */
  public void updateWeight(double errorTerm, double learningRate) {
    double delta = learningRate * errorTerm * from.calculate();
    if (momentum > 0.0) {   
      delta += momentum * previousDelta;
    }
    previousDelta = delta;
    weightChange += delta;
  }

}
