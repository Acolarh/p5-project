package dk.aau.cs.d501e13.anpi.neuralnetwork;

/**
 * Interface for the training samples of neural network.
 */
public interface NeuralNetworkTrainingSample {
  /**
   * @return A list of the inputs.
   */
  public double[] getInputs();
  /**
   * @return A list of the expected outputs.
   */
  public double[] getExpectedOutputs();

}
