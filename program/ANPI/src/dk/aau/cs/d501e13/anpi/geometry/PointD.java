package dk.aau.cs.d501e13.anpi.geometry;

/**
 * A point in two-dimensional Euclidean space with doubles.
 */
public class PointD {
  
  /*
   * x coordinate double version
   */
  public double x;
  
  /*
   * y coordinate double version
   */
  public double y;
  
  /**
   * Constructor
   * @param x X coordinate (double version)
   * @param y Y coordinate (double version)
   */
  public PointD(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  /**
   * Constructor.
   * @param point A point.
   */
  public PointD(Point point) {
    this.x = point.x;
    this.y = point.y;
  }
  
  /**
   * Converts a PointD to a Point 
   * @return a Point
   */
  public Point convertToPoint() {
    return new Point((int)Math.round(this.x), (int)Math.round(this.y));
  }
  
}
