package dk.aau.cs.d501e13.anpi.learning;

import java.util.ArrayList;
import java.util.List;

/**
 * A strong classifier as produced by AdaBoost
 * @param <T1> Sample type
 */
public class StrongClassifier<T1> implements Classifier<T1>  {
  private static final long serialVersionUID = 5748531484913400019L;

  /**
   * List of weighted classifiers
   */
  private List<WeightedClassifier<T1>> classifiers;
  
  /**
   * Constructor.
   * @param classifiers List of weighted classifiers
   */
  public StrongClassifier(List<WeightedClassifier<T1>> classifiers) {
    this.classifiers = classifiers;
  }
  public StrongClassifier() {
    this.classifiers = new ArrayList<>();
  }
  
  /**
   * Gets the weighted classifier contained at a given index
   * @param index The index.
   * @return A weighted classifier.
   */
  public WeightedClassifier<T1> get(int index){
    return classifiers.get(index);
  }
  
  /**
   * @return The size of the list of weighted classifiers.
   */
  public int size(){
    return classifiers.size();
  }
  
  public void add(BClassifier<T1> classifier, double weight){
    classifiers.add(new WeightedClassifier<>(classifier, weight));
  }
  
  @Override
  public double predictValue(T1 t) {
    double prediction = 0.0;
    for (WeightedClassifier<T1> classifier : classifiers){
      if (classifier.getClassifier().predict(t))
        prediction += classifier.getWeight();
      else
        prediction -= classifier.getWeight();
    }
    return prediction;
  }

}
