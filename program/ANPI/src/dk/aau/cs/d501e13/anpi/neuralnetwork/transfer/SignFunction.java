package dk.aau.cs.d501e13.anpi.neuralnetwork.transfer;

/**
 * The sign function as a transfer function.
 * Return 1 if input is greater than 0,
 * return -1 if input is less than 0,
 * otherwise return 0.
 */
public class SignFunction implements TransferFunction {
  private static final long serialVersionUID = -1165044386209702692L;

  @Override
  public double activate(double inputSum) {
    return inputSum == 0.0 ? 0.0 : 
      inputSum < 0.0  ? -1.0 : 
        1.0;
  }
  
  @Override
  public double getDerivative(double output) {
    return 1.0;
  }
  
}
