package dk.aau.cs.d501.e13.segmentingacceptance;

import java.io.File;
import java.io.IOException;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;

public class SegmentTester {

	public static void main(String[] args) throws ImageReadException, IOException {
		
		File numberPlateImageDirectory = new File("C:\\Users\\SimonBuus\\Documents\\tests\\segmentingTestImages\\");
		int NumberPlatecounter = 1, characterCounter = 1;
		
		Anpi anpi = new Anpi(new Config("C:\\Users\\SimonBuus\\Documents\\P5-project\\program\\ANPI\\my_configurations.yml"));
		
		for(File numberPlateImgPath : numberPlateImageDirectory.listFiles()) {
			System.out.println("numberplate: " + NumberPlatecounter);
			Image currentnumberPlateImage = new Image(numberPlateImgPath); 
			List<Image> characters = anpi.segmentPlate(currentnumberPlateImage);
			for(Image character : characters) {
				character.save("C:\\Users\\SimonBuus\\Documents\\tests\\segmentingTestImages\\" + numberPlateImgPath.getName() + characterCounter + ".png", "png");
				System.out.println("Character: " + characterCounter);
				characterCounter += 1;
			}
			characterCounter = 1;
			NumberPlatecounter += 1; 
		}

	}

}
