package dk.aau.cs.d501.e13.cornerdetectionacceptance;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.cornerdetector.CornerDetector;

public class CornerdetectionTester {
		
	public static void main(String[] args) throws IOException, ImageReadException {
		
		File imageDirectory = new File("C:\\Users\\SimonBuus\\Documents\\hjørnedetektionsresultater\\");
		int counter = 1;
		
		for(File imgPath : imageDirectory.listFiles()) {
			System.out.println(counter);
			Image currentImage = new Image(imgPath); 
			CornerDetector cd = new CornerDetector(currentImage);
			cd.getDebugImage().save("C:\\Users\\SimonBuus\\Documents\\hjørnedetektionscornerdetektionsresultater\\" + imgPath.getName(), "png");
			counter += 1; 
		}
	}
}
