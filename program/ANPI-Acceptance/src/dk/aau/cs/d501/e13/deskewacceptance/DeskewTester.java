package dk.aau.cs.d501.e13.deskewacceptance;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.deskewing.Deskew;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.geometry.Point;

public class DeskewTester {
	
	public static void main(String[] args) throws IOException, ImageReadException, InvalidParameterException {
		
		String imgPath = "C:\\Users\\SimonBuus\\Documents\\valideringsmængde\\";
		String Line = null;
		
		Scanner input = new Scanner(new FileReader(imgPath + "imgcorners.txt"));
		
		//Get lines in txt file
		input.nextLine();
		Line = input.findInLine("(\\w+):	(\\d+),(\\d+)	(\\d+),(\\d+)	(\\d+),(\\d+)	(\\d+),(\\d+)");
		
		for(int i = 1; i <= 100; i++) {
			MatchResult mr = input.match();
			File imgFile = new File(imgPath + mr.group(1) + ".jpg");
			Image img = new Image(imgFile);
			
			List<Point> corners = new ArrayList<Point>();
		    corners.add(new Point(Integer.parseInt(mr.group(2)), Integer.parseInt(mr.group(3))));
		    corners.add(new Point(Integer.parseInt(mr.group(4)), Integer.parseInt(mr.group(5))));
		    corners.add(new Point(Integer.parseInt(mr.group(6)), Integer.parseInt(mr.group(7))));
		    corners.add(new Point(Integer.parseInt(mr.group(8)), Integer.parseInt(mr.group(9))));
		    
		    //Save the de-skewed images
		    Deskew deskew = new Deskew(img, corners);
		    deskew.deskew().save("deskewed" + mr.group(1) + ".png", "png");
		    input.nextLine();
		    System.out.println(Line);
		    Line = input.findInLine("(\\w+):	(\\d+),(\\d+)	(\\d+),(\\d+)	(\\d+),(\\d+)	(\\d+),(\\d+)");
		}
		
		input.close();
	}
		
}

