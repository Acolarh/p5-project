package dk.aau.cs.d501e13.anpiacceptance;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.ailal.Ailal;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.detector.DetectorException;
import dk.aau.cs.d501e13.anpi.geometry.Point;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;

/**
 * Used to test correctness of detected rectangles as number plates.
 */
public class AcceptanceTester {
	private static final double threshold = 1.0 - 0.5;
	/**
	 * Config loader.
	 */
	private Config loader;
	
	/**
	 * ANPI object.
	 */
	private Anpi anpi;
	
	/**
	 * Acceptance tester.
	 */
	public class AcceptanceResult{
		
		private int correct = 0, total = 0, wrong = 0, images=0;
		private int classifierMatch = 0;
		private int undetectable = 0;
		
		private void outputResult(Image img, Rectangle np){
			try {
				
				int extendWidth = (int)(np.width * 0.20);
				int extendHeight = (int)(np.height * 0.20);
				Rectangle extended = new Rectangle(np.x-extendWidth, np.y-extendHeight, np.width+2*extendWidth, np.height+2*extendHeight);

				Image sub = img.getSubimage(np);
				Image subEx = img.getSubimage(extended);

				String plateDeskew = "Couldn't deskew";
				String plateNormal = "Couldn't indentify";
				
				List<Point> points = anpi.detectCorners(subEx);
				Image deskewed;
				try {
					deskewed = anpi.skewPlate(subEx, points);
					plateDeskew = anpi.identifyCharacters(deskewed);
				} catch (InvalidParameterException | CorruptedInputException e) {
				}

				try {
					plateNormal = anpi.identifyCharacters(sub);
				} catch (InvalidParameterException | CorruptedInputException e) {
				}
				System.out.println("ID: " + plateDeskew + " - " + plateNormal);
			} catch (ImageCloneException | IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		/**
		 * Tests the detection.
		 * @param img The image.
		 * @param detectedNP The list of detected number plates.
		 * @param truths The list of predicates.
		 */
		private void testDetection(Image img, List<Rectangle> detectedNP, NumberplateRecord[] truths){
			for(NumberplateRecord truth : truths){
				Rectangle other = new Rectangle(truth.x, truth.y, truth.width, truth.height);
				System.out.print("truth: ");
				outputResult(img, other);
				
				int minWidth = loader.getInt("minWidth", 0) * img.getWidth() / loader.getInt("detectorWidth");
				int minHeight = loader.getInt("minHeight", 0) * img.getWidth() / loader.getInt("detectorHeight"); 
				if(truth.width < minWidth || truth.height < minHeight){
					undetectable++;
					System.out.println("Number plate too small to detect!");
					continue;
				}
				
				double bestVal = Double.MIN_VALUE;
				Rectangle bestRec = null;
				for(Rectangle np : detectedNP){
					double comparison = np.compareRectangles(other);
					
					if(bestVal <= comparison){
						bestVal = comparison;
						bestRec = np;
					}
				}

				if(bestVal > threshold){
					System.out.print("bestRec: ");
					outputResult(img, bestRec);
					detectedNP.remove(bestRec);
					correct++;
				}
				System.out.println("Best overlap:" + bestVal);
			}
			wrong += detectedNP.size();
		}
		
		/**
		 * Test the classifier.
		 * @param img The image.
		 * @param truths The predicates.
		 */
		private void testClassifier(Image img, NumberplateRecord[] truths){
			try {
				BClassifier<Image> classifier = new Ailal<Image>(loader).getClassifier();
				
				for(NumberplateRecord truth : truths){
					try {
						Image plate = img.getSubimage(new Rectangle(truth.x, truth.y, truth.width, truth.height));
						if(classifier.predict(plate)){
							classifierMatch++;
						}
					} catch (ImageCloneException e) {
						e.printStackTrace();
						continue;
					}
					
				}
			} catch (ClassifiersLoadException e) {
				e.printStackTrace();
				//TODO:
			}
		}
		
		/**
		 * The next image to be tested.
		 * @param img The image.
		 * @param detectedNP THe list of detected number plates.
		 * @param truths An array of predicates. 
		 */
		public void addImage(Image img, List<Rectangle> detectedNP, NumberplateRecord[] truths){
			images++;
			total += truths.length;
			System.out.println("Number of plates in image: " + truths.length);
			testDetection(img, detectedNP, truths);
			testClassifier(img,truths);
		}
		
		public int amount(){
			return total - undetectable;
		}
		
		/**
		 * @return The detection rate.
		 */
		public double getDetectionRate(){
			int amount = total - undetectable;
			return 1.0 - (double)(amount - correct)/amount;
		}
		
		/**
		 * @return The error rate.
		 */
		public double getErrorRate(){
			return (double)wrong / images;
		}
		
		/**
		 * @return The classifier positive rate.
		 */
		public double getClassifierPositiveRate(){
			return (double)classifierMatch / total;
		}
	}

	/**
	 * Constructor for class.
	 * Input classifier path into ANPI and finds the relevant classifiers.
	 * Connects to database.
	 * 
	 * @param loader Represents an ANPI configuration
	 * @throws ClassifiersLoadException
	 */
	public AcceptanceTester(Config loader) throws ClassifiersLoadException{
		this(loader, new Anpi(loader));
	}
	
	/**
	 * Constructs an AcceptanceTester using an Anpi object
	 * @param dirName Path to the directory with all images
	 * @param anpi Anpi object for detecting images
	 */
	public AcceptanceTester(Config loader, Anpi anpi){
		this.loader = loader;
		this.anpi = anpi;
	}
	
	/**
	 * Creates statistics for how well the detector works
	 * @param images The calculated statistics
	 * @return Result of the acceptance test 
	 * @throws DetectorException 
	 * @throws ClassifiersLoadException 
	 */
	public AcceptanceResult testImages(ImageRecord[] images) throws DetectorException, ClassifiersLoadException{
		AcceptanceResult result = new AcceptanceResult();
		int current = 0;
		
		for(ImageRecord r : images){
			String path = loader.getString("imagesDir") + "/" + r.name;
			System.out.println("[" + ++current + "/" + images.length + "] Image: " + path);
			
			Image img;
			try {
				img = new Image(path);
			} catch (ImageReadException e) {
				System.out.println("Warning, could not load image file: " + path);
				continue;
			}
			
			NumberplateRecord[] truths;
			try {
				truths = r.getNumberplates();
			} catch (SQLException e) {
				System.out.println("Warning, numberplates could not be read for Image.id: " + r.id);
				continue;
			}
			
			result.addImage(img, anpi.detectPlatePositions(img), truths);
		}
		return result;
	}
	
	/**
	 * Calculates and prints percentage of correctly detected number plates in a list of images
	 * and also amount of falsely detected number plates.
	 * Each iteration finds the highest rate of correctly found number plate and removes this from 
	 * the list to find all the correctly detected plates and the false positives.
	 * @param images The upper limit for how many images should be examined, use negative values for using all images
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws DetectorException 
	 * @throws ClassifiersLoadException 
	 */
	public void correctness(int maxCount) throws SQLException, ClassNotFoundException, IOException, DetectorException, ClassifiersLoadException{
		Database db = new Database("db.properties");
		ImageModel imgModel = new ImageModel(db);
		new NumberplateModel(db);
		
		ImageRecord[] images = imgModel.findByDataset(ImageRecord.SET_TEST);
		if(maxCount < 0)
			maxCount = images.length;
		else
			maxCount = Math.min(maxCount, images.length);
		images = Arrays.copyOfRange(images, 0, maxCount);
		
		AcceptanceResult result = testImages( images );
		
		System.out.println("Amount of plates: " + result.amount());
		System.out.println("Detection rate: " + result.getDetectionRate() * 100 + " %");
		System.out.println("Falsely detected: " + result.getErrorRate() + " pr. image.");
		System.out.println("ClassifierPositiveRate: " + result.getClassifierPositiveRate() * 100 + " %");
		
	}

	/**
	 * @param args args[0] = directory path and args[1] = classifier path  
	 * @throws ClassifiersLoadException 
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws DetectorException 
	 */
	public static void main(String[] args) throws ClassifiersLoadException, ClassNotFoundException, SQLException, IOException, DetectorException {
		if(args.length != 1 && args.length != 2){
			System.out.println("Incorrect parameters. Use the following format:");
			System.out.println("executable path_to_proporties [max images=-1]");
			System.exit(-1);
		}
		
		int maxCount = -1;
		if( args.length == 2){
			try{
				maxCount = Integer.parseInt(args[1]);
			}
			catch(NumberFormatException e){
				System.out.println("Could not parse \"" + args[1] + "\" as an integer!");
				System.exit(-1);
			}
		}
		
		AcceptanceTester at = new AcceptanceTester(new Config(args[0]));
		at.correctness(maxCount);
	}
}