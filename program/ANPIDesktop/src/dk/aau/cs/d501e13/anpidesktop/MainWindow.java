package dk.aau.cs.d501e13.anpidesktop;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;


public class MainWindow {
    private JFrame frame;
	  private ImageFrame videoFrame = new ImageFrame();
	  private VideoInputAdapter inputAdapter; 

	  /**
	   * Launch the application.
	   */
	  public static void main(String[] args) {
	    try {
	      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (ClassNotFoundException | InstantiationException
	        | IllegalAccessException | UnsupportedLookAndFeelException e) {
	      System.out.println("Could not set native look and feel!");
	    }
	    EventQueue.invokeLater(new Runnable() {
	      public void run() {
	        try {
	          new MainWindow();
	        }
	        catch (Exception e) {
	          e.printStackTrace();
	        }
	      }
	    });
	  }

	  /**
	   * Create the application.
	   */
    public MainWindow() {
	    frame = new JFrame("ANPI");
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        inputAdapter = new VideoInputAdapter(videoFrame);
        
        //Create menubar
        JMenuBar menu = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenu viewMenu = new JMenu("View");
        menu.add(fileMenu);
        menu.add(viewMenu);
        
        //View Menu
        addMenuItem(viewMenu, "Normal", new AbstractAction(){
          private static final long serialVersionUID = 4814767374874922950L;

          @Override
          public void actionPerformed(ActionEvent e) {
            videoFrame.setEdgeDetection(false);
          }
        });
        addMenuItem(viewMenu, "Edge", new AbstractAction(){
          private static final long serialVersionUID = 6544642944913567502L;

          @Override
          public void actionPerformed(ActionEvent e) {
            videoFrame.setEdgeDetection(true);
          }
        });
        addMenuItem(viewMenu, "Plates on", new AbstractAction(){
          private static final long serialVersionUID = -5218142327697177063L;

          @Override
          public void actionPerformed(ActionEvent e) {
            videoFrame.setPlateDetection(true);
          }
        });
        addMenuItem(viewMenu, "Plates off", new AbstractAction(){
          private static final long serialVersionUID = 5878847136932878715L;

          @Override
          public void actionPerformed(ActionEvent e) {
            videoFrame.setPlateDetection(false);
          }
        });
        
        //File Menu
        addMenuItem(fileMenu, "Open Image", new AbstractAction(){
          private static final long serialVersionUID = 5878847136932878715L;

          @Override
          public void actionPerformed(ActionEvent e) {
            chooseImage();
          }
        });
        addMenuItem(fileMenu, "Start webcam", new AbstractAction(){
          private static final long serialVersionUID = -5218142327697177063L;

          @Override
          public void actionPerformed(ActionEvent e) {
            inputAdapter.start();
          }
        });
        
        
        videoFrame.setVisible(true);
        frame.getContentPane().add(videoFrame, BorderLayout.CENTER);
        frame.getContentPane().add(menu, BorderLayout.NORTH);
	  }
    
    /**
     * Create a JMenuItem and add it to a JMenu
     * @param parent The JMenu this new JMenuItem should be added to
     * @param name Text of the item
     * @param act Action which should be associated with this item
     * @return A reference to the new item
     */
    private JMenuItem addMenuItem(JMenu parent, String name, Action act){
      JMenuItem item = new JMenuItem(act);
      item.setText(name);
      parent.add(item);
      return item;
    }
	  
	  /**
	   * Show a file open dialog and show the image chosen
	   */
	  public void chooseImage(){
	    JFileChooser fc = new JFileChooser();
        if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            if (fc.getSelectedFile().exists()) {
                insertImage(fc.getSelectedFile());
            }
        }
	  }
	  
	  /**
	   * Show an image from a image file
	   * @param imgFile Where to acquire the file stream
	   */
	  public void insertImage(File imgFile) {
	    inputAdapter.stop();
		
		try {
		  videoFrame.setImage(new Image(imgFile));
		} catch (ImageReadException e) {
		  videoFrame.setImage(null);
		}
	  }
}
