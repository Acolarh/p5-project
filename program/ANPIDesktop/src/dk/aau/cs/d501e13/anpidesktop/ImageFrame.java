package dk.aau.cs.d501e13.anpidesktop;

import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.JPanel;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.ClassifiersLoadException;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.Numberplate;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.CorruptedInputException;

/**
 * JPanel which displays an Image on the screen
 */
public class ImageFrame extends JPanel {
  private static final long serialVersionUID = -1408835125375281895L;
  
  protected Image image = null;
  protected int offsetX;
  protected int offsetY;
  
  protected boolean edges = false;
  
  protected boolean detection = false;
  protected Anpi anpi = null;
  protected List<Numberplate> numberPlatesInImage = null;
  Semaphore loading = new Semaphore(1);
  
  public ImageFrame(){
    try {
      anpi = new Anpi(new Config("default.properties"));
    }
    catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  /**
   * Set a new image to display
   * @param image The image to display
   */
  public void setImage(final Image newImage) {
    if(detection && anpi!=null){
      new Thread(new Runnable(){
        public void run(){
          if( loading.tryAcquire() ){
            try {
              numberPlatesInImage = anpi.detectPlates(newImage);
            }
            catch (ClassifiersLoadException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            catch (InvalidParameterException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            catch (CorruptedInputException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
            }
            image = newImage;
            repaint();
            loading.release();
          }
        }
      }).start();
    }
    else{
      this.image = newImage;
      numberPlatesInImage = null;
      repaint();
    }
  }
  
  /**
   * Enable/disable showing the edge-detected version
   * @param enabled Whether or not edge-detection should be shown
   */
  public void setEdgeDetection( boolean enabled ){
    edges = enabled;
    repaint();
  }
  
  /**
   * Enable/disabled number plate detection
   * @param enabled Whether or not edge-detection should be used
   */
  public void setPlateDetection(boolean enabled){
    detection = enabled;
    repaint();
  }
  
  /**
   * Redraw the image on the screen
   */
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponents(g);
    g.setColor(Color.GRAY);
    g.fillRect(0, 0, getWidth(), getHeight());
    if (image != null) {
      //Keep aspect
      double aspectOrg = (double)image.getWidth() / image.getHeight();
      double aspectWindow = (double)getWidth() / getHeight();
      int width = getWidth();
      int height = getHeight();
      if( aspectOrg > aspectWindow ){
          height = (int)(width / aspectOrg + 0.5);
      }
      else{
          width = (int)(height * aspectOrg + 0.5);
      }
      
      //Find offsets
      offsetX = (getWidth() - width) / 2;
      offsetY = (getHeight() - height) / 2;
      g.drawImage(image.getBufferedImage() , offsetX, offsetY, width, height, this);
      // TODO FIX!!
//      g.drawImage(edges?image.getEdges():image.getInput() , offsetX, offsetY, width, height, this);
      
      //Draw a rectangle at each numberplate
      if(numberPlatesInImage != null){
        g.setColor(new Color(1.0f, 0.0f, 0.0f));
        for( Numberplate p : numberPlatesInImage ){
          g.drawRect( scaleX(p.region.x), scaleY(p.region.y), scaleWidth(p.region.width), scaleHeight(p.region.height));
        }
      }
    }
  }
  
  protected int scale(int x, int full, int actual) {
    double percentage = (double)x / full;
    return (int)(percentage * actual + 0.5);
  }

  protected int scaleX( int x ){
    return offsetX + scale(x, image.getWidth(), getWidth() - 2*offsetX);
  }

  protected int scaleY (int y) {
    return offsetY + scale(y, image.getHeight(), getHeight() - 2*offsetY);
  }

  protected int scaleWidth (int width) {
    return scale(width, image.getWidth(), getWidth() - 2*offsetX);
  }

  protected int scaleHeight (int height) {
    return scale(height, image.getHeight(), getHeight() - 2*offsetY);
  }
}