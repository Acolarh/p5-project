package dk.aau.cs.d501e13.anpidesktop;

import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import dk.aau.cs.d501e13.anpi.Image;


public class VideoInputAdapter implements Runnable {

  private FrameGrabber grabber = null;
  private ImageFrame output;
  private boolean running = false;
  
  /**
   * Construct VideoInputAdapter
   * @param output ImageFrame which will receive the camera input
   */
  public VideoInputAdapter(ImageFrame output) {
    this.output = output;
  }
  
  /**
   * Start the camera device and stream it to ImageFrame
   */
  public void start(){
    grabber = new MyOpenCVFrameGrabber(0);
    grabber.setImageHeight(480);
    grabber.setImageWidth(640);
    try {
      grabber.start();
      running = true;
      new Thread(this).start();
    }
    catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    //TODO: interesting? grabber.setImageMode(ImageMode.GRAY); ImageMode.RAW
  }
  
  /**
   * Stop streaming and close the camera device
   */
  public void stop(){
    if(running && grabber!=null){
      running = false;
      //TODO: wait for thread to finish so we can't mess up with grabber?
      try {
  		grabber.stop();
      } catch (Exception e) {
  		// TODO Auto-generated catch block
  		e.printStackTrace();
      }
    }
  }
  
  @Override
  public void run() {
      while (running && grabber!=null) {
        try {
          IplImage ipl = grabber.grab();
          output.setImage(new Image(ipl.getBufferedImage()));
        }
        catch (Exception e1) {
          output.setImage(null);
        }
        
        try {
          Thread.sleep(1000/30);
        } catch (InterruptedException e) { }
      }
  }
}
