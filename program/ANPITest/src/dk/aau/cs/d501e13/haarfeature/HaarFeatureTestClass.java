package dk.aau.cs.d501e13.haarfeature;

import static org.junit.Assert.*;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.*;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

public class HaarFeatureTestClass {
  
  // Specifying  the rectangles in the HaarFeature called hf1
  Rectangle hf1AddRect1 = new Rectangle(0, 0, 50, 50);
  Rectangle hf1AddRect2 = new Rectangle(50, 50, 50, 50);
  Rectangle hf1SubRect1 = new Rectangle(50, 0, 50, 50); 
  Rectangle hf1SubRect2 = new Rectangle(0, 50, 50, 50);
  Rectangle[] hf1AddRectangles = new Rectangle[]{hf1AddRect1, hf1AddRect2};
  Rectangle[] hf1SubRectangles = new Rectangle[]{hf1SubRect1, hf1SubRect2};
  HaarFeature hf1 = new HaarFeature(hf1AddRectangles, hf1SubRectangles, 100, 100);
  
  @Test
  public void testGetDiff() throws ImageReadException {
    Image img = new Image("assets/haarFeatureTestImg.png");
    // Test of the getDiff()-method on HaarFeature hf1
    assertEquals(((615060+625005)-(613785+622965)), hf1.predictValue(img), 0.001);
  }
  
  @Test
  public void testGetDiffScaled() throws ImageReadException {
    Image img = new Image("assets/haarFeatureTestImgScaledBigger.png");
    // Test of the getDiffScaled()-method thorugh the getDiff-method 
    // on HaarFeature hf1
    assertEquals(((615060+625005)-(613785+622965)), hf1.predictValue(img), 0.001);
  }
}
  
  