package dk.aau.cs.d501e13.anpi.learning;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;

public class LearnerTest {
  
  private static Database db;
  private static ImageModel model;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    db = new Database("db.properties");
    model = new ImageModel(db);
    model.truncate();
    ImageRecord image = model.create("test2.jpg", ImageRecord.SET_TRAINING);
    
    NumberplateModel numberplates = new NumberplateModel(db);
    numberplates.truncate();
    numberplates.create(image, 48, 1231, 127, 76, 0, "JW 31 271");
    numberplates.create(image, 810, 1196, 258, 102, 0, "EW 27 149");
    numberplates.create(image, 1891, 1478, 547, 243, 0, "ZE 43 218");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
//    model.truncate();
//    NumberplateModel numberplates = new NumberplateModel(db);
//    numberplates.truncate();
  }


}
