package dk.aau.cs.d501e13.anpi.neuralnetwork;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SignFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;


public class NeuronTest {
  
  @Test
  public void calculateTest() throws CorruptedInputException{
    InputNeuron in0 = new InputNeuron();
    in0.setValue(20.0);
    assertEquals(20.0, in0.calculate(), 0.0);
    
    NeuralNetwork nn = buildNetwork();
    double[] v = {0.2, 0.2};
    nn.setInputVector(v);

    /* add weight of one to all hidden*/
    for(int i = 0; i < nn.getHiddenNeurons().size(); i++){
      for(int j = 0; j < nn.getInputNeurons().size(); j++){
        nn.getHiddenNeurons().get(i).addInput(nn.getInputNeurons().get(j), 1.0);
      }
    }
    
    /* add weight of one to all output*/
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      for(int j = 0; j < nn.getHiddenNeurons().size(); j++){
        nn.getOutputNeurons().get(i).addInput(nn.getHiddenNeurons().get(j), 1.0);
      }
    }
    
    double res = 1 / (1 + Math.exp(-(0.2 * 1.0 + 0.2 * 1.0)));
    for(int i = 0; i < nn.getHiddenNeurons().size(); i++){
      assertEquals(res, nn.getHiddenNeurons().get(i).calculate(), 0.0);
    }
    
    NeuralNetwork nn2 = buildNetwork();
    nn2.setInputVector(0.2, 0.5);

    /* add weight of one to all hidden*/
    for(int i = 0; i < nn2.getHiddenNeurons().size(); i++){
      for(int j = 0; j < nn2.getInputNeurons().size(); j++){
        nn2.getHiddenNeurons().get(i).addInput(nn2.getInputNeurons().get(j), 0.0);
      }
    }
    
    /* add weight of one to all output*/
    for(int i = 0; i < nn2.getOutputNeurons().size(); i++){
      for(int j = 0; j < nn2.getHiddenNeurons().size(); j++){
        nn2.getOutputNeurons().get(i).addInput(nn2.getHiddenNeurons().get(j), 0.0);
      }
    }
    
    res = 1 / (1 + Math.exp(-(0.2 * 0.0 + 0.5 * 0.0)));
    for(int i = 0; i < nn2.getHiddenNeurons().size(); i++){
      assertEquals(res, nn2.getHiddenNeurons().get(i).calculate(), 0.0);
    }
  }  
  
  @Test
  public void errorTest(){
    Neuron n = new Neuron(new SigmoidFunction());
    n.setError(0.5);
    assertEquals(0.5, n.getError(), 0.0);
    n.setError(102.3);
    assertEquals(102.3, n.getError(), 0.0);
    
    InputNeuron in = new InputNeuron();
    in.setError(2.0);
    assertEquals(2.0, in.getError(), 0.0);
    in.setError(200.0*3.5);
    assertEquals(200.0*3.5, in.getError(), 0.0);
  }
  
  @Test
  public void  transferFunctionTest(){
    TransferFunction sigf = new SigmoidFunction();
    Neuron n = new Neuron(sigf);
    TransferFunction signf = new SignFunction();
    assertEquals(sigf, n.getTransferFunction());
    assertNotEquals(signf, n.getTransferFunction());
  }
  
  @Test
  public void addConnectionTest(){
    TransferFunction sigf = new SigmoidFunction();
    Neuron hn = new Neuron(sigf);
    Neuron hn2 = new InputNeuron();
    Neuron o = new Neuron(sigf);
    Neuron o2 = new Neuron(sigf);
    InputNeuron in = new InputNeuron();
    
    in.addConnection(new Connection(in, hn, 0.5));
    in.addConnection(new Connection(in, hn2, 0.6));
    assertEquals(0, in.getInputs().size());
    assertEquals(2, in.getOutputs().size());
    
    Connection cn = new Connection(hn, o, 0.7);
    hn.addConnection(cn);
    hn.addConnection(new Connection(hn, o2, 0.5));
    hn.addConnection(new Connection(o, hn, 0.7));
    hn.addConnection(new Connection(o2, hn, 0.5));
    hn2.addConnection(new Connection(hn2, o, 0.1));
    hn2.addConnection(new Connection(hn2, o2, 0.1));
    assertEquals(2, hn.getInputs().size());
    assertEquals(2, hn.getOutputs().size());
    assertEquals(cn, hn.getOutputs().get(0));
    assertEquals(0, hn2.getInputs().size());
    assertNotEquals(3, hn2.getOutputs().size());
    
  }
  
  private NeuralNetwork buildNetwork(){
    NeuralNetwork nn = new NeuralNetwork();
    TransferFunction sigmFunc = new SigmoidFunction();
    
    InputNeuron in1 = new InputNeuron();
    InputNeuron in2 = new InputNeuron();
    Neuron hn1 = new Neuron(sigmFunc);
    Neuron hn2 = new Neuron(sigmFunc);
    Neuron hn3 = new Neuron(sigmFunc);
    Neuron on1 = new Neuron(sigmFunc);
    Neuron on2 = new Neuron(sigmFunc);
    
    nn.addInput(in1);
    nn.addInput(in2);
    nn.addHidden(hn1);
    nn.addHidden(hn2);
    nn.addHidden(hn3);
    nn.addOutput(on1);
    nn.addOutput(on2);
    
    return nn;
    
  }
}
