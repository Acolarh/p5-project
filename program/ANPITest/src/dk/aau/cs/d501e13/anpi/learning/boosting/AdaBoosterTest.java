package dk.aau.cs.d501e13.anpi.learning.boosting;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.StrongClassifier;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;
import dk.aau.cs.d501e13.anpi.learning.boosting.AdaBooster;
import dk.aau.cs.d501e13.anpi.testclasses.TestClassifier;
import dk.aau.cs.d501e13.anpi.testclasses.TestSample;
import dk.aau.cs.d501e13.anpi.testclasses.TestWeakLearner;

public class AdaBoosterTest {
  
  @Test
  public void testNormalize() {   
     //{input, expected}
    double[][][] testCases = new double[][][]{
      {{10, 10, 10, 10}, {0.25, 0.25, 0.25, 0.25}},
      {{-5, 2, 39, 2, 5}, {-0.116, 0.047, 0.907, 0.047, 0.116}},
      {{0.28, 0.99, 15.5, 28.0, -89.899}, {-0.006, -0.022, -0.343, -0.620, 1.992}}
    };

    for (int i = 0; i < testCases.length; i++){
      AdaBooster.normalize(testCases[i][0]);
      double sum = 0;
      for (int p = 0; p < testCases[i][0].length; p++){
        assertEquals(testCases[i][1][p], testCases[i][0][p], 0.001);
        sum += testCases[i][0][p];
      }
      assertEquals(1.0, sum, 0.001);
    }
  }
    
  /**
   * A test based on the AdaBoost example from 
   * http://www.csie.ntu.edu.tw/~b92109/course/Machine%20Learning/AdaBoostExample.pdf 
   * @throws InvalidTrainingSamplesException 
   */
  @Test
  public void testBoost() throws InvalidTrainingSamplesException {
    int iterations = 3;
       
    //generate training data from example
    TestSample[] trainingSamples = new TestSample[]{
        new TestSample(true, 0),
        new TestSample(true, 1),
        new TestSample(true, 2),
        new TestSample(false, 3),
        new TestSample(false, 4),
        new TestSample(false, 5),
        new TestSample(true, 6),
        new TestSample(true, 7),
        new TestSample(true, 8),
        new TestSample(false, 9)
        };
    
    boolean[] predicates = new boolean[]{true, true, true, false, false, false, true, true, true, false};
    
    //use adaboost to boost classifiers
    AdaBooster<TestSample> b = new AdaBooster<TestSample>(iterations);
    WeakLearner<TestSample> weakLearner = new TestWeakLearner(); 
    ThresholdedClassifier<TestSample> boosted = b.boost(trainingSamples, predicates, weakLearner);
    StrongClassifier<TestSample> calculated = (StrongClassifier<TestSample>) boosted.getClassifier();
    
    //check that output of adaboost is as expected
    assertEquals(3, calculated.size());
    assertEquals(0.423649, calculated.get(0).getWeight(), 0.001);
    assertEquals(0.6496, calculated.get(1).getWeight(), 0.001);
    assertEquals(0.752, calculated.get(2).getWeight(), 0.001);
    assertEquals(2.5, ((TestClassifier)calculated.get(0).getClassifier()).getThreshold(), 0);
    assertEquals(8.5, ((TestClassifier)calculated.get(1).getClassifier()).getThreshold(), 0);
    assertEquals(5.5, ((TestClassifier)calculated.get(2).getClassifier()).getThreshold(), 0);
    
    //check that classifiers are correct
    assertEquals(true, ((TestClassifier)calculated.get(0).getClassifier()).predict(new TestSample(2)));
    assertEquals(false, ((TestClassifier)calculated.get(0).getClassifier()).predict(new TestSample(3)));
    assertEquals(true, ((TestClassifier)calculated.get(1).getClassifier()).predict(new TestSample(8)));
    assertEquals(false, ((TestClassifier)calculated.get(1).getClassifier()).predict(new TestSample(9)));
    assertEquals(false, ((TestClassifier)calculated.get(2).getClassifier()).predict(new TestSample(5)));
    assertEquals(true, ((TestClassifier)calculated.get(2).getClassifier()).predict(new TestSample(6)));

  }
}
