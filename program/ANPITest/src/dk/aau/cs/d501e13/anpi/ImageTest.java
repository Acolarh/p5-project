package dk.aau.cs.d501e13.anpi;

import static org.junit.Assert.*;

import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

public class ImageTest {

  @Test
  public void testConstructor() throws ImageReadException {
    String invalid_filename = "invalid_filename.com";
    Image image = new Image("assets/test.png");
    assertEquals(298, image.getWidth());
    assertEquals(430, image.getHeight());
    assertEquals(232, image.getRed(0, 0));
    assertEquals(230, image.getGreen(0, 0));
    assertEquals(235, image.getBlue(0, 0));
    assertEquals(231, image.getGray(0, 0));
    try {
      image = new Image(invalid_filename);
      fail("Expected ImageRedException");
    }
    catch (ImageReadException exception) {
      assertEquals("Unable to open file: " + invalid_filename, exception.getMessage());
    }
  }
  
  @Test
  public void testSetters() throws ImageReadException {
    Image image = new Image("assets/test.png");
    image.setRed(0, 0, 0);
    assertEquals(0, image.getRed(0, 0));
    image.setRed(0, 0, 117);
    assertEquals(117, image.getRed(0, 0));
    image.setRed(0, 0, 255);
    assertEquals(255, image.getRed(0, 0));
    image.setGreen(0, 0, 0);
    assertEquals(0, image.getGreen(0, 0));
    image.setGreen(0, 0, 117);
    assertEquals(117, image.getGreen(0, 0));
    image.setGreen(0, 0, 255);
    assertEquals(255, image.getGreen(0, 0));
    image.setBlue(0, 0, 0);
    assertEquals(0, image.getBlue(0, 0));
    image.setBlue(0, 0, 117);
    assertEquals(117, image.getBlue(0, 0));
    image.setBlue(0, 0, 255);
    assertEquals(255, image.getBlue(0, 0));
    assertEquals(255, image.getGray(0, 0));
    image.setGray(0, 0, 0);
    assertEquals(0, image.getGray(0, 0));
    image.setGray(0, 0, 117);
    assertEquals(117, image.getGray(0, 0));
    image.setGray(0, 0, 255);
    assertEquals(255, image.getGray(0, 0));
  }
  
  @Test
  public void testSubimage() throws ImageReadException, ImageCloneException {
    Image image = new Image("assets/test.png");
    Image sub = image.getSubimage(new Rectangle(10, 11, 12, 13));
    assertEquals(12, sub.getWidth());
    assertEquals(13, sub.getHeight());
    assertEquals(255, sub.getRed(0, 0));
    assertEquals(0, sub.getGreen(0, 0));
    assertEquals(0, sub.getBlue(0, 0));
  }
  
  @Test
  public void testResize() throws ImageReadException, ImageCloneException {
    Image image = new Image("assets/test.png");
    Image res = image.getResized(100, 102);
    assertEquals(100, res.getWidth());
    assertEquals(102, res.getHeight());
    res = res.getResized(1, 1);
    assertEquals(1, res.getWidth());
    assertEquals(1, res.getHeight());
    int r = res.getRed(0, 0);
    int g = res.getGreen(0, 0);
    int b = res.getBlue(0, 0);
    res.setGray(0, 0, 42);
    res = res.getResized(1000, 1000);
    assertEquals(r, res.getRed(500, 500));
    assertEquals(g, res.getGreen(500, 500));
    assertEquals(b, res.getBlue(500, 500));
    assertEquals(42, res.getGray(500, 500));
  }

  @Test
  public void testIntegralImage() throws ImageCloneException {
    Image image = new Image(3, 3);
    image.setGray(0, 0, 5);
    image.setGray(1, 0, 5);
    image.setGray(2, 0, 5);
    image.setGray(0, 1, 10);
    image.setGray(1, 1, 10);
    image.setGray(2, 1, 10);
    image.setGray(0, 2, 15);
    image.setGray(1, 2, 15);
    image.setGray(2, 2, 15);
    // Image:
    //   5   5   5
    //  10  10  10
    //  15  15  15
    // Should result in integral image:
    //   5  10  15
    //  15  30  45
    //  30  60  90
    assertEquals(5, image.getSum(0, 0));
    assertEquals(10, image.getSum(1, 0));
    assertEquals(15, image.getSum(2, 0));
    assertEquals(15, image.getSum(0, 1));
    assertEquals(30, image.getSum(1, 1));
    assertEquals(45, image.getSum(2, 1));
    assertEquals(30, image.getSum(0, 2));
    assertEquals(60, image.getSum(1, 2));
    assertEquals(90, image.getSum(2, 2));
    assertEquals(10 + 10 + 15 + 15, image.getSum(1, 1, 2, 2));
    assertEquals(10 + 10 + 15 + 15, image.getSum(new Rectangle(1, 1, 2, 2)));
    assertEquals(60, image.getSum(1, 0, 2, 3));
    assertEquals(75, image.getSum(0, 1, 3, 2));
    assertEquals(90, image.getSum(0, 0, 3, 3));
    Image sub = image.getSubimage(new Rectangle(1, 1, 1, 1));
    assertEquals(10, sub.getSum(0, 0, 1, 1));
  }
  
  @Test
  public void testEdges() throws ImageReadException {
    Image image = new Image("assets/test.png");
    Image edges = image.getEdges(new EdgeDetector() {
      @Override
      public void calculate(Raster input, WritableRaster output) {
        output.setSample(5, 5, 0, 42);
      }
    });
    assertEquals(42, edges.getGray(5, 5));
  }
  
  @Test
  public void testEquals() throws ImageReadException{
    Image image1 = new Image("assets/test.png");
    Image image2 = new Image("assets/negativeTest.png");
    Image image3 = new Image("assets/positiveTest.png");
    Image white = new Image("assets/whitepix.png");
    Image black = new Image("assets/blackpix.png");
    
    assertTrue(image1.equals(image1));
    assertTrue(image2.equals(image2));
    assertTrue(image3.equals(image3));
    assertTrue(black.equals(black));
    assertTrue(image2.equals(image3));
    
    assertFalse(image1.equals(image3));
    assertFalse(white.equals(black));
  }
}
