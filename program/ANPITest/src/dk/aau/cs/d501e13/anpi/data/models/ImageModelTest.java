package dk.aau.cs.d501e13.anpi.data.models;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.data.Database;

public class ImageModelTest {

  private static Database db;
  
  private ImageModel model;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    db = new Database("db.properties");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    db.close();
  }
  
  @Before
  public void setUp() throws SQLException {
    model = new ImageModel(db);
    new NumberplateModel(db);
    model.executeRaw("TRUNCATE " + model.name());
  }
  
  @After
  public void tearDown() throws SQLException {
    model.executeRaw("TRUNCATE " + model.name());
  }
  
  @Test
  public void testName() {
    assertEquals("test_images", model.name());
  }
  
  @Test
  public void testCreate() throws SQLException {
    ImageRecord record;
    
    record = model.create("test.png", ImageRecord.SET_TEST);
    assertNotNull(record);
    assertEquals("test.png", record.name);
    assertEquals(ImageRecord.SET_TEST, record.set);
    
    record = model.create("", ImageRecord.SET_TRAINING);
    assertNotNull(record);
    assertEquals("", record.name);
    assertEquals(ImageRecord.SET_TRAINING, record.set);
    
    try {
      // Name is unique
      model.create("test.png", ImageRecord.SET_UNSET);
      fail("Expected SQLException");
    }
    catch (SQLException exception) {
    }
  }
  
  @Test
  public void testFind() throws SQLException {
    ImageRecord record = model.create("test.png", ImageRecord.SET_UNSET);
    int id = record.id;

    record = model.find(id);
    assertNotNull(record);
    assertEquals("test.png", record.name);
    
    record.delete();
    
    record = model.find(id);
    assertNull(record);
  }
  
  @Test
  public void testFindByName() throws SQLException {
    ImageRecord record = model.create("test.png", ImageRecord.SET_UNSET);
    int id = record.id;

    record = model.findByName("test.png");
    assertNotNull(record);
    assertEquals("test.png", record.name);
    assertEquals(id, record.id);
    
    record.name = "test2.png";
    record.save();
    
    record = model.findByName("test.png");
    assertNull(record);

    record = model.findByName("test2.png");
    assertNotNull(record);
    assertEquals("test2.png", record.name);
    assertEquals(id, record.id);
    
    record.delete();
    
    record = model.findByName("test.png");
    assertNull(record);
  }
  
  @Test
  public void testQueryMultiple() throws SQLException {
    model.prepare("testMultiple", "SELECT * FROM " + model.name() + " WHERE name LIKE ?");
    
    model.create("test1.png", ImageRecord.SET_UNSET);
    model.create("test2.png", ImageRecord.SET_UNSET);
    model.create("test3.png", ImageRecord.SET_UNSET);
    model.create("not1.png", ImageRecord.SET_UNSET);
    model.create("not2.png", ImageRecord.SET_UNSET);
    
    ImageRecord[] records = (ImageRecord[])model.queryMultiple("testMultiple", "test%");
    assertEquals(3, records.length);
    for (ImageRecord r : records) {
      r.delete();
    }
    records = (ImageRecord[])model.queryMultiple("testMultiple", "test%");
    assertEquals(0, records.length);

    records = (ImageRecord[])model.queryMultiple("testMultiple", "not%");
    assertEquals(2, records.length);
  }

}
