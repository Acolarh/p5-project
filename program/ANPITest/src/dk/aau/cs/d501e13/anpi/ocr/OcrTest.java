package dk.aau.cs.d501e13.anpi.ocr;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.neuralnetwork.InputNeuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.NeuralNetwork;
import dk.aau.cs.d501e13.anpi.neuralnetwork.Neuron;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class OcrTest {
  
  @Test
  public void getValuesTest() throws IOException{
    OCR ocr = newOcr();
    
    assertEquals(36, ocr.getNumOutputs());
    assertEquals(10, ocr.getWidth());
    assertEquals(12, ocr.getHeight());
    assertEquals(1, ocr.getNumHiddenLayers());
    assertEquals(60, ocr.getNumHiddenNeurons());
    assertEquals(0.1, ocr.getThreshold(), 0.0);
    assertEquals(0.2, ocr.getLearningRate(), 0.0);
    assertEquals(0.0, ocr.getMomentum(), 0.0);
  }
  
  @Test
  public void setNeuralNetworkTest() throws IOException{
    NeuralNetwork nn = new NeuralNetwork();
    TransferFunction sig = new SigmoidFunction();
    InputNeuron input = new InputNeuron();
    Neuron hidden = new Neuron(sig);
    Neuron output = new Neuron(sig);
    nn.addInput(input);
    nn.addHidden(hidden);
    nn.addOutput(output);
    
    OCR ocr = newOcr(); 
    ocr.setNeuralNetwork(nn);
    
    assertEquals(nn, ocr.getNeuralNetwork());
    
  }
  
  @Test
  public void charAndOffsetTest() throws IOException{
    OCR ocr = newOcr();
    
    assertEquals(0, ocr.charToOffset('0'));
    assertEquals(9, ocr.charToOffset('9'));
    assertEquals(10, ocr.charToOffset('A'));
    assertEquals(35, ocr.charToOffset('Z'));
    
    assertEquals('0', ocr.offsetToChar(0));
    assertEquals('9', ocr.offsetToChar(9));
    assertEquals('A', ocr.offsetToChar(10));
    assertEquals('Z', ocr.offsetToChar(35));
  }
  
  @Test
  public void getExpectedOutputsTest() throws IOException{
    OCR ocr = newOcr();
    
    double[] actual = new double[ocr.getNumOutputs()];
    double[] expected = new double[ocr.getNumOutputs()];
    Arrays.fill(expected, 0.0);
    expected[10] = 1.0;
    actual = ocr.getExpectedOutputs('A');
    assertEquals(expected[10], actual[10], 0.0);
    
    expected[10] = 0.0;
    expected[0] = 1.0;
    actual = ocr.getExpectedOutputs('0');
    assertEquals(expected[0], actual[0], 0.0);

    expected[0] = 0.0;
    expected[9] = 1.0;
    actual = ocr.getExpectedOutputs('9');
    assertEquals(expected[9], actual[9], 0.0);
    
    expected[9] = 0.0;
    expected[35] = 1.0;
    actual = ocr.getExpectedOutputs('Z');
    assertEquals(expected[35], actual[35], 0.0);
    
  }
  
  @Test
  public void getCharacterTest() throws IOException{
    OCR ocr = newOcr();
    
    double[] outputs = newOutputs(10);
    assertEquals('A', ocr.getCharacter(outputs));
    
    outputs = newOutputs(0);
    assertEquals('0', ocr.getCharacter(outputs));
    
    outputs = newOutputs(9);
    assertEquals('9', ocr.getCharacter(outputs));
    
    outputs = newOutputs(35);
    assertEquals('Z', ocr.getCharacter(outputs));
    
  }
  
  @Test
  public void getWeigthedCharactersTest() throws IOException{
    OCR ocr = newOcr();
    
    int[] values = {10, 0, 9, 35};
    double[] outputs = newOutputs(values);
    double[] expected = new double[values.length / 2];
    
    expected[0] = 0.55;
    expected[1] = 0.5;
    
    List<WeightedChar> weightedActual = ocr.getCharacters(outputs);
    double[] actual = new double[weightedActual.size()];
    int i = 0;
    for(WeightedChar c : weightedActual){
      if(c.weight >= 0.5){
        actual[i++] = c.weight;
      }
    }
    
    assertEquals(expected.length, actual.length);
    
    for(i = 0; i < actual.length; i++){
      assertEquals(expected[i], actual[i], 0.0);
    }
    
  }
  
  @Test
  public void getInputVectorTest() throws ImageReadException, IOException{
    OCR ocr = newOcr();
    Image white = new Image("assets/white50x50.png");
    double[] inputs = ocr.getInputVector(white);
    double[] expected = new double[inputs.length];
    Arrays.fill(expected, 1.0);
    for(int i = 0; i < expected.length; i++){
      assertEquals(expected[i], inputs[i], 0.0);
    }
      
    Image black = new Image("assets/black50x50.png");
    inputs = ocr.getInputVector(black);
    Arrays.fill(expected, 0.0);
    for(int i = 0; i < expected.length; i++){
      assertEquals(expected[i], inputs[i], 0.0);
    }
    
  }
  
  /**
   * @return New OCR object.
   * @throws IOException
   */
  private OCR newOcr() throws IOException{
    Config config = new Config("assets/ocr.yml");
    OCR ocr = new OCR(config);
    return ocr;
  }
  
  /**
   * Creates a new array with outputs.
   * @param x If the length of x is 1 then outputs[x[0]] = 0.99. 
   *          Otherwise the first half of x will have high values, and the rest will have low. 
   * @return The outputs array.
   * @throws IOException
   */
  private double[] newOutputs(int... x) throws IOException{
    OCR ocr = newOcr();
    double[] outputs = new double[ocr.getNumOutputs()];
    for(int i = 0; i < outputs.length; i++){
      outputs[i] = Math.random() * 0.1;
    }
    
    if(x.length == 1){
      outputs[x[0]] = 0.99;
      return outputs;
    }
    
    double y = 1.0 / (double)x.length * 2.0;
    for(int i = 0; i < x.length / 2; i++){
      outputs[x[i]] = y;
      y += 0.05;
    }
    
    y = 1.0 / (double)x.length;
    for(int i = x.length / 2; i < x.length; i++){
      outputs[x[i]] = y;
      y += 0.05;
    }
    
    return outputs;
  }

}
