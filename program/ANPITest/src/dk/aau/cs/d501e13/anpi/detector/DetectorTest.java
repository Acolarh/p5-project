package dk.aau.cs.d501e13.anpi.detector;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.edgedetectors.PrewittEdgeDetector;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeature;

public class DetectorTest {
  private ThresholdedClassifier<Image> makeHaar(Rectangle[] adds, Rectangle[] subs, int w, int h){
    return new ThresholdedClassifier<>(
        new HaarFeature(adds,subs,w,h)
        , 0.0, true);
  }
  @Test
  public void TestFoundRegionsWithoutEdgeDetection() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{new Rectangle(0, 1, 1, 2)};
    
    Image image = new Image(100, 100);
    
    //Make a 21 x 21 square starting at pos 40 x 40
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
        if (x >= 40 && x <= 60 && y >= 40 && y <= 60)
          image.setGray(x, y, 255);
        else
          image.setGray(x, y, 0);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 3));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    detector.setMinHeight(3); //matching the Haar feature height
    detector.setMinWidth(1); //matching the Haar feature width
    detector.setMoveFactor(0);
    detector.setScaleFactor(0);
    
    List<Rectangle> rectangles = detector.detectPlates(image);
    
    assertFalse(contains(rectangles, new Rectangle(0, 0, 1, 1)));
    assertFalse(contains(rectangles, new Rectangle(0, 0, 10, 10)));
    assertFalse(contains(rectangles, new Rectangle(20, 20, 10, 10)));
    assertFalse(contains(rectangles, new Rectangle(40, 40, 21, 21)));
    assertFalse(contains(rectangles, new Rectangle(40, 57, 21, 4)));
    assertFalse(contains(rectangles, new Rectangle(40, 40, 21, 28)));
    
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 3)));
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 39)));
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 3)));
    assertTrue(contains(rectangles, new Rectangle(40, 57, 21, 9)));
  }
  
  @Test
  public void TestFoundRegionsWithEdgeDetection() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{new Rectangle(0, 1, 1, 2)};
    
    Image image = new Image(100, 100);
    
    image = image.getEdges(new PrewittEdgeDetector());

    //Make a 21 x 21 square starting at pos 40 x 40
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
        if (x >= 40 && x <= 60 && y >= 40 && y <= 60)
          image.setGray(x, y, 255);
        else
          image.setGray(x, y, 0);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 3));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    detector.setMinHeight(3); //matching the Haar feature height
    detector.setMinWidth(1); //matching the Haar feature width
    detector.setMoveFactor(0);
    detector.setScaleFactor(0);
    
    List<Rectangle> rectangles = detector.detectPlates(image);
    
    assertFalse(contains(rectangles, new Rectangle(0, 0, 1, 1)));
    assertFalse(contains(rectangles, new Rectangle(0, 0, 10, 10)));
    assertFalse(contains(rectangles, new Rectangle(20, 20, 10, 10)));
    assertFalse(contains(rectangles, new Rectangle(40, 40, 21, 21)));
    assertFalse(contains(rectangles, new Rectangle(40, 57, 21, 4)));
    assertFalse(contains(rectangles, new Rectangle(40, 40, 21, 28)));
    
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 3)));
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 39)));
    assertTrue(contains(rectangles, new Rectangle(40, 60, 21, 3)));
    assertTrue(contains(rectangles, new Rectangle(40, 57, 21, 9)));
  }
  
  @Test
  public void TestMoveFactor() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{};
    
    Image image = new Image(100, 100);
    
    //Make all pixels white
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
          image.setGray(x, y, 255);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 1));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    
    //Set an extremely high scale factor so frame size is never scaled, thus it is always (minWidth, minHeight)
    detector.setScaleFactor(100000);
    
    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setMoveFactor(0.9);
    List<Rectangle> rectangles = detector.detectPlates(image);
    assertEquals(4, rectangles.size());

    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setMoveFactor(0.4);
    rectangles = detector.detectPlates(image);
    assertEquals(9, rectangles.size());
    
    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setMoveFactor(0);
    rectangles = detector.detectPlates(image);
    assertEquals(51*51, rectangles.size());
    
    detector.setMinHeight(1);
    detector.setMinWidth(1);
    detector.setMoveFactor(0);
    rectangles = detector.detectPlates(image);
    assertEquals(100*100, rectangles.size());
    
    detector.setMinHeight(1);
    detector.setMinWidth(1);
    detector.setMoveFactor(10);
    rectangles = detector.detectPlates(image);
    assertEquals(10*10, rectangles.size());
    
    detector.setMinHeight(10);
    detector.setMinWidth(10);
    detector.setMoveFactor(10);
    rectangles = detector.detectPlates(image);
    assertEquals(1, rectangles.size());
    
    detector.setMinHeight(10);
    detector.setMinWidth(10);
    detector.setMoveFactor(4);
    rectangles = detector.detectPlates(image);
    assertEquals(3*3, rectangles.size());
  }
  
  @Test
  public void TestScaleFactor() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{};
    
    Image image = new Image(100, 100);
    
    //Make all pixels white
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
          image.setGray(x, y, 255);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 1));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    
    //Set an extremely high scale factor so frame size is never scaled, thus it is always (minWidth, minHeight)
    detector.setMoveFactor(10000000);
    
    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setScaleFactor(0.9);
    List<Rectangle> rectangles = detector.detectPlates(image);
    assertEquals(4, rectangles.size());
    
    detector.setMinHeight(100);
    detector.setMinWidth(1);
    detector.setScaleFactor(1);
    rectangles = detector.detectPlates(image);
    assertEquals(6, rectangles.size());
    
    detector.setMinHeight(1);
    detector.setMinWidth(100);
    detector.setScaleFactor(1);
    rectangles = detector.detectPlates(image);
    assertEquals(6, rectangles.size());
    
    detector.setMinHeight(100);
    detector.setMinWidth(1);
    detector.setScaleFactor(0);
    rectangles = detector.detectPlates(image);
    assertEquals(100, rectangles.size());
    
    detector.setMinHeight(99);
    detector.setMinWidth(99);
    detector.setScaleFactor(0);
    rectangles = detector.detectPlates(image);
    assertEquals(4, rectangles.size());
    
    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setScaleFactor(1);
    rectangles = detector.detectPlates(image);
    assertEquals(1, rectangles.size());
  }
  
  @Test
  public void TestCombinedScalefactorAndMovefactor() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{};
    
    Image image = new Image(100, 100);
    
    //Make all pixels white
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
          image.setGray(x, y, 255);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 1));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    
    detector.setMoveFactor(1);
    detector.setScaleFactor(1);
    detector.setMinHeight(30);
    detector.setMinWidth(30);
    List<Rectangle> rectangles = detector.detectPlates(image);
    assertEquals(16, rectangles.size());
    
    detector.setMoveFactor(2);
    detector.setScaleFactor(2);
    detector.setMinHeight(20);
    detector.setMinWidth(20);  
    rectangles = detector.detectPlates(image);
    assertEquals(9, rectangles.size());
    
    detector.setMoveFactor(3);
    detector.setScaleFactor(3);
    detector.setMinHeight(40);
    detector.setMinWidth(40);  
    rectangles = detector.detectPlates(image);
    assertEquals(1, rectangles.size());
  }
  
  @Test
  public void TestAspectratioTolerance() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{};
    
    Image image = new Image(100, 100);
    
    //Make all pixels white
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
          image.setGray(x, y, 255);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 1));
    detector.setAspectRatioMin(0.4);
    detector.setAspectRatioMax(2.2);
    detector.setMaxHeight(Integer.MAX_VALUE);
    detector.setMaxWidth(Integer.MAX_VALUE);
    
    //Set a high move factor so the frame can never be moved (without going outside detection frame)
    detector.setMoveFactor(100000);
    
    detector.setScaleFactor(1);
    detector.setMinHeight(20);
    detector.setMinWidth(20);
    List<Rectangle> rectangles = detector.detectPlates(image);
    assertEquals(7, rectangles.size());
    
    detector.setMinHeight(49);
    detector.setMinWidth(49);
    detector.setScaleFactor(0);
    detector.setAspectRatioMin(2);
    detector.setAspectRatioMax(2);
    rectangles = detector.detectPlates(image);
    assertEquals(2, rectangles.size());
    
    detector.setMinHeight(49);
    detector.setMinWidth(49);
    detector.setScaleFactor(0);
    detector.setAspectRatioMin(0.5);
    detector.setAspectRatioMax(0.5);
    rectangles = detector.detectPlates(image);
    assertEquals(2, rectangles.size());
    
    detector.setMinHeight(50);
    detector.setMinWidth(50);
    detector.setScaleFactor(0);
    detector.setAspectRatioMin(2.01);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    rectangles = detector.detectPlates(image);
    assertEquals(0, rectangles.size());
  }
  

  @Test
  public void TestMinMaxWidthAndHeight() throws DetectorException{
    Rectangle[] addRecs = new Rectangle[]{new Rectangle(0, 0, 1, 1)};
    Rectangle[] subRecs = new Rectangle[]{};
    
    Image image = new Image(100, 100);
    
    //Make all pixels white
    for (int x = 0; x < 100; x++){
      for (int y = 0; y < 100; y++){
          image.setGray(x, y, 255);
      }
    }
    
    Detector detector = new Detector(makeHaar(addRecs, subRecs, 1, 1));
    detector.setAspectRatioMin(Double.MIN_VALUE);
    detector.setAspectRatioMax(Double.MAX_VALUE);
    detector.setScaleFactor(0);
    detector.setMoveFactor(1000000);
    
    detector.setMinHeight(-100);
    detector.setMinWidth(-100);
    detector.setMaxHeight(200);
    detector.setMaxWidth(200);
    List<Rectangle> rectangles = detector.detectPlates(image);
    assertEquals(100*100, rectangles.size());
    
    detector.setMinHeight(40);
    detector.setMinWidth(30);
    detector.setMaxHeight(60);
    detector.setMaxWidth(80);
    rectangles = detector.detectPlates(image);
    assertEquals(51*21, rectangles.size());
    
    detector.setMinHeight(40);
    detector.setMinWidth(40);
    detector.setMaxHeight(40);
    detector.setMaxWidth(40);
    rectangles = detector.detectPlates(image);
    assertEquals(1, rectangles.size());
    
    detector.setMinHeight(40);
    detector.setMinWidth(40);
    detector.setMaxHeight(41);
    detector.setMaxWidth(41);
    rectangles = detector.detectPlates(image);
    assertEquals(4, rectangles.size());   
  }
  
  private boolean contains(List<Rectangle> list, Object o){
    for (int i = 0; i < list.size(); i++){
      if (list.get(i).equals(o))
        return true;
    }
    return false;
  }
}

