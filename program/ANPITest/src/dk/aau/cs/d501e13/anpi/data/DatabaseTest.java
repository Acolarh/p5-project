package dk.aau.cs.d501e13.anpi.data;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DatabaseTest {

  private static Database db;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    db = new Database("db.properties");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    db.close();
  }

  @Test
  public void testTableName() {
    assertEquals("test_images", db.tableName("images"));
    assertEquals("test_numberplates", db.tableName("numberplates"));
  }
  
  @Test
  public void testGetConnection() {
    assertNotNull(db.getConnection());
  }

}
