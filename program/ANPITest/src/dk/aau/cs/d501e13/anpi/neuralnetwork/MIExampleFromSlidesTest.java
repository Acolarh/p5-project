package dk.aau.cs.d501e13.anpi.neuralnetwork;

import static org.junit.Assert.*;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class MIExampleFromSlidesTest {

  @Test
  /*
   * The results after the first iteration has been validated using
   * the results from the MI Course slides
   * The results from both iterations and the use of momentum has been validated
   * using the Neural Network tool from AISpace.org, suggested in our MI Course
   */
  public void Test() throws CorruptedInputException{
    NeuralNetwork neuralNetwork = new NeuralNetwork();
    
    TransferFunction sigmoidFunc = new SigmoidFunction();
    
    InputNeuron in1 = new InputNeuron();
    InputNeuron in2 = new InputNeuron();
    in1.setValue(1);
    in2.setValue(0);
    
    Neuron hid1 = new Neuron(sigmoidFunc, 0.1);
    Neuron out1 = new Neuron(sigmoidFunc, 0.1);

    neuralNetwork.addInput(in1);
    neuralNetwork.addInput(in2);
    neuralNetwork.addHidden(hid1);
    neuralNetwork.addOutput(out1);
    
    Connection c1 = new Connection(in1, hid1, 0.1);
    Connection c2 = new Connection(in2, hid1, 0.1);
    Connection c3 = new Connection(hid1, out1, 0.1);
    
    hid1.addConnection(c1);
    hid1.addConnection(c2);
    out1.addConnection(c3);
    
    in1.addConnection(c1);
    in2.addConnection(c2);
    hid1.addConnection(c3);
    
 

    NeuralNetworkLearner learner = new NeuralNetworkLearner(neuralNetwork);
    
    NeuralNetworkTrainingSample trainingSample[] = new NeuralNetworkTrainingSample[]{
      new NeuralNetworkTrainingSample() {
      
        @Override
        public double[] getInputs() {
          return new double[]{1, 0};
        }
        
        @Override
        public double[] getExpectedOutputs() {
          return new double[]{1};
        }
      }
    };
    
    //expected output before any learning done
    assertEquals(0.5386685, neuralNetwork.calculate()[0], 0.0001);

    learner.setLearningRate(0.3);
    learner.learn(trainingSample);
    learner.setMomentum(0.99);
    
    //expected state after 1st iteration (momentum won't have impact yet)
    assertEquals(0.1146431, out1.getError(), 0.0001);
    assertEquals(0.002835, hid1.getError(), 0.0001);  
    
    assertEquals(0.100851, c1.weight, 0.0001);
    assertEquals(0.1,      c2.weight, 0.0001);
    assertEquals(0.100851, hid1.getThreshold(), 0.0001);
    assertEquals(0.118910, c3.weight, 0.0001);
    assertEquals(0.134392, out1.getThreshold(), 0.0001);
    
    learner.learn(trainingSample);
    
    //expected state after 2nd iteration (momentum: 0)
    /*
    assertEquals(0.101835, c1.weight, 0.0001);
    assertEquals(0.1,      c2.weight, 0.0001);
    assertEquals(0.101835, c3.weight, 0.0001);
    assertEquals(0.137305, c4.weight, 0.0001);
    assertEquals(0.167823, c5.weight, 0.0001);
    */
    
    //expected state after 2nd iteration (momentum: 0.99)
    assertEquals(0.102677, c1.weight, 0.0001);
    assertEquals(0.1,      c2.weight, 0.0001);
    assertEquals(0.102677, hid1.getThreshold(), 0.0001);
    assertEquals(0.156027, c3.weight, 0.0001);
    assertEquals(0.201872, out1.getThreshold(), 0.0001);
  }
  
  
  
  
}
