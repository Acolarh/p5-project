package dk.aau.cs.d501e13.anpi.learning.weak.haarfeature;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.learning.weak.ThresholdTrainer;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeature;

public class HaarFeatureTrainerTest {

  @Test
  public void testFastTrainedWithDistribution(){
    double[]       values = new double[]{   25,   75,  55,  35,   40,   70};
    boolean[]  predicates = new boolean[]{true,false,true,true,false,false};
    double[] distribution = new double[]{  0.1429,  0.0714, 0.2143, 0.1429,  0.2857,  0.1429};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
    
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();
    
    assertTrue("Got: " + threshold, threshold == 40);
    assertEquals(false, trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistribution(){
    double[]          values = new double[]{      25,   75,  55,  35,   40,   70};
    boolean[]  predicates = new boolean[]{true,false,true,true,false,false};
    double[] distribution = new double[]{ 0.1667,  0.1667, 0.1667, 0.1667,  0.1667,  0.1667};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();
    
    assertTrue("Got: " + threshold, threshold == 40 || threshold == 70);
    assertEquals(false, trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistribution2(){
    double[]          values = new double[]{      25,   75,  55,  35,   40,   70};
    boolean[]  predicates = new boolean[]{true,false,true,false,true,false};
    double[] distribution = new double[]{ 0.1667,  0.1667, 0.1667, 0.1667,  0.1667,  0.1667};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();
    
    assertEquals(70, threshold, 0.001);
    assertFalse(trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistributionNegativeSign(){
    double[]          values = new double[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    boolean[]  predicates = new boolean[]{ true, false, true, true, true, false, false, false, true, false };
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();
    
    assertEquals(6, threshold, 0.001);
    assertFalse(trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistributionPositiveSign(){
    double[]          values = new double[]{ 2, 3, 5, 6, 7, 8, 9, 10, 1, 4};
    boolean[]  predicates = new boolean[]{ true, false, false, true, true, true, false, true, false, false};
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();

    assertEquals(5, threshold, 0.001);
    assertTrue(trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistributionPositiveSign2(){
    double[]          values = new double[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    boolean[]  predicates = new boolean[]{ false, false, false, false, false, false, false, true, true, true };
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();

    assertEquals(7, threshold, 0.001);
    assertTrue(trainer.getSign());
  }
  
  @Test
  public void testFastTrainedWithNoDistributionNegativeSign2(){
    double[]          values = new double[]{ 7, 1, 2, 3, 4, 6,  8, 9, 10, 5};
    boolean[]  predicates = new boolean[]{ true, true, true, true, true, true, false, false, false, true};
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();

    assertEquals(8, threshold, 0.001);
    assertFalse(trainer.getSign());
  }
  
  @Test
  public void testFastTrainedNoDistributionAllPositive(){
    double[]          values = new double[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    boolean[]  predicates = new boolean[]{ true, true, true, true, true, true, true, true, true, true };
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();

    assertTrue("Got threshold: " + threshold + ", sign: " + trainer.getSign(), 
      (threshold == Double.NEGATIVE_INFINITY && trainer.getSign()) || (threshold == Double.MAX_VALUE && !trainer.getSign()));
  }
  
  @Test
  public void testFastTrainedNoDistributionAllNegative(){
    double[]          values = new double[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    boolean[]  predicates = new boolean[]{ false, false, false, false, false, false, false, false, false, false };
    double[] distribution = new double[]{ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
    HaarFeature feature = new HaarFeature(null, null, 0, 0);
     
    ThresholdTrainer<Image> trainer = new ThresholdTrainer<Image>(feature, values, predicates, distribution);
    
    trainer.train();
    
    double threshold = trainer.getThreshold();

    assertTrue("Got threshold: " + threshold + ", sign: " + trainer.getSign(), 
      (threshold == Double.NEGATIVE_INFINITY && !trainer.getSign()) || (threshold == Double.MAX_VALUE && trainer.getSign()));
  }
  
}
