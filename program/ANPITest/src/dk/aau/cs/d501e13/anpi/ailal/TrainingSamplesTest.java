package dk.aau.cs.d501e13.anpi.ailal;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.ailal.TrainingSamples.SamplesArray;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

public class TrainingSamplesTest {
  
  @Test
  public void testAddPositivesFromDb() throws ClassNotFoundException, IOException, 
      SQLException, ImageCloneException{
    TrainingSamples ts = new TrainingSamples(10, 5);
    
    Database db = new Database("db.properties");
    ImageModel img = new ImageModel(db);
    NumberplateModel npm = new NumberplateModel(db);
    
    img.executeRaw("TRUNCATE " + img.name());
    npm.executeRaw("TRUNCATE " + npm.name());
    
    ImageRecord imgRecord = img.create("test2.jpg", ImageRecord.SET_UNSET);
    NumberplateRecord npmRecord = npm.create(imgRecord, 10, 15, 20, 25, 0, "AA 11 111");
    
    assertNotNull(npmRecord);
    assertEquals(imgRecord.id, npmRecord.imageId);
    
    ts.addPositivesFromDb("assets/", img, npm);
    
    assertEquals(1, ts.size());
    
  }
  
  @Test
  public void testSave() throws ImageReadException {
    TrainingSamples ts = new TrainingSamples(10, 5);
    ts.load("assets/TestSamplesTest.png", "assets/TestSamplesTest.png");
    ts.save("assets/positiveTest.png", "assets/negativeTest.png");

    Image image1 = new Image("assets/positiveTest.png");
    Image image2 = new Image("assets/negativeTest.png");
    Image image3 = new Image("assets/TestSamplesTest.png");
    
    assertEquals(image1, image2);
    assertEquals(image2, image3);
    assertEquals(image1, image3);
  }
  
  @Test
  public void testLoad() throws ImageCloneException, ImageReadException {
    TrainingSamples ts = new TrainingSamples(10, 5);
    ts.load("assets/TestSamplesTest.png", "assets/TestSamplesTest.png");
    List<Image> pos = ts.getPositives();
    Image image3 = new Image("assets/TestSamplesTest.png");
    
    for(int i = 0; i < pos.size(); i++){
      assertEquals(pos.get(i), image3.getSubimage(new Rectangle(i * 10, 0, 10, 5)));
    }
  }
  
  @Test
  public void testSize() {
    TrainingSamples ts = new TrainingSamples(10, 5);
    assertEquals(0, ts.size());
    ts.loadPositives("assets/TestSamplesTest.png");
    assertEquals(5, ts.size());
    ts.loadNegatives("assets/TestSamplesTest.png");
    assertEquals(10, ts.size());
    assertEquals(5, ts.getNegatives().size());
    assertEquals(5, ts.getPositives().size());
    
    SamplesArray sa = ts.getSamplesArray();
    assertEquals(10, sa.predicates.length);
    assertEquals(10, sa.samples.length);
    
    int i, sumPos, sumNeg;
    
    for(i = 0, sumPos = 0, sumNeg = 0; i < sa.predicates.length; i++){
      if(sa.predicates[i] == true)
        sumPos++;
      else if(sa.predicates[i] == false)
        sumNeg++;
    }
    assertEquals(5, sumPos);
    assertEquals(5, sumNeg);
  } 
}
