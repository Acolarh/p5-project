package dk.aau.cs.d501e13.anpi.data.models;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.data.Database;

public class NumberplateModelTest {

  private static Database db;
  
  private ImageModel imageModel;
  private NumberplateModel numberplateModel;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    db = new Database("db.properties");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    db.close();
  }
  
  @Before
  public void setUp() throws SQLException {
    numberplateModel = new NumberplateModel(db);
    numberplateModel.executeRaw("TRUNCATE " + numberplateModel.name());
    
    imageModel = new ImageModel(db);
    imageModel.executeRaw("TRUNCATE " + imageModel.name());
  }
  
  @After
  public void tearDown() throws SQLException {
    numberplateModel.executeRaw("TRUNCATE " + numberplateModel.name());
    imageModel.executeRaw("TRUNCATE " + imageModel.name());
  }
  
  @Test
  public void testName() {
    assertEquals("test_numberplates", numberplateModel.name());
  }
  
  @Test
  public void testCreate() throws SQLException {
    ImageRecord imageRecord = imageModel.create("test.png", ImageRecord.SET_UNSET);
    
    NumberplateRecord record = numberplateModel.create(imageRecord, 10, 15, 20, 25, 0, "AA 11 111");
    assertNotNull(record);
    assertEquals(imageRecord.id, record.imageId);
    assertEquals(10, record.x);
    assertEquals(15, record.y);
    assertEquals(20, record.width);
    assertEquals(25, record.height);
    assertEquals(0, record.flags.getFlags());
    assertEquals("AA 11 111", record.value);
    
    ImageRecord imageRecord2 = record.getImage();
    
    assertEquals(imageRecord, imageRecord2);
    
    NumberplateRecord[] records = imageRecord.getNumberplates();
    assertEquals(1, records.length);
    records[0].delete();

    records = imageRecord.getNumberplates();
    assertEquals(0, records.length);
  }
  
  @Test
  public void testFind() throws SQLException {
    ImageRecord imageRecord = imageModel.create("test.png", ImageRecord.SET_UNSET);
    
    NumberplateRecord record = numberplateModel.create(imageRecord, 10, 15, 20, 25, 0, "AA 11 111");
    int id = record.id;

    record = numberplateModel.find(id);
    assertNotNull(record);
    assertEquals("AA 11 111", record.value);
    
    record.delete();
    
    record = numberplateModel.find(id);
    assertNull(record);
  }  

}
