package dk.aau.cs.d501e13.anpi.neuralnetwork;

public class NNTrainingSample implements NeuralNetworkTrainingSample {

  double[] inputs;
  double[] outputs;
  public NNTrainingSample(double[] inputs, double[] outputs){
    this.inputs = inputs;
    this.outputs = outputs;
  }
  
  @Override
  public double[] getInputs() {
    return inputs;
  }

  @Override
  public double[] getExpectedOutputs() {
    return outputs;
  }

}
