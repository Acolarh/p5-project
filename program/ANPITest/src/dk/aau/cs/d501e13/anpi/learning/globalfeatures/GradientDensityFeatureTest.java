package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;

public class GradientDensityFeatureTest {

  @Test
  public void thresholdTest(){
    GradientDensityFeature gdf = new GradientDensityFeature();
    assertEquals(0.5, gdf.getThreshold(), 0.0);
    gdf.setThreshold(0.7);
    assertEquals(0.7, gdf.getThreshold(), 0.0);
  }
  
  @Test
  public void getGradientDensityTest() throws ImageReadException{
    GradientDensityFeature gdf = new GradientDensityFeature();
    Image img = new Image("assets/black50x50.png");
    assertEquals(0.0, gdf.getGradientDensity(img), 0.0);
    
    img = new Image("assets/white50x50.png");
    assertEquals(255.0, gdf.getGradientDensity(img), 0.0);
    
    img = new Image("assets/blackwhite.png");
    assertEquals(127.5, gdf.getGradientDensity(img), 0.0);
    
    img = new Image("assets/black3-4.png");
    assertEquals(63.75, gdf.getGradientDensity(img), 0.0);
    
    img = new Image("assets/black1-2.png");
    assertEquals(127.5, gdf.getGradientDensity(img), 0.0);
    
  }
}
