package dk.aau.cs.d501e13.anpi.neuralnetwork;

import static org.junit.Assert.*;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class RobertGordonUniExample {

  @Test
  /*
   * Test based on neural network example calculations found at
   * the website of Robert Gordon University
   * http://www4.rgu.ac.uk/files/chapter3%20-%20bp.pdf
   */
  public void Test1() throws CorruptedInputException{
    NeuralNetwork neuralNetwork = new NeuralNetwork();
    
    TransferFunction sigmoidFunc = new SigmoidFunction();
    
    InputNeuron in1 = new InputNeuron();
    InputNeuron in2 = new InputNeuron();
    
    Neuron hid1 = new Neuron(sigmoidFunc);
    Neuron hid2 = new Neuron(sigmoidFunc);
    
    Neuron out1 = new Neuron(sigmoidFunc);

    neuralNetwork.addInput(in1);
    neuralNetwork.addInput(in2);
    neuralNetwork.addHidden(hid1);
    neuralNetwork.addHidden(hid2);
    neuralNetwork.addOutput(out1);
    
    Connection c1 = new Connection(in1, hid1, 0.6);
    Connection c2 = new Connection(in1, hid2, 0.8);
    Connection c3 = new Connection(in2, hid1, 0.4);
    Connection c4 = new Connection(in2, hid2, 0.1);
    Connection c5 = new Connection(hid1, out1, 0.9);
    Connection c6 = new Connection(hid2, out1, 0.3);
    
    in1.addConnection(c1);
    in1.addConnection(c2);
    in2.addConnection(c3);
    in2.addConnection(c4);
    
    hid1.addConnection(c1);
    hid2.addConnection(c2);
    hid1.addConnection(c3);
    hid2.addConnection(c4);
    hid1.addConnection(c5);
    hid2.addConnection(c6);
    
    out1.addConnection(c5);
    out1.addConnection(c6);
    
    in1.setValue(0.9);
    in2.setValue(0.35);
    
    double[] result = neuralNetwork.calculate();
    assertEquals(0.68, hid2.calculate(), 0.001);
    assertEquals(0.6637, hid1.calculate(), 0.0001);
    assertEquals(0.69, result[0], 0.001);
    
    NeuralNetworkLearner learner = new NeuralNetworkLearner(neuralNetwork);
    
    NNTrainingSample[] trainingSamples = new NNTrainingSample[]{
      new NNTrainingSample(new double[]{0.9, 0.35}, new double[]{0.5})
    };
    
    learner.setLearningRate(1);
    learner.learn(trainingSamples);
    
    assertEquals(-0.0406, out1.getError(), 0.0001);
    assertEquals(0.272392, c6.weight, 0.0001);
    assertEquals(0.87305, c5.weight, 0.0001);
    assertEquals(-0.002406, hid2.getError(), 0.001);
    assertEquals(-0.007916, hid1.getError(), 0.001);
    assertEquals(0.09916, c4.weight, 0.001);
    assertEquals(0.7978, c2.weight, 0.001);
    assertEquals(0.3972, c3.weight, 0.001);
    assertEquals(0.5928, c1.weight, 0.001);
   
  }
  
  
  
  
}
