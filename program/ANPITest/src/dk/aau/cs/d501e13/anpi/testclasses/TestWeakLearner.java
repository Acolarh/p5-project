package dk.aau.cs.d501e13.anpi.testclasses;

import java.util.ArrayList;

import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.learning.BClassifier;
import dk.aau.cs.d501e13.anpi.learning.InvalidTrainingSamplesException;
import dk.aau.cs.d501e13.anpi.learning.WeakLearner;

/*
 * Generates weak learners according to the AdaBoost example from 
 * http://www.csie.ntu.edu.tw/~b92109/course/Machine%20Learning/AdaBoostExample.pdf 
 */
public class TestWeakLearner implements WeakLearner<TestSample> {

  ArrayList<TestClassifier> classifiers;
  TestSample[] trainingSamples; 
  boolean[] predicates;
  double error;
  double[] distribution;
  int indicator;
  BClassifier<TestSample> lastReturnedClassifier;
  
  public TestWeakLearner(){
    classifiers = new ArrayList<TestClassifier>();
    //generate a set of classifiers according to the example  
    for (double i = -0.5; i < 11; i+= 1){
      classifiers.add(new TestClassifier(i));
      classifiers.add(new TestClassifier(i, true));
    }
  }
  
  @Override
  public void configure(Config conf) {
    // Not needed for unit test of adaboost
  }
  
  private BClassifier<TestSample> learn(TestSample[] trainingSamples,
    boolean[] predicates, double[] distribution){
    int bestClassifierIndex = 0;
    double bestScore = Double.MIN_VALUE;
    for (int c = 0; c < classifiers.size(); c++){
      double score = 0;
      for (int t = 0; t < trainingSamples.length; t++){
        if (classifiers.get(c).predict(trainingSamples[t]) == trainingSamples[t].predicate){
          score += distribution[t]; 
        }
      }
      if (score > bestScore){
        bestClassifierIndex = c;
        bestScore = score;
      }
    }
    error = 1.0 - bestScore;
    lastReturnedClassifier = classifiers.get(bestClassifierIndex);
    return classifiers.get(bestClassifierIndex);
  }

  @Override
  public BClassifier<TestSample> learn(TestSample[] trainingSamples,
    boolean[] predicates) throws InvalidTrainingSamplesException {
    distribution = new double[trainingSamples.length];
    for (int i = 0; i < trainingSamples.length; i++)
      distribution[i] = 1.0 / trainingSamples.length;
    return learn(trainingSamples, predicates, distribution);
  }

  @Override
  public void init(TestSample[] trainingSamples, boolean[] predicates) {
    this.trainingSamples = trainingSamples;
    this.predicates = predicates;
  }

  /*
   * Find the best classifier for the training samples, given their predicates and a distribution
   */
  @Override
  public BClassifier<TestSample> learn(double[] distribution) {
   return learn(this.trainingSamples, this.predicates, distribution);
  }

  @Override
  public double getError() {
    return error;
  }

  @Override
  public int getIndicator(int trainingSample) {
    return lastReturnedClassifier.predict(trainingSamples[trainingSample]) == predicates[trainingSample] ? -1 : 1;
  }

}
