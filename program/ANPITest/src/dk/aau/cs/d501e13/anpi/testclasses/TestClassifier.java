package dk.aau.cs.d501e13.anpi.testclasses;

import dk.aau.cs.d501e13.anpi.learning.BClassifier;

@SuppressWarnings("serial")
public class TestClassifier implements BClassifier<TestSample>{
  
  double threshold;
  boolean invert;
  
  public TestClassifier(double threshold, Boolean invert){
    this(threshold);
    this.invert = true;
  }
  
  public TestClassifier(double threshold){
    this.threshold = threshold;
  }
  
  public double getThreshold(){
    return threshold;
  }
    
  @Override
  public boolean predict(TestSample t) {
    //xor to invert prediction if this.inverted is set
    return  ((TestSample) t).getXValue() > threshold ^ this.invert;
  }
}

