package dk.aau.cs.d501e13.anpi.neuralnetwork;

import static org.junit.Assert.*;
import org.junit.Test;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class AISpaceOrgRandomExample {

  @Test
  /*
   * The results of all 3 iterations has been validated
   * using the Neural Network tool from AISpace.org, suggested in our MI Course
   */
  public void Test1() throws CorruptedInputException{
    NeuralNetwork neuralNetwork = new NeuralNetwork();
    
    TransferFunction sigmoidFunc = new SigmoidFunction();
    
    InputNeuron in1 = new InputNeuron();
    InputNeuron in2 = new InputNeuron();
    
    Neuron hid1 = new Neuron(sigmoidFunc, 0.1);
    Neuron hid2 = new Neuron(sigmoidFunc, 0.1);
    
    Neuron out1 = new Neuron(sigmoidFunc, 0.1);
    Neuron out2 = new Neuron(sigmoidFunc, 0.1);

    neuralNetwork.addInput(in1);
    neuralNetwork.addInput(in2);
    neuralNetwork.addHidden(hid1);
    neuralNetwork.addHidden(hid2);
    neuralNetwork.addOutput(out1);
    neuralNetwork.addOutput(out2);
    
    Connection c1 = new Connection(in1, hid1, 0.1);
    Connection c2 = new Connection(in1, hid2, 0.1);
    Connection c3 = new Connection(in2, hid1, 0.1);
    Connection c4 = new Connection(in2, hid2, 0.1);
    Connection c5 = new Connection(hid1, out1, 0.1);
    Connection c6 = new Connection(hid2, out2, 0.1);
    Connection c7 = new Connection(hid1, out1, 0.1);
    Connection c8 = new Connection(hid2, out2, 0.1);
    
    in1.addConnection(c1);
    in1.addConnection(c2);
    in2.addConnection(c3);
    in2.addConnection(c4);
    
    hid1.addConnection(c1);
    hid2.addConnection(c2);
    hid1.addConnection(c3);
    hid2.addConnection(c4);
    hid1.addConnection(c5);
    hid2.addConnection(c6);
    hid1.addConnection(c7);
    hid2.addConnection(c8);
    
    out1.addConnection(c5);
    out2.addConnection(c6);
    out1.addConnection(c7);
    out2.addConnection(c8);
    
    NeuralNetworkLearner learner = new NeuralNetworkLearner(neuralNetwork);
    
    NNTrainingSample[] trainingSamples = new NNTrainingSample[]{
      new NNTrainingSample(new double[]{1.4, -4.2}, new double[]{0.0, 1.0}),
    };
    
    //expected output before any learning done
    assertEquals(0.5511, neuralNetwork.calculate()[0], 0.0001);
    assertEquals(0.5511, neuralNetwork.calculate()[1], 0.0001);
    
    learner.setLearningRate(0.3);
    learner.learn(trainingSamples);

    //assertEquals(0.09982450167484494, hid1.getThreshold(), 0.0001);
    //assertEquals(0.09982450167484494, hid2.getThreshold(), 0.0001);
    assertEquals(0.059301552221419036, out1.getThreshold(), 0.0001);
    assertEquals(0.13362149962318945, out2.getThreshold(), 0.0001);
    //assertEquals(0.09975430234478291, c1.weight, 0.0001);
    //assertEquals(0.09975430234478291, c2.weight, 0.0001);
    //assertEquals(0.10073709296565127, c3.weight, 0.0001);
    //assertEquals(0.10073709296565127, c4.weight, 0.0001);
    assertEquals(0.08147727736833622, c5.weight, 0.0001);
    assertEquals(0.11530185414856725, c6.weight, 0.0001);
    assertEquals(0.08147727736833622, c7.weight, 0.0001);
    assertEquals(0.11530185414856725, c8.weight, 0.0001);
    
    //expected state after 1st iteration (momentum won't have impact yet)
  }
  
  
  
  
}
