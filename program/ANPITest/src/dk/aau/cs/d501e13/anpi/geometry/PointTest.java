/**
 * 
 */
package dk.aau.cs.d501e13.anpi.geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *
 */
public class PointTest {

  @Test
  public void testConstructor1() {
    Point p = new Point(0, 0);

    assertEquals(0, p.x);
    assertEquals(0, p.y);
  }
  
  @Test
  public void testContructor2() {
    Point p1 = new Point(27, 15);
    Point p2 = new Point(p1);
    
    assertEquals(27, p2.x);
    assertEquals(15, p2.y);
  }
  
  @Test
  public void testAttributes() {
    Point p = new Point(0, 0);
    p.x = 5;
    p.y = -5;
    
    assertEquals(5, p.x);
    assertEquals(-5, p.y);
  }

}
