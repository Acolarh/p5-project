package dk.aau.cs.d501e13.anpi.ocr;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class WeightedCharTest {

  @Test
  public void testCompare() {
    WeightedChar a = new WeightedChar('a', 0.6);
    WeightedChar b = new WeightedChar('b', 0.7);
    assertEquals(1, a.compareTo(b));
    assertEquals(-1, b.compareTo(a));
    
    List<WeightedChar> list = new ArrayList<WeightedChar>();
    list.add(a);
    list.add(b);
    assertSame(a, list.get(0));
    Collections.sort(list);
    assertSame(b, list.get(0));
  }

}
