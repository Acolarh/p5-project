package dk.aau.cs.d501e13.anpi.learning.weak.haarfeature;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeature;
import dk.aau.cs.d501e13.anpi.learning.weak.haarfeature.HaarFeatureGenerator;

public class HaarFeatureGeneratorTest {

  private int listSize(int frameWidth, int frameHeight, int featureType){
    return HaarFeatureGenerator.generateHaarFeatures((byte)frameWidth, (byte)frameHeight, featureType).size();
  }

  @Test
  public void TestGenerateType1HaarFeatures(){

    assertEquals(1,   listSize(2, 1, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(3,   listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(6,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(10,  listSize(2, 4, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(15,  listSize(2, 5, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(4,   listSize(3, 1, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(12,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(24,  listSize(3, 3, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(30,  listSize(4, 2, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(0,   listSize(1, 1, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(0,   listSize(1, 4, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(10,  listSize(4, 1, HaarFeatureGenerator.FEATURE_TYPE_1));
    assertEquals(100, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_1));

    ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)2, (byte)3, 
                                          HaarFeatureGenerator.FEATURE_TYPE_1);
    //Ensure that all 12 haar features on a (2,3) square are generated
    //newHaarFeature2(x, y, breadth, height, x2, y2, breadth2, height2)

    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 1, 1, 0, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 1, 1, 1, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 2, 1, 1, 1, 2, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 2, 1, 0, 1, 2)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 1, 2, 1, 1, 1, 2)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 3, 1, 0, 1, 3)));
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 1, 0, 0, 1, 1))); 
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 1, 1, 1, 0, 1, 1, 1)));
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 2, 1, 1, 0, 2, 1, 1)));
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 2, 0, 0, 1, 2)));
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 1, 1, 2, 0, 1, 1, 2))); 
 //   assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 3, 0, 0, 1, 3)));

    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)3, (byte)1, 
                   HaarFeatureGenerator.FEATURE_TYPE_1);
    //Ensure that all 8 haar features on a (3,1) square are generated
    //newHaarFeature2(x, y, breadth, height, x2, y2, breadth2, height2)

    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 1, 1, 0, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 1, 2, 0, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 1, 1, 0, 2, 1))); 
    assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 2, 1, 2, 0, 1, 1)));
//    assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 1, 0, 0, 1, 1)));
//    assertTrue(contains(haarFeatures, newHaarFeature2(2, 0, 1, 1, 1, 0, 1, 1)));
//    assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 2, 1, 0, 0, 1, 1))); 
//    assertTrue(contains(haarFeatures, newHaarFeature2(2, 0, 1, 1, 0, 0, 2, 1))); 
    
  }

  @Test
  public void TestGenerateType2HaarFeatures(){

	  assertEquals(3,   listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(100, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(6,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(12,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(60, listSize(3, 4, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(40,  listSize(4, 3, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(0,   listSize(4, 1, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(0,   listSize(5, 1, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(0,   listSize(8, 1, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(10,  listSize(1, 4, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(20,  listSize(1, 5, HaarFeatureGenerator.FEATURE_TYPE_2));
      assertEquals(168/2, listSize(1, 8, HaarFeatureGenerator.FEATURE_TYPE_2));

      //Ensure that all 24 haar features on a (2,3) square are generated
      ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)2, (byte)3, 
		                                    HaarFeatureGenerator.FEATURE_TYPE_2);

      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 1, 0, 1, 1, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 1, 1, 0, 0, 1, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 1, 1, 1, 1, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(1, 1, 1, 1, 1, 0, 1, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 1, 1, 0, 2, 1, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 2, 1, 1, 0, 1, 1, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(1, 1, 1, 1, 1, 2, 1, 1))); 
 //     assertTrue(contains(haarFeatures, newHaarFeature2(1, 2, 1, 1, 1, 1, 1, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 2, 1, 0, 1, 2, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 2, 1, 0, 0, 2, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 2, 1, 0, 2, 2, 1))); 
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 2, 2, 1, 0, 1, 2, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 2, 0, 2, 1, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 2, 1, 1, 0, 0, 1, 2))); 
      assertTrue(contains(haarFeatures, newHaarFeature2(1, 0, 1, 2, 1, 2, 1, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(1, 2, 1, 1, 1, 0, 1, 2)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 2, 2, 0, 2, 2, 1)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 2, 1, 0, 1, 2, 2))); 
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 1, 1, 0, 1, 1, 2)));
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 1, 2, 0, 0, 1, 1)));
      assertTrue(contains(haarFeatures, newHaarFeature2(0, 0, 2, 1, 0, 1, 2, 2))); 
//      assertTrue(contains(haarFeatures, newHaarFeature2(0, 1, 2, 2, 0, 0, 2, 1)));
  }

  @Test
  public void TestGenerateType3HaarFeatures(){

    assertEquals(0,  listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(30, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(3,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(0,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(10, listSize(3, 4, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(18, listSize(4, 3, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(3,  listSize(4, 1, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(7, listSize(5, 1, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(34, listSize(8, 1, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(0,  listSize(1, 4, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(0,  listSize(1, 5, HaarFeatureGenerator.FEATURE_TYPE_3));
    assertEquals(0,  listSize(1, 8, HaarFeatureGenerator.FEATURE_TYPE_3));

    ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)3, (byte)4, 
                                          HaarFeatureGenerator.FEATURE_TYPE_3);
    //Ensure that all 20 haar features on a (3,4) square are generated 
    //newHaarFeature3(x, y, breadth, height, x2, y2, breadth2, height2, x3, y3, breadth3, height3, invertedOrNot[true or false])
    
    //non-inverted ones
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 2, 0, 1, 1, 1, 0, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 2, 1, 1, 2, 2, 1, 1, 1, 2, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 3, 1, 1, 2, 3, 1, 1, 1, 3, 1, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 2, 2, 0, 1, 2, 1, 0, 1, 2, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 2, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 2, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 3, 2, 0, 1, 3, 1, 0, 1, 3, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 3, 2, 1, 1, 3, 1, 1, 1, 3, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 4, 2, 0, 1, 4, 1, 0, 1, 4, false)));
    //the inverted ones
 /*   assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 1, 0, 0, 1, 1, 2, 0, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 1, 0, 1, 1, 1, 2, 1, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 2, 1, 1, 0, 2, 1, 1, 2, 2, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 3, 1, 1, 0, 3, 1, 1, 2, 3, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 2, 0, 0, 1, 2, 2, 0, 1, 2, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 2, 0, 1, 1, 2, 2, 1, 1, 2, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 2, 1, 2, 0, 2, 1, 2, 2, 2, 1, 2, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 3, 0, 0, 1, 3, 2, 0, 1, 3, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 3, 0, 1, 1, 3, 2, 1, 1, 3, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 4, 0, 0, 1, 4, 2, 0, 1, 4, true)));
*/
    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)5, (byte)1, 
                   HaarFeatureGenerator.FEATURE_TYPE_3);
    //Ensure that all 14 haar features on a (5,1) square are generated 
    
    //non-inverted ones
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 2, 0, 1, 1, 1, 0, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 1, 3, 0, 1, 1, 2, 0, 1, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 1, 1, 4, 0, 1, 1, 3, 0, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 3, 0, 1, 1, 1, 0, 2, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 1, 4, 0, 1, 1, 2, 0, 2, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 4, 0, 1, 1, 1, 0, 3, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 2, 1, 3, 0, 2, 1, 2, 0, 1, 1, false)));
    //the inverted ones
/*    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 1, 0, 0, 1, 1, 2, 0, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 1, 1, 1, 0, 1, 1, 3, 0, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(3, 0, 1, 1, 2, 0, 1, 1, 4, 0, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 2, 1, 0, 0, 1, 1, 3, 0, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 2, 1, 1, 0, 1, 1, 4, 0, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 3, 1, 0, 0, 1, 1, 4, 0, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 1, 1, 0, 0, 2, 1, 3, 0, 2, 1, true)));
 */ 
  }

  @Test
  public void TestGenerateType4HaarFeatures(){
    assertEquals(0,  listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(30, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(0,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(3,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(18, listSize(3, 4, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(10, listSize(4, 3, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(0,  listSize(4, 1, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(0,  listSize(5, 1, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(0,  listSize(8, 1, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(3,  listSize(1, 4, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(7, listSize(1, 5, HaarFeatureGenerator.FEATURE_TYPE_4));
    assertEquals(34, listSize(1, 8, HaarFeatureGenerator.FEATURE_TYPE_4));

    //Ensure that all 20 haar features on a (4,3) square are generated 
    ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)4, (byte)3, 
                                          HaarFeatureGenerator.FEATURE_TYPE_4);
    
  //non-inverted ones
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 2, 1, 1, 0, 1, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(3, 0, 1, 1, 3, 2, 1, 1, 3, 1, 1, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 2, 1, 0, 2, 2, 1, 0, 1, 2, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 2, 1, 1, 2, 2, 1, 1, 1, 2, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 0, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 3, 1, 0, 2, 3, 1, 0, 1, 3, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 3, 1, 1, 2, 3, 1, 1, 1, 3, 1, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 4, 1, 0, 2, 4, 1, 0, 1, 4, 1, false)));
    //the inverted ones
/*    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 1, 0, 0, 1, 1, 0, 2, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 1, 1, 0, 1, 1, 1, 2, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 1, 1, 1, 2, 0, 1, 1, 2, 2, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(3, 1, 1, 1, 3, 0, 1, 1, 3, 2, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 2, 1, 0, 0, 2, 1, 0, 2, 2, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 2, 1, 1, 0, 2, 1, 1, 2, 2, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 1, 2, 1, 2, 0, 2, 1, 2, 2, 2, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 3, 1, 0, 0, 3, 1, 0, 2, 3, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 3, 1, 1, 0, 3, 1, 1, 2, 3, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 4, 1, 0, 0, 4, 1, 0, 2, 4, 1, true)));
*/ 
    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)1,(byte)5, 
                   HaarFeatureGenerator.FEATURE_TYPE_4);
    //Ensure that all 14 haar features on a (1,5) square are generated

    //non-inverted ones
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 2, 1, 1, 0, 1, 1, 1, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 3, 1, 1, 0, 1, 1, 2, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 4, 1, 1, 0, 1, 1, 3, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 3, 1, 1, 0, 1, 1, 2, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 1, 0, 4, 1, 1, 0, 2, 1, 2, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 1, 0, 4, 1, 1, 0, 1, 1, 3, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 1, 2, 0, 3, 1, 2, 0, 2, 1, 1, false)));
    //The inverted ones
/*    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 1, 0, 0, 1, 1, 0, 2, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 2, 0, 0, 1, 1, 0, 3, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 3, 0, 0, 1, 1, 0, 4, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 2, 0, 0, 1, 1, 0, 3, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 2, 1, 2, 0, 1, 1, 1, 0, 4, 1, 1, true))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 1, 1, 3, 0, 0, 1, 1, 0, 4, 1, 1, true)));
    assertTrue(contains(haarFeatures, newHaarFeature3(0, 2, 1, 1, 0, 0, 1, 2, 0, 3, 1, 2, true)));
*/
  }

  @Test
  public void TestGenerateType5HaarFeatures(){

    assertEquals(0,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(0,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(0,  listSize(0, 0, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(0,  listSize(1, 1, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(0,  listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(1,  listSize(3, 3, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(9, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(98/2, listSize(5, 5, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(3,  listSize(3, 4, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(3,  listSize(4, 3, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(21, listSize(5, 4, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(21, listSize(4, 5, HaarFeatureGenerator.FEATURE_TYPE_5));

    //Transposing the frame size must yield the same amount of Haar features for this particular type  
    assertEquals(listSize(10, 6, HaarFeatureGenerator.FEATURE_TYPE_5), listSize(6, 10, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(listSize(7, 4, HaarFeatureGenerator.FEATURE_TYPE_5), listSize(4, 7, HaarFeatureGenerator.FEATURE_TYPE_5));
    assertEquals(listSize(5, 8, HaarFeatureGenerator.FEATURE_TYPE_5), listSize(8, 5, HaarFeatureGenerator.FEATURE_TYPE_5));
 
    ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)3, (byte)3, 
                                          HaarFeatureGenerator.FEATURE_TYPE_5);
    //Ensure that both haar features on a (3,3) square are generated
 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 3, 3, false)));  
//    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, true)));

    //Ensure that all 6 Haar features on a (4,3) square are generated 
    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)4, (byte)3, HaarFeatureGenerator.FEATURE_TYPE_5);
      
    //non-inverted
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 3, 3, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(2, 1, 1, 1, 2, 1, 1, 1, 1, 0, 3, 3, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 2, 1, 1, 1, 2, 1, 0, 0, 4, 3, false)));   
    //inverted
//    assertTrue(contains(haarFeatures, newHaarFeature3(0, 0, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, true)));
//    assertTrue(contains(haarFeatures, newHaarFeature3(1, 0, 3, 3, 2, 1, 1, 1, 2, 1, 1, 1, true)));
//    assertTrue(contains(haarFeatures, newHaarFeature3( 0, 0, 4, 3, 1, 1, 2, 1, 1, 1, 2, 1, true)));
    
    
    //Find some of the haarFeatures at near the corners and edges on a (10,10) square 
    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)10, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_5);
      
    //only non-inverted, there are other tests showing that the inverted ones are genereted correct
    assertTrue(contains(haarFeatures, newHaarFeature3(8, 8, 1, 1, 8, 8, 1, 1, 7, 7, 3, 3, false)));
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 3, 3, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(1, 1, 8, 8, 1, 1, 8, 8, 0, 0, 10, 10, false))); 
    assertTrue(contains(haarFeatures, newHaarFeature3(4, 1, 2, 1, 4, 1, 2, 1, 3, 0, 4, 3, false)));   
    assertTrue(contains(haarFeatures, newHaarFeature3(4, 8, 2, 1, 4, 8, 2, 1, 3, 7, 4, 3, false)));   
    assertTrue(contains(haarFeatures, newHaarFeature3(4, 4, 1, 1, 4, 4, 1, 1, 3, 3, 3, 3, false)));  
  }

  @Test
  public void TestGenerateType6HaarFeatures(){

    assertEquals(2,  listSize(3, 2, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(2,  listSize(2, 3, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(0,  listSize(0, 0, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(0,  listSize(1, 1, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(1,  listSize(2, 2, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(4,  listSize(3, 3, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(16, listSize(4, 4, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(72/2, listSize(5, 5, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(8,  listSize(3, 4, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(8,  listSize(4, 3, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(24, listSize(5, 4, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(24, listSize(4, 5, HaarFeatureGenerator.FEATURE_TYPE_6));

    //Transposing the frame size must yield the same amount of Haar features for this particular type  
    assertEquals(listSize(10, 6, HaarFeatureGenerator.FEATURE_TYPE_6), listSize(6, 10, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(listSize(7, 4, HaarFeatureGenerator.FEATURE_TYPE_6), listSize(4, 7, HaarFeatureGenerator.FEATURE_TYPE_6));
    assertEquals(listSize(5, 8, HaarFeatureGenerator.FEATURE_TYPE_6), listSize(8, 5, HaarFeatureGenerator.FEATURE_TYPE_6));
 
    ArrayList<HaarFeature> haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)3, (byte)3, 
                                          HaarFeatureGenerator.FEATURE_TYPE_6);
    //Ensure that both haar features on a (3,3) square are generated
 
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1)));  
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 0, 1, 1, 2, 1, 1, 1, 2, 0, 1, 1, 1, 1, 1, 1)));  
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 2, 1, 1)));  
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1)));  

    //Ensure that all 6 Haar features on a (4,3) square are generated 
    haarFeatures = HaarFeatureGenerator.generateHaarFeatures((byte)4, (byte)3, HaarFeatureGenerator.FEATURE_TYPE_6);
      
    //2x2
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 0, 1, 1, 2, 1, 1, 1, 2, 0, 1, 1, 1, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 0, 1, 1, 3, 1, 1, 1, 3, 0, 1, 1, 2, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 0, 2, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 2, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 1, 1, 1, 3, 2, 1, 1, 3, 1, 1, 1, 2, 2, 1, 1)));
    
    //4x2
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 0, 2, 1, 2, 1, 2, 1, 2, 0, 2, 1, 0, 1, 2, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(0, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 0, 2, 2, 1)));
    
    //inverted
    
 /*   //2x2
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 2, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(3, 0, 1, 1, 2, 1, 1, 1, 2, 0, 1, 1, 3, 1, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(1, 1, 1, 1, 0, 2, 1, 1, 0, 1, 1, 1, 1, 2, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(3, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 3, 2, 1, 1)));
    
    //4x2
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 0, 2, 1, 0, 1, 2, 1, 0, 0, 2, 1, 2, 1, 2, 1)));
    assertTrue(contains(haarFeatures, newHaarFeature4(2, 1, 2, 1, 0, 2, 2, 1, 0, 1, 2, 1, 2, 2, 2, 1)));
*/
  }

  
  @Test
  public void TestHaarFeatureEquivalence(){
    HaarFeature a1 = newHaarFeature2(1, 2, 3, 4, 2, 3, 4, 5);
    HaarFeature a2 = newHaarFeature2(1, 2, 3, 4, 2, 3, 4, 5);
    HaarFeature b = newHaarFeature2(2, 3, 4, 5, 1, 2, 3, 4);
    HaarFeature c = newHaarFeature2(2, 2, 3, 4, 2, 3, 4, 5);
    HaarFeature d = newHaarFeature2(1, 1, 3, 4, 2, 3, 4, 5);
    HaarFeature e = newHaarFeature2(1, 2, 2, 4, 2, 3, 4, 5);
    HaarFeature f = newHaarFeature2(1, 2, 3, 3, 2, 3, 4, 5);
    HaarFeature g = newHaarFeature2(1, 2, 3, 4, 3, 3, 4, 5);
    HaarFeature h = newHaarFeature2(1, 2, 3, 4, 2, 4, 4, 5);
    HaarFeature i = newHaarFeature2(1, 2, 3, 4, 2, 3, 3, 5);
    HaarFeature j = newHaarFeature2(1, 2, 3, 4, 2, 3, 4, 4);
    
    HaarFeature k = new HaarFeature(
      new Rectangle[]{new Rectangle(1, 2, 3, 4), null}, 
      new Rectangle[]{new Rectangle(2, 3, 4, 5)}, (byte)0, (byte)0);
    HaarFeature l = new HaarFeature(
      new Rectangle[]{new Rectangle(1, 2, 3, 4)}, 
      new Rectangle[]{new Rectangle(2, 3, 4, 5), null}, (byte)0, (byte)0);

    HaarFeature aa1 = newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4);
    HaarFeature aa2 = newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4);
    HaarFeature bb  = newHaarFeature4(2, 2, 2, 2, 1, 1, 1, 1, 3, 3, 3, 3, 4, 4, 4, 4);
    HaarFeature cc  = newHaarFeature4(3, 3, 3, 3, 4, 4, 4, 4, 1, 1, 1, 1, 2, 2, 2, 2);
    HaarFeature dd  = newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 4, 4, 4, 4, 3, 3, 3, 3);
    HaarFeature ee  = newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 2, 4, 4, 4, 4);

    assertTrue(a1.equals(a2));
    assertTrue(a2.equals(a1));
    assertFalse(a1.equals(b));
    assertFalse(b.equals(a1));
    assertFalse(a1.equals(c));
    assertFalse(a1.equals(d));
    assertFalse(a1.equals(e));
    assertFalse(a1.equals(f));
    assertFalse(a1.equals(g));
    assertFalse(a1.equals(h));
    assertFalse(a1.equals(i));
    assertFalse(a1.equals(j));
    assertFalse(a1.equals(k));  
    assertFalse(a1.equals(l));  
    
    assertTrue(aa1.equals(aa2));
    assertTrue(aa2.equals(aa1));
    assertFalse(aa1.equals(bb));
    assertFalse(aa1.equals(cc));
    assertFalse(aa1.equals(dd));
    assertFalse(aa1.equals(ee));
  }
  @Test
  public void TestHaarFeatureBoundaries(){
    ArrayList<HaarFeature> type1 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    ArrayList<HaarFeature> type2 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    ArrayList<HaarFeature> type3 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    ArrayList<HaarFeature> type4 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    ArrayList<HaarFeature> type5 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    ArrayList<HaarFeature> type6 = 
      HaarFeatureGenerator.generateHaarFeatures((byte)40, (byte)10, HaarFeatureGenerator.FEATURE_TYPE_1);
    
    assertTrue(boundariesOk(type1, 40, 10));
    assertTrue(boundariesOk(type2, 40, 10));
    assertTrue(boundariesOk(type3, 40, 10));
    assertTrue(boundariesOk(type4, 40, 10));
    assertTrue(boundariesOk(type5, 40, 10));
    assertTrue(boundariesOk(type6, 40, 10));
  }
  private boolean boundariesOk(ArrayList<HaarFeature> list, int width, int height){
    for (HaarFeature hf : list){
      for (Rectangle r : hf.addRectangles)
        if (r.x + r.width > width || r.x < 0 || r.y + r.height > height || r.y < 0)
          return false;
      for (Rectangle r : hf.subtractRectangles)
        if (r.x + r.width > width || r.x < 0 || r.y + r.height > height || r.y < 0)
          return false;
    }
    return true;
  }
  @Test
  public void TestContainHelperMethod(){
    ArrayList<HaarFeature> list = new ArrayList<HaarFeature>();

    //Add some noise to the list
    for (int i = 0; i < 10; i++){
      list.add(newHaarFeature4(i, i, i, i, i+1, i+1, i+1, i+1, i, i, i, i, i+1, i+1, i+1, i+1));
    }

    //Add the HaarFeature we will search for soon
    list.add(newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 2, 4, 4, 4, 4));

    //Add some more noise to the list
    for (int i = 0; i < 10; i++){
      list.add(newHaarFeature4(i, i, i, i, i*2, i*2, i*2, i*2, i, i, i, i, i*2, i*2, i*2, i*2));
    }

    HaarFeature inList = newHaarFeature4(1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 2, 4, 4, 4, 4);
    HaarFeature notInList = newHaarFeature4(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);

    assertTrue(contains(list, inList));
    assertFalse(contains(list, notInList));
  }

  boolean contains(ArrayList<HaarFeature> list, HaarFeature searchFor){
    for (HaarFeature hf : list){
      if (hf.equals(searchFor)){
        return true;
      }
    }
    return false;
  }
   
  HaarFeature newHaarFeature2(int y, int x, int b, int h, int y2, int x2, int b2, int h2){
    return new HaarFeature(
      new Rectangle[]{new Rectangle(y, x, b, h)}, 
      new Rectangle[]{new Rectangle(y2, x2, b2, h2)}, (byte)0, (byte)0);
  }
 
  HaarFeature newHaarFeature3(int y, int x, int b, int h, int y2, int x2, int b2, int h2,
                              int y3, int x3, int b3, int h3, boolean i){ 
    if(i == false){                           
    return new HaarFeature(
      new Rectangle[]{new Rectangle(y, x, b, h), new Rectangle(y2, x2, b2, h2)}, 
      new Rectangle[]{new Rectangle(y3, x3, b3, h3)}, (byte)0, (byte)0);
    }
    else{
      return new HaarFeature(
        new Rectangle[]{new Rectangle(y, x, b, h)}, 
        new Rectangle[]{new Rectangle(y2, x2, b2, h2), new Rectangle(y3, x3, b3, h3)}, (byte)0, (byte)0);
    }
  }
  
  HaarFeature newHaarFeature4(int y, int x, int b, int h, int y2, int x2, int b2, int h2,
                              int y3, int x3, int b3, int h3, int y4, int x4, int b4, int h4){
    return new HaarFeature(
      new Rectangle[]{new Rectangle(y, x, b, h), new Rectangle(y2, x2, b2, h2)}, 
      new Rectangle[]{new Rectangle(y3, x3, b3, h3), new Rectangle(y4, x4, b4, h4)}, (byte)0, (byte)0);
  }
}

