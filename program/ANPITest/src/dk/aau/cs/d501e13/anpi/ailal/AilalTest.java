package dk.aau.cs.d501e13.anpi.ailal;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.learning.StrongClassifier;
import dk.aau.cs.d501e13.anpi.learning.ThresholdedClassifier;
import dk.aau.cs.d501e13.anpi.learning.WeightedClassifier;
import dk.aau.cs.d501e13.anpi.testclasses.FakeClassifier;
import dk.aau.cs.d501e13.anpi.testclasses.TestClassifier;
import dk.aau.cs.d501e13.anpi.testclasses.TestSample;

public class AilalTest {

  @Test
  public void testEmptySave(){
    String filepath = "emptyClassifiers.data";
    
    Ailal<TestSample> ailalOut = new Ailal<TestSample>();
    assertEquals( true, ailalOut.saveClassifier( filepath ) );
    
    File f = new File( filepath );
    assertEquals( true, f.exists() );
    
    Ailal<TestSample> ailalIn = new Ailal<TestSample>();
    assertEquals( true, ailalIn.loadClassifier( filepath ) );
  }
  
  @Test
  public void testSaveAndLoad() {
    ArrayList<WeightedClassifier<TestSample>> c = new ArrayList<WeightedClassifier<TestSample>>();
    c.add( new WeightedClassifier<TestSample>(new FakeClassifier( "Test", true, 35 ), 0.1));
    c.add( new WeightedClassifier<TestSample>(new TestClassifier(  25.4 ), -0.5));
    c.add( new WeightedClassifier<TestSample>(new FakeClassifier(  "fjsgkalg+ \0fds", false, 23454839), 2000));
    c.add( new WeightedClassifier<TestSample>(new TestClassifier(  Double.NaN ), 1));
    c.add( new WeightedClassifier<TestSample>(new FakeClassifier(  "", true, -345 ), 0));
    
    ThresholdedClassifier<TestSample> sClassifier 
      = new ThresholdedClassifier<TestSample>(new StrongClassifier<TestSample>(c), 0.0, true);
    
    String filepath = "differentClassifiers.data";
    
    Ailal<TestSample> ailalOut = new Ailal<TestSample>();
    ailalOut.setClassifier( sClassifier );
    assertEquals( true, ailalOut.saveClassifier( filepath ) );
    assertEquals( true, ailalOut.saveClassifier( filepath ) ); //Overwrite test
    
    File f = new File( filepath );
    assertEquals( true, f.exists() );
    
    Ailal<TestSample> ailalIn = new Ailal<TestSample>();
    assertEquals( true, ailalIn.loadClassifier( filepath ) );
    
/*    StrongClassifier<TestSample> in = (StrongClassifier<TestSample>)ailalIn.getClassifier();
    
    for( int i=0; i<c.size(); i++ ){
      
      double prevWeight = c.get(i).getWeight();
      double afterWeight = in.get(i).getWeight();

      assertEquals( prevWeight, afterWeight, 0.001 );
   
      
      if( in.get(i).getClassifier() instanceof FakeClassifier ){
        assertTrue(c.get(i).getClassifier() instanceof FakeClassifier );

        FakeClassifier prev = (FakeClassifier)c.get(i).getClassifier();
        FakeClassifier after = (FakeClassifier)in.get(i).getClassifier();

        assertEquals( prev.name, after.name );
        assertEquals( prev.someValue, after.someValue );
        assertEquals( prev.yay, after.yay );
      }
      else if( in.get(i).getClassifier() instanceof TestClassifier ){
        assertTrue(c.get(i).getClassifier() instanceof TestClassifier );
        
        TestClassifier prev = (TestClassifier)c.get(i).getClassifier();
        TestClassifier after = (TestClassifier)in.get(i).getClassifier();

        assertEquals( prev.getThreshold(), after.getThreshold(), 0.0 );
      }
      else{
        fail( "wrong classifier type" );
      }
    }*/
  }

}
