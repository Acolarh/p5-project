package dk.aau.cs.d501e13.anpi.testclasses;

public class TestSample{
  boolean predicate;
  int xValue;
  
  public TestSample(int xValue){
    this.xValue = xValue;
  }
  
  public TestSample(boolean predicate, int xValue){
    this(xValue);
    this.predicate = predicate;    
  }
  
  public int getXValue(){
    return this.xValue;
  }
}
