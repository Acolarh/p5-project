package dk.aau.cs.d501e13.anpi;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class ConfigTest {

  @Test
  public void testByte() throws IOException {
    Config c = new Config("assets/test.yml");
    assertEquals(0, c.getByte("byte1"));
    assertEquals(-5, c.getByte("byte2"));
    assertEquals(15, c.getByte("byte3"));
    assertEquals(127, c.getByte("byte4"));
    assertEquals(-128, c.getByte("byte5"));
  }

  @Test
  public void testShort() throws IOException {
    Config c = new Config("assets/test.yml");
    assertEquals(0, c.getShort("short1"));
    assertEquals(-5, c.getShort("short2"));
    assertEquals(15, c.getShort("short3"));
    assertEquals(32767, c.getShort("short4"));
    assertEquals(-32768, c.getShort("short5"));
  }
  
  @Test
  public void testInteger() throws IOException {
    Config c = new Config("assets/test.yml");
    assertEquals(0, c.getInt("integer1"));
    assertEquals(-5, c.getInt("integer2"));
    assertEquals(15, c.getInt("integer3"));
    assertEquals(2147483647, c.getInt("integer4"));
    assertEquals(-2147483648, c.getInt("integer5"));
  }
  
  @Test
  public void testLong() throws IOException {
    Config c = new Config("assets/test.yml");
    assertEquals(0, c.getLong("long1"));
    assertEquals(-5, c.getLong("long2"));
    assertEquals(15, c.getLong("long3"));
    assertEquals(Long.MAX_VALUE, c.getLong("long4"));
    assertEquals(-Long.MIN_VALUE, c.getLong("long5"));
  }
  
  @Test
  public void testString() throws IOException {
    Config c = new Config("assets/test.yml");
    assertEquals("", c.getString("string1"));
    assertEquals("hello, world!", c.getString("string2"));
    assertEquals("hello, world!", c.getString("string3"));
  }

}
