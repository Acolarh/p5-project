/**
 * 
 */
package dk.aau.cs.d501e13.anpi.geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *
 */
public class CornerTest {

  @Test
  public void testConstructor1() {
    Corner c = new Corner(15, -232, 5.2);
    
    assertEquals(15, c.x);
    assertEquals(-232, c.y);
    assertEquals(5.2, c.angle, 0.0);
  }
  
  @Test
  public void testConstructor2() {
    Point p = new Point(13, 27);
    Corner c = new Corner(p, -1.0);
    
    assertEquals(13, c.x);
    assertEquals(27, c.y);
    assertEquals(-1.0, c.angle, 0.0);
  }
  
  @Test
  public void testConstructor3() {
    Corner c1 = new Corner(19, 18, 3.1415926);
    Corner c2 = new Corner(c1);
    
    assertEquals(19, c2.x);
    assertEquals(18, c2.y);
    assertEquals(3.1415926, c2.angle, 0.0);
  }

}
