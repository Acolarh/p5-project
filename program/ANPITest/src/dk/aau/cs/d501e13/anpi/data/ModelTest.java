package dk.aau.cs.d501e13.anpi.data;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ModelTest {

  private static Database db;
  
  private static Model model;
  
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    db = new Database("db.properties");
    model = new Model(db, "test", new String[]{"test"}) {
      @Override
      public Record[] makeRecordList(ResultSet r) throws SQLException {
        return new Record[]{};
      }
      
      @Override
      public Record makeRecord(ResultSet r) throws SQLException {
        return new Record(this) {
          
          @Override
          public void save() throws SQLException {
          }
          
          @Override
          public void delete() throws SQLException {
          }
        };
      }
    };
    
    model.executeRaw("DELETE FROM " + model.name());
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    db.close();
  }
  
  @Test
  public void testName() {
    assertEquals("test_test", model.name());
  }
}
