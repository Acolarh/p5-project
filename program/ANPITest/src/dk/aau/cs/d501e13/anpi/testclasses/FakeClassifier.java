package dk.aau.cs.d501e13.anpi.testclasses;

import dk.aau.cs.d501e13.anpi.learning.BClassifier;

@SuppressWarnings("serial")
public class FakeClassifier implements BClassifier<TestSample>{
  public String name;
  public boolean someValue;
  public int yay;
  
  public FakeClassifier(String name, boolean someValue, int yay ){
    this.name = name;
    this.someValue = someValue;
    this.yay = yay;
  }
  
  @Override
  public boolean predict(TestSample t) {
    return someValue;
  }
}