package dk.aau.cs.d501e13.anpi.deskewing;

import static org.junit.Assert.*;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.deskewing.Deskew;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.geometry.Point;

public class DeskewTest {
  
  public Image testImg;
 
  //1. Test list of Points
  public List<Point> cornerTestList1 = new ArrayList<Point>();
  public Point p11 = new Point(0,0);
  public Point p12 = new Point(49,0);
  public Point p13 = new Point(0,49);
  public Point p14 = new Point(49,49);
  
  //2. Test list of Points
  public List<Point> cornerTestList2 = new ArrayList<Point>();
  public Point p21 = new Point(25,25);
  public Point p22 = new Point(26,25);
  public Point p23 = new Point(25,26);
  public Point p24 = new Point(26,26);
  
  //3. Test list of Points
  public List<Point> cornerTestList3 = new ArrayList<Point>();
  public Point p31 = new Point(10,20);
  public Point p32 = new Point(35,15);
  public Point p33 = new Point(15,25);
  public Point p34 = new Point(30,30);
  
  @Test
  public void testDetermineCorners() throws InvalidParameterException, ImageReadException {
    
    testImg = new Image("C:\\Users\\SimonBuus\\Documents\\P5-project\\program\\ANPITest\\assets\\deskewTestImg.png");
    
    // Test 1
    addPointsToTestList(cornerTestList1, p11, p14, p13, p12);
    Deskew deskewer1 = new Deskew(testImg, cornerTestList1); 
    assertEquals(0, deskewer1.cornerTopLeft.y);
    assertEquals(0, deskewer1.cornerTopLeft.x);
    assertEquals(0, deskewer1.cornerTopRight.y);
    assertEquals(49, deskewer1.cornerTopRight.x);
    assertEquals(49, deskewer1.cornerBottomLeft.y);
    assertEquals(0, deskewer1.cornerBottomLeft.x);
    assertEquals(49, deskewer1.cornerBottomRight.y);
    assertEquals(49, deskewer1.cornerBottomRight.x);
    
    // Test 2
    addPointsToTestList(cornerTestList2, p21, p24, p23, p22);
    Deskew deskewer2 = new Deskew(testImg, cornerTestList2);
    assertEquals(25, deskewer2.cornerTopLeft.y);
    assertEquals(25, deskewer2.cornerTopLeft.x);
    assertEquals(25, deskewer2.cornerTopRight.y);
    assertEquals(26, deskewer2.cornerTopRight.x);
    assertEquals(26, deskewer2.cornerBottomLeft.y);
    assertEquals(25, deskewer2.cornerBottomLeft.x);
    assertEquals(26, deskewer2.cornerBottomRight.y);
    assertEquals(26, deskewer2.cornerBottomRight.x);
    
    // Test 3
    addPointsToTestList(cornerTestList3, p31, p34, p33, p32);
    Deskew deskewer3 = new Deskew(testImg, cornerTestList3);
    assertEquals(20, deskewer3.cornerTopLeft.y);
    assertEquals(10, deskewer3.cornerTopLeft.x);
    assertEquals(15, deskewer3.cornerTopRight.y);
    assertEquals(35, deskewer3.cornerTopRight.x);
    assertEquals(25, deskewer3.cornerBottomLeft.y);
    assertEquals(15, deskewer3.cornerBottomLeft.x);
    assertEquals(30, deskewer3.cornerBottomRight.y);
    assertEquals(30, deskewer3.cornerBottomRight.x);
  }
  
  @Test
  public void TestDeskew() throws ImageReadException, InvalidParameterException {
    
    testImg = new Image("C:\\Users\\SimonBuus\\Documents\\P5-project\\program\\ANPITest\\assets\\deskewTestImg.png");
    Image newTestImg;
    
    //Test 1
    addPointsToTestList(cornerTestList1, p11, p14, p13, p12);
    Deskew deskewer1 = new Deskew(testImg, cornerTestList1);
    newTestImg = deskewer1.deskew();
    assertEquals(49, newTestImg.getHeight());
    assertEquals(49, newTestImg.getWidth());
    
    //Test 2
    addPointsToTestList(cornerTestList2, p21, p24, p23, p22);
    Deskew deskewer2 = new Deskew(testImg, cornerTestList2);
    newTestImg = deskewer2.deskew();
    assertEquals(1, newTestImg.getHeight());
    assertEquals(1, newTestImg.getWidth());
    
    //Test 3
    addPointsToTestList(cornerTestList3, p31, p34, p33, p32);
    Deskew deskewer3 = new Deskew(testImg, cornerTestList3);
    newTestImg = deskewer3.deskew();
    assertEquals(10, newTestImg.getHeight());
    assertEquals(20, newTestImg.getWidth());
  }
  
  public void addPointsToTestList(List<Point> cornerTestList, Point p1, Point p2, Point p3, Point p4) {
    cornerTestList.add(p1);
    cornerTestList.add(p2);
    cornerTestList.add(p3);
    cornerTestList.add(p4);
  }
}
