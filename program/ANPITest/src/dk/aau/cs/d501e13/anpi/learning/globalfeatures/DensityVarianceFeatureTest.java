package dk.aau.cs.d501e13.anpi.learning.globalfeatures;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.deskewing.InvalidParameterException;

public class DensityVarianceFeatureTest {

  
  @Test
  public void getFieldsTest(){
    DensityVarianceFeature dvf = new DensityVarianceFeature(2, 5);
    assertEquals(0.5, dvf.getThreshold(), 0.0);
    assertEquals(2, dvf.getSplitVertical());
    assertEquals(5, dvf.getSplitHorizontal());
    
    dvf.setThreshold(0.7);
    dvf.setSplitHorizontal(-1);
    dvf.setSplitVertical(200);
    assertEquals(0.7, dvf.getThreshold(), 0.0);
    assertEquals(200, dvf.getSplitVertical());
    assertEquals(-1, dvf.getSplitHorizontal());
    
  }


  @Test
  public void getDensityVarianceTest() throws InvalidParameterException, ImageReadException{
    DensityVarianceFeature dvf = new DensityVarianceFeature(5, 2);
    Image img = new Image("assets/black50x50.png");
    assertEquals(0.0, dvf.getDensityVariance(img), 0.0);
    dvf.setSplitHorizontal(6);
    dvf.setSplitVertical(3);
    assertEquals(0.0, dvf.getDensityVariance(img), 0.0);
    
    Image img2 = new  Image("assets/white50x50.png");
    assertEquals(0.0, dvf.getDensityVariance(img2), 0.0);
    dvf.setSplitHorizontal(2);
    dvf.setSplitVertical(5);
    assertEquals(0.0, dvf.getDensityVariance(img2), 0.0);
    
    Image img3 = new  Image("assets/dvtestneg.png");
    assertEquals(1.4, dvf.getDensityVariance(img3), 0.0);
    
    Image img4 = new  Image("assets/dvtestpos.png");
    assertEquals(1.0, dvf.getDensityVariance(img4), 0.0);
    
    Image img5 = new  Image("assets/dvtestneginv.png");
    assertEquals(0.6, dvf.getDensityVariance(img5), 0.0);
  }
}
