package dk.aau.cs.d501e13.anpi.neuralnetwork;

import static org.junit.Assert.*;

import org.junit.Test;

import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.SigmoidFunction;
import dk.aau.cs.d501e13.anpi.neuralnetwork.transfer.TransferFunction;

public class NeuralNetworkTest {
  
  @Test
  public void addNeuronsTest(){
    NeuralNetwork nn = buildNetwork();
    
    assertEquals(2, nn.getInputNeurons().size());
    assertEquals(3, nn.getHiddenNeurons().size());
    assertEquals(2, nn.getOutputNeurons().size());
    
  }
  
  @Test
  public void setInputVectorTest() throws CorruptedInputException{
    NeuralNetwork nn = buildNetwork();
    double[] v = {0.0, 0.1};
    
    nn.setInputVector(v);
    
    for(int i = 0; i < v.length; i++){
      assertEquals(v[i], nn.getInputNeurons().get(i).calculate(), 0.0);
    }
    
  }
  
  @Test
  public void calculateTest() throws CorruptedInputException{
    NeuralNetwork nn = buildNetwork();
    nn.setInputVector(0.2, 0.2); 
    nn = addAllInput(nn, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0);
    
    double cRes = 1 / (1 + Math.exp(-(0.2 * 1.0 + 0.2 * 1.0))); 
    cRes = 1 / (1 + Math.exp(-(cRes * 1.0 + cRes * 1.0 + cRes * 1.0)));
    double[] outputVector = nn.calculate();
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      assertEquals(cRes, outputVector[i], 0.0);
    }
    
    nn.setInputVector(0.1, 0.4);
    
    cRes = 1 / (1 + Math.exp(-(0.1 * 1.0 + 0.4 * 1.0)));
    cRes = 1 / (1 + Math.exp(-(cRes * 1.0 + cRes * 1.0 + cRes * 1.0)));
    outputVector = nn.calculate();
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      assertEquals(cRes, outputVector[i], 0.0);
    }
   
    NeuralNetwork nn2 = buildNetwork();
    nn2.setInputVector(0.1, 0.4);
    nn2 = addAllInput(nn2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    cRes = 1 / (1 + Math.exp(-(0.1 * 0.0 + 0.4 * 0.0))); 
    cRes = 1 / (1 + Math.exp(-(cRes * 0.0 + cRes * 0.0 + cRes * 0.0)));
    outputVector = nn2.calculate();
    for(int i = 0; i < nn2.getOutputNeurons().size(); i++){
      assertEquals(cRes, outputVector[i], 0.0);
    }
    
    NeuralNetwork nn3 = buildNetwork();
    nn3.setInputVector(0.3, 0.6);
    double[] hw = {.1, .3, .4, .5, .6, .7};
    nn3 = addAllInput(nn3, hw);
    
    int k = 0;
    double[] newRes = new double[nn3.getHiddenNeurons().size()]; 
    for(int i = 0; i < nn3.getHiddenNeurons().size(); i++){
      newRes[i] = 1 / (1 + Math.exp(-(.3 * hw[k++] + .6 * hw[k++])));
    }
    k = 0;
    double[] tempRes = {newRes[0], newRes[1], newRes[2]};
    for(int i = 0; i < nn3.getOutputNeurons().size(); i++){
      newRes[i] = 1 / (1 + Math.exp(-(tempRes[0] * hw[k++] + tempRes[1] * hw[k++] + tempRes[2] * hw[k++])));
    }

    outputVector = nn3.calculate();
    for(int i = 0; i < nn3.getOutputNeurons().size(); i++){
      assertEquals(newRes[i], outputVector[i], 0.0);
    }
    
  }
  
  @Test
  public void flushTest() throws CorruptedInputException{
    NeuralNetwork nn =  buildNetwork();
    nn.setInputVector(0.2, 0.2);
    nn = addAllInput(nn, .5, .5, .5, .5, .5, .5);
    
    double[] ov = nn.calculate();
    double cRes = 1 / (1 + Math.exp(-(0.2 * .5 + 0.2 * .5))); 
    cRes = 1 / (1 + Math.exp(-(cRes * .5 + cRes * .5 + cRes * .5)));
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      assertEquals(cRes, ov[i], 0.0);
    }
    
    nn.setInputVector(0.2, 0.4);
    ov = nn.calculate();
    cRes = 1 / (1 + Math.exp(-(0.2 * .5 + 0.4 * .5))); 
    cRes = 1 / (1 + Math.exp(-(cRes * .5 + cRes * .5 + cRes * .5)));
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      assertEquals(cRes, ov[i], 0.0);
    }
    
  }
  
  public NeuralNetwork addAllInput(NeuralNetwork nn, double ... v){
    int k = 0;
    for(int i = 0; i < nn.getHiddenNeurons().size(); i++){
      for(int j = 0; j < nn.getInputNeurons().size(); j++){
        nn.getHiddenNeurons().get(i).addInput(nn.getInputNeurons().get(j), v[k]);
        k++;
      }
    }
    k = 0;
    for(int i = 0; i < nn.getOutputNeurons().size(); i++){
      for(int j = 0; j < nn.getHiddenNeurons().size(); j++){
        nn.getOutputNeurons().get(i).addInput(nn.getHiddenNeurons().get(j), v[k]);
        k++;
      }
    }
  
    return nn;
  }
    
  private NeuralNetwork buildNetwork(){
    NeuralNetwork nn = new NeuralNetwork();
    TransferFunction sigmoidFunction = new SigmoidFunction();
    
    InputNeuron in1 = new InputNeuron();
    InputNeuron in2 = new InputNeuron();
    Neuron hn1 = new Neuron(sigmoidFunction);
    Neuron hn2 = new Neuron(sigmoidFunction);
    Neuron hn3 = new Neuron(sigmoidFunction);
    Neuron on1 = new Neuron(sigmoidFunction);
    Neuron on2 = new Neuron(sigmoidFunction);
    
    nn.addInput(in1);
    nn.addInput(in2);
    nn.addHidden(hn1);
    nn.addHidden(hn2);
    nn.addHidden(hn3);
    nn.addOutput(on1);
    nn.addOutput(on2);
    
    return nn;
    
  }

}
