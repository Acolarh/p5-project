/**
 * 
 */
package dk.aau.cs.d501e13.anpi.geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 *
 */
public class RectangleTest {

  @Test
  public void testConstructor1() {
    Rectangle r = new Rectangle(15, -232, 20, 30);
    
    assertEquals(15, r.x);
    assertEquals(-232, r.y);
    assertEquals(20, r.width);
    assertEquals(30, r.height);
  }
  
  @Test
  public void testConstructor2() {
    Point p = new Point(13, 27);
    Rectangle r = new Rectangle(p, 20, 10);

    assertEquals(13, r.x);
    assertEquals(27, r.y);
    assertEquals(20, r.width);
    assertEquals(10, r.height);
  }
  
  @Test
  public void testConstructor3() {
    Rectangle r1 = new Rectangle(19, 18, 2, 5);
    Rectangle r2 = new Rectangle(r1);

    assertEquals(19, r2.x);
    assertEquals(18, r2.y);
    assertEquals(2, r2.width);
    assertEquals(5, r2.height);
  }
  
  @Test
  public void testIntersects() {
    Rectangle r1 = new Rectangle(5, 10, 10, 5);
    
    assertFalse(r1.intersects(new Rectangle(0, 0, 5, 10)));
    assertFalse(r1.intersects(new Rectangle(0, 0, 5, 11)));
    assertFalse(r1.intersects(new Rectangle(0, 0, 6, 10)));
    assertTrue(r1.intersects(new Rectangle(0, 0, 6, 11)));
    assertFalse(r1.intersects(new Rectangle(5, 9, 10, 1)));
    assertTrue(r1.intersects(new Rectangle(5, 9, 10, 2)));
    assertTrue(r1.intersects(r1));
    assertFalse(r1.intersects(new Rectangle(15, 15, 1, 1)));
    assertTrue(r1.intersects(new Rectangle(14, 14, 1, 1)));
    
    assertTrue(r1.intersects(new Rectangle(3, 5, 20, 10)));
    assertTrue(r1.intersects(new Rectangle(7, 12, 3, 2)));
  }
  
  private Rectangle r(int x,int y,int w,int h){
    return new Rectangle(x,y,w,h);
  }
  private void compare(Rectangle r1, Rectangle r2){
    assertEquals( r1.x, r2.x );
    assertEquals( r1.y, r2.y );
    assertEquals( r1.width, r2.width );
    assertEquals( r1.height, r2.height );
  }
  
  @Test
  public void testIntersection(){
    compare(r(0,0,35,35).getIntersection(r(20,0,35,35)),r(20,0,15,35));
    compare(r(20,0,35,35).getIntersection(r(0,0,35,35)),r(20,0,15,35));
    compare(r(0,0,35,35).getIntersection(r(0,20,35,35)),r(0,20,35,15));
    compare(r(0,20,35,35).getIntersection(r(0,0,35,35)),r(0,20,35,15));

    compare(r(0,0,35,35).getIntersection(r(0,10,35,15)),r(0,10,35,15));
    compare(r(0,10,35,15).getIntersection(r(0,0,35,35)),r(0,10,35,15));
    compare(r(10,0,15,35).getIntersection(r(0,0,35,35)),r(10,0,15,35));
    compare(r(0,0,35,35).getIntersection(r(10,0,15,35)),r(10,0,15,35));
    
    assertEquals(r(0, 0, 20, 20).getIntersection(r(-20,-20,20,20)), null);
  }
  
  private void testSymitry( Rectangle r1, Rectangle r2 ){
    assertEquals( r1.compareRectangles(r2), r2.compareRectangles(r1), 0.01 );
  }
  
  @Test
  public void testCompareRectangles(){
    r(20,20,0,0).compareRectangles(r(20,20,0,0));
    assertEquals(1.0, r(20,20,40,40).compareRectangles(r(20,20,40,40)), 0.1);
    assertEquals(0.5, r(20,40,40,40).compareRectangles(r(20,20,40,40)), 0.1);
    assertEquals(0.5, r(20,20,40,40).compareRectangles(r(40,20,40,40)), 0.1);

    testSymitry(r(34,23,534,34), r(43,16,234,123));
    testSymitry(r(-54,-34,534,340), r(43,16,12,43));
    assertEquals(r(0,0,40,40).compareRectangles(r(200,200,40,40)), 0.0, 0.1);
  }

}
