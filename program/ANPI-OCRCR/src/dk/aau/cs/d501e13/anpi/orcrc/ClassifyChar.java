package dk.aau.cs.d501e13.anpi.orcrc;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.event.CellEditorListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import dk.aau.cs.d501e13.anpi.Anpi;
import dk.aau.cs.d501e13.anpi.Config;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;
import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.CharacterModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;

public class ClassifyChar extends JPanel implements ActionListener, KeyListener {

	private class Character {
		public Image image;
		public int value;

		public Character(Image image, int value) {
			this.image = image;
			this.value = value;
		}
	}

	private Config conf;

	private Anpi anpi;

	private Database db;
	private ImageModel images;
	private NumberplateModel numberplates;
	private CharacterModel characters;

	private VideoPanel characterPanel = new VideoPanel();

	private VideoPanel platePanel = new VideoPanel();

	private JButton saveButton = new JButton("Save");
	private JButton unreadableButton = new JButton("Unreadable");
	private JButton okButton = new JButton("OK");

	private List<Image> characterQueue = new ArrayList<Image>();

	private List<Character> history = new ArrayList<Character>();

	private Image currentCharacter;

	private JTextField characterField = new JTextField();

	private JLabel status = new JLabel("Loading...");

	private DefaultTableModel historyTableModel = new DefaultTableModel();

	private NumberplateRecord record;

	private JTable historyTable = new JTable(historyTableModel) {
		public boolean isCellEditable(int row, int column) {
			return false;
		};
	};

	public ClassifyChar(Config conf) throws SQLException,
			ClassNotFoundException {
		super();
		setLayout(new GridLayout(1, 2));
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		historyTableModel.addColumn("Image");
		historyTableModel.addColumn("Value");
		historyTable.getColumn("Image").setCellRenderer(
				new TableCellRenderer() {
					@Override
					public Component getTableCellRendererComponent(
							JTable table, Object value, boolean isSelected,
							boolean hasFocus, int row, int column) {
						return new JLabel((ImageIcon) value);
					}
				});
		historyTable.setRowHeight(30);
		historyTable.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					characterQueue.add(history.remove(historyTable
							.getSelectedRow()).image);
					updateTable();
					updateStatus();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		JScrollPane scrollPane = new JScrollPane(historyTable);
		historyTable.setFillsViewportHeight(true);
		historyTable.setShowGrid(true);
		add(scrollPane);
		add(main);
		addKeyListener(this);
		this.conf = conf;
		anpi = new Anpi(conf);
		db = new Database(conf.getConfig("database"));
		images = new ImageModel(db);
		numberplates = new NumberplateModel(db);
		characters = new CharacterModel(db);
		JPanel buttonSet = new JPanel(new GridLayout(1, 3));
		buttonSet.add(saveButton);
		saveButton.addActionListener(this);
		buttonSet.add(unreadableButton);
		unreadableButton.addActionListener(this);
		buttonSet.add(okButton);
		okButton.addActionListener(this);
		okButton.setEnabled(false);
		characterField.setEnabled(false);
		characterField.setColumns(1);
		characterField.setFont(characterField.getFont().deriveFont(120f));
		characterField.setHorizontalAlignment(JTextField.CENTER);
		characterField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_ESCAPE:
					if ((e.getModifiers() & KeyEvent.SHIFT_MASK) != 0) {
						characterQueue.clear();
						try {
							getNext();
						} catch (SQLException | ImageReadException
								| ImageCloneException e1) {
							e1.printStackTrace();
						}
					} else {
						unreadableButton.doClick();
					}
					return;
				case KeyEvent.VK_S:
					if ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0) {
						characterField.setText("");
						saveButton.doClick();
						return;
					}
					break;
				case KeyEvent.VK_ENTER:
					okButton.doClick();
					return;
				case KeyEvent.VK_SHIFT:
				case KeyEvent.VK_TAB:
					return;
				}
				String value = characterField.getText();
				if (value.length() > 1) {
					value = value.substring(0, 1);
				}
				value = value.toUpperCase();
				if (value.length() > 0) {
					char c = value.charAt(0);
					if (c < '0' || c > 'Z' || (c < 'A' && c > '9')) {
						value = "";
					}
				}
				characterField.setText(value);
				okButton.setEnabled(value.length() > 0);
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		JPanel platePanelContainer = new JPanel();
		JPanel characterPanelContainer = new JPanel();
		platePanel.setPreferredSize(new Dimension(400, 100));
		characterPanel.setPreferredSize(new Dimension(100, 120));
		platePanelContainer.add(platePanel);
		characterPanelContainer.add(characterPanel);
		main.add(status);
		main.add(platePanelContainer);
		main.add(characterPanelContainer);
		main.add(characterField);
		main.add(buttonSet);
	}

	private void updateTable() {
		historyTableModel.setRowCount(history.size());
		int row = 0;
		for (Character c : history) {
			historyTableModel
					.setValueAt(new ImageIcon(c.image.getResized(25, 30)
							.getBufferedImage()), row, 0);
			if (c.value == 0) {
				historyTableModel.setValueAt("?", row, 1);
			} else {
				historyTableModel
						.setValueAt("'" + (char) c.value + "'", row, 1);
			}
			row++;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (e.getSource() == saveButton) {
				for (Character c : history) {
					if (c.value != 0)
						characters.create(c.value, c.image);
				}
				history.clear();
				updateTable();
			}
			if (e.getSource() == okButton) {
				if (characterField.getText().length() != 1) {
					JOptionPane.showMessageDialog(this, "Input error",
							"Invalid length of input.",
							JOptionPane.ERROR_MESSAGE);
				} else {
					history.add(new Character(currentCharacter, characterField
							.getText().charAt(0)));
					updateTable();
					getNext();
				}
			}
			if (e.getSource() == unreadableButton) {
				history.add(new Character(currentCharacter, 0));
				updateTable();
				getNext();
			}
		} catch (SQLException | ImageReadException | ImageCloneException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public Image getPlate(NumberplateRecord record) throws ImageReadException,
			ImageCloneException, SQLException {
		ImageRecord imageRecord = record.getImage();
		Image image = new Image(conf.getString("imagesDir") + "/"
				+ imageRecord.name);
		return image.getSubimage(record.getRectangle());
	}

	private void updateStatus() {
		status.setText("Characters in queue: " + characterQueue.size());
	}

	public void getNext() throws SQLException, ImageReadException,
			ImageCloneException {
		if (!characterQueue.isEmpty()) {
			currentCharacter = characterQueue.remove(0);
			characterPanel.setImage(currentCharacter);
			characterField.setText("");
			characterField.requestFocusInWindow();
			okButton.setEnabled(false);
			characterField.setEnabled(true);
			updateStatus();
			return;
		}
		if (record != null) {
			record.flags.setOcrDone(true);
			record.save();
		}
		NumberplateRecord[] plates = numberplates.findByValue("");
		if (plates.length == 0) {
			System.exit(1);
		}
		record = plates[(int) (Math.random() * plates.length)];
		int i = plates.length - 1;
		while (record.flags.isOcrDone()) {
			record = plates[i--];
			if (i < 0) {
				System.exit(2);
			}
		}
		// NumberplateRecord record = plates[0];

		Image plate = getPlate(record);
		platePanel.setImage(plate);
		List<Image> plateCharacters = anpi.segmentPlate(plate);
		plateCharacters = anpi.resizeCharacters(plateCharacters, 100, 120);
		if (plateCharacters.size() > 0) {
			System.out.println(plateCharacters.size() + " characters found");
			for (Image character : plateCharacters) {
				characterQueue.add(character);
			}
		}
		getNext();
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("usage: classify CONFIG_FILE");
			System.exit(1);
		}

		JFrame f = new JFrame("OCR Classification version 1.0");

		try {
			Config conf = new Config(args[0]);
			ClassifyChar classify = new ClassifyChar(conf);
			f.add(classify);
			f.pack();

			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setResizable(true);
			f.setVisible(true);

			classify.getNext();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			JOptionPane
					.showMessageDialog(f, "Database error",
							"Unable to connect to database.",
							JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(f, "Configuration error",
					"Unable to load configuration.", JOptionPane.ERROR_MESSAGE);
		} catch (ImageReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImageCloneException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
