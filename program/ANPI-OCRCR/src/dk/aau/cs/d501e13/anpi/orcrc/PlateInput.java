package dk.aau.cs.d501e13.anpi.orcrc;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dk.aau.cs.d501e13.anpi.Image;

public class PlateInput extends JPanel {
  
  private JTextField[] fields;

  public PlateInput(List<Image> characters, final JButton unreadable, final JButton ok) {
    setLayout(new GridLayout(1, characters.size()));
    fields = new JTextField[characters.size()];
    int i = 0;
    for (Image character : characters) {
      JPanel p = new JPanel(new GridLayout(2, 1));
      p.add(new VideoPanel(character));
      final JTextField text = new JTextField();
      final int thisI = i;
      fields[i++] = text;
      text.setColumns(1);
      text.setFont(text.getFont().deriveFont(120f));
      text.setHorizontalAlignment(JTextField.CENTER);
      text.addKeyListener(new KeyListener() {
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_BACK_SPACE:
              if (text.getText().length() < 1 && thisI > 0) {
                fields[thisI - 1].setText("");
                fields[thisI - 1].requestFocus();
              }
              return;
            case KeyEvent.VK_ESCAPE:
              unreadable.doClick();
              return;
            case KeyEvent.VK_ENTER:
              ok.doClick();
              return;
            case KeyEvent.VK_SHIFT:
            case KeyEvent.VK_TAB:
              return;
          }
          if (text.getText().length() > 1) {
            text.setText(text.getText().substring(0, 1));
          }
          text.setText(text.getText().toUpperCase());
          if (thisI + 1 < fields.length) {
            fields[thisI + 1].requestFocus();
          }
        }
        
        @Override
        public void keyPressed(KeyEvent e) {
        }
      });
      p.add(text);
      add(p);
    }
  }
  
  @Override
  public void requestFocus() {
    super.requestFocus();
    fields[0].requestFocusInWindow();
  }
  
  public boolean isValidValue() {
    for (JTextField field : fields) {
      if (field.getText().length() != 1) {
        return false;
      }
    }
    return true;
  }
  
  public String getValue() {
    String value = "";
    for (JTextField field : fields) {
      value += field.getText();
    }
    return value;
  }
}
