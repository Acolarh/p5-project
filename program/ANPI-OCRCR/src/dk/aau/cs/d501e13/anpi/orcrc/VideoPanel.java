package dk.aau.cs.d501e13.anpi.orcrc;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JPanel;

import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;

class VideoPanel extends JPanel {
  private Image image = null;

  public VideoPanel() {
    
  }
  
  public VideoPanel(Image image) {
    this.image = image;
  }

  public void setImage(Image image) {
    this.image = image;
    repaint();
  }
  
  public Image getRoi(Rectangle roi) {
    if (image == null) {
      return null;
    }
    try {
      return image.getSubimage(roi);
    }
    catch (ImageCloneException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }
  
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (image == null) {
      return;
    }
    g.drawImage(image.getBufferedImage(), 0, 0, getWidth(), getHeight(), this);
  }
}