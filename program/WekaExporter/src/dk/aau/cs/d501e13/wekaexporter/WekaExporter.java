package dk.aau.cs.d501e13.wekaexporter;
import java.awt.Graphics2D;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import dk.aau.cs.d501e13.anpi.data.Database;
import dk.aau.cs.d501e13.anpi.data.models.ImageModel;
import dk.aau.cs.d501e13.anpi.data.models.ImageRecord;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateModel;
import dk.aau.cs.d501e13.anpi.data.models.NumberplateRecord;
import dk.aau.cs.d501e13.anpi.geometry.Rectangle;
import dk.aau.cs.d501e13.anpi.haarfeature.HaarFeature;
import dk.aau.cs.d501e13.anpi.haarfeature.HaarFeatureGenerator;
import dk.aau.cs.d501e13.anpi.Image;
import dk.aau.cs.d501e13.anpi.ImageCloneException;
import dk.aau.cs.d501e13.anpi.ImageReadException;

public class WekaExporter {
  static void Export(String outputPath, List<HaarFeature> haarfeatures, Image[] images, boolean[] predicates ){
    int numHaarFeatures = 10000;
    
    Collections.shuffle(haarfeatures);
    PrintWriter writer;
    try {
      writer = new PrintWriter(outputPath, "UTF-8");
      
      //Column names
      writer.print("hasNumberplate, ");
      for (int i = 0; i < haarfeatures.size() && i < numHaarFeatures; i++){
        if (i != 0)
          writer.print(", ");
        writer.print("hf"+(i));
      }
      writer.println();
      
 
      
      //matrix values for haar features
      for (int i = 0; i < images.length; i++){
        System.out.println("Saving sample " + i + "/ " + images.length);
        writer.print(predicates[i] ? "true" : "false");

        //haar features
        for (int h = 0; h < haarfeatures.size() && h < numHaarFeatures; h++){
          writer.print(", " + haarfeatures.get(h).getDif(images[i]));
        }
        writer.println();
      }
    
      writer.close();
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }
  
  
  
  public static void main(String[] args) throws ImageCloneException, SQLException, ClassNotFoundException, IOException{

    String exportPath = "C:/Users/Kent/Desktop/data.csv";
    String imageDir = "C:/Users/Kent/Desktop/p5-nummerplader";
    byte width = 34;
    byte height = 9;
    boolean isWhitelist = false;
    int numberplateFlags =  NumberplateRecord.FLAG_BLURRY | NumberplateRecord.FLAG_YELLOW;
    int npp = 2; //negatives per positive
    int haarFeatureTypes = HaarFeatureGenerator.FEATURE_TYPE_ALL;
    
    Random rand = new Random();
    List<HaarFeature> features = HaarFeatureGenerator.generateHaarFeatures(width, height, haarFeatureTypes);
    System.out.println(features.size() + " features generated");
    Image positives = null;
    Image negatives = null;
    boolean load = false;
    Database db = new Database("db.properties");
    ImageModel images = new ImageModel(db);
    NumberplateModel numberplates = new NumberplateModel(db);

    Image[] samples = null;
    boolean[] predicates = null;
    
    String w = isWhitelist ? "W" : "B";
    String positiveName = "positives_" + width + "_" + height + "_" + w + numberplateFlags + ".png";
    String negativeName = "negatives_" + width + "_" + height + "_" + w + numberplateFlags + ".png";
    
    if (load) {
      try {
        positives = new Image(positiveName);
        System.out.println("Loaded positives: " + positives.getWidth() + "x" + positives.getHeight());
        negatives = new Image(negativeName);
        System.out.println("Loaded negatives: " + negatives.getWidth() + "x" + negatives.getHeight());
        
        int numPlates = positives.getWidth() / width;
        int numNegatives = negatives.getWidth() / width;
        
        int size = numPlates + numNegatives;
        int sampleId = 0;
        samples = new Image[size];
        predicates = new boolean[size];
        
        for (int i = 0; i < numPlates; i++) {
          System.out.println("Adding positive " + (i + 1) + " of " + numPlates);
          samples[sampleId] = positives.getSubimage(new Rectangle(i * width, 0, width, height));
          predicates[sampleId] = true;
          sampleId++;
        }
        for (int i = 0; i < numNegatives; i++) {
          System.out.println("Adding negative " + (i + 1) + " of " + numNegatives);
          samples[sampleId] = negatives.getSubimage(new Rectangle(i * width, 0, width, height));
          predicates[sampleId] = false;
          sampleId++;
        }
      }
      catch (ImageReadException e) {
        System.out.println("ERROR: Loading images failed. Regenerating...");
        load = false;
      }
    }
    if (!load) {
      System.out.println("Getting list of images...");
      if (isWhitelist) {
        numberplates.prepare("count-flags", "SELECT COUNT(*) FROM " + numberplates.name()
          + " WHERE (flags & " + numberplateFlags + ") = " + numberplateFlags);
        numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
          + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = " + numberplateFlags);
      }
      else {
        numberplates.prepare("count-flags", "SELECT COUNT(*) FROM " + numberplates.name()
          + " WHERE (flags & " + numberplateFlags + ") = 0");
        numberplates.prepare("select-flags", "SELECT * FROM " + numberplates.name()
          + " WHERE image_id = ? AND  (flags & " + numberplateFlags + ") = 0");
      }
      ImageRecord[] records = images.all();
      int numPlates = numberplates.queryCount("count-flags");
      int numNegatives = numPlates * npp;
      
      int size = numPlates + numNegatives;
      int sampleId = 0;
      samples = new Image[size];
      predicates = new boolean[size];
      
      System.out.println("Creating positives picture: " + (width * numPlates) + "x" + (height));
      positives = new Image(width * numPlates, height);
      System.out.println("Creating negatives picture: " + (width * numPlates) + "x" + (height));
      negatives = new Image(width * numNegatives, height);
      Graphics2D positiveG = positives.getInput().createGraphics();
      Graphics2D negativeG = negatives.getInput().createGraphics();
      int positiveOffset = 0;
      int negativeOffset = 0;
      for (ImageRecord record : records) {
        NumberplateRecord[] plates = (NumberplateRecord[])numberplates.queryMultiple("select-flags", record.id);
//        NumberplateRecord[] plates = record.getNumberplates();
        if (plates.length == 0) {
          continue;
        }
        Image img;
        System.out.println("Loading image: " + imageDir + "/" + record.name);
        try {
          img = new Image(imageDir + "/" + record.name);
        }
        catch (ImageReadException e) {
          System.out.println("ERROR: Unable to read image: " + imageDir + "/" + record.name);
          continue;
        }
        // Add positive samples
        Rectangle[] rects = new Rectangle[plates.length];
        for (int j = 0; j < plates.length; j++) {
          System.out.println("Adding positive " + (positiveOffset + 1) + " of " + numPlates);
          NumberplateRecord plate = plates[j];
          rects[j] = new Rectangle(plate.x, plate.y, plate.width, plate.height);
          positiveG.drawImage(
            img.getSubimage(rects[j]).getResized(width, height).getInput(),
            positiveOffset * width,
            0,
            null
          );
          
          samples[sampleId] = positives.getSubimage(new Rectangle(positiveOffset * width, 0, width, height));
          predicates[sampleId] = true;
          sampleId++;
          
          positiveOffset++;
        }
        int numPlateNegatives = plates.length * npp;
        for (int j = 0; j < numPlateNegatives; j++) {
          System.out.println("Adding negative " + (negativeOffset + 1) + " of " + numNegatives);
          Rectangle r;
          do {
            int randomW = rand.nextInt(img.getWidth() - width) + 1 + width;
            int randomH = rand.nextInt(img.getHeight() - height) + 1 + height;
            int randomX = rand.nextInt(img.getWidth() - randomW + 1);
            int randomY = rand.nextInt(img.getHeight() - randomH + 1);
            r = new Rectangle(randomX, randomY, randomW, randomH);
          } while (r.intersects(rects));
          negativeG.drawImage(
            img.getSubimage(r).getResized(width, height).getInput(),
            negativeOffset * width,
            0,
            null
          );
          
          samples[sampleId] = negatives.getSubimage(new Rectangle(negativeOffset * width, 0, width, height));
          predicates[sampleId] = false;
          sampleId++;
          negativeOffset++;
        }
      }
      negativeG.dispose();
      positiveG.dispose();
    }
    
    //Export matrix to a format WEKA can use
    Export(exportPath, features, samples, predicates);
  }
}
